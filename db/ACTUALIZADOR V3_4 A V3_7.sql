/**
** SCRIPT ACTUALIZACION SIMETRA A V3.7
**/

/**
 * Author:  Ing. Guido Nicolás Quadrini
 * Created: 07/01/2019
 **/

USE salud_banco;

/** 170901 - update.sql **/
-- Creacion de las siguientes tablas: bolsas, bolsas_marcas, bolsas_tipos, ocu_ocupaciones, pedidos_insumos, pedidos_insumos_detalle, pedidos_insumos_log, ppr_perfiles_produccion, 
-- productos, productos_categorias, stock_consolidado,
-- stock_descartes, stock_insumos_movimientos, stock_insumos_movimientos_consolidados, stock_solicitudes, tdo_tipos_donaciones, tipos_movimentos_stock.
-- Se cargan datos segun corresponda.


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bolsas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas`;
CREATE TABLE `bolsas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_bolsa` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dias` int(11) NOT NULL,
  `activo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bolsas
-- ----------------------------
INSERT INTO `bolsas` VALUES ('1', 'ACD/CPD', '21', '1');
INSERT INTO `bolsas` VALUES ('2', 'CPD-A', '35', '1');
INSERT INTO `bolsas` VALUES ('3', 'CPDA-1', '35', '1');
INSERT INTO `bolsas` VALUES ('4', 'SAG Manitol', '42', '1');

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bolsas_marcas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas_marcas`;
CREATE TABLE `bolsas_marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsa_marca` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bolsas_marcas
-- ----------------------------
INSERT INTO `bolsas_marcas` VALUES ('1', 'Rivero');
INSERT INTO `bolsas_marcas` VALUES ('2', 'Baxter');
INSERT INTO `bolsas_marcas` VALUES ('3', 'Terumo');
INSERT INTO `bolsas_marcas` VALUES ('4', 'Grifols');
INSERT INTO `bolsas_marcas` VALUES ('5', 'Troge');


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bolsas_tipos
-- ----------------------------
DROP TABLE IF EXISTS `bolsas_tipos`;
CREATE TABLE `bolsas_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsa_tipo` varchar(50) DEFAULT NULL,
  `peso_minimo` decimal(5,3) DEFAULT NULL,
  `peso_maximo` decimal(5,3) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bolsas_tipos
-- ----------------------------
INSERT INTO `bolsas_tipos` VALUES ('1', 'Simple', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('2', 'Doble', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('3', 'Triple', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('4', 'Cuadruple', '0.450', '0.550', 'Bolsa especial');
INSERT INTO `bolsas_tipos` VALUES ('5', 'Quintuple', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('6', 'PRP', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('7', 'Descartable Aferesis', null, null, null);


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ocu_ocupaciones
-- ----------------------------
DROP TABLE IF EXISTS `ocu_ocupaciones`;
CREATE TABLE `ocu_ocupaciones` (
  `ocu_id` int(11) NOT NULL AUTO_INCREMENT,
  `ocu_descripcion` varchar(90) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ocu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ocu_ocupaciones
-- ----------------------------
INSERT INTO `ocu_ocupaciones` VALUES ('1', 'Ama de casa');
INSERT INTO `ocu_ocupaciones` VALUES ('2', 'Artesano');
INSERT INTO `ocu_ocupaciones` VALUES ('3', 'Camionero');
INSERT INTO `ocu_ocupaciones` VALUES ('4', 'Carpintero');
INSERT INTO `ocu_ocupaciones` VALUES ('5', 'Cobrador');
INSERT INTO `ocu_ocupaciones` VALUES ('6', 'Colectivero');
INSERT INTO `ocu_ocupaciones` VALUES ('7', 'Comerciante');
INSERT INTO `ocu_ocupaciones` VALUES ('8', 'Cuentapropista');
INSERT INTO `ocu_ocupaciones` VALUES ('9', 'Changarin');
INSERT INTO `ocu_ocupaciones` VALUES ('10', 'Deportista');
INSERT INTO `ocu_ocupaciones` VALUES ('11', 'Desocupado');
INSERT INTO `ocu_ocupaciones` VALUES ('12', 'Discapacitado');
INSERT INTO `ocu_ocupaciones` VALUES ('13', 'Directivo de Empresa');
INSERT INTO `ocu_ocupaciones` VALUES ('14', 'Docente');
INSERT INTO `ocu_ocupaciones` VALUES ('15', 'Electricista');
INSERT INTO `ocu_ocupaciones` VALUES ('16', 'Empleada Domestica');
INSERT INTO `ocu_ocupaciones` VALUES ('17', 'Empleado');
INSERT INTO `ocu_ocupaciones` VALUES ('18', 'Empleado Administrativo');
INSERT INTO `ocu_ocupaciones` VALUES ('19', 'Empleado de Comercio');
INSERT INTO `ocu_ocupaciones` VALUES ('20', 'Empleado Ferroviario');
INSERT INTO `ocu_ocupaciones` VALUES ('21', 'Empleado Gastronomico');
INSERT INTO `ocu_ocupaciones` VALUES ('22', 'Empleado Judicial');
INSERT INTO `ocu_ocupaciones` VALUES ('23', 'Empleado Rural');
INSERT INTO `ocu_ocupaciones` VALUES ('24', 'Empleado de Seguridad');
INSERT INTO `ocu_ocupaciones` VALUES ('25', 'Estudiante');
INSERT INTO `ocu_ocupaciones` VALUES ('26', 'Fletero');
INSERT INTO `ocu_ocupaciones` VALUES ('27', 'Gomero');
INSERT INTO `ocu_ocupaciones` VALUES ('28', 'Gasista');
INSERT INTO `ocu_ocupaciones` VALUES ('29', 'Herrero');
INSERT INTO `ocu_ocupaciones` VALUES ('30', 'Jardinero');
INSERT INTO `ocu_ocupaciones` VALUES ('31', 'Jubilado');
INSERT INTO `ocu_ocupaciones` VALUES ('32', 'Mecanico');
INSERT INTO `ocu_ocupaciones` VALUES ('33', 'Medico');
INSERT INTO `ocu_ocupaciones` VALUES ('34', 'Metalurgico');
INSERT INTO `ocu_ocupaciones` VALUES ('35', 'Obrero de la Construccion');
INSERT INTO `ocu_ocupaciones` VALUES ('36', 'Operario');
INSERT INTO `ocu_ocupaciones` VALUES ('37', 'Personal de Salud');
INSERT INTO `ocu_ocupaciones` VALUES ('38', 'Policia');
INSERT INTO `ocu_ocupaciones` VALUES ('39', 'Profesional');
INSERT INTO `ocu_ocupaciones` VALUES ('40', 'Recolector de Basura');
INSERT INTO `ocu_ocupaciones` VALUES ('41', 'Taxista');
INSERT INTO `ocu_ocupaciones` VALUES ('42', 'Remisero');
INSERT INTO `ocu_ocupaciones` VALUES ('43', 'Transportista');
INSERT INTO `ocu_ocupaciones` VALUES ('44', 'Vendedor Ambulante');
INSERT INTO `ocu_ocupaciones` VALUES ('45', 'Viajante');



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pedidos_insumos
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos`;
CREATE TABLE `pedidos_insumos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL COMMENT '1 = Generado\r\n2 = Enviado\r\n3 = Recep. Parcial\r\n4 = Cerrado\r\n5 = Cancelado\r\n6 = Pend. QA\r\n7 = Autorizar Pedido\r\n8 = Nuevo Envio',
  `observaciones` text COLLATE utf8_unicode_ci,
  `efector_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) unsigned zerofill DEFAULT NULL,
  `ultima_recepcion_observaciones` text COLLATE utf8_unicode_ci,
  `ultima_recepcion_fecha` datetime DEFAULT NULL,
  `ultima_recepcion_usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`),
  KEY `estado` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pedidos_insumos
-- ----------------------------
INSERT INTO `pedidos_insumos` VALUES ('1', '2017-05-10 17:00:00', '3', 'Ver caja que este en buenas condiciones para el envio', '785', '2017-05-18 12:29:29', '00000000001', 'ASSSSSSSSS', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('2', '2017-05-22 00:00:00', '3', 'Ver cuestion obvia', '785', '2017-05-22 17:22:20', '00000000001', 'acsacscas', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('3', '2017-05-22 09:00:00', '4', 'Ver cosas nuevas', '785', '2017-05-22 17:23:00', '00000000068', 'ASCASCASSCA', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('4', '2017-05-24 20:00:00', '2', 'Cosa nostra', '785', '2017-05-26 15:10:14', '00000000001', null, null, null);
INSERT INTO `pedidos_insumos` VALUES ('5', '2017-05-29 18:00:00', '5', 'AAAAAAAAA', '785', '2017-05-29 11:25:02', '00000000068', null, null, null);
INSERT INTO `pedidos_insumos` VALUES ('6', '2017-06-01 00:00:00', '4', 'Primero de Junio 1', '785', '2017-06-01 11:54:00', '00000000001', 'La caja vino correctamente cerrada', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('7', '2017-06-01 00:00:00', '2', 'Primero de Junio 2', '785', '2017-06-01 11:54:22', '00000000001', null, null, null);
INSERT INTO `pedidos_insumos` VALUES ('8', '2017-06-02 00:00:00', '1', 'Embalar correctamente por favor', '785', '2017-06-02 08:17:07', '00000000001', null, null, null);



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pedidos_insumos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos_detalle`;
CREATE TABLE `pedidos_insumos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  `cantidad_enviada` decimal(10,2) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `producto_id` (`producto_id`) USING BTREE,
  KEY `pedido_id` (`pedido_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pedidos_insumos_detalle
-- ----------------------------
INSERT INTO `pedidos_insumos_detalle` VALUES ('1', '1', '5', '400.00', null, null, 'Plasticos borde blanco');
INSERT INTO `pedidos_insumos_detalle` VALUES ('2', '1', '3', '500.00', null, null, 'Especiales de Poliester');
INSERT INTO `pedidos_insumos_detalle` VALUES ('3', '1', '2', '600.00', null, null, 'Color Blanco');
INSERT INTO `pedidos_insumos_detalle` VALUES ('4', '1', '4', '700.00', null, null, 'Azules preferentemente');
INSERT INTO `pedidos_insumos_detalle` VALUES ('5', '1', '1', '300.00', null, null, 'Inorganico');
INSERT INTO `pedidos_insumos_detalle` VALUES ('6', '2', '1', '150.00', '57.00', null, 'Unidades basicas de control');
INSERT INTO `pedidos_insumos_detalle` VALUES ('7', '3', '1', '300.00', null, null, 'Verificar las etiquetas');
INSERT INTO `pedidos_insumos_detalle` VALUES ('8', '3', '4', '500.00', null, null, 'Color azul');
INSERT INTO `pedidos_insumos_detalle` VALUES ('9', '4', '1', '200.00', null, null, 'AAAAAAAAA');
INSERT INTO `pedidos_insumos_detalle` VALUES ('10', '4', '2', '4.00', null, null, 'BBBBBBBBBB');
INSERT INTO `pedidos_insumos_detalle` VALUES ('11', '5', '3', '22.00', null, null, 'radioactivos clase A33');
INSERT INTO `pedidos_insumos_detalle` VALUES ('12', '5', '1', '23.00', null, null, 'Reactivo clase A1');
INSERT INTO `pedidos_insumos_detalle` VALUES ('13', '6', '1', '470.00', null, null, 'Verificarlo correctamente');
INSERT INTO `pedidos_insumos_detalle` VALUES ('14', '7', '1', '200.00', null, null, 'Verlo bien');
INSERT INTO `pedidos_insumos_detalle` VALUES ('15', '8', '6', '100.00', null, null, 'Linea 1 de reactivos');
INSERT INTO `pedidos_insumos_detalle` VALUES ('16', '8', '7', '200.00', null, null, 'Linea 2 de reactivos');




SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pedidos_insumos_log
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos_log`;
CREATE TABLE `pedidos_insumos_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `accion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `datos` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `pedido_id` (`pedido_id`) USING BTREE,
  KEY `efector_id` (`efector_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pedidos_insumos_log
-- ----------------------------
INSERT INTO `pedidos_insumos_log` VALUES ('1', '1', '785', '2017-05-18 12:29:29', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"400\",\"notas\":\"Plasticos borde blanco\"},\"linea_1\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"500\",\"notas\":\"Especiales de Poliester\"},\"linea_2\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"600\",\"notas\":\"Color Blanco\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"700\",\"notas\":\"Azules preferentemente\"},\"linea_4\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300\",\"notas\":\"Inorganico\"},\"header\":{\"fecha\":\"2017-05-10\",\"observaciones\":\"Ver caja que este en buenas condiciones para el envio\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('2', '1', '785', '2017-05-18 12:30:37', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"5\",\"notas\":\"L1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"6\",\"notas\":\"L2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"7\",\"notas\":\"L3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"8\",\"notas\":\"L4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"9\",\"notas\":\"L5\"},\"header\":{\"fecha\":\"2017-05-18 1212:0505:3636\",\"observaciones\":\"Sin remito\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('3', '1', '785', '2017-05-19 10:20:09', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"AA1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"2\",\"notas\":\"AA2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"2\",\"notas\":\"AA3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"AA4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"2\",\"notas\":\"AA5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:0808\",\"observaciones\":\"Nuevamente revisar el remito de envio\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('4', '1', '785', '2017-05-19 10:53:33', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"M1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"3\",\"notas\":\"M2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"3\",\"notas\":\"M3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"M4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"3\",\"notas\":\"M5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:3333\",\"observaciones\":\"Vino sin remito otra vez\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('5', '1', '785', '2017-05-19 10:55:38', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"M1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"3\",\"notas\":\"M2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"3\",\"notas\":\"M3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"M4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"3\",\"notas\":\"M5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:3737\",\"observaciones\":\"Vino sin remito otra vez\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('6', '1', '785', '2017-05-19 10:56:10', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"M1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"3\",\"notas\":\"M2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"3\",\"notas\":\"M3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"M4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"3\",\"notas\":\"M5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:1010\",\"observaciones\":\"Vino sin remito otra vez\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('7', '1', '785', '2017-05-19 10:57:21', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"4\",\"notas\":\"B1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"B2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"4\",\"notas\":\"B3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"4\",\"notas\":\"B4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"4\",\"notas\":\"B5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:2020\",\"observaciones\":\"Conseguir caja nueva\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('8', '1', '785', '2017-05-19 10:58:48', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"4\",\"notas\":\"B1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"B2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"4\",\"notas\":\"B3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"4\",\"notas\":\"B4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"4\",\"notas\":\"B5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:4747\",\"observaciones\":\"Conseguir caja nueva\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('9', '1', '785', '2017-05-19 11:50:43', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"1\",\"notas\":\"D1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"1\",\"notas\":\"D2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"1\",\"notas\":\"D3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"1\",\"notas\":\"D4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"1\",\"notas\":\"D5\"},\"header\":{\"fecha\":\"2017-05-19 1111:0505:4343\",\"observaciones\":\"Cuco\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('10', '1', '785', '2017-05-19 12:03:36', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"C1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"2\",\"notas\":\"C2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"2\",\"notas\":\"C3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"C4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"2\",\"notas\":\"C5\"},\"header\":{\"fecha\":\"2017-05-19 1212:0505:3535\",\"observaciones\":\"Cosa loca\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('11', '2', '785', '2017-05-22 17:22:21', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"150\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-05-22\",\"observaciones\":\"Ver cuestion obvia\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('12', '3', '785', '2017-05-22 17:23:01', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300\",\"notas\":\"Verificar las etiquetas\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"500\",\"notas\":\"Color azul\"},\"header\":{\"fecha\":\"2017-05-22\",\"observaciones\":\"Ver cosas nuevas\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('13', '3', '785', '2017-05-22 17:24:40', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"10\",\"notas\":\"PRUEBA 1 LINEA 1\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"10\",\"notas\":\"PRUEBA 1 LINEA 2\"},\"header\":{\"fecha\":\"2017-05-22 0505:0505:4040\",\"observaciones\":\"Sin remito a la vista\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('14', '4', '785', '2017-05-26 15:10:14', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"200\",\"notas\":\"AAAAAAAAA\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"BBBBBBBBBB\"},\"header\":{\"fecha\":\"2017-05-24\",\"observaciones\":\"Cosa nostra\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('15', '2', '785', '2017-05-26 15:10:38', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"10\",\"notas\":\"CUUUUU\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:3737\",\"observaciones\":\"aaaaaaaaaa\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('16', '2', '785', '2017-05-26 15:16:44', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"LALALA LANS\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:4444\",\"observaciones\":\"advasvavsa\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('17', '3', '785', '2017-05-26 15:18:57', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"GGG\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"4\",\"notas\":\"HHHHHH\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:5757\",\"observaciones\":\"ascasc\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('18', '2', '785', '2017-05-26 15:25:29', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"GGHHEE\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:2929\",\"observaciones\":\"cscacsA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('19', '2', '785', '2017-05-26 15:48:56', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"GGHHEE\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:5656\",\"observaciones\":\"cscacsA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('20', '1', '785', '2017-05-26 15:49:24', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"R1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"R2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"5\",\"notas\":\"R3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"6\",\"notas\":\"R4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"7\",\"notas\":\"R5\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:2424\",\"observaciones\":\"ASSSSSSSSS\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('21', '3', '785', '2017-05-26 15:51:24', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"A1\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"A2\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:2424\",\"observaciones\":\"ASCASCASSCA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('22', '3', '785', '2017-05-26 15:55:39', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"A1\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"A2\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:3939\",\"observaciones\":\"ASCASCASSCA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('23', '2', '785', '2017-05-26 15:56:03', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"5\",\"notas\":\"TEST 5\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:0202\",\"observaciones\":\"acsacscas\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('24', '2', '785', '2017-05-26 16:14:23', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"5\",\"notas\":\"TEST 5\"},\"header\":{\"fecha\":\"2017-05-26 0404:0505:2323\",\"observaciones\":\"acsacscas\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('25', '3', '785', '2017-05-29 11:21:59', 'Cierre del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300.00\",\"notas\":\"Verificar las etiquetas\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"500.00\",\"notas\":\"Color azul\"},\"header\":{\"fecha\":\"2017-05-29 1111:0505:5959\",\"observaciones\":\"Pedido Cerrado/Finalizado\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('26', '5', '785', '2017-05-29 11:25:03', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"22\",\"notas\":\"radioactivos clase A33\"},\"linea_1\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"23\",\"notas\":\"Reactivo clase A1\"},\"header\":{\"fecha\":\"2017-05-29\",\"observaciones\":\"AAAAAAAAA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('27', '6', '785', '2017-06-01 11:54:01', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"470\",\"notas\":\"Verificarlo correctamente\"},\"header\":{\"fecha\":\"2017-06-01\",\"observaciones\":\"Primero de Junio 1\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('28', '7', '785', '2017-06-01 11:54:22', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"200\",\"notas\":\"Verlo bien\"},\"header\":{\"fecha\":\"2017-06-01\",\"observaciones\":\"Primero de Junio 2\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('29', '6', '785', '2017-06-01 11:55:12', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300\",\"notas\":\"recibimos la mitad\"},\"header\":{\"fecha\":\"2017-06-01 1111:0606:1212\",\"observaciones\":\"La caja vino correctamente cerrada\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('30', '6', '785', '2017-06-01 11:56:36', 'Cierre del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"470.00\",\"notas\":\"Verificarlo correctamente\"},\"header\":{\"fecha\":\"2017-06-01 1111:0606:3636\",\"observaciones\":\"Pedido Cerrado/Finalizado\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('31', '8', '785', '2017-06-02 08:17:08', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"6\",\"descripcion\":\"Reacitvo B1\",\"cantidad\":\"100\",\"notas\":\"Linea 1 de reactivos\"},\"linea_1\":{\"producto_id\":\"7\",\"descripcion\":\"Reacitvo B22\",\"cantidad\":\"200\",\"notas\":\"Linea 2 de reactivos\"},\"header\":{\"fecha\":\"2017-06-02\",\"observaciones\":\"Embalar correctamente por favor\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('37', '2', '785', '2017-06-12 12:56:36', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-12 1212:0606:3636\",\"observaciones\":\"AAAAAAAAAAA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('38', '2', '785', '2017-06-12 12:57:34', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-12 1212:0606:3434\",\"observaciones\":\"AAAAAAAAAAA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('39', '2', '785', '2017-06-13 08:04:32', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control XX\"},\"header\":{\"fecha\":\"2017-06-13 0808:0606:3131\",\"observaciones\":\"FFFFFFFFFFFFF\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('40', '2', '785', '2017-06-13 13:52:27', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-13 0101:0606:2727\",\"observaciones\":\"SSSSSSSSSSS\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('41', '2', '785', '2017-06-13 14:13:50', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-13 0202:0606:5050\",\"observaciones\":\"RECEPCION  2 UNIDADES\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('42', '0', '785', '2017-06-30 10:59:24', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"-2\",\"notas\":\"ajuste por robo\"},\"header\":{\"fecha\":\"2017-06-30 1010:0606:2323\",\"observaciones\":\"Ajuste Manual de Stock\"}}');




SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ppr_perfiles_produccion
-- ----------------------------
DROP TABLE IF EXISTS `ppr_perfiles_produccion`;
CREATE TABLE `ppr_perfiles_produccion` (
  `ppr_id` int(11) NOT NULL AUTO_INCREMENT,
  `ppr_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `ppr_genero` enum('A','F','M') CHARACTER SET latin1 NOT NULL DEFAULT 'A',
  `ppr_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`ppr_id`),
  UNIQUE KEY `nombre` (`ppr_descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ppr_perfiles_produccion
-- ----------------------------
INSERT INTO `ppr_perfiles_produccion` VALUES ('1', 'Alergia', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('2', 'Polimedicado/a', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('3', 'Politransfundido/a', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('4', 'Nro. de Gestas', 'A', '1');


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productocategoria_id` int(11) DEFAULT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_producto` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '0',
  `ultimo_costo` decimal(10,2) DEFAULT NULL,
  `stock_punto_reposicion` decimal(10,2) DEFAULT NULL,
  `stock_minimo` decimal(10,2) DEFAULT NULL,
  `stock_maximo` decimal(10,2) DEFAULT NULL,
  `control_calidad` int(4) DEFAULT NULL,
  `proveedor_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productocategoria_id` (`productocategoria_id`) USING BTREE,
  KEY `coidigo` (`codigo`) USING BTREE,
  KEY `nombre_producto` (`nombre_producto`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of productos
-- ----------------------------
INSERT INTO `productos` VALUES ('1', '1', 'REAC001', 'Reactivo para Laboratorio A1', '1', '230.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('2', '2', 'GUAN002', 'Guantes de Latex por 100 Und.', '1', '300.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('3', '1', 'FILT2221', 'FIltro para Laboratoio  A33', '1', '1200.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('4', '2', 'GUANIT', 'Guantes de Nitrilo x 100 und.', '1', '500.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('5', '1', 'Cloro5', 'Bidon de cloro x 5lts.', '1', '150.00', null, null, null, null, null, '2017-03-03 11:53:27', '1');
INSERT INTO `productos` VALUES ('6', '3', 'BB1', 'Reacitvo B1', '1', '100.00', null, null, null, null, null, '2017-06-01 12:35:01', '1');
INSERT INTO `productos` VALUES ('7', '3', 'BB22', 'Reacitvo B22', '1', '122.00', null, null, null, null, null, '2017-06-01 12:35:24', '1');



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for productos_categorias
-- ----------------------------
DROP TABLE IF EXISTS `productos_categorias`;
CREATE TABLE `productos_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(100) CHARACTER SET latin1 NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nombre:_categoria` (`nombre_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of productos_categorias
-- ----------------------------
INSERT INTO `productos_categorias` VALUES ('1', 'Insumos', '1');
INSERT INTO `productos_categorias` VALUES ('2', 'Material Descartable', '1');
INSERT INTO `productos_categorias` VALUES ('3', 'Reactivos', '1');
INSERT INTO `productos_categorias` VALUES ('4', 'Limpieza', '1');




SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for stock_consolidado
-- ----------------------------
DROP TABLE IF EXISTS `stock_consolidado`;
CREATE TABLE `stock_consolidado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_consolidacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efector_id` int(11) DEFAULT '0',
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha_consolidacion` (`fecha_consolidacion`) USING BTREE,
  KEY `hemocomponente_id` (`hemocomponente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_consolidado
-- ----------------------------
INSERT INTO `stock_consolidado` VALUES ('1', null, '285', '1', 'A', '+', null, '0', '600.000', '0.000', '0', null);



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for stock_descartes
-- ----------------------------
DROP TABLE IF EXISTS `stock_descartes`;
CREATE TABLE `stock_descartes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_descarte` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT '0',
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `fecha_consolidacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `fecha_consolidacion` (`fecha_consolidacion`),
  KEY `fecha_descarte` (`fecha_descarte`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_descartes
-- ----------------------------
INSERT INTO `stock_descartes` VALUES ('1', null, '285', '1', 'A', '+', null, '0', '600.000', '0.000', '0', null, null);



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for stock_insumos_movimientos
-- ----------------------------
DROP TABLE IF EXISTS `stock_insumos_movimientos`;
CREATE TABLE `stock_insumos_movimientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `tipomovimientoajuste_id` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(16,2) DEFAULT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  `fecha_consolidado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`),
  KEY `pedido_id` (`pedido_id`),
  KEY `tipomovimientoajuste_id` (`tipomovimientoajuste_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_insumos_movimientos
-- ----------------------------
INSERT INTO `stock_insumos_movimientos` VALUES ('26', '2017-05-18', '785', '1', '1', '1', '5.00', 'L1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('27', '2017-05-18', '785', '1', '1', '2', '6.00', 'L2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('28', '2017-05-18', '785', '1', '1', '3', '7.00', 'L3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('29', '2017-05-18', '785', '1', '1', '4', '8.00', 'L4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('30', '2017-05-18', '785', '1', '1', '5', '9.00', 'L5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('31', '2017-05-19', '785', '1', '1', '1', '2.00', 'AA1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('32', '2017-05-19', '785', '1', '1', '2', '2.00', 'AA2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('33', '2017-05-19', '785', '1', '1', '3', '2.00', 'AA3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('34', '2017-05-19', '785', '1', '1', '4', '2.00', 'AA4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('35', '2017-05-19', '785', '1', '1', '5', '2.00', 'AA5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('36', '2017-05-19', '785', '1', '1', '1', '3.00', 'M1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('37', '2017-05-19', '785', '1', '1', '2', '3.00', 'M2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('38', '2017-05-19', '785', '1', '1', '3', '3.00', 'M3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('39', '2017-05-19', '785', '1', '1', '4', '3.00', 'M4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('40', '2017-05-19', '785', '1', '1', '5', '3.00', 'M5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('41', '2017-05-19', '785', '1', '1', '1', '3.00', 'M1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('42', '2017-05-19', '785', '1', '1', '2', '3.00', 'M2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('43', '2017-05-19', '785', '1', '1', '3', '3.00', 'M3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('44', '2017-05-19', '785', '1', '1', '4', '3.00', 'M4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('45', '2017-05-19', '785', '1', '1', '5', '3.00', 'M5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('46', '2017-05-19', '785', '1', '1', '1', '3.00', 'M1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('47', '2017-05-19', '785', '1', '1', '2', '3.00', 'M2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('48', '2017-05-19', '785', '1', '1', '3', '3.00', 'M3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('49', '2017-05-19', '785', '1', '1', '4', '3.00', 'M4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('50', '2017-05-19', '785', '1', '1', '5', '3.00', 'M5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('51', '2017-05-19', '785', '1', '1', '1', '4.00', 'B1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('52', '2017-05-19', '785', '1', '1', '2', '4.00', 'B2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('53', '2017-05-19', '785', '1', '1', '3', '4.00', 'B3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('54', '2017-05-19', '785', '1', '1', '4', '4.00', 'B4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('55', '2017-05-19', '785', '1', '1', '5', '4.00', 'B5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('56', '2017-05-19', '785', '1', '1', '1', '4.00', 'B1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('57', '2017-05-19', '785', '1', '1', '2', '4.00', 'B2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('58', '2017-05-19', '785', '1', '1', '3', '4.00', 'B3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('59', '2017-05-19', '785', '1', '1', '4', '4.00', 'B4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('60', '2017-05-19', '785', '1', '1', '5', '4.00', 'B5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('61', '2017-05-19', '785', '1', '1', '1', '1.00', 'D1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('62', '2017-05-19', '785', '1', '1', '2', '1.00', 'D2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('63', '2017-05-19', '785', '1', '1', '3', '1.00', 'D3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('64', '2017-05-19', '785', '1', '1', '4', '1.00', 'D4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('65', '2017-05-19', '785', '1', '1', '5', '1.00', 'D5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('66', '2017-05-19', '785', '1', '1', '1', '2.00', 'C1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('67', '2017-05-19', '785', '1', '1', '2', '2.00', 'C2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('68', '2017-05-19', '785', '1', '1', '3', '2.00', 'C3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('69', '2017-05-19', '785', '1', '1', '4', '2.00', 'C4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('70', '2017-05-19', '785', '1', '1', '5', '2.00', 'C5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('71', '2017-05-22', '785', '1', '3', '1', '10.00', 'PRUEBA 1 LINEA 1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('72', '2017-05-22', '785', '1', '3', '4', '10.00', 'PRUEBA 1 LINEA 2', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('73', '2017-05-26', '785', '1', '2', '1', '10.00', 'CUUUUU', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('74', '2017-05-26', '785', '1', '2', '1', '3.00', 'LALALA LANS', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('75', '2017-05-26', '785', '1', '3', '1', '3.00', 'GGG', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('76', '2017-05-26', '785', '1', '3', '4', '4.00', 'HHHHHH', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('77', '2017-05-26', '785', '1', '2', '1', '2.00', 'GGHHEE', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('78', '2017-05-26', '785', '1', '2', '1', '2.00', 'GGHHEE', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('79', '2017-05-26', '785', '1', '1', '1', '3.00', 'R1', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('80', '2017-05-26', '785', '1', '1', '2', '4.00', 'R2', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('81', '2017-05-26', '785', '1', '1', '3', '5.00', 'R3', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('82', '2017-05-26', '785', '1', '1', '4', '6.00', 'R4', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('83', '2017-05-26', '785', '1', '1', '5', '7.00', 'R5', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('84', '2017-05-26', '785', '1', '3', '1', '2.00', 'A1', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('85', '2017-05-26', '785', '1', '3', '4', '2.00', 'A2', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('86', '2017-05-26', '785', '1', '3', '1', '2.00', 'A1', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('87', '2017-05-26', '785', '1', '3', '4', '2.00', 'A2', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('88', '2017-05-26', '785', '1', '2', '1', '5.00', 'TEST 5', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('89', '2017-05-26', '785', '1', '2', '1', '5.00', 'TEST 5', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('90', '2017-06-01', '785', '1', '6', '1', '300.00', 'recibimos la mitad', '2017-06-01 11:56:43');
INSERT INTO `stock_insumos_movimientos` VALUES ('91', '2017-06-13', '785', '1', '2', '1', '2.00', 'Unidades basicas de control', '2017-07-07 11:31:06');
INSERT INTO `stock_insumos_movimientos` VALUES ('92', '2017-06-30', '785', '2', '0', '5', '-2.00', 'ajuste por robo', '2017-07-07 11:31:08');
INSERT INTO `stock_insumos_movimientos` VALUES ('93', '2017-06-30', '785', '1', '0', '4', '3.00', 'Mal contado', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('94', '2017-06-30', '785', '1', '0', '4', '12.00', 'AJUSTE EN POSITIVO', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('95', '2017-06-30', '785', '3', '0', '4', '-3.00', 'SSSSSS', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('96', '2017-06-30', '785', '3', '0', '4', '-3.00', 'SSSSSS', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('97', '2017-07-03', '785', '2', '0', '4', '-3.00', 'Por vencimiento', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('98', '2017-07-03', '785', '2', '0', '4', '-3.00', 'Vencimiento', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('99', '2017-07-05', '785', '3', '0', '4', '8.00', 'pppppppp', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('100', '2017-07-05', '785', '1', '0', '4', '1.00', 'dsfsdf', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('101', '2017-07-05', '785', '1', '0', '4', '1.00', 'dsfsdf', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('102', '2017-07-05', '785', '1', '0', '4', '1.00', 'asd', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('103', '2017-07-05', '785', '1', '0', '4', '1.00', 'asdasd', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('104', '2017-07-05', '785', '1', '0', '4', '2.00', 'asdasda', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('105', '2017-07-05', '785', '1', '0', '1', '1.00', 'asdasdas', '2017-07-07 11:31:06');




SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for stock_insumos_movimientos_consolidados
-- ----------------------------
DROP TABLE IF EXISTS `stock_insumos_movimientos_consolidados`;
CREATE TABLE `stock_insumos_movimientos_consolidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_insumos_movimientos_consolidados
-- ----------------------------
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('1', '2017-05-26 15:07:15', '785', '1', '37.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('2', '2017-05-26 15:07:16', '785', '2', '28.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('3', '2017-05-26 15:07:16', '785', '3', '29.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('4', '2017-05-26 15:07:17', '785', '4', '40.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('5', '2017-05-26 15:07:17', '785', '5', '31.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('6', '2017-05-26 16:33:45', '785', '1', '37.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('7', '2017-05-26 16:33:45', '785', '2', '4.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('8', '2017-05-26 16:33:45', '785', '3', '5.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('9', '2017-05-26 16:33:46', '785', '4', '14.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('10', '2017-05-26 16:33:46', '785', '5', '7.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('11', '2017-06-01 11:56:43', '785', '1', '300.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('12', '2017-07-07 11:31:06', '785', '1', '3.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('13', '2017-07-07 11:31:07', '785', '4', '17.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('14', '2017-07-07 11:31:08', '785', '5', '-2.00');




SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for stock_solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `stock_solicitudes`;
CREATE TABLE `stock_solicitudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(4) DEFAULT NULL COMMENT '1 = Pendiente (pedido/solicitud abierta)\r\n2 = Comprometida por un envio (respuesta a una solicitud)\r\n3 = Pendiente Recepción  (respondida con un cantidad y pendiente de ingreso)\r\n4 = Cerrada   (cerrada / finalizada)\r\n5 = Descartada',
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `responde_efector_id` int(11) DEFAULT NULL,
  `responde_fecha` datetime DEFAULT NULL,
  `responde_usuario_id` int(11) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) DEFAULT NULL,
  `factor` varchar(1) DEFAULT NULL,
  `grupo_inverso` varchar(5) DEFAULT NULL,
  `irradiado` int(1) DEFAULT NULL,
  `fenotipo` varchar(50) DEFAULT NULL,
  `prioridad` int(4) DEFAULT NULL COMMENT '1 Baja, 2 Media, 3 Alta, 4 Urgente',
  `cantidad_solicitada` decimal(10,3) DEFAULT NULL,
  `cantidad_entregada` decimal(10,3) DEFAULT NULL,
  `observaciones` text,
  `donacion_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `cierre_usuario_id` int(11) DEFAULT NULL,
  `cierre_fecha` datetime DEFAULT NULL,
  `cierre_observaciones` text,
  PRIMARY KEY (`id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha_vencimiento` (`fecha_vencimiento`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `prioridad` (`prioridad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_solicitudes
-- ----------------------------
INSERT INTO `stock_solicitudes` VALUES ('1', '1', '2016-11-01 11:38:22', '184', '3', '2016-11-05 11:38:42', '0', '0000-00-00 00:00:00', '0', '3', 'A', '-', null, '1', null, '3', '20.000', null, null, '1111', null, null, null, null);
INSERT INTO `stock_solicitudes` VALUES ('2', '3', '2017-01-13 08:18:09', '184', '3', '2017-01-28 08:18:21', '0', '0000-00-00 00:00:00', '0', '5', 'AB', '+', null, '0', null, '1', '7.000', '3.000', 'anular la parte superior', '2222', null, null, null, null);
INSERT INTO `stock_solicitudes` VALUES ('3', '3', '2017-01-15 08:56:45', '184', '3', '2017-01-27 08:56:57', '0', '0000-00-00 00:00:00', '0', '4', '0', '+', null, '0', null, '2', '5.000', '5.000', 'verificar correctamente', '3333', null, null, null, null);
INSERT INTO `stock_solicitudes` VALUES ('4', '1', '2017-03-01 00:00:00', '785', '1', '2017-06-24 09:13:36', '0', null, null, '5', '0', '+', null, '1', null, null, '1.000', null, 'AAAA', null, '2017-03-22 10:59:50', null, null, null);



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tdo_tipos_donaciones
-- ----------------------------
DROP TABLE IF EXISTS `tdo_tipos_donaciones`;
CREATE TABLE `tdo_tipos_donaciones` (
  `tdo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tdo_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tdo_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tdo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tdo_tipos_donaciones
-- ----------------------------
INSERT INTO `tdo_tipos_donaciones` VALUES ('1', 'Aferesis', '1');
INSERT INTO `tdo_tipos_donaciones` VALUES ('2', 'Donante', '1');
INSERT INTO `tdo_tipos_donaciones` VALUES ('3', 'Pre-Donante', '1');



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tipos_movimentos_stock
-- ----------------------------
DROP TABLE IF EXISTS `tipos_movimentos_stock`;
CREATE TABLE `tipos_movimentos_stock` (
  `tipo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tipo_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipos_movimentos_stock
-- ----------------------------
INSERT INTO `tipos_movimentos_stock` VALUES ('1', 'Ingreso en Almacen', '1');
INSERT INTO `tipos_movimentos_stock` VALUES ('2', 'Egreso de Almacen', '1');
INSERT INTO `tipos_movimentos_stock` VALUES ('3', 'Egreso por Vencimiento', '1');
INSERT INTO `tipos_movimentos_stock` VALUES ('4', 'Egreso por Descarte', '1');


/** 170901 - update.sql **/
-- Creacion de las siguientes tablas: tipo_donacion, clasificacion_donante, .
-- Se cargan datos segun corresponda.
-- Se alteran y ajustan datos en varias tablas: don_donaciones_ext -> se cambia tipo de datos y agrega dato por defecto null; se agrega clave foranea; se actualizan datos de campo tipo; 
-- se cambia el nombre del campo tipo por 
-- tipo_donacion entero de 6 valor por defecto null y a continuacion del campo cod_colecta; se agrega clave foranea en tipo de donacion; se agrega la columna clasificaciondonante_id 
-- luego de calle otra.
-- Tabla per_personas: Se actualiza valor de claseificacion donante para todos a 1; se agrega fk clasificaciondonante_id
-- Se alteran y ajustan datos en varias tablas


/*GNQ: Los tipos de bolsa ahora estan tipificados en tabla.*/
ALTER TABLE `don_donaciones_ext` 
CHANGE COLUMN `tipo_bolsa` `tipo_bolsa` INT NULL DEFAULT NULL ;


/* JIM: se agrega FK para tipos de bolsas */
ALTER TABLE `don_donaciones_ext` ADD CONSTRAINT `don_donaciones_ext_tipobolsa` FOREIGN KEY (`tipo_bolsa`) REFERENCES `bolsas_tipos` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


/* JIM: tipo de donacion (Relacionado, Autologo, Voluntario)  */
CREATE TABLE `tipo_donacion` (
`id`  int NOT NULL AUTO_INCREMENT ,
`nomeclatura`  varchar(6) NOT NULL ,
`descripcion`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`id`)
);

INSERT INTO `tipo_donacion` VALUES ('1', 'SCLA', 'Sin Clasificar');
INSERT INTO `tipo_donacion` VALUES ('2', 'NOREL', 'No Relacionado');
INSERT INTO `tipo_donacion` VALUES ('3', 'RELA', 'Relacionado');
INSERT INTO `tipo_donacion` VALUES ('4', 'AUTO', 'Autologo');


/* JIM: actualizamos los nuevos valores segun tabla tipo_donacion en la tabla don_donaciones_ext ¨*/
UPDATE don_donaciones_ext SET tipo='1'  WHERE tipo = '';
UPDATE don_donaciones_ext SET tipo='2'  WHERE tipo = 'Voluntaria';
UPDATE don_donaciones_ext SET tipo='3'  WHERE tipo = 'Reposicion';
UPDATE don_donaciones_ext SET tipo='4'  WHERE tipo = 'Autologa';

/* JIM: se cambia el campo tipo por tipo_donacion  */
ALTER TABLE `don_donaciones_ext`
CHANGE COLUMN `tipo` `tipo_donacion` int(6) NULL DEFAULT NULL AFTER `cod_colecta`;

/* creamos FK don_donaciones_ext con tipo_donacion  */
ALTER TABLE `don_donaciones_ext` ADD CONSTRAINT `don_donaciones_ext_tipodonacion` FOREIGN KEY (`tipo_donacion`) REFERENCES `tipo_donacion` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

/* JIM: clasificacion donante (No Relacionado y sus variaciones)  */
CREATE TABLE `clasificacion_donante` (
`id`  int NOT NULL AUTO_INCREMENT ,
`nomeclatura`  varchar(6) NOT NULL ,
`descripcion`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`id`)
);

INSERT INTO `clasificacion_donante` VALUES ('1', 'SCLA', 'Sin Clasificar');
INSERT INTO `clasificacion_donante` VALUES ('2', 'NRN', 'No Relacionado Nuevo');
INSERT INTO `clasificacion_donante` VALUES ('3', 'NRUH', 'No Relacionado Ulterior Habitual');
INSERT INTO `clasificacion_donante` VALUES ('4', 'NRUNH', 'No Relacionado Ulterior No Habitual');


/* JIM: en per_personas se agrego el campo clasificacion donante */
ALTER TABLE `per_personas`
ADD COLUMN `clasificaciondonante_id`  int NULL;

/* actualizamos el nuevo campo con un valor por defecto a los registros existentes  */
UPDATE per_personas SET clasificaciondonante_id='1';

/* creamos una FK per_personas con clasificacio_donante  */
ALTER TABLE `per_personas` ADD CONSTRAINT `per_persona_clasificaciondonante` FOREIGN KEY (`clasificaciondonante_id`) REFERENCES `clasificacion_donante` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


/** 170912 - update.sql **/
-- Se agregan columnas y modifican otras en tabla per_personas.
-- Se actualizan datos segun corresponda.

/* MODIFICACION tabla per_personas, normalizacion con SICAP y nuevos requerimientos */
ALTER TABLE `per_personas`
MODIFY COLUMN `per_tipodoc`  char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `sicap_id`,
MODIFY COLUMN `clasificaciondonante_id`  int(11) NULL DEFAULT NULL AFTER `per_sexo`,
add COLUMN `pais_otro`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `clasificaciondonante_id`,
add COLUMN `localidad_otra`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `pais_otro`,
add COLUMN `calle_otra`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `localidad_otra`,
add COLUMN `pais_nacimiento_otro`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `calle_otra`,
add COLUMN `localidad_nacimiento_otra`  varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `pais_nacimiento_otro`,
ADD COLUMN `codOcupacion`  char(3) NULL AFTER `clasificaciondonante_id`,
ADD COLUMN `codEstCivil`  char(1) NULL AFTER `codOcupacion`,
ADD COLUMN `codNivelEducacion`  char(2) NULL AFTER `codEstCivil`,
ADD COLUMN `codPais`  char(3) NULL AFTER `codNivelEducacion`,
ADD COLUMN `codProvincia`  char(2) NULL AFTER `pais_otro`,
ADD COLUMN `codLocalidad`  varchar(8) NULL AFTER `codProvincia`,
ADD COLUMN `codPostal`  varchar(4) NULL AFTER `localidad_otra`,
ADD COLUMN `municipio`  varchar(13) NULL AFTER `codPostal`,
ADD COLUMN `distrito`  varchar(15) NULL AFTER `municipio`,
ADD COLUMN `calle`  varchar(40) NULL AFTER `distrito`,
ADD COLUMN `numero`  varchar(5) NULL AFTER `calle_otra`,
ADD COLUMN `piso`  varchar(5) NULL AFTER `numero`,
ADD COLUMN `depto`  char(3) NULL AFTER `piso`,
ADD COLUMN `manzana`  varchar(7) NULL AFTER `depto`,
ADD COLUMN `entreCalleA`  varchar(27) NULL AFTER `manzana`,
ADD COLUMN `entreCalleB`  varchar(27) NULL AFTER `entreCalleA`,
ADD COLUMN `bis`  char(1) NOT NULL DEFAULT 'N' AFTER `entreCalleB`,
ADD COLUMN `gps_Longitud`  varchar(20) NULL AFTER `bis`,
ADD COLUMN `gps_Latitud`  varchar(20) NULL AFTER `gps_Longitud`,
ADD COLUMN `telefono`  varchar(15) NULL AFTER `gps_Latitud`,
ADD COLUMN `celular`  varchar(15) NULL AFTER `telefono`,
ADD COLUMN `email`  varchar(60) NULL AFTER `celular`,
ADD COLUMN `codPaisNacimiento`  varchar(20) NULL AFTER `email`,
ADD COLUMN `codProvinciaNacimiento`  varchar(11) NULL AFTER `pais_nacimiento_otro`,
ADD COLUMN `codLocalidadNacimiento`  varchar(11) NULL AFTER `codProvinciaNacimiento`,
ADD COLUMN `observacion`  varchar(255) NULL AFTER `ult_serologia`,
ADD COLUMN `fuente`  varchar(20) NULL AFTER `observacion`,
ADD COLUMN `fechaAlta`  datetime NOT NULL AFTER `fuente`,
ADD COLUMN `usuarioIngreso`  varchar(20) NOT NULL AFTER `fechaAlta`,
ADD COLUMN `fechaModificacion`  datetime NULL AFTER `usuarioIngreso`,
ADD COLUMN `usuarioModificacion`  varchar(20) NULL AFTER `fechaModificacion`,
ADD COLUMN `fechaBaja`  datetime NULL AFTER `usuarioModificacion`,
ADD COLUMN `usuarioBaja`  varchar(20) NULL AFTER `fechaBaja`,
ADD COLUMN `motivoBaja`  varchar(255) NULL AFTER `usuarioBaja`;

ALTER TABLE `per_personas`
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;

/*  Unificar datos de pacientes con per_personas */

UPDATE  per_personas a 
  JOIN  paciente b 
    ON  a.sicap_id = b.NumeroPaciente
   SET 
        a.codOcupacion = b.CodOcupacion,
        a.codEstCivil = b.CodEstCiv,
        a.codNivelEducacion = b.CodNivelEduc,
        a.codPais = b.CodPais,
        a.codProvincia = b.CodProvincia,
        a.codLocalidad = b.ClaveLocalidad,
        a.codPostal = b.CodPostal,
        a.municipio = b.municipio,
        a.distrito = b.distrito,
        a.calle = b.Calle,
        a.numero = b.Numero,
        a.piso = b.Piso,
        a.depto = b.Depto,
        a.manzana = b.manzana,
        a.entreCalleA = b.entrecalle,
        a.entreCalleB = b.ycalle,
        a.bis = b.bis,
        a.gps_Longitud = b.long_x,
        a.gps_Latitud = b.lat_y,
        a.telefono = b.Telefono,
        a.celular = b.celular,
        a.email = b.email,
        a.codPaisNacimiento = b.paisNacimiento,
        a.codProvinciaNacimiento = b.provinciaNacimiento,
        a.codLocalidadNacimiento = b.localidadNacimiento,
        a.observacion = b.observacion,
        a.fuente = b.FUENTE,
        a.fechaAlta = b.FechaAta, 
        a.usuarioIngreso = b.UsuarioIngreso,
        a.fechaModificacion = b.FechaModificacion,
        a.usuarioModificacion = b.UsuarioModificacion;

/* Se Elimina tabla temporal paciente */
DROP TABLE paciente;

/* FIN DE ARCHIVO */




/** 170913 - update.sql **/

/* Se actualiza tabla solicitudes para agregar campo observaciones */

ALTER TABLE `sol_solicitudes`
ADD COLUMN `observacion`  text NULL AFTER `plaqueta`;


/* Se crea tabla sol_diferimientos */

CREATE TABLE `sol_diferimientos` (
`id`  int NOT NULL AUTO_INCREMENT ,
`sol_id`  int NOT NULL ,
`mot_id`  int NOT NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `idx_sol_mot` (`sol_id`, `mot_id`) 
);

/* Se crea una FK del Motivo ID en sol_diferimientos  */
ALTER TABLE `sol_diferimientos` ADD CONSTRAINT `fk_mot_id` FOREIGN KEY (`mot_id`) REFERENCES `mot_motivos` (`mot_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
/* Se crea una FK del Solicitud ID en sol_diferimientos  */
ALTER TABLE `sol_diferimientos` ADD CONSTRAINT `fk_sol_id` FOREIGN KEY (`sol_id`) REFERENCES `sol_solicitudes` (`sol_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/* Normalizacion de datos existentes en sol_solicitudes para agregarlos en sol_diferimientos  */
INSERT INTO sol_diferimientos (sol_id, mot_id) (SELECT sol_id, mot_id FROM sol_solicitudes WHERE mot_id <> 0);

/* Se elimina el campo mot_id de la tabla sol_solicitudes (reemplazado por tabla sol_difirimientos)  */
ALTER TABLE `sol_solicitudes`
DROP COLUMN `mot_id`;

/** 171102 - update.sql **/

/* Se actualiza tabla Donaciones Ext a UTF-8  */
ALTER TABLE `don_donaciones_ext`
DEFAULT CHARACTER SET=utf8;


/* Se actualiza campo nroTubuladora de Donaciones Ext (don_donaciones_ext) de Int a Varchar */

ALTER TABLE `don_donaciones_ext`
MODIFY COLUMN `nroTubuladura`  varchar(20) NULL DEFAULT NULL AFTER `nroLote_bolsa`;

/* Se actualiza campo cod_colecta de Donaciones Ext (don_donaciones_ext) mas amplio */
ALTER TABLE `don_donaciones_ext`
MODIFY COLUMN `cod_colecta`  varchar(140) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `don_id`;

/* Se actualizan campos de la tabla Donaciones Ext a UTF-8  */
ALTER TABLE `don_donaciones_ext`
MODIFY COLUMN `cod_colecta`  varchar(140) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `don_id`,
MODIFY COLUMN `nroLote_bolsa`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `marca_bolsa`,
MODIFY COLUMN `nroTubuladura`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `nroLote_bolsa`,
DEFAULT CHARACTER SET=utf8;

/* Se actualizan campos de para Hemovigilancia en el Examen */
ALTER TABLE `don_donaciones`
CHANGE COLUMN `don_fh_aceptacion` `don_fh_examen`  datetime NULL DEFAULT NULL AFTER `don_usu_extraccion`,
CHANGE COLUMN `usu_aceptacion` `usu_examen`  int(11) NULL DEFAULT NULL AFTER `don_fh_examen`;




/** 180313 - cuestionarios_respuestas_detalle.sql **/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cuestionarios_respuestas_detalle
-- ----------------------------
DROP TABLE IF EXISTS `cuestionarios_respuestas_detalle`;
CREATE TABLE `cuestionarios_respuestas_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuestionario_respuesta_id` int(11) DEFAULT NULL,
  `pregunta_id` int(11) DEFAULT NULL,
  `respuesta_id` int(11) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cuestionario_respuesta_id` (`cuestionario_respuesta_id`),
  CONSTRAINT `fk_cuestionario_respuesta_id` FOREIGN KEY (`cuestionario_respuesta_id`) REFERENCES `cuestionarios_respuestas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cuestionarios_respuestas_detalle
-- ----------------------------

/** 180409 - cuestionario_preguntas_categorias.sql **/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cuestionario_preguntas_categorias
-- ----------------------------
DROP TABLE IF EXISTS `cuestionario_preguntas_categorias`;
CREATE TABLE `cuestionario_preguntas_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` varchar(255) NOT NULL COMMENT '0 = inactiva\r\n1 = activa',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_orden` (`orden`),
  KEY `idx_nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cuestionario_preguntas_categorias
-- ----------------------------
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('1', 'Comprensión de la información al donante', null, '1', '1');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('2', 'Precauciones en las próximas 12 horas', null, '1', '2');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('3', 'Medicación', null, '1', '3');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('4', 'En las últimas dos semanas', null, '1', '4');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('5', 'En el último mes', null, '1', '5');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('6', 'En los últimos 12 meses', null, '1', '6');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('7', 'En alguna ocasión, en el transcurso de su vida', null, '1', '7');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('8', 'Viajes', null, '1', '8');
INSERT INTO `cuestionario_preguntas_categorias` VALUES ('9', 'Por favor responda con sinceridad', null, '1', '9');



/** 180409 - plantillas_cuestionario.sql **/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for plantillas_cuestionario
-- ----------------------------
DROP TABLE IF EXISTS `plantillas_cuestionario`;
CREATE TABLE `plantillas_cuestionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(20) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `estado` int(4) NOT NULL COMMENT '0 = sin uso\r\n1 = en uso (activa)\r\n2 = archivada (mantiene datos historicos)',
  `fecha_creacion` datetime NOT NULL,
  `usuario_creacion_id` int(11) NOT NULL,
  `usuario_revision_id` int(11) DEFAULT NULL,
  `fecha_revision` datetime DEFAULT NULL,
  `usuario_aprobacion_id` int(11) DEFAULT NULL,
  `fecha_aprobacion` datetime DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_version` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plantillas_cuestionario
-- ----------------------------
INSERT INTO `plantillas_cuestionario` VALUES ('1', '1', 'Cuestionario 2018', '1', '2018-01-01 00:00:00', '1', null, null, null, null, null);


/** 180409 - plantillas_cuestionario_detalle.sql **/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for plantillas_cuestionario_detalle
-- ----------------------------
DROP TABLE IF EXISTS `plantillas_cuestionario_detalle`;
CREATE TABLE `plantillas_cuestionario_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla_cuestionario_id` int(11) DEFAULT NULL,
  `cuestionario_pregunta_categoria_id` int(11) DEFAULT NULL,
  `codigo_pregunta` varchar(255) NOT NULL,
  `pregunta` varchar(255) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `criterio_evaluacion` varchar(255) DEFAULT NULL,
  `estado` int(4) NOT NULL COMMENT '0 = sin uso\r\n1 = en uso (activa)\r\n2 = archivada (mantiene datos historicos)',
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_plantilla_cuestionario_id` (`planilla_cuestionario_id`),
  KEY `fk_cuestionario_pregunta_categoria_id` (`cuestionario_pregunta_categoria_id`),
  CONSTRAINT `fk_plantilla_cuestionario_id` FOREIGN KEY (`planilla_cuestionario_id`) REFERENCES `plantillas_cuestionario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plantillas_cuestionario_detalle
-- ----------------------------
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('1', '1', '1', '', ' ¿Se le ha proporcionado información escrita para su autoexclusión pre-donación?', null, null, '1', '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('2', '1', '1', '', ' ¿Ha comprendido las situaciones de riesgo incrementado para la donación de sangre, descriptas en el Documento para la autoexclusión pre-donación?', null, null, '1', '2');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('3', '1', '1', '', ' ¿Alguna vez ha donado sangre, plasma o plaquetas? ¿Dónde?', null, null, '1', '3');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('4', '1', '1', '', ' ¿Actualmente se siente Ud. bien y goza de buena salud?', null, null, '1', '4');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('5', '1', '2', '', ' ¿Debe realizar alguna actividad laboral o deportiva de riesgo (trabajo en altura, escalada) o conducir un vehículo de transporte público?', null, null, '1', '5');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('6', '1', '3', '', ' ¿Está tomando medicamentos o ha tomado en los últimos días? ¿Cuáles?', null, null, '1', '6');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('7', '1', '3', '', ' ¿Está tomando o ha tomado alguna vez medicación para el tratamiento de la psoriasis, el acné o la próstata? ¿Cuál?', null, null, '1', '7');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('8', '1', '3', '', ' ¿Tomó aspirina o analgésicos en los últimos dos días?', null, null, '1', '8');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('9', '1', '4', '', ' ¿Ha tenido fiebre, acompañada de dolor de cabeza, dolor articular, conjuntivitis o malestar general?', null, null, '1', '9');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('10', '1', '4', '', ' ¿Ha recibido tratamiento odontológico o tiene alguna infección dentaria?', null, null, '1', '10');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('11', '1', '5', '', ' ¿Ha recibido alguna vacuna? ¿Cuál?', null, null, '1', '11');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('12', '1', '5', '', ' ¿Ha estado en contacto con alguna persona que padeciera una enfermedad?', null, null, '1', '12');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('13', '1', '6', '', ' ¿Ha recibido Gamma Globulina (por exposición a hepatitis u otras infecciones)?', null, null, '1', '13');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('14', '1', '6', '', ' Mujeres: ¿Estuvo embarazada (parto normal, cesárea, aborto) o lo está ahora? ¿Está amamantando? Nro. de gestas', null, null, '1', '14');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('15', '1', '6', '', ' ¿Se ha realizado tatuajes, perforaciones en orejas u otra parte del cuerpo (“piercing”), tratamientos estéticos invasivos en la piel o acupuntura?', null, null, '1', '15');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('16', '1', '6', '', ' ¿Ha recibido transfusiones de sangre, hemocomponentes, trasplantes de órganos o tejidos?', null, null, '1', '16');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('17', '1', '6', '', ' ¿Le realizaron algún tratamiento médico, cirugía mayor-menor, cirugía laparoscópica o estudios endoscópicos (colonoscopía, endoscopia, artroscopia, etc.)?', null, null, '1', '17');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('18', '1', '6', '', ' ¿Estuvo en contacto con sangre humana o fluidos corporales a través de piel o mucosas?', null, null, '1', '18');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('19', '1', '6', '', ' ¿Convive o ha convivido con alguien que padece hepatitis o ictericia?', null, null, '1', '19');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('20', '1', '6', '', ' ¿Ha padecido Fiebre Hemorrágica Argentina (\"Mal de los rastrojos\")?', null, null, '1', '20');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('21', '1', '6', '', ' ¿Tuvo contacto sexual con alguna persona con VIH/SIDA, Hepatitis B, Hepatitis C, o HTLV I/II o análisis positivos para alguna de estas infecciones?', null, null, '1', '21');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('22', '1', '6', '', ' ¿Tuvo contacto sexual con personas hemodializadas, con hemofilia, o que hayan recibido concentrados de factores de la coagulación, o transfusiones de componentes de la sangre?', null, null, '1', '22');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('23', '1', '6', '', ' ¿Recibió dinero, drogas o cualquier otro tipo de pago a cambio de sexo?', null, null, '1', '23');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('24', '1', '6', '', ' ¿Tuvo contacto sexual con una persona con hepatitis?', null, null, '1', '24');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('25', '1', '6', '', ' ¿Fue tratado por sífilis o gonorrea? ¿Tuvo alguna otra Enfermedad de Transmisión Sexual (ETS) o estudios positivos para ETS?', null, null, '1', '25');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('26', '1', '6', '', ' ¿Ha estado detenido/a o demorado durante 3 o más dias en cárceles o comisarías?', null, null, '1', '26');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('27', '1', '6', '', ' ¿Tuvo Ud. alguna/s de las situaciones consideradas de riesgo incrementado para la donación de sangre o tiene motivos para creer que su pareja las tuvo?', null, null, '1', '27');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('28', '1', '7', '', ' ¿Ha padecido enfermedades cardíacas como angina de pecho, infarto, arritmias, trombosis o algún problema de la coagulación?', null, null, '1', '28');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('29', '1', '7', '', ' ¿Ha tenido convulsiones, desmayos o epilepsia?', null, null, '1', '29');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('30', '1', '7', '', ' ¿Es diabético insulinodependiente?', null, null, '1', '30');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('31', '1', '7', '', ' ¿Padece enfermedades renales?', null, null, '1', '31');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('32', '1', '7', '', ' ¿Ha sufrido alguna enfermedad del aparato digestivo, glándulas tiroides, paratiroides u otras?', null, null, '1', '32');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('33', '1', '7', '', ' ¿Alguna vez le diagnosticaron cáncer o recibió quimioterapia o radiaciones?', null, null, '1', '33');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('34', '1', '7', '', ' ¿Ha tenido hemorragias severas, o alguna afección de la sangre como anemias o leucemia?', null, null, '1', '34');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('35', '1', '7', '', ' ¿Ha recibido tratamiento con hormonas del crecimiento de origen humano (antes de 1987)?', null, null, '1', '35');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('36', '1', '7', '', ' ¿Ha recibido tejido procedente de otra persona (duramadre, córnea, otros)?', null, null, '1', '36');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('37', '1', '7', '', ' ¿Ud. o algún familiar sufre o ha sufrido la enfermedad de Creutzfeldt-Jakob?', null, null, '1', '37');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('38', '1', '7', '', ' ¿Ha padecido Malaria o Paludismo, Dengue, Fiebre Zika o Chinkungunya? ¿Recibió tratamiento?  ¿Viajó a zona endémica? ¿Algún conviviente suyo lo ha padecido?', null, null, '1', '38');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('39', '1', '7', '', ' ¿Ha padecido hepatitis (luego de los 11 años), ictericia o enfermedades del hígado?', null, null, '1', '39');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('40', '1', '7', '', ' ¿Ha padecido Enfermedad de Chagas Ud. o alguien de su familia?', null, null, '1', '40');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('41', '1', '7', '', ' ¿Conoce la vinchuca?', null, null, '1', '41');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('42', '1', '7', '', ' ¿Ha padecido cualquier otra enfermedad no mencionada hasta ahora? ¿Cuál?', null, null, '1', '42');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('43', '1', '7', '', ' ¿Usó o usa drogas ilegales?', null, null, '1', '43');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('44', '1', '7', '', ' ¿Tuvo análisis positivos para VIH o recibió tratamiento para infección por VIH/SIDA?', null, null, '1', '44');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('45', '1', '7', '', ' ¿Ha tenido fatiga, sudoración nocturna, pérdida de peso inexplicable, ganglios inflamados o lesiones de la piel y mucosas?', null, null, '1', '45');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('46', '1', '7', '', ' ¿Ha padecido, recibido tratamiento o le han dicho que tiene análisis positivos para alguna de estas enfermedades: virus de la Hepatitis B o C, virus HTLV I/II, Virus VIH, Chagas, Brucelosis, Sífilis o alguna otra enfermedad de transmisión sexual?', null, null, '1', '46');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('47', '1', '8', '', ' ¿Ha estado durante más de un año (sumando los períodos de estadía) en Gran Bretaña o Irlanda del Norte (entre 1980-1996)?', null, null, '1', '47');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('48', '1', '8', '', ' ¿Ha vivido en o viajado a otra zona de Argentina u otro país extranjero? ¿Cuál?', null, null, '1', '48');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('49', '1', '9', '', ' ¿Está donando sangre para que le hagan análisis de VIH/SIDA?', null, null, '1', '49');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('50', '1', '9', '', ' ¿Recibió dinero, algún tipo de compensación, beneficio o se sintió presionado para donar sangre?', null, null, '1', '50');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('51', '1', '9', '', ' ¿Entendió todas las preguntas del cuestionario?', null, null, '1', '51');


/** 180531 - cuestionarios_respuestas.sql **/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cuestionarios_respuestas
-- ----------------------------
DROP TABLE IF EXISTS `cuestionarios_respuestas`;
CREATE TABLE `cuestionarios_respuestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plantilla_cuestionario_id` int(11) DEFAULT NULL,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `posta_id` int(11) NOT NULL,
  `estado` int(11) DEFAULT NULL COMMENT '0 = NO aprobada\r\n1 = Aprobada\r\n',
  `usuario_responsable_id` int(11) DEFAULT NULL,
  `profesional_responsable_id` int(11) DEFAULT NULL,
  `donacion_id` int(11) DEFAULT NULL,
  `don_nro` varchar(50) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_plantilla_cuestionario_version_id` (`plantilla_cuestionario_id`),
  KEY `idx_don_nro` (`don_nro`),
  CONSTRAINT `fk_plantilla_cuestionario_version_id` FOREIGN KEY (`plantilla_cuestionario_id`) REFERENCES `plantillas_cuestionario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cuestionarios_respuestas
-- ----------------------------


/** 180618 - update.sql **/


SET FOREIGN_KEY_CHECKS=0;
/* Se actualiza tabla Donaciones Ext a UTF-8  */
ALTER TABLE `don_donaciones_ext`
DEFAULT CHARACTER SET=utf8;


/* Se crea campo brazo_intervenido de Donaciones Ext */

ALTER TABLE `don_donaciones_ext`
ADD COLUMN `brazo_intervenido` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `reaccion`;


/** 180910 - reporte_errores.sql **/


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for reporte_errores
-- ----------------------------
DROP TABLE IF EXISTS `reporte_errores`;
CREATE TABLE `reporte_errores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `contacto` varchar(255) DEFAULT NULL,
  `email_contacto` varchar(255) DEFAULT NULL,
  `mensaje` text NOT NULL,
  `exepcion` text NOT NULL,
  `trace` text NOT NULL,
  `predondicion` text,
  `accionEsperada` text,
  `captura_pantalla` text NOT NULL,
  `observacion` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/** 180912 - modificaion-12-09-2018.sql **/
ALTER TABLE `don_donaciones_ext`
CHANGE COLUMN `tipo_donacion` `tipo_extraccion`  int(6) NULL DEFAULT NULL AFTER `cod_colecta`;


/** 180913 - CreacionTablaParametrosGlobales.sql **/ 


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for parametros_globales
-- ----------------------------
DROP TABLE IF EXISTS `parametros_globales`;
CREATE TABLE `parametros_globales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variable` varchar(255) NOT NULL,
  `valor` varchar(255) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `tipo_acceso` int(1) NOT NULL DEFAULT '1' COMMENT '0:Solo Lectura, 1:LecturaEscritura',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/** 180927 - colectas.sql **/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for colectas
-- ----------------------------
DROP TABLE IF EXISTS `colectas`;
CREATE TABLE `colectas` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Identificacion unico para la colecta',
  `codigo_colecta` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `ubicacion` varchar(255) DEFAULT NULL,
  `fecha_colecta` datetime DEFAULT NULL,
  `referente_local` varchar(255) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '0' COMMENT 'Estados posibles para la colecta\r\n\r\n0:Pendiente Publicacion\r\n1:Publicada\r\n2:Terminada\r\n3:Suspendida\r\n4:Eliminada',
  `efector` varchar(255) NOT NULL COMMENT 'Efector del que depende la colecta. Generalmente CRH',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_codigo_colecta` (`codigo_colecta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/** 181018 - Actualizacion comentario en solicitud.sql **/
ALTER TABLE `sol_solicitudes`
MODIFY COLUMN `tdo_id`  int(11) NULL DEFAULT NULL COMMENT 'Estado de la Solicitud' AFTER `usu_creacion`;


/** 181018 - Actualizacion Donaciones_ext.sql **/
ALTER TABLE `don_donaciones_ext`
MODIFY COLUMN `reaccion`  varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL AFTER `nroTubuladura`;



/** 181019 - Actualizacion Donaciones Ext - agregar columna fecha_vencimiento_bolsa.sql **/
ALTER TABLE `don_donaciones_ext`
ADD COLUMN `fecha_vencimiento_bolsa`  datetime NULL AFTER `brazo_intervenido`;

/** 181019 - Actualizacion Donaciones EXT - Solucion Conservadora.sql **/
ALTER TABLE `don_donaciones_ext`
ADD COLUMN `bolsas_id`  int NULL COMMENT 'Solucion Conservadora ID' AFTER `fecha_vencimiento_bolsa`;




/** 181019 - Se agregan campos extras para validar recepcion.sql **/
ALTER TABLE `don_donaciones`
ADD COLUMN `recepcion_cuestionario`  int(3) NULL AFTER `recepcion_muestra`,
ADD COLUMN `recepcion_autoexclusion`  int(3) NULL AFTER `recepcion_cuestionario`;

ALTER TABLE `don_donaciones`
ADD COLUMN `envio_muestras`  int(3) NULL AFTER `pro_estado`;

ALTER TABLE `hem_hemocomponentes`
ADD COLUMN `hem_valor_referencia`  decimal(5,2) NULL AFTER `hem_peso_max`;


/** 181109 - Agregar Campo kell en tabla de inmuno.sql **/
ALTER TABLE `inmunologia`
ADD COLUMN `kell`  varchar(255) NULL AFTER `fenotipo`;


/** 181206 - Creacion tabla analisis-enfermedades.sql **/
CREATE TABLE `analisis_enfermedades` (
`id`  int NOT NULL AUTO_INCREMENT ,
`analisis_id`  int NOT NULL ,
`enfermedaes_id`  int NOT NULL ,
`estado`  int(3) NOT NULL DEFAULT 1 COMMENT 'ESTADOS POSIBLES\r\n0: DESPUBLICADO -1:PUBLICADO 999:ELIMINADO' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `idx-enfermedades-analisis` (`analisis_id`, `enfermedaes_id`) 
)
;


/** 181210 - Crear campo tipo ingreso en determinaciones.sql **/
ALTER TABLE `determinaciones`
ADD COLUMN `tipo_ingreso`  varchar(255) NULL AFTER `resultado`;

/** 181212 - CREAR TABLA SEROLOGIAS-ENFERMEDADES.sql **/
CREATE TABLE serologias_enfermedades (
`id`  int NOT NULL AUTO_INCREMENT ,
`serologias_id`  int NOT NULL ,
`enfermedades_id`  int NOT NULL ,
`determinacion`  varchar(255) NOT NULL COMMENT 'ESTADOS POSIBLES\r\nREACTIVO\r\nNO REACTIVO\r\nDETECTABLE\r\nNO DETECTABLE\r\nINDETERMINADO\r\nNO ESTUDIAR' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `idx_serologia_enfermedad` (`serologias_id`, `enfermedades_id`) 
)
;

/** 181212 ACTUALIZACION TABLA SEROLOGIA - COMPATIBILIDAD.sql **/
ALTER TABLE `serologia`
ADD COLUMN `OTRAS`  varchar(20) NULL AFTER `htlv`;


/** AJUSTES VARIOS SIN DOCUMENTAR **/
SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `analisis_enfermedades` DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
ALTER TABLE `bolsas_marcas` ADD COLUMN `marca_bolsa`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `bolsas_marcas` DROP COLUMN `bolsa_marca`;
CREATE TABLE `donantes` (
`Id`  int(11) NOT NULL AUTO_INCREMENT ,
`Sicap_id`  int(11) NULL DEFAULT NULL ,
`CodTipDocumento`  char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`NroDocumento`  varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Nombre`  varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Apellido`  varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`CodOcupacion`  char(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`CodEstCivil`  char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`CodNivelEducacion`  char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`CodPais`  char(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Pais_otro`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`CodSexo`  char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`CodLocalidad`  varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Localidad_otra`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`Calle`  varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`calle_otra`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`Numero`  varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Piso`  varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Depto`  char(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Manzana`  varchar(17) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`EntrecalleA`  varchar(27) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`EntrecalleB`  varchar(27) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`Municipio`  varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`Distrito`  varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`Bis`  char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N' ,
`CodPostal`  varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`GPS_Longitud`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' ,
`GPS_Latitud`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' ,
`Telefono`  varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Celular`  varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`Email`  varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`ApellidoCasada`  varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`FechaNacimiento`  date NULL DEFAULT NULL ,
`CodPaisNacimiento`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' ,
`PaisNacimiento_otro`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`CodProvinciaNacimiento`  varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' ,
`CodLocalidadNacimiento`  varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' ,
`LocalidadNacimiento_otra`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`CodProvincia`  char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`Observacion`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
`Fuente`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`FechaAta`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ,
`UsuarioIngreso`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`FechaModificacion`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ,
`UsuarioModificacion`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`FechaBaja`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ,
`UsuarioBaja`  varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`MotivoBaja`  varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
PRIMARY KEY (`Id`),
UNIQUE INDEX `idx_persona` (`CodTipDocumento`, `NroDocumento`, `FechaNacimiento`, `CodSexo`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
ROW_FORMAT=Compact
;
ALTER TABLE `don_donaciones_ext` ADD COLUMN `hematocritos`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `hb`;
ALTER TABLE `don_donaciones_ext` ADD COLUMN `temperatura_corporal`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `pulso`;

ALTER TABLE `mot_motivos` MODIFY COLUMN `mot_estado`  tinyint(1) NOT NULL COMMENT '0: Inactivo - 1:Activo' AFTER `dias`;
ALTER TABLE `reporte_errores` MODIFY COLUMN `captura_pantalla`  longblob NOT NULL AFTER `accionEsperada`;
ALTER TABLE `reporte_errores` ADD COLUMN `url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `observacion`;
ALTER TABLE `serologias_enfermedades` DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
ALTER TABLE `serologias_enfermedades` MODIFY COLUMN `determinacion`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ESTADOS POSIBLES\r\nREACTIVO\r\nNO REACTIVO\r\nDETECTABLE\r\nNO DETECTABLE\r\nINDETERMINADO\r\nNO ESTUDIAR' AFTER `enfermedades_id`;
ALTER TABLE `sol_diferimientos` DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
ALTER TABLE `stock` DROP PRIMARY KEY;
ALTER TABLE `stock` ADD COLUMN `fecha`  datetime NOT NULL AFTER `id`;
ALTER TABLE `stock` MODIFY COLUMN `estado`  int(4) NULL DEFAULT NULL AFTER `efector_id`;
ALTER TABLE `stock` ADD COLUMN `cantidad`  decimal(10,3) NULL DEFAULT NULL AFTER `peso_produccion`;
ALTER TABLE `stock` ADD COLUMN `fecha_vencimiento`  datetime NOT NULL AFTER `cantidad_utilizada`;
ALTER TABLE `stock` ADD COLUMN `fecha_descarte`  date NULL DEFAULT NULL AFTER `irradiado`;
ALTER TABLE `stock` ADD COLUMN `grupo`  varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `fraccionado`;
ALTER TABLE `stock` ADD COLUMN `factor`  varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `grupo`;
ALTER TABLE `stock` DROP COLUMN `fec_movimiento`;
ALTER TABLE `stock` DROP COLUMN `fec_vencimiento`;
ALTER TABLE `stock` ADD PRIMARY KEY (`id`);
CREATE INDEX `efector_id` ON `stock`(`efector_id`) USING BTREE ;
CREATE INDEX `fecha` ON `stock`(`fecha`) USING BTREE ;
CREATE INDEX `fecha_vencimiento` ON `stock`(`fecha_vencimiento`) USING BTREE ;
CREATE INDEX `fecha_descarte` ON `stock`(`fecha_descarte`) USING BTREE ;
CREATE TABLE `stock_hemocomponentes-usar-esta-no-stock` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`fecha`  datetime NOT NULL ,
`efector_id`  int(11) NOT NULL ,
`estado`  int(4) NULL DEFAULT NULL ,
`donacion_id`  int(11) NOT NULL ,
`hemocomponente_id`  int(11) NOT NULL ,
`peso_produccion`  decimal(10,3) NOT NULL COMMENT 'Peso registrado en produccion' ,
`cantidad`  decimal(10,3) NULL DEFAULT NULL ,
`cantidad_utilizada`  decimal(10,3) NOT NULL COMMENT 'Peso de lo que se usó.' ,
`fecha_vencimiento`  datetime NOT NULL ,
`irradiado`  tinyint(4) NOT NULL COMMENT 'Si fue irradiado' ,
`fecha_descarte`  date NULL DEFAULT NULL ,
`fraccionado`  tinyint(4) NOT NULL COMMENT 'Si fue fraccionado' ,
`grupo`  varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`factor`  varchar(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
`observaciones`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `id` (`id`) USING BTREE ,
INDEX `efector_id` (`efector_id`) USING BTREE ,
INDEX `fecha` (`fecha`) USING BTREE ,
INDEX `fecha_vencimiento` (`fecha_vencimiento`) USING BTREE ,
INDEX `fecha_descarte` (`fecha_descarte`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci
ROW_FORMAT=Compact
;
DROP TABLE `codigoscentralizados`;
DROP TABLE `stock_descartes`;
SET FOREIGN_KEY_CHECKS=1;


/** AJUSTE EN PLANILLA CUESTIONARIO DETALLE plantillas_cuestionario_detalle-AGREGADA-PREGUNTA-28.sql **/


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for plantillas_cuestionario_detalle
-- ----------------------------
DROP TABLE IF EXISTS `plantillas_cuestionario_detalle`;
CREATE TABLE `plantillas_cuestionario_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planilla_cuestionario_id` int(11) DEFAULT NULL,
  `cuestionario_pregunta_categoria_id` int(11) DEFAULT NULL,
  `codigo_pregunta` varchar(255) NOT NULL,
  `orden` int(11) NOT NULL,
  `pregunta` varchar(255) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `criterio_evaluacion` varchar(255) DEFAULT NULL,
  `estado` int(4) NOT NULL COMMENT '0 = sin uso\r\n1 = en uso (activa)\r\n2 = archivada (mantiene datos historicos)',
  PRIMARY KEY (`id`),
  KEY `fk_plantilla_cuestionario_id` (`planilla_cuestionario_id`),
  KEY `fk_cuestionario_pregunta_categoria_id` (`cuestionario_pregunta_categoria_id`),
  CONSTRAINT `fk_plantilla_cuestionario_id` FOREIGN KEY (`planilla_cuestionario_id`) REFERENCES `plantillas_cuestionario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plantillas_cuestionario_detalle
-- ----------------------------
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('1', '1', '1', ' ', '1', '¿Se le ha proporcionado información escrita para su autoexclusión pre-donación?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('2', '1', '1', ' ', '2', '¿Ha comprendido las situaciones de riesgo incrementado para la donación de sangre, descriptas en el Documento para la autoexclusión pre-donación?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('3', '1', '1', ' ', '3', '¿Alguna vez ha donado sangre, plasma o plaquetas? ¿Dónde?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('4', '1', '1', ' ', '4', '¿Actualmente se siente Ud. bien y goza de buena salud?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('5', '1', '2', ' ', '5', '¿Debe realizar alguna actividad laboral o deportiva de riesgo (trabajo en altura, escalada) o conducir un vehículo de transporte público?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('6', '1', '3', ' ', '6', '¿Está tomando medicamentos o ha tomado en los últimos días? ¿Cuáles?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('7', '1', '3', ' ', '7', '¿Está tomando o ha tomado alguna vez medicación para el tratamiento de la psoriasis, el acné o la próstata? ¿Cuál?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('8', '1', '3', ' ', '8', '¿Tomó aspirina o analgésicos en los últimos dos días?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('9', '1', '4', ' ', '9', '¿Ha tenido fiebre, acompañada de dolor de cabeza, dolor articular, conjuntivitis o malestar general?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('10', '1', '4', ' ', '10', '¿Ha recibido tratamiento odontológico o tiene alguna infección dentaria?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('11', '1', '5', ' ', '11', '¿Ha recibido alguna vacuna? ¿Cuál?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('12', '1', '5', ' ', '12', '¿Ha estado en contacto con alguna persona que padeciera una enfermedad?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('13', '1', '6', ' ', '13', '¿Ha recibido Gamma Globulina (por exposición a hepatitis u otras infecciones)?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('14', '1', '6', '', '14', 'Mujeres: ¿Estuvo embarazada (parto normal, cesárea, aborto) o lo está ahora? ¿Está amamantando? Nro. de gestas\r\n', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('15', '1', '6', '', '15', '¿Se ha realizado tatuajes, perforaciones en orejas u otra parte del cuerpo (“piercing”), tratamientos estéticos invasivos en la piel o acupuntura?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('16', '1', '6', '', '16', '¿Ha recibido transfusiones de sangre, hemocomponentes, trasplantes de órganos o tejidos?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('17', '1', '6', '', '17', '¿Le realizaron algún tratamiento médico, cirugía mayor-menor, cirugía laparoscópica o estudios endoscópicos (colonoscopía, endoscopia, artroscopia, etc.)?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('18', '1', '6', '', '18', '¿Estuvo en contacto con sangre humana o fluidos corporales a través de piel o mucosas?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('19', '1', '6', '', '19', '¿Convive o ha convivido con alguien que padece hepatitis o ictericia?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('20', '1', '6', '', '20', '¿Ha padecido Fiebre Hemorrágica Argentina (\"Mal de los rastrojos\")?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('21', '1', '6', '', '21', '¿Tuvo contacto sexual con alguna persona con VIH/SIDA, Hepatitis B, Hepatitis C, o HTLV I/II o análisis positivos para alguna de estas infecciones?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('22', '1', '6', '', '22', '¿Tuvo contacto sexual con personas hemodializadas, con hemofilia, o que hayan recibido concentrados de factores de la coagulación, o transfusiones de componentes de la sangre?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('23', '1', '6', '', '23', '¿Recibió dinero, drogas o cualquier otro tipo de pago a cambio de sexo?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('24', '1', '6', '', '24', '¿Tuvo contacto sexual con una persona con hepatitis?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('25', '1', '6', '', '25', '¿Fue tratado por sífilis o gonorrea? ¿Tuvo alguna otra Enfermedad de Transmisión Sexual (ETS) o estudios positivos para ETS?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('26', '1', '6', '', '26', '¿Ha estado detenido/a o demorado durante 3 o más dias en cárceles o comisarías?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('27', '1', '6', '', '27', '¿Tuvo Ud. alguna/s de las  situaciones consideradas de riesgo incrementado para la donación de sangre o tiene motivos para creer que su pareja las tuvo?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('28', '1', '6', '', '28', '¿tiene pareja sexual estable? SI/no', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('29', '1', '7', '', '29', '¿Ha padecido enfermedades cardíacas como angina de pecho, infarto, arritmias, trombosis o algún problema de la coagulación?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('30', '1', '7', '', '30', '¿Ha tenido convulsiones, desmayos o epilepsia?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('31', '1', '7', '', '31', '¿Es diabético insulinodependiente?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('32', '1', '7', '', '32', '¿Padece enfermedades renales?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('33', '1', '7', '', '33', '¿Ha sufrido alguna enfermedad del aparato digestivo, glándulas tiroides, paratiroides u otras?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('34', '1', '7', '', '34', '¿Alguna vez le diagnosticaron cáncer o recibió quimioterapia o radiaciones?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('35', '1', '7', '', '35', '¿Ha tenido hemorragias severas, o alguna afección de la sangre como anemias o leucemia?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('36', '1', '7', '', '36', '¿Ha recibido tratamiento con hormonas del crecimiento de origen humano (antes de 1987)?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('37', '1', '7', '', '37', '¿Ha recibido tejido procedente de otra persona (duramadre, córnea, otros)?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('38', '1', '7', '', '38', '¿Ud. o algún familiar sufre o ha sufrido la enfermedad de Creutzfeldt-Jakob?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('39', '1', '7', '', '39', '¿Ha padecido Malaria o Paludismo, Dengue, Fiebre Zika o Chinkungunya? ¿Recibió tratamiento?  ¿Viajó a zona endémica? ¿Algún conviviente suyo lo ha padecido?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('40', '1', '7', '', '40', '¿Ha padecido hepatitis (luego de los 11 años), ictericia o enfermedades del hígado?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('41', '1', '7', '', '41', '¿Ha padecido Enfermedad de Chagas Ud. o alguien de su familia?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('42', '1', '7', '', '42', '¿Conoce la vinchuca?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('43', '1', '7', '', '43', '¿Ha padecido cualquier otra enfermedad no mencionada hasta ahora? ¿Cuál?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('44', '1', '7', '', '44', '¿Usó o usa drogas ilegales?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('45', '1', '7', '', '45', '¿Tuvo análisis positivos para VIH o recibió tratamiento para infección por VIH/SIDA?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('46', '1', '7', '', '46', '¿Ha tenido fatiga, sudoración nocturna, pérdida de peso inexplicable, ganglios inflamados o lesiones de la piel y mucosas?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('47', '1', '8', '', '47', '¿Ha padecido, recibido tratamiento o le han dicho que tiene análisis positivos para alguna de estas enfermedades: virus de la Hepatitis B o C, virus HTLV I/II, Virus VIH, Chagas, Brucelosis, Sífilis o alguna otra enfermedad de transmisión sexual?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('48', '1', '8', '', '48', '¿Ha estado durante más de un año (sumando los períodos de estadía) en Gran Bretaña o Irlanda del Norte (entre 1980-1996)?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('49', '1', '9', '', '49', '¿Ha vivido en o viajado a otra zona de Argentina u otro país extranjero? ¿Cuál?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('50', '1', '9', '', '50', '¿Está donando sangre para que le hagan análisis de VIH/SIDA?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('51', '1', '9', '', '51', '¿Recibió dinero, algún tipo de compensación, beneficio o se sintió presionado para donar sangre?', null, null, '1');
INSERT INTO `plantillas_cuestionario_detalle` VALUES ('52', '1', '9', '', '52', '¿Entendió todas las preguntas del cuestionario?', null, null, '1');


/** ACTUALIZACION DE DATOS **/


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for analisis
-- ----------------------------
TRUNCATE TABLE `analisis`;
-- ----------------------------
-- Records of analisis
-- ----------------------------
INSERT INTO `analisis` VALUES ('2', '1', '371T', '0', 'T.P.H.A. (T.PALIDUM)', '1');
INSERT INTO `analisis` VALUES ('4', '2', '494', '0', 'HUDDLESSON REACCION', '1');
INSERT INTO `analisis` VALUES ('5', '2', '960', '0', 'BRUCELOSIS: WRIGHT REACCION C/2ME', '1');
INSERT INTO `analisis` VALUES ('6', '2', '961', '0', 'BRUCELOSIS: WRIGHT REACCION S/2ME', '1');
INSERT INTO `analisis` VALUES ('7', '3', '243Q', '1', 'CHAGAS: Ac (QL)', '1');
INSERT INTO `analisis` VALUES ('8', '3', '243E', '1', 'CHAGAS: Ac. (ELISA)', '1');
INSERT INTO `analisis` VALUES ('9', '4', '503', '0', 'HEPATITIS B: Ag.SUP. (HBsAg)', '1');
INSERT INTO `analisis` VALUES ('10', '4', '087', '0', 'HEPATITIS B: Ac. ANTI-CORE IgG', '1');
INSERT INTO `analisis` VALUES ('11', '5', '495D', '0', 'HEPATITIS C (Ac. + Ag)', '1');
INSERT INTO `analisis` VALUES ('12', '6', '063', '0', 'HIV: Ag-Ac (ELISA)', '1');
INSERT INTO `analisis` VALUES ('13', '6', '063Q', '1', 'HIV: Ag-Ac (QL)', '1');
INSERT INTO `analisis` VALUES ('14', '7', '063HTL', '0', 'HTLV I y II: Ac. (ELISA)', '1');
INSERT INTO `analisis` VALUES ('15', '99', '2506', '1', 'NAT', '1');
INSERT INTO `analisis` VALUES ('16', '2', '494RB', '1', 'Brucelosis - Rosa de Bengala', '1');
INSERT INTO `analisis` VALUES ('17', '1', '933', '1', 'VDRL', '1');
INSERT INTO `analisis` VALUES ('18', '4', '087Q', '1', 'HEPATITIS B: Ac. ANTI-CORE IgG QL', '1');
INSERT INTO `analisis` VALUES ('19', '7', '063HTLQ', '1', 'HTLV I y II: Ac. (QL)', '1');
INSERT INTO `analisis` VALUES ('20', '4', '503Q', '1', 'HEPATITIS B: Ag.SUP. (HBsAg) QL', '1');
INSERT INTO `analisis` VALUES ('21', '5', '495Q', '1', 'HEPATITIS C Ac.', '1');
INSERT INTO `analisis` VALUES ('22', '2', '494BPA', '0', 'Brucelosis BPA', '1');

TRUNCATE TABLE `bolsas_marcas`;
-- ----------------------------
-- Records of bolsas_marcas
-- ----------------------------
INSERT INTO `bolsas_marcas` VALUES ('1', 'Rivero');
INSERT INTO `bolsas_marcas` VALUES ('2', 'Baxter');
INSERT INTO `bolsas_marcas` VALUES ('3', 'Terumo');
INSERT INTO `bolsas_marcas` VALUES ('4', 'Grifols');
INSERT INTO `bolsas_marcas` VALUES ('5', 'Troge');


TRUNCATE TABLE `efectores_parametros`;
-- ----------------------------
-- Records of efectores_parametros
-- ----------------------------
INSERT INTO `efectores_parametros` VALUES ('1', '184', 'HPR', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('2', '167', 'HEE', '77777', 'Medico', '45', '0');
INSERT INTO `efectores_parametros` VALUES ('3', '181', 'HZN', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('4', '657', 'HNV', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('5', '660', 'RSP', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('6', '838', 'CEM', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('7', '785', 'CRH', '77777', 'Medico', '45', '1');


TRUNCATE TABLE `enfermedades`;
-- ----------------------------
-- Records of enfermedades
-- ----------------------------
INSERT INTO `enfermedades` VALUES ('1', 'Sifilis');
INSERT INTO `enfermedades` VALUES ('2', 'Brucelosis');
INSERT INTO `enfermedades` VALUES ('3', 'Chagas');
INSERT INTO `enfermedades` VALUES ('4', 'Hepatitis B');
INSERT INTO `enfermedades` VALUES ('5', 'Hepatitis C');
INSERT INTO `enfermedades` VALUES ('6', 'HIV');
INSERT INTO `enfermedades` VALUES ('7', 'HTLV');
INSERT INTO `enfermedades` VALUES ('99', 'NAT');


TRUNCATE TABLE `parametros_globales`;
-- ----------------------------
-- Records of parametros_globales
-- ----------------------------
INSERT INTO `parametros_globales` VALUES (null,'', '', null, '1');
INSERT INTO `parametros_globales` VALUES (null, 'dias_entre_donaciones', '3', 'Cantidad de Dias que deben pasar entre donaciones para que un donante pueda volver a donar.', '1');


TRUNCATE TABLE `ppr_perfiles_produccion`;
-- ----------------------------
-- Records of ppr_perfiles_produccion
-- ----------------------------
INSERT INTO `ppr_perfiles_produccion` VALUES ('1', 'Alergia', 'A', '0');
INSERT INTO `ppr_perfiles_produccion` VALUES ('2', 'Polimedicado/a', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('3', 'Politransfundido/a', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('4', 'Multipara', 'F', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('5', 'AAS3', 'A', '1');


TRUNCATE TABLE `tdo_tipos_donaciones`;
-- ----------------------------
-- Records of tdo_tipos_donaciones
-- ----------------------------
INSERT INTO `tdo_tipos_donaciones` VALUES ('1', 'Aferesis', '1');
INSERT INTO `tdo_tipos_donaciones` VALUES ('2', 'Sangre Normal', '1');
INSERT INTO `tdo_tipos_donaciones` VALUES ('3', 'Toma de muestra', '1');

/** FIN ACTUALIZADOR **/





