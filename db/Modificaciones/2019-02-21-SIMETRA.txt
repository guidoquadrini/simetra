------------------------------------------
* Usuarios
------------------------------------------
Se ajusto vista cambiar password para que figure, nombre y apellido + nombre de usuario
Se corrigio controladora para que filtre correctamente el nombre del usuario

* /Acceso/controllers/UsuariosController.php
* /Acceso/views/scripts/usuarios/cambiarpassword.phtml




------------------------------------------
* Donaciones
------------------------------------------
Se corrigio modelo getSinDespachoByEfe() 
agregando filtro de donaciones diferidas temporal y permanente para envios a CRH 

* /Banco/models/Donaciones.php



------------------------------------------
* Examenes Medicos
------------------------------------------
Se mejoro la grabacion de los registros diferidos temporales o permanentes
en la controladora --> cambiarEstadoAction()

* /Efector/controllers/ExamenesController.php



------------------------------------------
* Inmunologia
------------------------------------------
Se modifico la controladora para que en el verpdfAction() se ampliara la visualizacion
del campo observaciones.

* /Banco/controllers/InmunologiaController.php
