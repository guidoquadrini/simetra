/*
Navicat MySQL Data Transfer

Source Server         : MySQL-locahost
Source Server Version : 50624
Source Host           : 127.0.0.1:3306
Source Database       : dsalud_banco

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-09-06 11:50:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for afe_tipos_aferesis
-- ----------------------------
DROP TABLE IF EXISTS `afe_tipos_aferesis`;
CREATE TABLE `afe_tipos_aferesis` (
  `afe_id` int(11) NOT NULL AUTO_INCREMENT,
  `afe_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `afe_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`afe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for analisis
-- ----------------------------
DROP TABLE IF EXISTS `analisis`;
CREATE TABLE `analisis` (
  `ana_id` int(11) NOT NULL AUTO_INCREMENT,
  `enf_id` int(11) NOT NULL,
  `cod` varchar(10) CHARACTER SET latin1 NOT NULL,
  `def` int(11) NOT NULL,
  `descripcion` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ana_id`),
  KEY `enf_id` (`enf_id`),
  CONSTRAINT `analisis_ibfk_1` FOREIGN KEY (`enf_id`) REFERENCES `enfermedades` (`enf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bolsas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas`;
CREATE TABLE `bolsas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_bolsa` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dias` int(11) NOT NULL,
  `activo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for bolsas_marcas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas_marcas`;
CREATE TABLE `bolsas_marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsa_marca` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bolsas_tipos
-- ----------------------------
DROP TABLE IF EXISTS `bolsas_tipos`;
CREATE TABLE `bolsas_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsa_tipo` varchar(50) DEFAULT NULL,
  `peso_minimo` decimal(5,3) DEFAULT NULL,
  `peso_maximo` decimal(5,3) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for centralizados
-- ----------------------------
DROP TABLE IF EXISTS `centralizados`;
CREATE TABLE `centralizados` (
  `don_id` int(11) NOT NULL AUTO_INCREMENT,
  `nroLote` int(11) NOT NULL,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) DEFAULT NULL,
  `per_dni` char(20) DEFAULT NULL,
  `don_nro` char(32) NOT NULL,
  `don_fh_inicio` datetime DEFAULT NULL,
  `usu_inicio` int(11) NOT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  `lote_estado` int(11) NOT NULL,
  PRIMARY KEY (`don_id`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for codigoscentralizados
-- ----------------------------
DROP TABLE IF EXISTS `codigoscentralizados`;
CREATE TABLE `codigoscentralizados` (
  `don_id` int(11) NOT NULL,
  `nroLote` int(11) NOT NULL,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) DEFAULT NULL,
  `per_dni` char(20) DEFAULT NULL,
  `don_nro` char(32) NOT NULL,
  `don_fh_inicio` datetime NOT NULL,
  `usu_inicio` int(11) NOT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  `lote_estado` int(11) NOT NULL,
  PRIMARY KEY (`don_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for contingencia
-- ----------------------------
DROP TABLE IF EXISTS `contingencia`;
CREATE TABLE `contingencia` (
  `don_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) DEFAULT NULL,
  `per_dni` char(20) CHARACTER SET latin1 DEFAULT NULL,
  `don_nro` char(32) CHARACTER SET latin1 NOT NULL,
  `don_fh_inicio` datetime NOT NULL,
  `usu_inicio` int(11) NOT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  PRIMARY KEY (`don_id`),
  KEY `idefector` (`efe_id`),
  KEY `finicio` (`don_fh_inicio`),
  KEY `estado` (`don_estado`),
  KEY `nro` (`don_nro`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for detalles_preproduccion
-- ----------------------------
DROP TABLE IF EXISTS `detalles_preproduccion`;
CREATE TABLE `detalles_preproduccion` (
  `dpr_id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `hem_id` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `tipo_prod` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dpr_id`),
  KEY `iddonacion` (`don_id`),
  KEY `idhemoc` (`hem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for detalles_produccion
-- ----------------------------
DROP TABLE IF EXISTS `detalles_produccion`;
CREATE TABLE `detalles_produccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iddonacion` int(11) NOT NULL,
  `idhemocomponente` int(11) NOT NULL,
  `nrobolsa` int(11) DEFAULT NULL,
  `fini_prod` datetime NOT NULL,
  `ffin_prod` datetime DEFAULT NULL,
  `f_vencimiento` datetime DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `detalle` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `validado` int(11) DEFAULT NULL,
  `peso` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `hem_temp` int(11) DEFAULT NULL,
  `usu_id_valida` int(11) DEFAULT NULL,
  `env_prod_id` int(11) DEFAULT NULL,
  `irradiado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for determinaciones
-- ----------------------------
DROP TABLE IF EXISTS `determinaciones`;
CREATE TABLE `determinaciones` (
  `det_id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_ana` varchar(10) CHARACTER SET latin1 NOT NULL,
  `don_id` int(11) NOT NULL,
  `fecha_exp` timestamp NULL DEFAULT NULL,
  `usu_exp_id` int(11) DEFAULT NULL,
  `fecha_imp` timestamp NULL DEFAULT NULL,
  `usu_imp_id` int(11) DEFAULT NULL,
  `resultado` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`det_id`),
  KEY `ana_id` (`cod_ana`),
  KEY `don_id` (`don_id`),
  CONSTRAINT `determinaciones_ibfk_2` FOREIGN KEY (`don_id`) REFERENCES `don_donaciones` (`don_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for det_determinaciones
-- ----------------------------
DROP TABLE IF EXISTS `det_determinaciones`;
CREATE TABLE `det_determinaciones` (
  `det_id` int(11) NOT NULL AUTO_INCREMENT,
  `det_cod` varchar(15) CHARACTER SET latin1 NOT NULL,
  `det_cod_lab` varchar(15) CHARACTER SET latin1 NOT NULL,
  `det_descripcion` varchar(120) CHARACTER SET latin1 NOT NULL,
  `det_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for don_donaciones
-- ----------------------------
DROP TABLE IF EXISTS `don_donaciones`;
CREATE TABLE `don_donaciones` (
  `don_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) NOT NULL,
  `per_id` char(20) CHARACTER SET latin1 NOT NULL,
  `don_nro` char(32) CHARACTER SET latin1 NOT NULL,
  `don_fh_inicio` datetime NOT NULL,
  `usu_inicio` int(11) NOT NULL,
  `ppr_id` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_usu_extraccion` int(11) DEFAULT NULL,
  `don_fh_aceptacion` datetime DEFAULT NULL,
  `usu_aceptacion` int(11) DEFAULT NULL,
  `despacho_id` int(11) DEFAULT NULL,
  `mot_id` int(11) DEFAULT NULL,
  `motivos_diferimiento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  `don_tiempo_extraccion` tinyint(4) DEFAULT NULL,
  `don_peso` int(11) DEFAULT NULL,
  `pro_estado` int(11) DEFAULT NULL,
  `recepcion_bolsa` int(3) unsigned DEFAULT NULL,
  `recepcion_muestra` int(3) unsigned DEFAULT NULL,
  `don_fh_recepcion` datetime DEFAULT NULL,
  `don_usu_recepcion` int(11) DEFAULT NULL,
  `don_temp` int(11) DEFAULT NULL,
  `cod_muni` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`don_id`),
  KEY `idefector` (`efe_id`),
  KEY `finicio` (`don_fh_inicio`),
  KEY `estado` (`don_estado`),
  KEY `iddespacho` (`despacho_id`),
  KEY `nro` (`don_nro`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for don_donaciones_ext
-- ----------------------------
DROP TABLE IF EXISTS `don_donaciones_ext`;
CREATE TABLE `don_donaciones_ext` (
  `ext_id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `cod_colecta` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `tipo` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `peso` decimal(5,2) DEFAULT NULL,
  `hb` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `hematocritos` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ta` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `pulso` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `temperatura_corporal` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `tipo_bolsa` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `marca_bolsa` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `nroLote_bolsa` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nroTubuladura` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reaccion` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ext_id`,`don_id`),
  KEY `don_id` (`don_id`),
  CONSTRAINT `don_donaciones_ext_ibfk_1` FOREIGN KEY (`don_id`) REFERENCES `don_donaciones` (`don_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for efectores_parametros
-- ----------------------------
DROP TABLE IF EXISTS `efectores_parametros`;
CREATE TABLE `efectores_parametros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_establecimiento` int(11) NOT NULL,
  `nombre_corto` varchar(3) CHARACTER SET latin1 NOT NULL,
  `medico_matricula` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medico_nombre` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `servicio_id` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpo_impresion` int(1) NOT NULL DEFAULT '0' COMMENT '0:Autogenerado;1:Centralizado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for enfermedades
-- ----------------------------
DROP TABLE IF EXISTS `enfermedades`;
CREATE TABLE `enfermedades` (
  `enf_id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`enf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for envios_produccion
-- ----------------------------
DROP TABLE IF EXISTS `envios_produccion`;
CREATE TABLE `envios_produccion` (
  `env_prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_origen` int(11) NOT NULL,
  `efe_destino` int(11) NOT NULL,
  `otro_destino` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `fecha_env` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_rec` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `usu_env` int(11) NOT NULL,
  `usu_rec` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `observaciones` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`env_prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for env_envios
-- ----------------------------
DROP TABLE IF EXISTS `env_envios`;
CREATE TABLE `env_envios` (
  `env_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_id` int(11) NOT NULL,
  `efe_nombre_origen` varchar(100) CHARACTER SET latin1 NOT NULL,
  `efe_id_destino` int(11) DEFAULT NULL,
  `env_nro` varchar(50) CHARACTER SET latin1 NOT NULL,
  `env_fh` datetime NOT NULL,
  `env_usuario` int(11) NOT NULL,
  `env_estado` tinyint(1) NOT NULL,
  `observaciones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`env_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) CHARACTER SET latin1 NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`grupo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for hemocomponentesxperfilprod
-- ----------------------------
DROP TABLE IF EXISTS `hemocomponentesxperfilprod`;
CREATE TABLE `hemocomponentesxperfilprod` (
  `hem_fil_id` int(11) NOT NULL AUTO_INCREMENT,
  `idperfil` int(11) NOT NULL,
  `idhemocomponente` int(11) NOT NULL,
  PRIMARY KEY (`hem_fil_id`),
  KEY `idperfil` (`idperfil`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for hem_hemocomponentes
-- ----------------------------
DROP TABLE IF EXISTS `hem_hemocomponentes`;
CREATE TABLE `hem_hemocomponentes` (
  `hem_id` int(11) NOT NULL AUTO_INCREMENT,
  `hem_codigo` char(5) NOT NULL,
  `hem_descripcion` varchar(60) NOT NULL,
  `hem_dias_aptitud` smallint(6) NOT NULL,
  `hem_nrobolsa` int(1) DEFAULT '1',
  `hem_estado` int(11) NOT NULL DEFAULT '0',
  `hem_caducidad` int(11) NOT NULL,
  `hem_esperado` int(11) NOT NULL DEFAULT '0',
  `hem_temp` varchar(12) NOT NULL,
  `hem_peso_min` int(11) NOT NULL,
  `hem_peso_max` int(11) NOT NULL,
  PRIMARY KEY (`hem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for inmunologia
-- ----------------------------
DROP TABLE IF EXISTS `inmunologia`;
CREATE TABLE `inmunologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `grupo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `factor` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `rastreo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `grupo_inverso` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `du` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cde` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fenotipo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mot_descartes
-- ----------------------------
DROP TABLE IF EXISTS `mot_descartes`;
CREATE TABLE `mot_descartes` (
  `mot_id` int(11) NOT NULL AUTO_INCREMENT,
  `mot_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `mot_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`mot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mot_motivos
-- ----------------------------
DROP TABLE IF EXISTS `mot_motivos`;
CREATE TABLE `mot_motivos` (
  `mot_id` int(11) NOT NULL AUTO_INCREMENT,
  `mot_cat` enum('DIF','DES','REC','PPR') CHARACTER SET latin1 NOT NULL,
  `mot_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `dias` int(11) DEFAULT NULL,
  `mot_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`mot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for movimientos
-- ----------------------------
DROP TABLE IF EXISTS `movimientos`;
CREATE TABLE `movimientos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `efector_id` bigint(20) DEFAULT NULL,
  `destino` int(11) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `refencia` varchar(50) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for movimientos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `movimientos_detalle`;
CREATE TABLE `movimientos_detalle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `movimiento_id` bigint(20) DEFAULT NULL,
  `donacion_id` bigint(20) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ocu_ocupaciones
-- ----------------------------
DROP TABLE IF EXISTS `ocu_ocupaciones`;
CREATE TABLE `ocu_ocupaciones` (
  `ocu_id` int(11) NOT NULL AUTO_INCREMENT,
  `ocu_descripcion` varchar(90) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ocu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for pedidos_insumos
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos`;
CREATE TABLE `pedidos_insumos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL COMMENT '1 = Generado\r\n2 = Enviado\r\n3 = Recep. Parcial\r\n4 = Cerrado\r\n5 = Cancelado\r\n6 = Pend. QA\r\n7 = Autorizar Pedido\r\n8 = Nuevo Envio',
  `observaciones` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `efector_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) unsigned zerofill DEFAULT NULL,
  `ultima_recepcion_observaciones` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ultima_recepcion_fecha` datetime DEFAULT NULL,
  `ultima_recepcion_usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`),
  KEY `estado` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pedidos_insumos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos_detalle`;
CREATE TABLE `pedidos_insumos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  `cantidad_enviada` decimal(10,2) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `notas` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `producto_id` (`producto_id`) USING BTREE,
  KEY `pedido_id` (`pedido_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pedidos_insumos_log
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos_log`;
CREATE TABLE `pedidos_insumos_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `accion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `datos` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `pedido_id` (`pedido_id`) USING BTREE,
  KEY `efector_id` (`efector_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for per_personas
-- ----------------------------
DROP TABLE IF EXISTS `per_personas`;
CREATE TABLE `per_personas` (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `sicap_id` int(11) DEFAULT NULL,
  `per_nrodoc` char(14) CHARACTER SET latin1 NOT NULL,
  `per_tipodoc` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `per_nombres` varchar(120) CHARACTER SET latin1 NOT NULL,
  `per_apellido` varchar(80) CHARACTER SET latin1 NOT NULL,
  `per_fnac` date NOT NULL,
  `per_sexo` char(1) CHARACTER SET latin1 NOT NULL,
  `ult_serologia` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `pais_nacimiento_otro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad_nacimiento_otra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pais_otro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad_otra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calle_otra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE KEY `nrodoc` (`per_nrodoc`),
  KEY `fnac` (`per_fnac`),
  KEY `sexo` (`per_sexo`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for ppr_perfiles_produccion
-- ----------------------------
DROP TABLE IF EXISTS `ppr_perfiles_produccion`;
CREATE TABLE `ppr_perfiles_produccion` (
  `ppr_id` int(11) NOT NULL AUTO_INCREMENT,
  `ppr_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `ppr_genero` enum('A','F','M') CHARACTER SET latin1 NOT NULL DEFAULT 'A',
  `ppr_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`ppr_id`),
  UNIQUE KEY `nombre` (`ppr_descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productocategoria_id` int(11) DEFAULT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_producto` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '0',
  `ultimo_costo` decimal(10,2) DEFAULT NULL,
  `stock_punto_reposicion` decimal(10,2) DEFAULT NULL,
  `stock_minimo` decimal(10,2) DEFAULT NULL,
  `stock_maximo` decimal(10,2) DEFAULT NULL,
  `control_calidad` int(4) DEFAULT NULL,
  `proveedor_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productocategoria_id` (`productocategoria_id`) USING BTREE,
  KEY `coidigo` (`codigo`) USING BTREE,
  KEY `nombre_producto` (`nombre_producto`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for productos_categorias
-- ----------------------------
DROP TABLE IF EXISTS `productos_categorias`;
CREATE TABLE `productos_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(100) CHARACTER SET latin1 NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nombre:_categoria` (`nombre_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for serologia
-- ----------------------------
DROP TABLE IF EXISTS `serologia`;
CREATE TABLE `serologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `sifilis` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brucelosis` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chagas` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hepatitisB` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hepatitisC` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hiv` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htlv` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `validado` int(11) DEFAULT NULL,
  `usu_id_valida` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sol_solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `sol_solicitudes`;
CREATE TABLE `sol_solicitudes` (
  `sol_id` int(11) NOT NULL AUTO_INCREMENT,
  `sol_cod` char(32) CHARACTER SET latin1 NOT NULL,
  `sol_fh_ini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sol_fh_fin` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `per_id` char(20) CHARACTER SET latin1 NOT NULL,
  `efe_id` int(11) DEFAULT NULL,
  `usu_creacion` int(11) NOT NULL,
  `tdo_id` int(11) DEFAULT NULL,
  `afe_id` int(11) DEFAULT NULL,
  `ocu_id` int(11) NOT NULL,
  `usu_cierre` int(11) DEFAULT NULL,
  `mot_id` int(11) DEFAULT '0',
  `motivos_diferimiento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sol_diferido` tinyint(4) DEFAULT NULL,
  `sol_estado` tinyint(1) NOT NULL,
  `cph` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `plaqueta` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`sol_id`),
  KEY `per_id` (`per_id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for stock
-- ----------------------------
DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `efector_id` int(11) DEFAULT NULL,
  `donacion_id` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_descarte` datetime DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fecha_vencimiento` datetime DEFAULT NULL,
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `usu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `donacion_id` (`donacion_id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `hemocomponente_id` (`hemocomponente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for stock_descartes
-- ----------------------------
DROP TABLE IF EXISTS `stock_descartes`;
CREATE TABLE `stock_descartes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `efector_id` int(11) DEFAULT NULL,
  `donacion_id` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_descarte` datetime DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fecha_vencimiento` datetime DEFAULT NULL,
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `donacion_id` (`donacion_id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `hemocomponente_id` (`hemocomponente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for stock_descartes_renamed_y_no_se_x_cual
-- ----------------------------
DROP TABLE IF EXISTS `stock_descartes_renamed_y_no_se_x_cual`;
CREATE TABLE `stock_descartes_renamed_y_no_se_x_cual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT '0',
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `fecha_consolidacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `fecha_consolidacion` (`fecha_consolidacion`),
  KEY `fecha` (`fecha`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for stock_insumos_movimientos
-- ----------------------------
DROP TABLE IF EXISTS `stock_insumos_movimientos`;
CREATE TABLE `stock_insumos_movimientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `tipomovimientoajuste_id` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(16,2) DEFAULT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  `fecha_consolidado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`),
  KEY `pedido_id` (`pedido_id`),
  KEY `tipomovimientoajuste_id` (`tipomovimientoajuste_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for stock_insumos_movimientos_consolidados
-- ----------------------------
DROP TABLE IF EXISTS `stock_insumos_movimientos_consolidados`;
CREATE TABLE `stock_insumos_movimientos_consolidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for stock_movimientos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `stock_movimientos_detalle`;
CREATE TABLE `stock_movimientos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `don_id` int(11) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `abo` varchar(5) DEFAULT NULL,
  `rh` varchar(1) DEFAULT NULL,
  `irradiado` int(1) DEFAULT NULL,
  `cantidad` decimal(10,3) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `observaciones` text,
  PRIMARY KEY (`id`),
  KEY `don_id` (`don_id`),
  KEY `stock_id` (`stock_id`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `fecha` (`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stock_solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `stock_solicitudes`;
CREATE TABLE `stock_solicitudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(4) DEFAULT NULL COMMENT '1 = Pendiente (pedido/solicitud abierta)\r\n2 = Comprometida por un envio (respuesta a una solicitud)\r\n3 = Pendiente Recepción  (respondida con un cantidad y pendiente de ingreso)\r\n4 = Cerrada   (cerrada / finalizada)\r\n5 = Descartada',
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `responde_efector_id` int(11) DEFAULT NULL,
  `responde_fecha` datetime DEFAULT NULL,
  `responde_usuario_id` int(11) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) DEFAULT NULL,
  `factor` varchar(1) DEFAULT NULL,
  `grupo_inverso` varchar(5) DEFAULT NULL,
  `irradiado` int(1) DEFAULT NULL,
  `fenotipo` varchar(50) DEFAULT NULL,
  `prioridad` int(4) DEFAULT NULL COMMENT '1 Baja, 2 Media, 3 Alta, 4 Urgente',
  `cantidad_solicitada` decimal(10,3) DEFAULT NULL,
  `cantidad_entregada` decimal(10,3) DEFAULT NULL,
  `observaciones` text,
  `donacion_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `cierre_usuario_id` int(11) DEFAULT NULL,
  `cierre_fecha` datetime DEFAULT NULL,
  `cierre_observaciones` text,
  PRIMARY KEY (`id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha_vencimiento` (`fecha_vencimiento`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `prioridad` (`prioridad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tdo_tipos_donaciones
-- ----------------------------
DROP TABLE IF EXISTS `tdo_tipos_donaciones`;
CREATE TABLE `tdo_tipos_donaciones` (
  `tdo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tdo_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tdo_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tdo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for tipos_movimentos_stock
-- ----------------------------
DROP TABLE IF EXISTS `tipos_movimentos_stock`;
CREATE TABLE `tipos_movimentos_stock` (
  `tipo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tipo_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for ____stock_insumos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `____stock_insumos_detalle`;
CREATE TABLE `____stock_insumos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insumo_id` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `proucto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `insumos_id` (`insumo_id`) USING BTREE,
  KEY `productos_id` (`proucto_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
