/*
Navicat MySQL Data Transfer

Source Server         : MySQL-locahost
Source Server Version : 50624
Source Host           : 127.0.0.1:3306
Source Database       : dsalud_banco

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-09-06 11:50:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for afe_tipos_aferesis
-- ----------------------------
DROP TABLE IF EXISTS `afe_tipos_aferesis`;
CREATE TABLE `afe_tipos_aferesis` (
  `afe_id` int(11) NOT NULL AUTO_INCREMENT,
  `afe_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `afe_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`afe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of afe_tipos_aferesis
-- ----------------------------
INSERT INTO `afe_tipos_aferesis` VALUES ('1', 'Auto1', '0');
INSERT INTO `afe_tipos_aferesis` VALUES ('2', 'Hema Aferesis', '0');
INSERT INTO `afe_tipos_aferesis` VALUES ('3', 'Leucoaféresis', '1');
INSERT INTO `afe_tipos_aferesis` VALUES ('4', 'Plaquetoaféresis', '1');
INSERT INTO `afe_tipos_aferesis` VALUES ('5', 'Plasmaféresis', '1');
INSERT INTO `afe_tipos_aferesis` VALUES ('6', 'Eritroaféresis', '1');

-- ----------------------------
-- Table structure for analisis
-- ----------------------------
DROP TABLE IF EXISTS `analisis`;
CREATE TABLE `analisis` (
  `ana_id` int(11) NOT NULL AUTO_INCREMENT,
  `enf_id` int(11) NOT NULL,
  `cod` varchar(10) CHARACTER SET latin1 NOT NULL,
  `def` int(11) NOT NULL,
  `descripcion` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ana_id`),
  KEY `enf_id` (`enf_id`),
  CONSTRAINT `analisis_ibfk_1` FOREIGN KEY (`enf_id`) REFERENCES `enfermedades` (`enf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of analisis
-- ----------------------------
INSERT INTO `analisis` VALUES ('2', '1', '371T', '0', 'T.P.H.A. (T.PALIDUM)', '1');
INSERT INTO `analisis` VALUES ('4', '2', '494', '0', 'HUDDLESSON REACCION', '1');
INSERT INTO `analisis` VALUES ('5', '2', '960', '0', 'BRUCELOSIS: WRIGHT REACCION C/2ME', '1');
INSERT INTO `analisis` VALUES ('6', '2', '961', '0', 'BRUCELOSIS: WRIGHT REACCION S/2ME', '1');
INSERT INTO `analisis` VALUES ('7', '3', '243Q', '1', 'CHAGAS: Ac (QL)', '1');
INSERT INTO `analisis` VALUES ('8', '3', '243E', '1', 'CHAGAS: Ac. (ELISA)', '1');
INSERT INTO `analisis` VALUES ('9', '4', '503', '1', 'HEPATITIS B: Ag.SUP. (HBsAg)', '1');
INSERT INTO `analisis` VALUES ('10', '4', '087', '1', 'HEPATITIS B: Ac. ANTI-CORE IgG', '1');
INSERT INTO `analisis` VALUES ('11', '5', '495D', '1', 'HEPATITIS C: DUO Ag y Ac.', '1');
INSERT INTO `analisis` VALUES ('12', '6', '063', '1', 'HIV: Ag-Ac (ELISA)', '1');
INSERT INTO `analisis` VALUES ('13', '6', '063Q', '1', 'HIV: Ag-Ac (QL)', '1');
INSERT INTO `analisis` VALUES ('14', '7', '063HTL', '1', 'HTLV I y II: Ac. (ELISA)', '1');

-- ----------------------------
-- Table structure for bolsas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas`;
CREATE TABLE `bolsas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_bolsa` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dias` int(11) NOT NULL,
  `activo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bolsas
-- ----------------------------
INSERT INTO `bolsas` VALUES ('1', 'ACD/CPD', '21', '1');
INSERT INTO `bolsas` VALUES ('2', 'CPD-A', '35', '1');
INSERT INTO `bolsas` VALUES ('3', 'CPDA-1', '35', '1');
INSERT INTO `bolsas` VALUES ('4', 'SAG Manitol', '42', '1');

-- ----------------------------
-- Table structure for bolsas_marcas
-- ----------------------------
DROP TABLE IF EXISTS `bolsas_marcas`;
CREATE TABLE `bolsas_marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsa_marca` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bolsas_marcas
-- ----------------------------
INSERT INTO `bolsas_marcas` VALUES ('1', 'Rivero');
INSERT INTO `bolsas_marcas` VALUES ('2', 'Baxter');
INSERT INTO `bolsas_marcas` VALUES ('3', 'Terumo');
INSERT INTO `bolsas_marcas` VALUES ('4', 'Grifols');
INSERT INTO `bolsas_marcas` VALUES ('5', 'Troge');

-- ----------------------------
-- Table structure for bolsas_tipos
-- ----------------------------
DROP TABLE IF EXISTS `bolsas_tipos`;
CREATE TABLE `bolsas_tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bolsa_tipo` varchar(50) DEFAULT NULL,
  `peso_minimo` decimal(5,3) DEFAULT NULL,
  `peso_maximo` decimal(5,3) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bolsas_tipos
-- ----------------------------
INSERT INTO `bolsas_tipos` VALUES ('1', 'Simple', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('2', 'Doble', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('3', 'Triple', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('4', 'Cuadruple', '0.450', '0.550', 'Bolsa especial');
INSERT INTO `bolsas_tipos` VALUES ('5', 'Quintuple', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('6', 'PRP', null, null, null);
INSERT INTO `bolsas_tipos` VALUES ('7', 'Descartable Aferesis', null, null, null);

-- ----------------------------
-- Table structure for centralizados
-- ----------------------------
DROP TABLE IF EXISTS `centralizados`;
CREATE TABLE `centralizados` (
  `don_id` int(11) NOT NULL AUTO_INCREMENT,
  `nroLote` int(11) NOT NULL,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) DEFAULT NULL,
  `per_dni` char(20) DEFAULT NULL,
  `don_nro` char(32) NOT NULL,
  `don_fh_inicio` datetime DEFAULT NULL,
  `usu_inicio` int(11) NOT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  `lote_estado` int(11) NOT NULL,
  PRIMARY KEY (`don_id`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of centralizados
-- ----------------------------
INSERT INTO `centralizados` VALUES ('110', '4', '184', '104', null, 'HPRC15000001', '2015-11-17 08:56:30', '1', null, '2', '1');
INSERT INTO `centralizados` VALUES ('111', '4', '184', '105', null, 'HPRC15000002', '2015-11-17 10:39:43', '1', null, '2', '1');
INSERT INTO `centralizados` VALUES ('112', '4', '184', '107', null, 'HPRC15000003', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('113', '4', '184', '108', null, 'HPRC15000004', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('114', '4', '184', '110', null, 'HPRC15000005', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('115', '4', '184', '111', null, 'HPRC15000006', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('116', '4', '184', '112', null, 'HPRC15000007', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('117', '4', '184', '113', null, 'HPRC15000008', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('118', '4', '184', '114', null, 'HPRC15000009', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('119', '4', '184', '115', null, 'HPRC15000010', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('120', '4', '184', '116', null, 'HPRC15000011', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('121', '4', '184', '117', null, 'HPRC15000012', '2015-01-01 13:00:00', '1', '2015-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('122', '4', '184', '118', null, 'HPRC15000013', '2015-01-31 01:00:00', '1', '2015-01-31 01:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('123', '4', '184', '119', null, 'HPRC15000014', '2015-01-31 01:00:00', '1', '2015-01-31 01:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('124', '4', '184', '120', null, 'HPRC15000015', '2015-01-31 01:00:00', '1', '2015-01-31 01:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('125', '4', '184', '125', null, 'HPRC15000016', '2015-11-23 08:38:16', '1', null, '2', '1');
INSERT INTO `centralizados` VALUES ('126', '4', '184', '134', null, 'HPRC15000017', '2016-01-01 13:00:00', '1', '2016-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('127', '4', '184', '129', null, 'HPRC15000018', '2015-11-26 08:06:53', '1', null, '2', '1');
INSERT INTO `centralizados` VALUES ('128', '4', '184', '131', null, 'HPRC15000019', '2016-01-01 13:00:00', '1', '2016-01-01 13:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('129', '4', '184', null, null, '', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('130', '4', '184', '137', null, 'HPRC15000021', '2016-03-28 11:45:17', '1', null, '2', '1');
INSERT INTO `centralizados` VALUES ('131', '4', '184', null, null, 'HPRC15000022', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('132', '4', '184', null, null, 'HPRC15000023', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('133', '4', '184', null, null, 'HPRC15000024', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('134', '4', '184', null, null, 'HPRC15000025', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('135', '4', '184', null, null, 'HPRC15000026', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('136', '4', '184', '135', null, 'HPRC15000027', '2016-12-31 12:59:00', '1', '2016-12-31 12:59:00', '2', '1');
INSERT INTO `centralizados` VALUES ('137', '4', '184', null, null, 'HPRC15000028', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('138', '4', '184', null, null, 'HPRC15000029', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('139', '4', '184', null, null, 'HPRC15000030', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('140', '4', '184', null, null, 'HPRC15000031', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('141', '4', '184', null, null, 'HPRC15000032', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('142', '4', '184', null, null, 'HPRC15000033', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('143', '4', '184', null, null, 'HPRC15000034', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('144', '4', '184', null, null, 'HPRC15000035', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('145', '4', '184', null, null, 'HPRC15000036', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('146', '4', '184', null, null, 'HPRC15000037', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('147', '4', '184', null, null, 'HPRC15000038', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('148', '4', '184', null, null, 'HPRC15000039', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('149', '4', '184', null, null, 'HPRC15000040', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('150', '4', '184', null, null, 'HPRC15000041', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('151', '4', '184', null, null, 'HPRC15000042', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('152', '4', '184', null, null, 'HPRC15000043', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('153', '4', '184', null, null, 'HPRC15000044', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('154', '4', '184', null, null, 'HPRC15000045', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('155', '4', '184', null, null, 'HPRC15000046', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('156', '4', '184', null, null, 'HPRC15000047', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('157', '4', '184', null, null, 'HPRC15000048', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('158', '4', '184', null, null, 'HPRC15000049', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('159', '4', '184', '106', null, 'HPRC15000050', '2015-11-15 12:00:00', '1', '2015-11-15 12:00:00', '2', '1');
INSERT INTO `centralizados` VALUES ('160', '5', '785', '140', null, 'BDSC15000001', '2016-12-22 11:42:09', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('161', '5', '785', '141', null, 'BDSC15000002', '2017-06-09 08:49:21', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('162', '5', '785', null, null, 'BDSC15000003', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('163', '5', '785', null, null, 'BDSC15000004', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('164', '5', '785', null, null, 'BDSC15000005', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('165', '5', '785', null, null, 'BDSC15000006', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('166', '5', '785', null, null, 'BDSC15000007', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('167', '5', '785', null, null, 'BDSC15000008', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('168', '6', '785', null, null, 'BDSC15000009', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('169', '6', '785', null, null, 'BDSC15000010', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('170', '6', '785', null, null, 'BDSC15000011', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('171', '6', '785', '128', null, 'BDSC15000012', '2016-10-31 16:16:54', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('172', '6', '785', null, null, 'BDSC15000013', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('173', '6', '785', null, null, 'BDSC15000014', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('174', '6', '785', null, null, 'BDSC15000015', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('175', '6', '785', null, null, 'BDSC15000016', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('176', '6', '785', null, null, 'BDSC15000017', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('177', '6', '785', null, null, 'BDSC15000018', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('178', '7', '785', '122', null, 'BDSC15000019', '2015-11-20 11:25:30', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('179', '7', '785', '123', null, 'BDSC15000020', '2015-11-20 11:27:02', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('180', '7', '785', null, null, 'BDSC15000021', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('181', '7', '785', null, null, 'BDSC15000022', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('182', '7', '785', null, null, 'BDSC15000023', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('183', '7', '785', null, null, 'BDSC15000024', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('184', '7', '785', '128', null, 'BDSC15000025', '2016-10-31 16:02:12', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('185', '7', '785', '88', null, 'BDSC15000026', '2016-10-31 15:56:54', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('186', '7', '785', null, null, 'BDSC15000027', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('187', '7', '785', null, null, 'BDSC15000028', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('188', '8', '785', '126', null, 'BDSC15000029', '2015-11-23 12:37:53', '85', null, '2', '1');
INSERT INTO `centralizados` VALUES ('189', '9', '184', null, null, 'HPRC15000051', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('190', '9', '184', null, null, 'HPRC15000052', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('191', '10', '167', null, null, 'HEEC15000001', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('192', '10', '167', null, null, 'HEEC15000002', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('193', '11', '167', null, null, 'HEEC15000003', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('194', '11', '167', null, null, 'HEEC15000004', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('195', '12', '167', null, null, 'HEEC15000005', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('196', '12', '167', null, null, 'HEEC15000006', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('197', '13', '167', null, null, 'HEEC15000007', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('198', '13', '167', null, null, 'HEEC15000008', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('199', '14', '167', null, null, 'HEEC15000009', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('200', '14', '167', null, null, 'HEEC15000010', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('201', '15', '167', null, null, 'HEEC15000011', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('202', '15', '167', null, null, 'HEEC15000012', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('203', '16', '167', null, null, 'HEEC15000013', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('204', '16', '167', null, null, 'HEEC15000014', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('205', '16', '167', null, null, 'HEEC15000015', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('206', '16', '167', null, null, 'HEEC15000016', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('207', '16', '167', null, null, 'HEEC15000017', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('208', '16', '167', null, null, 'HEEC15000018', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('209', '16', '167', null, null, 'HEEC15000019', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('210', '16', '167', null, null, 'HEEC15000020', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('211', '16', '167', null, null, 'HEEC15000021', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('212', '16', '167', null, null, 'HEEC15000022', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('213', '17', '167', null, null, 'HEEC15000023', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('214', '17', '167', null, null, 'HEEC15000024', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('215', '17', '167', null, null, 'HEEC15000025', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('216', '17', '167', null, null, 'HEEC15000026', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('217', '17', '167', null, null, 'HEEC15000027', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('218', '17', '167', null, null, 'HEEC15000028', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('219', '17', '167', null, null, 'HEEC15000029', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('220', '17', '167', null, null, 'HEEC15000030', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('221', '17', '167', null, null, 'HEEC15000031', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('222', '17', '167', null, null, 'HEEC15000032', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('223', '18', '167', null, null, 'HEEC15000033', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('224', '18', '167', null, null, 'HEEC15000034', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('225', '19', '167', null, null, 'HEEC15000035', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('226', '19', '167', null, null, 'HEEC15000036', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('227', '20', '167', null, null, 'HEEC15000037', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('228', '20', '167', null, null, 'HEEC15000038', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('229', '21', '167', null, null, 'HEEC15000039', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('230', '21', '167', null, null, 'HEEC15000040', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('231', '22', '785', null, null, 'BDSC15000030', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('232', '22', '785', null, null, 'BDSC15000031', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('233', '23', '167', null, null, 'HEEC15000041', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('234', '24', '167', null, null, 'HEEC15000042', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('235', '24', '167', null, null, 'HEEC15000043', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('236', '25', '167', null, null, 'HEEC15000044', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('237', '25', '167', null, null, 'HEEC15000045', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('238', '26', '167', null, null, 'HEEC15000046', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('239', '26', '167', null, null, 'HEEC15000047', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('240', '27', '167', null, null, 'HEEC15000048', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('241', '27', '167', null, null, 'HEEC15000049', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('242', '28', '167', null, null, 'HEEC15000050', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('243', '28', '167', null, null, 'HEEC15000051', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('244', '29', '167', null, null, 'HEEC15000052', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('245', '29', '167', null, null, 'HEEC15000053', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('246', '30', '167', null, null, 'HEEC15000054', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('247', '30', '167', null, null, 'HEEC15000055', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('248', '31', '167', null, null, 'HEEC15000056', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('249', '31', '167', null, null, 'HEEC15000057', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('250', '32', '167', null, null, 'HEEC15000058', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('251', '32', '167', null, null, 'HEEC15000059', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('252', '33', '167', null, null, 'HEEC15000060', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('253', '33', '167', null, null, 'HEEC15000061', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('254', '34', '167', null, null, 'HEEC15000062', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('255', '34', '167', null, null, 'HEEC15000063', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('256', '35', '167', null, null, 'HEEC15000064', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('257', '35', '167', null, null, 'HEEC15000065', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('258', '36', '167', null, null, 'HEEC15000066', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('259', '36', '167', null, null, 'HEEC15000067', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('260', '37', '167', null, null, 'HEEC15000068', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('261', '37', '167', null, null, 'HEEC15000069', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('262', '37', '167', null, null, 'HEEC15000070', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('263', '38', '167', null, null, 'HEEC15000071', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('264', '38', '167', null, null, 'HEEC15000072', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('265', '39', '785', null, null, 'BDSC15000032', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('266', '39', '785', null, null, 'BDSC15000033', null, '85', null, '1', '1');
INSERT INTO `centralizados` VALUES ('267', '40', '167', null, null, 'HEEC15000073', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('268', '41', '167', null, null, 'HEEC15000074', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('269', '42', '167', null, null, 'HEEC16000001', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('270', '42', '167', null, null, 'HEEC16000002', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('271', '43', '184', '130', null, 'HPRC16000001', '2016-01-22 09:09:04', '1', null, '2', '1');
INSERT INTO `centralizados` VALUES ('272', '44', '167', null, null, 'HEEC16000003', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('273', '45', '167', null, null, 'HEEC16000004', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('274', '46', '167', null, null, 'HEEC16000005', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('275', '47', '167', null, null, 'HEEC16000006', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('276', '47', '167', null, null, 'HEEC16000007', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('277', '48', '167', null, null, 'HEEC16000008', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('278', '48', '167', null, null, 'HEEC16000009', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('279', '49', '167', null, null, 'HEEC16000010', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('280', '50', '167', null, null, 'HEEC16000011', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('281', '50', '167', null, null, 'HEEC16000012', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('282', '51', '167', null, null, 'HEEC16000013', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('283', '52', '167', null, null, 'HEEC16000014', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('284', '53', '167', null, null, 'HEEC16000015', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('285', '54', '167', null, null, 'HEEC16000016', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('286', '54', '167', null, null, 'HEEC16000017', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('287', '55', '167', null, null, 'HEEC16000018', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('288', '56', '167', null, null, 'HEEC16000019', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('289', '57', '167', null, null, 'HEEC16000020', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('290', '57', '167', null, null, 'HEEC16000021', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('291', '57', '167', null, null, 'HEEC16000022', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('292', '57', '167', null, null, 'HEEC16000023', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('293', '57', '167', null, null, 'HEEC16000024', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('294', '57', '167', null, null, 'HEEC16000025', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('295', '57', '167', null, null, 'HEEC16000026', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('296', '57', '167', null, null, 'HEEC16000027', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('297', '57', '167', null, null, 'HEEC16000028', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('298', '57', '167', null, null, 'HEEC16000029', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('299', '58', '167', null, null, 'HEEC16000030', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('300', '59', '167', null, null, 'HEEC16000031', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('301', '59', '167', null, null, 'HEEC16000032', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('302', '60', '167', null, null, 'HEEC16000033', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('303', '61', '167', null, null, 'HEEC16000034', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('304', '62', '167', null, null, 'HEEC16000035', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('305', '63', '167', null, null, 'HEEC16000036', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('306', '64', '167', null, null, 'HEEC16000037', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('307', '65', '167', null, null, 'HEEC16000038', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('308', '66', '167', null, null, 'HEEC16000039', null, '1', null, '1', '1');
INSERT INTO `centralizados` VALUES ('309', '66', '167', null, null, 'HEEC16000040', null, '1', null, '1', '1');

-- ----------------------------
-- Table structure for codigoscentralizados
-- ----------------------------
DROP TABLE IF EXISTS `codigoscentralizados`;
CREATE TABLE `codigoscentralizados` (
  `don_id` int(11) NOT NULL,
  `nroLote` int(11) NOT NULL,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) DEFAULT NULL,
  `per_dni` char(20) DEFAULT NULL,
  `don_nro` char(32) NOT NULL,
  `don_fh_inicio` datetime NOT NULL,
  `usu_inicio` int(11) NOT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  `lote_estado` int(11) NOT NULL,
  PRIMARY KEY (`don_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of codigoscentralizados
-- ----------------------------

-- ----------------------------
-- Table structure for contingencia
-- ----------------------------
DROP TABLE IF EXISTS `contingencia`;
CREATE TABLE `contingencia` (
  `don_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) DEFAULT NULL,
  `per_dni` char(20) CHARACTER SET latin1 DEFAULT NULL,
  `don_nro` char(32) CHARACTER SET latin1 NOT NULL,
  `don_fh_inicio` datetime NOT NULL,
  `usu_inicio` int(11) NOT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  PRIMARY KEY (`don_id`),
  KEY `idefector` (`efe_id`),
  KEY `finicio` (`don_fh_inicio`),
  KEY `estado` (`don_estado`),
  KEY `nro` (`don_nro`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contingencia
-- ----------------------------
INSERT INTO `contingencia` VALUES ('1', '1249', null, null, '---E16000001', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('2', '1249', null, null, '---E16000002', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('3', '1249', null, null, '---E16000003', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('4', '1249', null, null, '---E16000004', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('5', '1249', null, null, '---E16000005', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('6', '1249', null, null, '---E16000006', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('7', '1249', null, null, '---E16000007', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');
INSERT INTO `contingencia` VALUES ('8', '1249', null, null, '---E16000008', '2016-03-23 11:49:21', '90', '2016-03-23 11:49:21', '1');

-- ----------------------------
-- Table structure for detalles_preproduccion
-- ----------------------------
DROP TABLE IF EXISTS `detalles_preproduccion`;
CREATE TABLE `detalles_preproduccion` (
  `dpr_id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `hem_id` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `tipo_prod` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dpr_id`),
  KEY `iddonacion` (`don_id`),
  KEY `idhemoc` (`hem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of detalles_preproduccion
-- ----------------------------
INSERT INTO `detalles_preproduccion` VALUES ('1', '35', '3', '1', '1', '2015-11-17 08:57:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('2', '35', '5', '1', '1', '2015-11-17 08:57:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('3', '35', '2', '1', '1', '2015-11-17 08:57:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('4', '35', '1', '1', '1', '2015-11-17 08:57:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('5', '35', '6', '1', '1', '2015-11-17 08:57:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('6', '35', '4', '1', '1', '2015-11-17 08:57:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('7', '48', '3', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('8', '48', '5', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('9', '48', '2', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('10', '48', '1', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('11', '48', '6', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('12', '48', '4', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('13', '53', '3', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('14', '53', '5', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('15', '53', '2', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('16', '53', '1', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('17', '53', '6', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('18', '53', '4', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('19', '54', '3', '1', '1', '2015-11-17 13:11:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('20', '54', '5', '1', '1', '2015-11-17 13:11:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('21', '54', '2', '1', '1', '2015-11-17 13:11:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('22', '54', '1', '1', '1', '2015-11-17 13:11:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('23', '54', '6', '1', '1', '2015-11-17 13:11:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('24', '54', '4', '1', '1', '2015-11-17 13:11:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('25', '55', '3', '1', '1', '2015-11-17 13:13:09', '1');
INSERT INTO `detalles_preproduccion` VALUES ('26', '55', '5', '1', '1', '2015-11-17 13:13:09', '1');
INSERT INTO `detalles_preproduccion` VALUES ('27', '55', '2', '1', '1', '2015-11-17 13:13:09', '1');
INSERT INTO `detalles_preproduccion` VALUES ('28', '55', '1', '1', '1', '2015-11-17 13:13:09', '1');
INSERT INTO `detalles_preproduccion` VALUES ('29', '55', '6', '1', '1', '2015-11-17 13:13:09', '1');
INSERT INTO `detalles_preproduccion` VALUES ('30', '55', '4', '1', '1', '2015-11-17 13:13:09', '1');
INSERT INTO `detalles_preproduccion` VALUES ('31', '56', '3', '1', '1', '2015-11-17 13:15:04', '1');
INSERT INTO `detalles_preproduccion` VALUES ('32', '56', '5', '1', '1', '2015-11-17 13:15:04', '1');
INSERT INTO `detalles_preproduccion` VALUES ('33', '56', '2', '1', '1', '2015-11-17 13:15:04', '1');
INSERT INTO `detalles_preproduccion` VALUES ('34', '56', '1', '1', '1', '2015-11-17 13:15:04', '1');
INSERT INTO `detalles_preproduccion` VALUES ('35', '56', '6', '1', '1', '2015-11-17 13:15:04', '1');
INSERT INTO `detalles_preproduccion` VALUES ('36', '56', '4', '1', '1', '2015-11-17 13:15:04', '1');
INSERT INTO `detalles_preproduccion` VALUES ('37', '57', '3', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('38', '57', '5', '2', '1', '2016-03-14 08:40:05', '1');
INSERT INTO `detalles_preproduccion` VALUES ('39', '57', '2', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('40', '57', '1', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('41', '57', '6', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('42', '57', '4', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('43', '58', '3', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('44', '58', '5', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('45', '58', '2', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('46', '58', '1', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('47', '58', '6', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('48', '58', '4', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('49', '59', '3', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('50', '59', '5', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('51', '59', '2', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('52', '59', '1', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('53', '59', '6', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('54', '59', '4', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('55', '60', '3', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('56', '60', '5', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('57', '60', '2', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('58', '60', '1', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('59', '60', '6', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('60', '60', '4', '2', '1', '2016-03-14 08:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('61', '62', '3', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('62', '62', '5', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('63', '62', '2', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('64', '62', '1', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('65', '62', '6', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('66', '62', '4', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('67', '49', '3', '1', '85', '2015-11-23 12:36:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('68', '49', '5', '1', '85', '2015-11-23 12:36:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('69', '49', '2', '1', '85', '2015-11-23 12:36:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('70', '49', '1', '1', '85', '2015-11-23 12:36:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('71', '49', '6', '1', '85', '2015-11-23 12:36:47', '1');
INSERT INTO `detalles_preproduccion` VALUES ('72', '49', '4', '1', '85', '2015-11-23 12:36:48', '1');
INSERT INTO `detalles_preproduccion` VALUES ('73', '65', '3', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('74', '65', '5', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('75', '65', '2', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('76', '65', '1', '2', '85', '2015-11-23 12:40:06', '1');
INSERT INTO `detalles_preproduccion` VALUES ('77', '65', '6', '2', '85', '2015-11-23 12:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('78', '65', '4', '2', '85', '2015-11-23 12:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('79', '66', '3', '2', '1', '2015-11-26 08:16:27', '2');
INSERT INTO `detalles_preproduccion` VALUES ('80', '66', '5', '1', '1', '2015-11-26 08:16:27', '2');
INSERT INTO `detalles_preproduccion` VALUES ('81', '66', '2', '1', '1', '2015-11-26 08:16:27', '2');
INSERT INTO `detalles_preproduccion` VALUES ('82', '66', '1', '1', '1', '2015-11-26 08:16:27', '2');
INSERT INTO `detalles_preproduccion` VALUES ('83', '66', '6', '1', '1', '2015-11-26 08:16:27', '2');
INSERT INTO `detalles_preproduccion` VALUES ('84', '66', '4', '1', '1', '2015-11-26 08:16:27', '2');
INSERT INTO `detalles_preproduccion` VALUES ('85', '68', '3', '2', '1', '2016-03-14 08:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('86', '68', '5', '2', '1', '2016-03-14 08:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('87', '68', '2', '2', '1', '2016-03-14 08:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('88', '68', '1', '2', '1', '2016-03-14 08:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('89', '68', '6', '2', '1', '2016-03-14 08:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('90', '68', '4', '2', '1', '2016-03-14 08:40:07', '1');
INSERT INTO `detalles_preproduccion` VALUES ('91', '51', '3', '1', '1', '2016-02-15 09:38:54', '1');
INSERT INTO `detalles_preproduccion` VALUES ('92', '51', '5', '1', '1', '2016-02-15 09:38:54', '1');
INSERT INTO `detalles_preproduccion` VALUES ('93', '51', '2', '1', '1', '2016-02-15 09:38:54', '1');
INSERT INTO `detalles_preproduccion` VALUES ('94', '51', '1', '1', '1', '2016-02-15 09:38:54', '1');
INSERT INTO `detalles_preproduccion` VALUES ('95', '51', '6', '1', '1', '2016-02-15 09:38:54', '1');
INSERT INTO `detalles_preproduccion` VALUES ('96', '51', '4', '1', '1', '2016-02-15 09:38:54', '1');
INSERT INTO `detalles_preproduccion` VALUES ('97', '69', '3', '2', '1', '2016-03-14 11:15:46', '2');
INSERT INTO `detalles_preproduccion` VALUES ('98', '69', '5', '2', '1', '2016-03-14 10:51:26', '1');
INSERT INTO `detalles_preproduccion` VALUES ('99', '69', '2', '2', '1', '2016-03-14 10:51:27', '1');
INSERT INTO `detalles_preproduccion` VALUES ('100', '69', '1', '2', '1', '2016-03-14 10:51:27', '1');
INSERT INTO `detalles_preproduccion` VALUES ('101', '69', '6', '2', '1', '2016-03-14 10:51:27', '1');
INSERT INTO `detalles_preproduccion` VALUES ('102', '69', '4', '2', '1', '2016-03-14 10:51:27', '1');
INSERT INTO `detalles_preproduccion` VALUES ('103', '70', '3', '1', '1', '2016-03-14 11:06:19', '2');
INSERT INTO `detalles_preproduccion` VALUES ('104', '70', '5', '1', '1', '2016-03-14 11:06:19', '2');
INSERT INTO `detalles_preproduccion` VALUES ('105', '70', '2', '1', '1', '2016-03-14 11:06:19', '2');
INSERT INTO `detalles_preproduccion` VALUES ('106', '70', '1', '2', '1', '2016-03-14 11:06:19', '2');
INSERT INTO `detalles_preproduccion` VALUES ('107', '70', '6', '1', '1', '2016-03-14 11:06:19', '2');
INSERT INTO `detalles_preproduccion` VALUES ('108', '70', '4', '1', '1', '2016-03-14 11:06:19', '2');

-- ----------------------------
-- Table structure for detalles_produccion
-- ----------------------------
DROP TABLE IF EXISTS `detalles_produccion`;
CREATE TABLE `detalles_produccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iddonacion` int(11) NOT NULL,
  `idhemocomponente` int(11) NOT NULL,
  `nrobolsa` int(11) DEFAULT NULL,
  `fini_prod` datetime NOT NULL,
  `ffin_prod` datetime DEFAULT NULL,
  `f_vencimiento` datetime DEFAULT NULL,
  `estado` int(1) NOT NULL,
  `detalle` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `validado` int(11) DEFAULT NULL,
  `peso` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `hem_temp` int(11) DEFAULT NULL,
  `usu_id_valida` int(11) DEFAULT NULL,
  `env_prod_id` int(11) DEFAULT NULL,
  `irradiado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of detalles_produccion
-- ----------------------------
INSERT INTO `detalles_produccion` VALUES ('1', '62', '3', null, '2015-11-23 12:40:06', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('2', '62', '2', null, '2015-11-23 12:40:06', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('3', '62', '1', null, '2015-11-23 12:40:06', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('4', '65', '3', null, '2015-11-23 12:40:06', '2015-11-23 12:40:53', '2015-12-28 12:40:53', '2', '', '1', '280', '85', '0', '85', null, null);
INSERT INTO `detalles_produccion` VALUES ('5', '65', '2', null, '2015-11-23 12:40:06', '2015-11-23 12:40:32', '2015-11-28 12:40:32', '2', '', '1', '70', '85', '0', '85', null, null);
INSERT INTO `detalles_produccion` VALUES ('6', '65', '1', null, '2015-11-23 12:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('7', '66', '3', null, '2015-11-26 08:16:27', '2016-02-18 11:46:55', '2016-03-24 11:55:11', '4', '', '1', '200', '1', '0', '1', '1', null);
INSERT INTO `detalles_produccion` VALUES ('8', '48', '3', null, '2016-03-14 08:40:07', '2016-03-14 10:33:49', '2016-04-18 10:43:11', '3', '2131', null, '200', '1', '0', null, null, null);
INSERT INTO `detalles_produccion` VALUES ('9', '48', '5', null, '2016-03-14 08:40:07', null, null, '3', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('10', '48', '6', null, '2016-03-14 08:40:07', null, null, '3', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('11', '53', '3', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('12', '53', '5', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('13', '53', '6', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('14', '57', '3', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('15', '57', '5', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('16', '57', '6', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('17', '58', '3', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('18', '58', '5', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('19', '58', '6', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('20', '59', '3', null, '2016-03-14 08:40:07', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('21', '59', '5', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('22', '59', '6', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('23', '60', '3', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('24', '60', '5', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('25', '60', '6', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('26', '68', '3', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('27', '68', '5', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('28', '68', '6', null, '2016-03-14 08:40:08', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('30', '69', '5', null, '2016-03-14 10:51:27', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('31', '69', '6', null, '2016-03-14 10:51:27', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('32', '70', '1', null, '2016-03-14 11:06:19', null, null, '1', null, null, null, null, null, null, null, null);
INSERT INTO `detalles_produccion` VALUES ('33', '69', '3', null, '2016-03-14 11:15:46', null, null, '1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for determinaciones
-- ----------------------------
DROP TABLE IF EXISTS `determinaciones`;
CREATE TABLE `determinaciones` (
  `det_id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_ana` varchar(10) CHARACTER SET latin1 NOT NULL,
  `don_id` int(11) NOT NULL,
  `fecha_exp` timestamp NULL DEFAULT NULL,
  `usu_exp_id` int(11) DEFAULT NULL,
  `fecha_imp` timestamp NULL DEFAULT NULL,
  `usu_imp_id` int(11) DEFAULT NULL,
  `resultado` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`det_id`),
  KEY `ana_id` (`cod_ana`),
  KEY `don_id` (`don_id`),
  CONSTRAINT `determinaciones_ibfk_2` FOREIGN KEY (`don_id`) REFERENCES `don_donaciones` (`don_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of determinaciones
-- ----------------------------
INSERT INTO `determinaciones` VALUES ('1', '063', '48', null, null, '2015-11-18 11:09:13', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('2', '063Q', '48', null, null, '2015-11-18 11:09:13', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('3', '087', '48', null, null, '2015-11-18 11:09:13', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('4', '243E', '48', null, null, '2015-11-18 11:09:13', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('5', '243Q', '48', null, null, '2015-11-18 11:09:13', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('6', '495D', '48', null, null, '2015-11-18 11:09:14', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('7', '503', '48', null, null, '2015-11-18 11:09:14', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('8', '063HTL', '48', null, null, '2015-11-18 11:09:14', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('9', '063', '48', null, null, '2015-11-18 11:53:03', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('10', '063Q', '48', null, null, '2015-11-18 11:53:03', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('11', '087', '48', null, null, '2015-11-18 11:53:03', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('12', '243E', '48', null, null, '2015-11-18 11:53:03', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('13', '243Q', '48', null, null, '2015-11-18 11:53:03', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('14', '495D', '48', null, null, '2015-11-18 11:53:04', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('15', '503', '48', null, null, '2015-11-18 11:53:04', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('16', '063HTL', '48', null, null, '2015-11-18 11:53:04', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('17', '063', '48', null, null, '2015-11-18 11:53:50', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('18', '063Q', '48', null, null, '2015-11-18 11:53:50', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('19', '087', '48', null, null, '2015-11-18 11:53:50', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('20', '243E', '48', null, null, '2015-11-18 11:53:50', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('21', '243Q', '48', null, null, '2015-11-18 11:53:50', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('22', '495D', '48', null, null, '2015-11-18 11:53:51', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('23', '503', '48', null, null, '2015-11-18 11:53:51', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('24', '063HTL', '48', null, null, '2015-11-18 11:53:51', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('25', '063', '50', null, null, '2015-11-18 12:07:11', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('26', '063Q', '50', null, null, '2015-11-18 12:07:11', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('27', '087', '50', null, null, '2015-11-18 12:07:11', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('28', '243E', '50', null, null, '2015-11-18 12:07:12', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('29', '243Q', '50', null, null, '2015-11-18 12:07:12', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('30', '495D', '50', null, null, '2015-11-18 12:07:12', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('31', '503', '50', null, null, '2015-11-18 12:07:12', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('32', '063HTL', '50', null, null, '2015-11-18 12:07:12', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('33', '063', '57', null, null, '2015-11-18 12:08:12', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('34', '063Q', '57', null, null, '2015-11-18 12:08:12', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('35', '087', '57', null, null, '2015-11-18 12:08:12', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('36', '243E', '57', null, null, '2015-11-18 12:08:12', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('37', '243Q', '57', null, null, '2015-11-18 12:08:13', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('38', '495D', '57', null, null, '2015-11-18 12:08:13', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('39', '503', '57', null, null, '2015-11-18 12:08:13', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('40', '063HTL', '57', null, null, '2015-11-18 12:08:13', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('41', '243Q', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('42', '243E', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', '', '1');
INSERT INTO `determinaciones` VALUES ('43', '503', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('44', '087', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', '', '1');
INSERT INTO `determinaciones` VALUES ('45', '495D', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('46', '063', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', '', '1');
INSERT INTO `determinaciones` VALUES ('47', '063Q', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:19', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('48', '063HTL', '58', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', '', '1');
INSERT INTO `determinaciones` VALUES ('49', '243Q', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('50', '243E', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('51', '503', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('52', '087', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('53', '495D', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('54', '063', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('55', '063Q', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('56', '063HTL', '59', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:20', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('57', '243Q', '60', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:22', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('58', '243E', '60', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:21', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('59', '503', '60', '2015-11-18 12:22:22', '1', '2015-11-18 13:10:22', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('60', '087', '60', '2015-11-18 12:22:23', '1', '2015-11-18 13:10:21', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('61', '495D', '60', '2015-11-18 12:22:23', '1', '2015-11-18 13:10:22', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('62', '063', '60', '2015-11-18 12:22:23', '1', '2015-11-18 13:10:21', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('63', '063Q', '60', '2015-11-18 12:22:23', '1', '2015-11-18 13:10:21', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('64', '063HTL', '60', '2015-11-18 12:22:23', '1', '2015-11-18 13:10:22', '1', 'NO REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('65', '243Q', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('66', '243E', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('67', '503', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('68', '087', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('69', '495D', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('70', '063', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('71', '063Q', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('72', '063HTL', '62', '2015-11-20 11:41:56', '85', null, null, null, '1');
INSERT INTO `determinaciones` VALUES ('73', '243Q', '49', '2016-02-15 09:39:29', '1', '2016-02-15 09:50:45', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('74', '243E', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('75', '503', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('76', '087', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('77', '495D', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('78', '063', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:44', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('79', '063Q', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:44', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('80', '063HTL', '49', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('81', '243Q', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('82', '243E', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('83', '503', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('84', '087', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('85', '495D', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('86', '063', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('87', '063Q', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:45', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('88', '063HTL', '51', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('89', '243Q', '68', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:47', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('90', '243E', '68', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:47', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('91', '503', '68', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:47', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('92', '087', '68', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('93', '495D', '68', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:47', '1', 'REACTIVO', '1');
INSERT INTO `determinaciones` VALUES ('94', '063', '68', '2016-02-15 09:39:30', '1', '2016-02-15 09:50:46', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('95', '063Q', '68', '2016-02-15 09:39:31', '1', '2016-02-15 09:50:46', '1', 'NR', '1');
INSERT INTO `determinaciones` VALUES ('96', '063HTL', '68', '2016-02-15 09:39:31', '1', '2016-02-15 09:50:47', '1', 'NR', '1');

-- ----------------------------
-- Table structure for det_determinaciones
-- ----------------------------
DROP TABLE IF EXISTS `det_determinaciones`;
CREATE TABLE `det_determinaciones` (
  `det_id` int(11) NOT NULL AUTO_INCREMENT,
  `det_cod` varchar(15) CHARACTER SET latin1 NOT NULL,
  `det_cod_lab` varchar(15) CHARACTER SET latin1 NOT NULL,
  `det_descripcion` varchar(120) CHARACTER SET latin1 NOT NULL,
  `det_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of det_determinaciones
-- ----------------------------

-- ----------------------------
-- Table structure for don_donaciones
-- ----------------------------
DROP TABLE IF EXISTS `don_donaciones`;
CREATE TABLE `don_donaciones` (
  `don_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_id` int(11) NOT NULL,
  `sol_id` int(11) NOT NULL,
  `per_id` char(20) CHARACTER SET latin1 NOT NULL,
  `don_nro` char(32) CHARACTER SET latin1 NOT NULL,
  `don_fh_inicio` datetime NOT NULL,
  `usu_inicio` int(11) NOT NULL,
  `ppr_id` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `don_fh_extraccion` datetime DEFAULT NULL,
  `don_usu_extraccion` int(11) DEFAULT NULL,
  `don_fh_aceptacion` datetime DEFAULT NULL,
  `usu_aceptacion` int(11) DEFAULT NULL,
  `despacho_id` int(11) DEFAULT NULL,
  `mot_id` int(11) DEFAULT NULL,
  `motivos_diferimiento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observaciones` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `don_estado` int(1) NOT NULL,
  `don_tiempo_extraccion` tinyint(4) DEFAULT NULL,
  `don_peso` int(11) DEFAULT NULL,
  `pro_estado` int(11) DEFAULT NULL,
  `recepcion_bolsa` int(3) unsigned DEFAULT NULL,
  `recepcion_muestra` int(3) unsigned DEFAULT NULL,
  `don_fh_recepcion` datetime DEFAULT NULL,
  `don_usu_recepcion` int(11) DEFAULT NULL,
  `don_temp` int(11) DEFAULT NULL,
  `cod_muni` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`don_id`),
  KEY `idefector` (`efe_id`),
  KEY `finicio` (`don_fh_inicio`),
  KEY `estado` (`don_estado`),
  KEY `iddespacho` (`despacho_id`),
  KEY `nro` (`don_nro`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of don_donaciones
-- ----------------------------
INSERT INTO `don_donaciones` VALUES ('48', '184', '105', '4', 'HPRC15000002', '2015-11-17 10:39:43', '1', '', '2015-11-17 10:39:48', '1', '2015-11-17 10:39:43', '1', '28', null, null, 'No se observaron complicaciones.', '10', '10', '600', '1', '1', '1', '2015-11-17 10:41:59', '1', '24', null);
INSERT INTO `don_donaciones` VALUES ('49', '184', '106', '1', 'HPRC15000050', '2015-11-15 12:00:00', '1', '', '2015-11-17 12:06:01', '1', '2015-11-17 12:06:01', '1', '29', null, null, null, '10', '10', '650', '1', '1', '1', '2015-11-23 12:36:47', '85', '20', '');
INSERT INTO `don_donaciones` VALUES ('50', '184', '107', '4', 'HPRC15000003', '2015-01-01 13:00:00', '1', '', '2015-11-17 12:42:32', '1', '2015-11-17 12:42:32', '1', '30', null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-17 12:42:32', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('51', '184', '111', '4', 'HPRC15000006', '2015-01-01 13:00:00', '1', '', '2015-11-17 12:57:49', '1', '2015-11-17 12:57:49', '1', '31', null, null, null, '10', '10', '300', '1', '1', '1', '2016-02-15 09:38:54', '1', '1', '');
INSERT INTO `don_donaciones` VALUES ('52', '184', '112', '4', 'HPRC15000007', '2015-01-01 13:00:00', '1', '', '2015-11-17 12:58:15', '1', '2015-11-17 12:58:15', '1', '32', null, null, null, '10', '10', '120', '0', '1', '1', '2015-11-17 12:58:15', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('53', '184', '113', '4', 'HPRC15000008', '2015-01-01 13:00:00', '1', '', '2015-11-17 13:07:18', '1', '2015-11-17 13:07:18', '1', '33', null, null, null, '10', '10', '120', '0', '1', '1', '2015-11-17 13:07:18', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('54', '184', '114', '4', 'HPRC15000009', '2015-01-01 13:00:00', '1', '', '2015-11-17 13:11:47', '1', '2015-11-17 13:11:47', '1', '49', null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-17 13:11:47', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('55', '184', '115', '4', 'HPRC15000010', '2015-01-01 13:00:00', '1', '', '2015-11-17 13:13:08', '1', '2015-11-17 13:13:08', '1', null, null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-17 13:13:08', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('56', '184', '116', '4', 'HPRC15000011', '2015-01-01 13:00:00', '1', '', '2015-11-17 13:15:04', '1', '2015-11-17 13:15:04', '1', null, null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-17 13:15:04', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('57', '184', '117', '4', 'HPRC15000012', '2015-01-01 13:00:00', '1', '', '2015-11-17 13:22:04', '1', '2015-11-17 13:22:04', '1', '37', null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-17 13:22:04', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('58', '184', '118', '6', 'HPRC15000013', '2015-01-31 01:00:00', '1', '', '2015-11-18 12:20:46', '1', '2015-11-18 12:20:46', '1', '38', null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-18 12:20:46', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('59', '184', '119', '6', 'HPRC15000014', '2015-01-31 01:00:00', '1', '', '2015-11-18 12:21:45', '1', '2015-11-18 12:21:45', '1', '39', null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-18 12:21:45', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('60', '184', '120', '6', 'HPRC15000015', '2015-01-31 01:00:00', '1', '', '2015-11-18 12:21:58', '1', '2015-11-18 12:21:58', '1', '40', null, null, null, '10', '10', '120', '1', '1', '1', '2015-11-18 12:21:58', '1', '24', '');
INSERT INTO `don_donaciones` VALUES ('61', '658', '121', '8', '---15000001', '2016-01-01 13:05:00', '89', '', '2016-03-14 10:49:50', '1', '2016-03-14 10:49:57', '89', '47', null, null, null, '4', null, null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('62', '785', '122', '5', 'BDSC15000019', '2015-11-20 11:25:30', '85', '', '2015-11-20 11:25:39', '85', '2015-11-20 11:25:30', '85', '41', null, null, 'No se observaron complicaciones.', '10', '10', '600', '1', '1', '1', '2015-11-20 11:30:32', '85', '24', null);
INSERT INTO `don_donaciones` VALUES ('63', '785', '123', '5', 'BDSC15000020', '2015-11-20 11:27:02', '85', '', '2015-11-20 11:27:30', '85', '2015-11-20 11:27:02', '85', '42', null, null, 'No se observaron complicaciones.', '10', '12', '600', '3', '1', '1', '2015-11-20 11:32:05', '85', '24', null);
INSERT INTO `don_donaciones` VALUES ('64', '184', '125', '6', 'HPRC15000016', '2015-11-23 08:38:16', '1', '', '2016-09-26 12:49:05', '1', '2015-11-23 08:38:16', '1', null, null, null, 'No se observaron complicaciones.', '1', '10', null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('65', '785', '126', '1', 'BDSC15000029', '2015-11-23 12:37:53', '85', '', '2015-11-23 12:38:01', '85', '2015-11-23 12:37:53', '85', '43', null, null, 'No se observaron complicaciones.', '10', '10', '650', '1', '1', '1', '2015-11-23 12:38:30', '85', '20', null);
INSERT INTO `don_donaciones` VALUES ('66', '184', '129', '9', 'HPRC15000018', '2015-11-26 08:06:53', '1', '', '2015-11-26 08:06:59', '1', '2015-11-26 08:06:53', '1', '44', null, null, 'No se observaron complicaciones.', '10', '10', '650', '1', '1', '1', '2015-11-26 08:07:22', '1', '20', null);
INSERT INTO `don_donaciones` VALUES ('67', '184', '130', '1', 'HPRC16000001', '2016-01-22 09:09:04', '1', '', '2016-01-22 09:09:21', '1', '2016-01-22 09:09:04', '1', '45', null, null, 'No se observaron complicaciones.', '9', '10', null, '0', null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('68', '184', '131', '1', 'HPRC15000019', '2016-01-01 13:00:00', '1', '', '2016-02-15 09:38:20', '1', '2016-02-15 09:38:20', '1', '46', null, null, null, '10', '10', '1', '1', '1', '1', '2016-02-15 09:38:20', '1', '1', '');
INSERT INTO `don_donaciones` VALUES ('69', '184', '134', '8', 'HPRC15000017', '2016-01-01 13:00:00', '1', '', '2016-03-14 10:46:54', '1', '2016-03-14 10:46:54', '1', '47', null, null, null, '10', '10', '123', '1', '1', '1', '2016-03-14 10:46:54', '1', '12', '');
INSERT INTO `don_donaciones` VALUES ('70', '184', '135', '12', 'HPRC15000027', '2016-12-31 12:59:00', '1', '', '2016-03-14 10:50:19', '1', '2016-03-14 10:50:19', '1', '48', null, null, null, '10', '10', '1', '1', '1', '1', '2016-03-14 10:50:19', '1', '1', '');
INSERT INTO `don_donaciones` VALUES ('71', '1249', '133', '11', '---16000001', '2016-03-23 11:51:13', '90', '', '2016-03-23 11:54:36', '90', '2016-03-23 11:51:13', '90', '49', null, null, 'No se observaron complicaciones.', '9', '10', null, '0', null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('72', '184', '137', '1', 'HPRC15000021', '2016-03-28 11:45:17', '1', '', '2016-09-26 13:27:12', '1', '2016-03-28 11:45:17', '1', null, null, null, 'No se observaron complicaciones.', '1', '12', null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('73', '785', '88', '4', 'BDSC15000026', '2016-10-31 15:56:54', '1', '', '2016-10-31 17:14:20', '1', '2016-10-31 15:56:54', '1', null, null, null, 'No se observaron complicaciones.', '1', '10', null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('74', '785', '128', '7', 'BDSC15000025', '2016-10-31 16:02:12', '1', '', '2016-10-31 16:02:12', '1', '2016-10-31 16:02:12', '1', null, null, null, null, '4', '10', null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('75', '785', '128', '7', 'BDSC15000012', '2016-10-31 16:16:54', '1', '', '2016-10-31 16:16:54', '1', '2016-10-31 16:16:54', '1', null, null, null, null, '4', '10', null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('76', '785', '140', '2', 'BDSC15000001', '2016-12-22 11:42:09', '1', '', '2016-12-22 11:42:40', '1', '2016-12-22 11:42:09', '1', null, null, null, 'No se observaron complicaciones.', '1', '10', null, null, null, null, null, null, null, null);
INSERT INTO `don_donaciones` VALUES ('77', '785', '141', '21', 'BDSC15000002', '2017-06-09 08:49:21', '1', '', '2017-06-09 09:03:13', '1', '2017-06-09 08:49:21', '1', '50', null, null, 'No se observaron complicaciones.', '9', '10', null, '0', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for don_donaciones_ext
-- ----------------------------
DROP TABLE IF EXISTS `don_donaciones_ext`;
CREATE TABLE `don_donaciones_ext` (
  `ext_id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `cod_colecta` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `tipo` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `peso` decimal(5,2) DEFAULT NULL,
  `hb` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `hematocritos` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ta` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `pulso` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `temperatura_corporal` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `tipo_bolsa` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `marca_bolsa` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `nroLote_bolsa` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nroTubuladura` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reaccion` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ext_id`,`don_id`),
  KEY `don_id` (`don_id`),
  CONSTRAINT `don_donaciones_ext_ibfk_1` FOREIGN KEY (`don_id`) REFERENCES `don_donaciones` (`don_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of don_donaciones_ext
-- ----------------------------
INSERT INTO `don_donaciones_ext` VALUES ('40', '48', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('41', '49', null, 'Voluntaria', '650.00', '0', '', '0', '0', null, '1', 'Baxter', '', '1', null);
INSERT INTO `don_donaciones_ext` VALUES ('42', '50', null, 'Voluntaria', '600.00', '30', '', '20', '8', null, '1', 'Rivero', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('43', '51', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('44', '52', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('45', '53', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('46', '54', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('47', '55', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('48', '56', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('49', '57', null, 'Voluntaria', '600.00', '30', '', '24', '8', null, '1', 'Baxter', '321', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('50', '58', null, 'Voluntaria', '600.00', '30', '', '30', '30', null, '1', 'Baxter', '123', '12', null);
INSERT INTO `don_donaciones_ext` VALUES ('51', '59', null, 'Voluntaria', '600.00', '30', '', '30', '30', null, '1', 'Baxter', '123', '12', null);
INSERT INTO `don_donaciones_ext` VALUES ('52', '60', null, 'Voluntaria', '600.00', '30', '', '30', '30', null, '1', 'Baxter', '123', '12', null);
INSERT INTO `don_donaciones_ext` VALUES ('53', '61', '', 'Voluntaria', '95.00', '15', '', '120/80', '70', null, '1', 'terumo', '11w2117', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('54', '62', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('55', '63', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('56', '64', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('57', '65', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('58', '66', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('59', '67', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('60', '68', null, 'Voluntaria', '300.00', '1', '', '1', '1', null, '1', 'Baxter', '123', '32', null);
INSERT INTO `don_donaciones_ext` VALUES ('61', '69', null, 'Voluntaria', '300.00', '12', '', '12', '12', null, '1', 'Baxter', '123', '12', null);
INSERT INTO `don_donaciones_ext` VALUES ('62', '70', null, 'Voluntaria', '300.00', '1', '', '1', '1', null, '1', 'Baxter', '123', '123', null);
INSERT INTO `don_donaciones_ext` VALUES ('63', '71', '0', '', '100.00', '14', '', '130/70', '60', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('64', '72', '0', '', '0.00', '', '', '', '0', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('65', '73', '0', 'Voluntaria', '85.00', '400', '9400', '12 80', '101', '37.1', '1', '', 'aaaaaaaaaaaa', '0', '');
INSERT INTO `don_donaciones_ext` VALUES ('66', '75', 'BDS785', 'Voluntaria', '999.99', '', '7888', '', '', null, '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('67', '76', '0', 'Voluntaria', '0.00', '', '', '12 80', '90', '37.2', '1', '', '', '1', '');
INSERT INTO `don_donaciones_ext` VALUES ('68', '77', '0', 'Voluntaria', '0.00', '', '', '120/80', '70', '37', '1', 'RIVERO', 'BOLSA0000002901', 'LZNOC000000018', '');

-- ----------------------------
-- Table structure for efectores_parametros
-- ----------------------------
DROP TABLE IF EXISTS `efectores_parametros`;
CREATE TABLE `efectores_parametros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_establecimiento` int(11) NOT NULL,
  `nombre_corto` varchar(3) CHARACTER SET latin1 NOT NULL,
  `medico_matricula` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medico_nombre` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `servicio_id` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpo_impresion` int(1) NOT NULL DEFAULT '0' COMMENT '0:Autogenerado;1:Centralizado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of efectores_parametros
-- ----------------------------
INSERT INTO `efectores_parametros` VALUES ('1', '184', 'HPR', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('2', '167', 'HEE', '77777', 'Medico', '45', '0');
INSERT INTO `efectores_parametros` VALUES ('3', '181', 'ZON', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('4', '657', 'VIL', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('5', '660', 'ROQ', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('6', '838', 'CEM', '77777', 'Medico', '45', '1');
INSERT INTO `efectores_parametros` VALUES ('7', '785', 'BDS', '77777', 'Medico', '45', '1');

-- ----------------------------
-- Table structure for enfermedades
-- ----------------------------
DROP TABLE IF EXISTS `enfermedades`;
CREATE TABLE `enfermedades` (
  `enf_id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`enf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of enfermedades
-- ----------------------------
INSERT INTO `enfermedades` VALUES ('1', 'sifilis');
INSERT INTO `enfermedades` VALUES ('2', 'brucelosis');
INSERT INTO `enfermedades` VALUES ('3', 'chagas');
INSERT INTO `enfermedades` VALUES ('4', 'hepatitisB');
INSERT INTO `enfermedades` VALUES ('5', 'hepatitisC');
INSERT INTO `enfermedades` VALUES ('6', 'hiv');
INSERT INTO `enfermedades` VALUES ('7', 'htlv');

-- ----------------------------
-- Table structure for envios_produccion
-- ----------------------------
DROP TABLE IF EXISTS `envios_produccion`;
CREATE TABLE `envios_produccion` (
  `env_prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_origen` int(11) NOT NULL,
  `efe_destino` int(11) NOT NULL,
  `otro_destino` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `fecha_env` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_rec` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `usu_env` int(11) NOT NULL,
  `usu_rec` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `observaciones` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`env_prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of envios_produccion
-- ----------------------------
INSERT INTO `envios_produccion` VALUES ('1', '184', '657', null, '2016-02-18 12:10:26', '0000-00-00 00:00:00', '1', null, '4', '');

-- ----------------------------
-- Table structure for env_envios
-- ----------------------------
DROP TABLE IF EXISTS `env_envios`;
CREATE TABLE `env_envios` (
  `env_id` int(11) NOT NULL AUTO_INCREMENT,
  `efe_id` int(11) NOT NULL,
  `efe_nombre_origen` varchar(100) CHARACTER SET latin1 NOT NULL,
  `efe_id_destino` int(11) DEFAULT NULL,
  `env_nro` varchar(50) CHARACTER SET latin1 NOT NULL,
  `env_fh` datetime NOT NULL,
  `env_usuario` int(11) NOT NULL,
  `env_estado` tinyint(1) NOT NULL,
  `observaciones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`env_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of env_envios
-- ----------------------------
INSERT INTO `env_envios` VALUES ('1', '184', 'HOSP PROVINCIAL', null, '18420151113130338', '2015-11-13 12:59:34', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('2', '184', 'HOSP PROVINCIAL', null, '18420151113130533', '2015-11-13 13:01:29', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('3', '184', 'HOSP PROVINCIAL', null, '18420151113130635', '2015-11-13 13:02:31', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('4', '184', 'HOSP PROVINCIAL', null, '18420151113130711', '2015-11-13 13:03:07', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('5', '184', 'HOSP PROVINCIAL', null, '18420151113130847', '2015-11-13 13:04:43', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('6', '184', 'HOSP PROVINCIAL', null, '18420151113130939', '2015-11-13 13:05:35', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('7', '184', 'HOSP PROVINCIAL', null, '18420151116091407', '2015-11-16 09:09:54', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('8', '184', 'HOSP PROVINCIAL', null, '18420151116091515', '2015-11-16 09:11:02', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('9', '167', 'HOSP EVA PERON', null, '16720151116103324', '2015-11-16 10:29:11', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('10', '167', 'HOSP EVA PERON', null, '16720151116103433', '2015-11-16 10:30:19', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('11', '167', 'HOSP EVA PERON', null, '16720151116103555', '2015-11-16 10:31:41', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('12', '167', 'HOSP EVA PERON', null, '16720151116103750', '2015-11-16 10:33:37', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('13', '167', 'HOSP EVA PERON', null, '16720151116103818', '2015-11-16 10:34:04', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('14', '167', 'HOSP EVA PERON', null, '16720151116103858', '2015-11-16 10:34:45', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('15', '167', 'HOSP EVA PERON', null, '16720151116103911', '2015-11-16 10:34:58', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('16', '167', 'HOSP EVA PERON', null, '16720151116104151', '2015-11-16 10:37:37', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('17', '167', 'HOSP EVA PERON', null, '16720151116104223', '2015-11-16 10:38:09', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('18', '167', 'HOSP EVA PERON', null, '16720151116104315', '2015-11-16 10:39:02', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('19', '167', 'HOSP EVA PERON', null, '16720151116104332', '2015-11-16 10:39:18', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('20', '167', 'HOSP EVA PERON', null, '16720151116110756', '2015-11-16 11:03:42', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('21', '167', 'HOSP EVA PERON', null, '16720151116110820', '2015-11-16 11:04:07', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('22', '167', 'HOSP EVA PERON', null, '16720151116110838', '2015-11-16 11:04:25', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('23', '167', 'HOSP EVA PERON', null, '16720151116110850', '2015-11-16 11:04:36', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('24', '167', 'HOSP EVA PERON', null, '16720151116110925', '2015-11-16 11:05:11', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('25', '167', 'HOSP EVA PERON', null, '16720151116110949', '2015-11-16 11:05:35', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('26', '167', 'HOSP EVA PERON', null, '16720151116111447', '2015-11-16 11:10:33', '1', '1', 'Sin observaciones.\r\n                ');
INSERT INTO `env_envios` VALUES ('27', '184', 'HOSP PROVINCIAL', null, '18420151117090102', '2015-11-17 08:56:49', '1', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('28', '184', 'HOSP PROVINCIAL', null, '18420151117104556', '2015-11-17 10:41:43', '1', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('29', '184', 'HOSP PROVINCIAL', null, '18420151117121016', '2015-11-17 12:06:02', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('30', '184', 'HOSP PROVINCIAL', null, '18420151117124645', '2015-11-17 12:42:32', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('31', '184', 'HOSP PROVINCIAL', null, '18420151117130203', '2015-11-17 12:57:50', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('32', '184', 'HOSP PROVINCIAL', null, '18420151117130229', '2015-11-17 12:58:15', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('33', '184', 'HOSP PROVINCIAL', null, '18420151117131132', '2015-11-17 13:07:19', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('34', '184', 'HOSP PROVINCIAL', null, '18420151117131601', '2015-11-17 13:11:47', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('35', '184', 'HOSP PROVINCIAL', null, '18420151117131722', '2015-11-17 13:13:09', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('36', '184', 'HOSP PROVINCIAL', null, '18420151117131917', '2015-11-17 13:15:04', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('37', '184', 'HOSP PROVINCIAL', null, '18420151117132618', '2015-11-17 13:22:05', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('38', '184', 'HOSP PROVINCIAL', null, '18420151118122501', '2015-11-18 12:20:46', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('39', '184', 'HOSP PROVINCIAL', null, '18420151118122601', '2015-11-18 12:21:45', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('40', '184', 'HOSP PROVINCIAL', null, '18420151118122614', '2015-11-18 12:21:59', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('41', '785', 'BANCO CENTRAL DE SANGRE', null, '78520151120112605', '2015-11-20 11:26:05', '85', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('42', '785', 'BANCO CENTRAL DE SANGRE', null, '78520151120112749', '2015-11-20 11:27:49', '85', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('43', '785', 'BANCO CENTRAL DE SANGRE', null, '78520151123123812', '2015-11-23 12:38:12', '85', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('44', '184', 'HOSP PROVINCIAL', null, '18420151126080705', '2015-11-26 08:07:06', '1', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('45', '184', 'HOSP PROVINCIAL', null, '18420160122090926', '2016-01-22 09:09:26', '1', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('46', '184', 'HOSP PROVINCIAL', null, '18420160215094628', '2016-02-15 09:38:20', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('47', '184', 'HOSP PROVINCIAL', null, '18420160314105616', '2016-03-14 10:46:54', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('48', '184', 'HOSP PROVINCIAL', null, '18420160314105940', '2016-03-14 10:50:19', '1', '1', 'Sin observaciones.\r\n                            ');
INSERT INTO `env_envios` VALUES ('49', '1249', 'HOSPITAL DR. ROQUE SAENZ PEÑA', null, '124920160323115732', '2016-03-23 11:57:32', '90', '1', 'No se observaron complicaciones.');
INSERT INTO `env_envios` VALUES ('50', '785', 'BANCO CENTRAL DE SANGRE', null, '78520170609090454', '2017-06-09 09:04:54', '1', '1', 'No se observaron complicaciones.');

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `grupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) CHARACTER SET latin1 NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`grupo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('1', 'Administradores', '1');
INSERT INTO `grupos` VALUES ('2', 'Soporte', '1');
INSERT INTO `grupos` VALUES ('3', 'Usuarios Regulares', '1');
INSERT INTO `grupos` VALUES ('4', 'Admision', '1');
INSERT INTO `grupos` VALUES ('5', 'Medicos', '1');
INSERT INTO `grupos` VALUES ('6', 'Tecnicos', '1');

-- ----------------------------
-- Table structure for hemocomponentesxperfilprod
-- ----------------------------
DROP TABLE IF EXISTS `hemocomponentesxperfilprod`;
CREATE TABLE `hemocomponentesxperfilprod` (
  `hem_fil_id` int(11) NOT NULL AUTO_INCREMENT,
  `idperfil` int(11) NOT NULL,
  `idhemocomponente` int(11) NOT NULL,
  PRIMARY KEY (`hem_fil_id`),
  KEY `idperfil` (`idperfil`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of hemocomponentesxperfilprod
-- ----------------------------
INSERT INTO `hemocomponentesxperfilprod` VALUES ('1', '1', '2');
INSERT INTO `hemocomponentesxperfilprod` VALUES ('2', '5', '5');
INSERT INTO `hemocomponentesxperfilprod` VALUES ('3', '3', '5');
INSERT INTO `hemocomponentesxperfilprod` VALUES ('4', '2', '2');
INSERT INTO `hemocomponentesxperfilprod` VALUES ('5', '3', '2');

-- ----------------------------
-- Table structure for hem_hemocomponentes
-- ----------------------------
DROP TABLE IF EXISTS `hem_hemocomponentes`;
CREATE TABLE `hem_hemocomponentes` (
  `hem_id` int(11) NOT NULL AUTO_INCREMENT,
  `hem_codigo` char(5) NOT NULL,
  `hem_descripcion` varchar(60) NOT NULL,
  `hem_dias_aptitud` smallint(6) NOT NULL,
  `hem_nrobolsa` int(1) DEFAULT '1',
  `hem_estado` int(11) NOT NULL DEFAULT '0',
  `hem_caducidad` int(11) NOT NULL,
  `hem_esperado` int(11) NOT NULL DEFAULT '0',
  `hem_temp` varchar(12) NOT NULL,
  `hem_peso_min` int(11) NOT NULL,
  `hem_peso_max` int(11) NOT NULL,
  PRIMARY KEY (`hem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of hem_hemocomponentes
-- ----------------------------
INSERT INTO `hem_hemocomponentes` VALUES ('1', 'PF', 'Plasma Fresco', '360', '1', '1', '8', '1', '-40', '150', '300');
INSERT INTO `hem_hemocomponentes` VALUES ('2', 'PLT', 'Plaquetas', '5', '1', '1', '8', '1', '20 a 22', '50', '110');
INSERT INTO `hem_hemocomponentes` VALUES ('3', 'CGR', 'Concentrado Glóbulos Rojos', '35', '1', '1', '0', '1', '2 a 8', '200', '500');
INSERT INTO `hem_hemocomponentes` VALUES ('4', 'PN', 'Plasma Normal', '360', '1', '1', '0', '0', '-40', '150', '300');
INSERT INTO `hem_hemocomponentes` VALUES ('5', 'CRIO', 'Crioprecipitado', '360', '1', '1', '8', '0', '-40', '40', '100');
INSERT INTO `hem_hemocomponentes` VALUES ('6', 'PM', 'Plasma Modificado', '360', '1', '1', '8', '0', '-40', '150', '300');

-- ----------------------------
-- Table structure for inmunologia
-- ----------------------------
DROP TABLE IF EXISTS `inmunologia`;
CREATE TABLE `inmunologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `grupo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `factor` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `rastreo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `grupo_inverso` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `du` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cde` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fenotipo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inmunologia
-- ----------------------------
INSERT INTO `inmunologia` VALUES ('1', '65', 'AB', '+', '', 'AB', '2015-11-23 12:38:40', '85', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('2', '62', 'AB', '-', '', 'AB', '2015-11-23 12:46:02', '85', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('3', '66', 'AB', '+', '', 'AB', '2015-11-26 08:07:38', '1', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('4', '70', 'AB', '+', '', 'AB', '2016-03-14 11:05:19', '1', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('5', '69', 'AB', '+', '', 'AB', '2016-09-19 10:13:57', '1', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('6', '68', 'AB', '+', '', 'AB', '2016-10-05 09:08:14', '1', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('7', '60', 'AB', '+', '', 'AB', '2016-10-05 09:08:23', '1', '+', '+', '');
INSERT INTO `inmunologia` VALUES ('8', '60', 'AB', '+', 'zsxas', 'AB', '2016-10-05 09:08:59', '1', '+', '+', '');

-- ----------------------------
-- Table structure for mot_descartes
-- ----------------------------
DROP TABLE IF EXISTS `mot_descartes`;
CREATE TABLE `mot_descartes` (
  `mot_id` int(11) NOT NULL AUTO_INCREMENT,
  `mot_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `mot_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`mot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mot_descartes
-- ----------------------------
INSERT INTO `mot_descartes` VALUES ('1', 'Faltan tubos', '1');
INSERT INTO `mot_descartes` VALUES ('2', 'Bolsa sobrante', '1');
INSERT INTO `mot_descartes` VALUES ('3', 'Bolsa dañada', '1');
INSERT INTO `mot_descartes` VALUES ('4', 'Bajo peso', '1');
INSERT INTO `mot_descartes` VALUES ('5', 'Temperatura inadecuada', '1');

-- ----------------------------
-- Table structure for mot_motivos
-- ----------------------------
DROP TABLE IF EXISTS `mot_motivos`;
CREATE TABLE `mot_motivos` (
  `mot_id` int(11) NOT NULL AUTO_INCREMENT,
  `mot_cat` enum('DIF','DES','REC','PPR') CHARACTER SET latin1 NOT NULL,
  `mot_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `dias` int(11) DEFAULT NULL,
  `mot_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`mot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mot_motivos
-- ----------------------------
INSERT INTO `mot_motivos` VALUES ('1', 'DIF', 'Aborto', '12', '1');
INSERT INTO `mot_motivos` VALUES ('2', 'REC', 'Acupuntura/ mesoterapia', null, '1');
INSERT INTO `mot_motivos` VALUES ('3', 'DES', 'Descarte #1', null, '1');
INSERT INTO `mot_motivos` VALUES ('4', 'DIF', 'ACV', '5', '1');
INSERT INTO `mot_motivos` VALUES ('5', 'DIF', 'Alcoholismo', '1', '1');
INSERT INTO `mot_motivos` VALUES ('6', 'DIF', 'Alergia', '2', '1');
INSERT INTO `mot_motivos` VALUES ('7', 'DIF', 'Asma', '3', '1');
INSERT INTO `mot_motivos` VALUES ('8', 'DIF', 'Betabloqueantes', null, '1');
INSERT INTO `mot_motivos` VALUES ('9', 'DIF', 'Brucelosis', null, '1');
INSERT INTO `mot_motivos` VALUES ('10', 'DIF', 'cirugia mayor/menor/ laparoscopia/ endoscopia', null, '1');
INSERT INTO `mot_motivos` VALUES ('11', 'DIF', 'contacto sexual con personas con infeccion', null, '1');
INSERT INTO `mot_motivos` VALUES ('12', 'DIF', 'cocaina', null, '1');
INSERT INTO `mot_motivos` VALUES ('13', 'DIF', 'colon irritable', null, '1');
INSERT INTO `mot_motivos` VALUES ('14', 'DIF', 'contacto estrecho con pacientes con hepatitis viral', null, '1');
INSERT INTO `mot_motivos` VALUES ('15', 'DIF', 'dengue', null, '1');
INSERT INTO `mot_motivos` VALUES ('16', 'DIF', 'depresión', null, '1');
INSERT INTO `mot_motivos` VALUES ('17', 'DIF', 'desmayo/lipotimia', null, '1');
INSERT INTO `mot_motivos` VALUES ('18', 'DIF', 'embarazoen los ultimos 6 meses', null, '1');
INSERT INTO `mot_motivos` VALUES ('19', 'DIF', 'enfermedades endocrinologica', null, '1');
INSERT INTO `mot_motivos` VALUES ('20', 'DIF', 'enfermedades dermatologicas', null, '1');
INSERT INTO `mot_motivos` VALUES ('21', 'DIF', 'enfermedades hematologicas', null, '1');
INSERT INTO `mot_motivos` VALUES ('22', 'DIF', 'enfermedades respiratorias', null, '1');
INSERT INTO `mot_motivos` VALUES ('23', 'DIF', 'esplenectomia ', null, '1');
INSERT INTO `mot_motivos` VALUES ('24', 'DIF', 'exposicion accidental con sangre', null, '1');
INSERT INTO `mot_motivos` VALUES ('25', 'DIF', 'fiebre', null, '1');
INSERT INTO `mot_motivos` VALUES ('26', 'DIF', 'finasteride', null, '1');
INSERT INTO `mot_motivos` VALUES ('27', 'DIF', 'flebitis', null, '1');
INSERT INTO `mot_motivos` VALUES ('28', 'DIF', 'gripe', null, '1');
INSERT INTO `mot_motivos` VALUES ('29', 'DIF', 'guillan barre', null, '1');
INSERT INTO `mot_motivos` VALUES ('30', 'DIF', 'hipertension', null, '1');
INSERT INTO `mot_motivos` VALUES ('31', 'DIF', 'hipotension', null, '1');
INSERT INTO `mot_motivos` VALUES ('32', 'DIF', 'hipertiroidismo', null, '1');
INSERT INTO `mot_motivos` VALUES ('33', 'DIF', 'HTLV I/II', null, '1');
INSERT INTO `mot_motivos` VALUES ('34', 'DIF', 'inmunoglobulinas', null, '1');
INSERT INTO `mot_motivos` VALUES ('35', 'DIF', 'malaria/paludismo', null, '1');
INSERT INTO `mot_motivos` VALUES ('36', 'DIF', 'medicamentos', null, '1');
INSERT INTO `mot_motivos` VALUES ('37', 'DIF', 'mononucleosis infecciosa', null, '1');
INSERT INTO `mot_motivos` VALUES ('38', 'DIF', 'osteomielitis', null, '1');
INSERT INTO `mot_motivos` VALUES ('39', 'DIF', 'piercing', null, '1');
INSERT INTO `mot_motivos` VALUES ('40', 'DIF', 'poliglobulia/policitemia', null, '1');
INSERT INTO `mot_motivos` VALUES ('41', 'DIF', 'purpura trombocitopenica idiopatica', null, '1');
INSERT INTO `mot_motivos` VALUES ('42', 'DIF', 'tamoxifeno', null, '1');
INSERT INTO `mot_motivos` VALUES ('43', 'DIF', 'tatuaje', null, '1');
INSERT INTO `mot_motivos` VALUES ('44', 'DIF', 'toxoplasmmosis', null, '1');
INSERT INTO `mot_motivos` VALUES ('45', 'DIF', 'transfusion de sangre', null, '1');
INSERT INTO `mot_motivos` VALUES ('46', 'DIF', 'tuberculosis', null, '1');
INSERT INTO `mot_motivos` VALUES ('47', 'DIF', 'vacunas', null, '1');
INSERT INTO `mot_motivos` VALUES ('48', 'REC', 'Acne tto acitretina', null, '1');
INSERT INTO `mot_motivos` VALUES ('49', 'DIF', 'enfermedades infecciosas', null, '1');
INSERT INTO `mot_motivos` VALUES ('50', 'REC', 'Autoinmune', null, '1');
INSERT INTO `mot_motivos` VALUES ('51', 'REC', 'Babebiosis', null, '1');
INSERT INTO `mot_motivos` VALUES ('52', 'REC', 'Behcet', null, '1');
INSERT INTO `mot_motivos` VALUES ('53', 'REC', 'cáncer', null, '1');
INSERT INTO `mot_motivos` VALUES ('54', 'REC', 'cirrosis hepatica', null, '1');
INSERT INTO `mot_motivos` VALUES ('55', 'REC', 'claudicacion intermitente', null, '1');
INSERT INTO `mot_motivos` VALUES ('56', 'REC', 'colagenosis', null, '1');
INSERT INTO `mot_motivos` VALUES ('57', 'REC', 'colitis ulcerosa', null, '1');
INSERT INTO `mot_motivos` VALUES ('58', 'REC', 'convulsiones', null, '1');
INSERT INTO `mot_motivos` VALUES ('59', 'REC', 'cornea transplante', null, '1');
INSERT INTO `mot_motivos` VALUES ('60', 'REC', 'crohn', null, '1');
INSERT INTO `mot_motivos` VALUES ('61', 'REC', 'chagas +', null, '1');
INSERT INTO `mot_motivos` VALUES ('62', 'REC', 'diabetes', null, '1');
INSERT INTO `mot_motivos` VALUES ('63', 'REC', 'drogas', null, '1');
INSERT INTO `mot_motivos` VALUES ('64', 'REC', 'encefalitis virales', null, '1');
INSERT INTO `mot_motivos` VALUES ('65', 'REC', 'endocarditis', null, '1');
INSERT INTO `mot_motivos` VALUES ('66', 'REC', 'enfermedades autoinmunes', null, '1');
INSERT INTO `mot_motivos` VALUES ('67', 'REC', 'enfermedades del snc', null, '1');
INSERT INTO `mot_motivos` VALUES ('68', 'REC', 'esclerosis lateral amiotrofica', null, '1');
INSERT INTO `mot_motivos` VALUES ('69', 'REC', 'hepatitis B;C', null, '1');
INSERT INTO `mot_motivos` VALUES ('70', 'REC', 'IAM', null, '1');
INSERT INTO `mot_motivos` VALUES ('71', 'REC', 'lepra', null, '1');
INSERT INTO `mot_motivos` VALUES ('72', 'REC', 'litio', null, '1');
INSERT INTO `mot_motivos` VALUES ('73', 'REC', 'melanoma', null, '1');
INSERT INTO `mot_motivos` VALUES ('74', 'REC', 'miastenia', null, '1');
INSERT INTO `mot_motivos` VALUES ('75', 'REC', 'micosis fungoide', null, '1');
INSERT INTO `mot_motivos` VALUES ('76', 'REC', 'parkinson', null, '1');
INSERT INTO `mot_motivos` VALUES ('77', 'REC', 'pericarditis', null, '1');
INSERT INTO `mot_motivos` VALUES ('78', 'REC', 'trombosis arterial/venosa profunda', null, '1');
INSERT INTO `mot_motivos` VALUES ('79', 'REC', 'valvulopatias', null, '1');
INSERT INTO `mot_motivos` VALUES ('80', 'REC', 'VIH', null, '1');
INSERT INTO `mot_motivos` VALUES ('81', 'REC', 'wilson enf', null, '1');

-- ----------------------------
-- Table structure for movimientos
-- ----------------------------
DROP TABLE IF EXISTS `movimientos`;
CREATE TABLE `movimientos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `efector_id` bigint(20) DEFAULT NULL,
  `destino` int(11) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `refencia` varchar(50) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of movimientos
-- ----------------------------

-- ----------------------------
-- Table structure for movimientos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `movimientos_detalle`;
CREATE TABLE `movimientos_detalle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `movimiento_id` bigint(20) DEFAULT NULL,
  `donacion_id` bigint(20) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of movimientos_detalle
-- ----------------------------

-- ----------------------------
-- Table structure for ocu_ocupaciones
-- ----------------------------
DROP TABLE IF EXISTS `ocu_ocupaciones`;
CREATE TABLE `ocu_ocupaciones` (
  `ocu_id` int(11) NOT NULL AUTO_INCREMENT,
  `ocu_descripcion` varchar(90) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ocu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ocu_ocupaciones
-- ----------------------------
INSERT INTO `ocu_ocupaciones` VALUES ('1', 'Ama de casa');
INSERT INTO `ocu_ocupaciones` VALUES ('2', 'Artesano');
INSERT INTO `ocu_ocupaciones` VALUES ('3', 'Camionero');
INSERT INTO `ocu_ocupaciones` VALUES ('4', 'Carpintero');
INSERT INTO `ocu_ocupaciones` VALUES ('5', 'Cobrador');
INSERT INTO `ocu_ocupaciones` VALUES ('6', 'Colectivero');
INSERT INTO `ocu_ocupaciones` VALUES ('7', 'Comerciante');
INSERT INTO `ocu_ocupaciones` VALUES ('8', 'Cuentapropista');
INSERT INTO `ocu_ocupaciones` VALUES ('9', 'Changarin');
INSERT INTO `ocu_ocupaciones` VALUES ('10', 'Deportista');
INSERT INTO `ocu_ocupaciones` VALUES ('11', 'Desocupado');
INSERT INTO `ocu_ocupaciones` VALUES ('12', 'Discapacitado');
INSERT INTO `ocu_ocupaciones` VALUES ('13', 'Directivo de Empresa');
INSERT INTO `ocu_ocupaciones` VALUES ('14', 'Docente');
INSERT INTO `ocu_ocupaciones` VALUES ('15', 'Electricista');
INSERT INTO `ocu_ocupaciones` VALUES ('16', 'Empleada Domestica');
INSERT INTO `ocu_ocupaciones` VALUES ('17', 'Empleado');
INSERT INTO `ocu_ocupaciones` VALUES ('18', 'Empleado Administrativo');
INSERT INTO `ocu_ocupaciones` VALUES ('19', 'Empleado de Comercio');
INSERT INTO `ocu_ocupaciones` VALUES ('20', 'Empleado Ferroviario');
INSERT INTO `ocu_ocupaciones` VALUES ('21', 'Empleado Gastronomico');
INSERT INTO `ocu_ocupaciones` VALUES ('22', 'Empleado Judicial');
INSERT INTO `ocu_ocupaciones` VALUES ('23', 'Empleado Rural');
INSERT INTO `ocu_ocupaciones` VALUES ('24', 'Empleado de Seguridad');
INSERT INTO `ocu_ocupaciones` VALUES ('25', 'Estudiante');
INSERT INTO `ocu_ocupaciones` VALUES ('26', 'Fletero');
INSERT INTO `ocu_ocupaciones` VALUES ('27', 'Gomero');
INSERT INTO `ocu_ocupaciones` VALUES ('28', 'Gasista');
INSERT INTO `ocu_ocupaciones` VALUES ('29', 'Herrero');
INSERT INTO `ocu_ocupaciones` VALUES ('30', 'Jardinero');
INSERT INTO `ocu_ocupaciones` VALUES ('31', 'Jubilado');
INSERT INTO `ocu_ocupaciones` VALUES ('32', 'Mecanico');
INSERT INTO `ocu_ocupaciones` VALUES ('33', 'Medico');
INSERT INTO `ocu_ocupaciones` VALUES ('34', 'Metalurgico');
INSERT INTO `ocu_ocupaciones` VALUES ('35', 'Obrero de la Construccion');
INSERT INTO `ocu_ocupaciones` VALUES ('36', 'Operario');
INSERT INTO `ocu_ocupaciones` VALUES ('37', 'Personal de Salud');
INSERT INTO `ocu_ocupaciones` VALUES ('38', 'Policia');
INSERT INTO `ocu_ocupaciones` VALUES ('39', 'Profesional');
INSERT INTO `ocu_ocupaciones` VALUES ('40', 'Recolector de Basura');
INSERT INTO `ocu_ocupaciones` VALUES ('41', 'Taxista');
INSERT INTO `ocu_ocupaciones` VALUES ('42', 'Remisero');
INSERT INTO `ocu_ocupaciones` VALUES ('43', 'Transportista');
INSERT INTO `ocu_ocupaciones` VALUES ('44', 'Vendedor Ambulante');
INSERT INTO `ocu_ocupaciones` VALUES ('45', 'Viajante');

-- ----------------------------
-- Table structure for pedidos_insumos
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos`;
CREATE TABLE `pedidos_insumos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL COMMENT '1 = Generado\r\n2 = Enviado\r\n3 = Recep. Parcial\r\n4 = Cerrado\r\n5 = Cancelado\r\n6 = Pend. QA\r\n7 = Autorizar Pedido\r\n8 = Nuevo Envio',
  `observaciones` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `efector_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) unsigned zerofill DEFAULT NULL,
  `ultima_recepcion_observaciones` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ultima_recepcion_fecha` datetime DEFAULT NULL,
  `ultima_recepcion_usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`),
  KEY `estado` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pedidos_insumos
-- ----------------------------
INSERT INTO `pedidos_insumos` VALUES ('1', '2017-05-10 17:00:00', '3', 'Ver caja que este en buenas condiciones para el envio', '785', '2017-05-18 12:29:29', '00000000001', 'ASSSSSSSSS', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('2', '2017-05-22 00:00:00', '3', 'Ver cuestion obvia', '785', '2017-05-22 17:22:20', '00000000001', 'RECEPCION  2 UNIDADES', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('3', '2017-05-22 09:00:00', '4', 'Ver cosas nuevas', '785', '2017-05-22 17:23:00', '00000000068', 'ASCASCASSCA', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('4', '2017-05-24 20:00:00', '2', 'Cosa nostra', '785', '2017-05-26 15:10:14', '00000000001', null, null, null);
INSERT INTO `pedidos_insumos` VALUES ('5', '2017-05-29 18:00:00', '5', 'AAAAAAAAA', '785', '2017-05-29 11:25:02', '00000000068', null, null, null);
INSERT INTO `pedidos_insumos` VALUES ('6', '2017-06-01 00:00:00', '4', 'Primero de Junio 1', '785', '2017-06-01 11:54:00', '00000000001', 'La caja vino correctamente cerrada', '0000-00-00 00:00:00', '1');
INSERT INTO `pedidos_insumos` VALUES ('7', '2017-06-01 00:00:00', '2', 'Primero de Junio 2', '785', '2017-06-01 11:54:22', '00000000001', null, null, null);
INSERT INTO `pedidos_insumos` VALUES ('8', '2017-06-02 00:00:00', '1', 'Embalar correctamente por favor', '785', '2017-06-02 08:17:07', '00000000001', null, null, null);

-- ----------------------------
-- Table structure for pedidos_insumos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos_detalle`;
CREATE TABLE `pedidos_insumos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  `cantidad_enviada` decimal(10,2) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `notas` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `producto_id` (`producto_id`) USING BTREE,
  KEY `pedido_id` (`pedido_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pedidos_insumos_detalle
-- ----------------------------
INSERT INTO `pedidos_insumos_detalle` VALUES ('1', '1', '5', '400.00', null, null, 'Plasticos borde blanco');
INSERT INTO `pedidos_insumos_detalle` VALUES ('2', '1', '3', '500.00', null, null, 'Especiales de Poliester');
INSERT INTO `pedidos_insumos_detalle` VALUES ('3', '1', '2', '600.00', null, null, 'Color Blanco');
INSERT INTO `pedidos_insumos_detalle` VALUES ('4', '1', '4', '700.00', null, null, 'Azules preferentemente');
INSERT INTO `pedidos_insumos_detalle` VALUES ('5', '1', '1', '300.00', null, null, 'Inorganico');
INSERT INTO `pedidos_insumos_detalle` VALUES ('6', '2', '1', '150.00', '57.00', null, 'Unidades basicas de control');
INSERT INTO `pedidos_insumos_detalle` VALUES ('7', '3', '1', '300.00', null, null, 'Verificar las etiquetas');
INSERT INTO `pedidos_insumos_detalle` VALUES ('8', '3', '4', '500.00', null, null, 'Color azul');
INSERT INTO `pedidos_insumos_detalle` VALUES ('9', '4', '1', '200.00', null, null, 'AAAAAAAAA');
INSERT INTO `pedidos_insumos_detalle` VALUES ('10', '4', '2', '4.00', null, null, 'BBBBBBBBBB');
INSERT INTO `pedidos_insumos_detalle` VALUES ('11', '5', '3', '22.00', null, null, 'radioactivos clase A33');
INSERT INTO `pedidos_insumos_detalle` VALUES ('12', '5', '1', '23.00', null, null, 'Reactivo clase A1');
INSERT INTO `pedidos_insumos_detalle` VALUES ('13', '6', '1', '470.00', null, null, 'Verificarlo correctamente');
INSERT INTO `pedidos_insumos_detalle` VALUES ('14', '7', '1', '200.00', null, null, 'Verlo bien');
INSERT INTO `pedidos_insumos_detalle` VALUES ('15', '8', '6', '100.00', null, null, 'Linea 1 de reactivos');
INSERT INTO `pedidos_insumos_detalle` VALUES ('16', '8', '7', '200.00', null, null, 'Linea 2 de reactivos');

-- ----------------------------
-- Table structure for pedidos_insumos_log
-- ----------------------------
DROP TABLE IF EXISTS `pedidos_insumos_log`;
CREATE TABLE `pedidos_insumos_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `accion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `datos` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `pedido_id` (`pedido_id`) USING BTREE,
  KEY `efector_id` (`efector_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pedidos_insumos_log
-- ----------------------------
INSERT INTO `pedidos_insumos_log` VALUES ('1', '1', '785', '2017-05-18 12:29:29', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"400\",\"notas\":\"Plasticos borde blanco\"},\"linea_1\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"500\",\"notas\":\"Especiales de Poliester\"},\"linea_2\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"600\",\"notas\":\"Color Blanco\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"700\",\"notas\":\"Azules preferentemente\"},\"linea_4\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300\",\"notas\":\"Inorganico\"},\"header\":{\"fecha\":\"2017-05-10\",\"observaciones\":\"Ver caja que este en buenas condiciones para el envio\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('2', '1', '785', '2017-05-18 12:30:37', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"5\",\"notas\":\"L1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"6\",\"notas\":\"L2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"7\",\"notas\":\"L3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"8\",\"notas\":\"L4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"9\",\"notas\":\"L5\"},\"header\":{\"fecha\":\"2017-05-18 1212:0505:3636\",\"observaciones\":\"Sin remito\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('3', '1', '785', '2017-05-19 10:20:09', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"AA1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"2\",\"notas\":\"AA2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"2\",\"notas\":\"AA3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"AA4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"2\",\"notas\":\"AA5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:0808\",\"observaciones\":\"Nuevamente revisar el remito de envio\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('4', '1', '785', '2017-05-19 10:53:33', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"M1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"3\",\"notas\":\"M2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"3\",\"notas\":\"M3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"M4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"3\",\"notas\":\"M5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:3333\",\"observaciones\":\"Vino sin remito otra vez\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('5', '1', '785', '2017-05-19 10:55:38', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"M1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"3\",\"notas\":\"M2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"3\",\"notas\":\"M3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"M4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"3\",\"notas\":\"M5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:3737\",\"observaciones\":\"Vino sin remito otra vez\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('6', '1', '785', '2017-05-19 10:56:10', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"M1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"3\",\"notas\":\"M2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"3\",\"notas\":\"M3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"M4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"3\",\"notas\":\"M5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:1010\",\"observaciones\":\"Vino sin remito otra vez\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('7', '1', '785', '2017-05-19 10:57:21', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"4\",\"notas\":\"B1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"B2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"4\",\"notas\":\"B3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"4\",\"notas\":\"B4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"4\",\"notas\":\"B5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:2020\",\"observaciones\":\"Conseguir caja nueva\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('8', '1', '785', '2017-05-19 10:58:48', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"4\",\"notas\":\"B1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"B2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"4\",\"notas\":\"B3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"4\",\"notas\":\"B4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"4\",\"notas\":\"B5\"},\"header\":{\"fecha\":\"2017-05-19 1010:0505:4747\",\"observaciones\":\"Conseguir caja nueva\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('9', '1', '785', '2017-05-19 11:50:43', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"1\",\"notas\":\"D1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"1\",\"notas\":\"D2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"1\",\"notas\":\"D3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"1\",\"notas\":\"D4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"1\",\"notas\":\"D5\"},\"header\":{\"fecha\":\"2017-05-19 1111:0505:4343\",\"observaciones\":\"Cuco\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('10', '1', '785', '2017-05-19 12:03:36', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"C1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"2\",\"notas\":\"C2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"2\",\"notas\":\"C3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"C4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"2\",\"notas\":\"C5\"},\"header\":{\"fecha\":\"2017-05-19 1212:0505:3535\",\"observaciones\":\"Cosa loca\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('11', '2', '785', '2017-05-22 17:22:21', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"150\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-05-22\",\"observaciones\":\"Ver cuestion obvia\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('12', '3', '785', '2017-05-22 17:23:01', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300\",\"notas\":\"Verificar las etiquetas\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"500\",\"notas\":\"Color azul\"},\"header\":{\"fecha\":\"2017-05-22\",\"observaciones\":\"Ver cosas nuevas\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('13', '3', '785', '2017-05-22 17:24:40', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"10\",\"notas\":\"PRUEBA 1 LINEA 1\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"10\",\"notas\":\"PRUEBA 1 LINEA 2\"},\"header\":{\"fecha\":\"2017-05-22 0505:0505:4040\",\"observaciones\":\"Sin remito a la vista\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('14', '4', '785', '2017-05-26 15:10:14', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"200\",\"notas\":\"AAAAAAAAA\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"BBBBBBBBBB\"},\"header\":{\"fecha\":\"2017-05-24\",\"observaciones\":\"Cosa nostra\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('15', '2', '785', '2017-05-26 15:10:38', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"10\",\"notas\":\"CUUUUU\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:3737\",\"observaciones\":\"aaaaaaaaaa\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('16', '2', '785', '2017-05-26 15:16:44', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"LALALA LANS\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:4444\",\"observaciones\":\"advasvavsa\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('17', '3', '785', '2017-05-26 15:18:57', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"GGG\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"4\",\"notas\":\"HHHHHH\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:5757\",\"observaciones\":\"ascasc\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('18', '2', '785', '2017-05-26 15:25:29', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"GGHHEE\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:2929\",\"observaciones\":\"cscacsA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('19', '2', '785', '2017-05-26 15:48:56', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"GGHHEE\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:5656\",\"observaciones\":\"cscacsA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('20', '1', '785', '2017-05-26 15:49:24', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"3\",\"notas\":\"R1\"},\"linea_1\":{\"producto_id\":\"2\",\"descripcion\":\"Guantes de Latex por 100 Und.\",\"cantidad\":\"4\",\"notas\":\"R2\"},\"linea_2\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"5\",\"notas\":\"R3\"},\"linea_3\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"6\",\"notas\":\"R4\"},\"linea_4\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"7\",\"notas\":\"R5\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:2424\",\"observaciones\":\"ASSSSSSSSS\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('21', '3', '785', '2017-05-26 15:51:24', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"A1\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"A2\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:2424\",\"observaciones\":\"ASCASCASSCA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('22', '3', '785', '2017-05-26 15:55:39', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"A1\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"A2\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:3939\",\"observaciones\":\"ASCASCASSCA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('23', '2', '785', '2017-05-26 15:56:03', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"5\",\"notas\":\"TEST 5\"},\"header\":{\"fecha\":\"2017-05-26 0303:0505:0202\",\"observaciones\":\"acsacscas\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('24', '2', '785', '2017-05-26 16:14:23', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"5\",\"notas\":\"TEST 5\"},\"header\":{\"fecha\":\"2017-05-26 0404:0505:2323\",\"observaciones\":\"acsacscas\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('25', '3', '785', '2017-05-29 11:21:59', 'Cierre del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300.00\",\"notas\":\"Verificar las etiquetas\"},\"linea_1\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"500.00\",\"notas\":\"Color azul\"},\"header\":{\"fecha\":\"2017-05-29 1111:0505:5959\",\"observaciones\":\"Pedido Cerrado/Finalizado\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('26', '5', '785', '2017-05-29 11:25:03', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"3\",\"descripcion\":\"FIltro para Laboratoio  A33\",\"cantidad\":\"22\",\"notas\":\"radioactivos clase A33\"},\"linea_1\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"23\",\"notas\":\"Reactivo clase A1\"},\"header\":{\"fecha\":\"2017-05-29\",\"observaciones\":\"AAAAAAAAA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('27', '6', '785', '2017-06-01 11:54:01', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"470\",\"notas\":\"Verificarlo correctamente\"},\"header\":{\"fecha\":\"2017-06-01\",\"observaciones\":\"Primero de Junio 1\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('28', '7', '785', '2017-06-01 11:54:22', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"200\",\"notas\":\"Verlo bien\"},\"header\":{\"fecha\":\"2017-06-01\",\"observaciones\":\"Primero de Junio 2\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('29', '6', '785', '2017-06-01 11:55:12', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"300\",\"notas\":\"recibimos la mitad\"},\"header\":{\"fecha\":\"2017-06-01 1111:0606:1212\",\"observaciones\":\"La caja vino correctamente cerrada\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('30', '6', '785', '2017-06-01 11:56:36', 'Cierre del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"470.00\",\"notas\":\"Verificarlo correctamente\"},\"header\":{\"fecha\":\"2017-06-01 1111:0606:3636\",\"observaciones\":\"Pedido Cerrado/Finalizado\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('31', '8', '785', '2017-06-02 08:17:08', 'Alta del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"6\",\"descripcion\":\"Reacitvo B1\",\"cantidad\":\"100\",\"notas\":\"Linea 1 de reactivos\"},\"linea_1\":{\"producto_id\":\"7\",\"descripcion\":\"Reacitvo B22\",\"cantidad\":\"200\",\"notas\":\"Linea 2 de reactivos\"},\"header\":{\"fecha\":\"2017-06-02\",\"observaciones\":\"Embalar correctamente por favor\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('37', '2', '785', '2017-06-12 12:56:36', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-12 1212:0606:3636\",\"observaciones\":\"AAAAAAAAAAA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('38', '2', '785', '2017-06-12 12:57:34', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-12 1212:0606:3434\",\"observaciones\":\"AAAAAAAAAAA\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('39', '2', '785', '2017-06-13 08:04:32', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control XX\"},\"header\":{\"fecha\":\"2017-06-13 0808:0606:3131\",\"observaciones\":\"FFFFFFFFFFFFF\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('40', '2', '785', '2017-06-13 13:52:27', 'Nuevo Envio', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-13 0101:0606:2727\",\"observaciones\":\"SSSSSSSSSSS\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('41', '2', '785', '2017-06-13 14:13:50', 'Recepcion del Pedido', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"2\",\"notas\":\"Unidades basicas de control\"},\"header\":{\"fecha\":\"2017-06-13 0202:0606:5050\",\"observaciones\":\"RECEPCION  2 UNIDADES\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('42', '0', '785', '2017-06-30 10:59:24', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"5\",\"descripcion\":\"Bidon de cloro x 5lts.\",\"cantidad\":\"-2\",\"notas\":\"ajuste por robo\"},\"header\":{\"fecha\":\"2017-06-30 1010:0606:2323\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('43', '0', '785', '2017-06-30 11:05:34', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"3\",\"notas\":\"Mal contado\"},\"header\":{\"fecha\":\"2017-06-30 1111:0606:3434\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('44', '0', '785', '2017-06-30 11:10:30', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"12\",\"notas\":\"AJUSTE EN POSITIVO\"},\"header\":{\"fecha\":\"2017-06-30 1111:0606:3030\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('45', '0', '785', '2017-06-30 12:06:53', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"-3\",\"notas\":\"SSSSSS\"},\"header\":{\"fecha\":\"2017-06-30 1212:0606:5252\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('46', '0', '785', '2017-06-30 12:16:21', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"-3\",\"notas\":\"SSSSSS\"},\"header\":{\"fecha\":\"2017-06-30 1212:0606:2121\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('47', '0', '785', '2017-07-03 10:29:09', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"-3\",\"notas\":\"Por vencimiento\"},\"header\":{\"fecha\":\"2017-07-03 1010:0707:0909\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('48', '0', '785', '2017-07-03 10:35:46', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"-3\",\"notas\":\"Vencimiento\"},\"header\":{\"fecha\":\"2017-07-03 1010:0707:4646\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('49', '0', '785', '2017-07-05 11:56:18', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"8\",\"notas\":\"pppppppp\"},\"header\":{\"fecha\":\"2017-07-05 1111:0707:1717\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('50', '0', '785', '2017-07-05 18:32:42', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"1\",\"notas\":\"dsfsdf\"},\"header\":{\"fecha\":\"2017-07-05 0606:0707:4141\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('51', '0', '785', '2017-07-05 18:33:04', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"1\",\"notas\":\"dsfsdf\"},\"header\":{\"fecha\":\"2017-07-05 0606:0707:0404\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('52', '0', '785', '2017-07-05 18:36:18', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"1\",\"notas\":\"asd\"},\"header\":{\"fecha\":\"2017-07-05 0606:0707:1717\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('53', '0', '785', '2017-07-05 18:37:13', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"1\",\"notas\":\"asdasd\"},\"header\":{\"fecha\":\"2017-07-05 0606:0707:1313\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('54', '0', '785', '2017-07-05 18:40:20', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"4\",\"descripcion\":\"Guantes de Nitrilo x 100 und.\",\"cantidad\":\"2\",\"notas\":\"asdasda\"},\"header\":{\"fecha\":\"2017-07-05 0606:0707:2020\",\"observaciones\":\"Ajuste Manual de Stock\"}}');
INSERT INTO `pedidos_insumos_log` VALUES ('55', '0', '785', '2017-07-05 18:40:42', 'Ajuste Manual del Insumo', '1', '{\"linea_0\":{\"producto_id\":\"1\",\"descripcion\":\"Reactivo para Laboratorio A1\",\"cantidad\":\"1\",\"notas\":\"asdasdas\"},\"header\":{\"fecha\":\"2017-07-05 0606:0707:4242\",\"observaciones\":\"Ajuste Manual de Stock\"}}');

-- ----------------------------
-- Table structure for per_personas
-- ----------------------------
DROP TABLE IF EXISTS `per_personas`;
CREATE TABLE `per_personas` (
  `per_id` int(11) NOT NULL AUTO_INCREMENT,
  `sicap_id` int(11) DEFAULT NULL,
  `per_nrodoc` char(14) CHARACTER SET latin1 NOT NULL,
  `per_tipodoc` char(2) CHARACTER SET latin1 DEFAULT NULL,
  `per_nombres` varchar(120) CHARACTER SET latin1 NOT NULL,
  `per_apellido` varchar(80) CHARACTER SET latin1 NOT NULL,
  `per_fnac` date NOT NULL,
  `per_sexo` char(1) CHARACTER SET latin1 NOT NULL,
  `ult_serologia` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `pais_nacimiento_otro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad_nacimiento_otra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pais_otro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad_otra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calle_otra` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`per_id`),
  UNIQUE KEY `nrodoc` (`per_nrodoc`),
  KEY `fnac` (`per_fnac`),
  KEY `sexo` (`per_sexo`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_personas
-- ----------------------------
INSERT INTO `per_personas` VALUES ('1', '2097677', '8', '1', 'ASDF', 'SDA', '1998-05-29', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('2', '2097777', '16793756', '1', 'IINES', 'BUSTAMANTE', '1964-01-18', 'F', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('3', '2097771', '22906007', '1', 'LILIANA BEATRIZ', 'DI TULLIO BUDASSI', '1972-06-29', 'F', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('4', '2097769', '29639965', '6', 'GUIDO NICOLAS', 'QUADRINI', '1982-11-06', 'M', '', null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('5', '2097764', '111', '1', 'GUILLERMO', 'TOMÃ‰', '1986-06-10', 'M', '14', null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('6', '2097763', '888', '1', 'PRRRR', 'PRUEBAP', '1999-03-13', 'M', '59', null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('7', '2097767', '7', '1', 'MARIANO', 'DIAZ', '1999-05-04', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('8', '2097778', '21635343', '1', 'LUIS ', 'FERRARI ', '1970-10-29', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('9', '2097779', '77', '1', 'JUAN', 'BAZAN', '1999-11-26', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('10', '2097625', '66666666', '1', 'HERNÃN', 'FUNES', '1983-12-29', '2', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('11', '2097780', '92494168', '1', 'FRANCISCO', 'JUAREZ', '1975-02-10', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('12', '2097670', '88', '1', 'PERFIL1', 'PRUEBA2', '1998-05-19', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('13', '2097781', '24945430', '1', 'DANIEL', 'FLORES', '1976-01-10', 'M', null, null, null, null, null, null);
INSERT INTO `per_personas` VALUES ('14', '2097782', '20298930', '1', 'RUBEN ALFREDO', 'DIAZ', '1999-10-06', 'M', null, '', '', '', '', '');
INSERT INTO `per_personas` VALUES ('15', '2097783', '20298931', '1', 'SUSANA', 'SUAREZ', '1998-07-08', 'F', null, '', '', '', '', 'Calle San Joaquin');
INSERT INTO `per_personas` VALUES ('16', '2097784', '20298932', '1', 'ROBERTO', 'GUTIERREZ', '1984-10-16', 'M', null, '', '', '', 'TatanÃ©', 'Ruta 4 Km. 196');
INSERT INTO `per_personas` VALUES ('17', '2097785', '20298933', '1', 'SANDRA', 'RODRIGUEZ', '2000-09-22', 'F', null, '', '', '', 'Sidney', 'Canguro');
INSERT INTO `per_personas` VALUES ('18', '2097786', '20298934', '1', 'JULIO', 'TOLEDO', '1977-09-19', 'M', null, '', 'Charata', '', '', '');
INSERT INTO `per_personas` VALUES ('19', '2097787', '20298936', '1', 'GABINO', 'SOSA', '2000-09-16', 'M', null, '', 'Charata', '', '', '');
INSERT INTO `per_personas` VALUES ('20', '2097788', '20298937', '1', 'JULIO ARGENTINO', 'ROCA', '1981-09-07', 'M', null, '', 'Berlin', '', '', '');
INSERT INTO `per_personas` VALUES ('21', '2097789', '20298935', '1', 'JORGE', 'MANZUR', '1969-05-06', 'M', null, '', '', '', '', '');

-- ----------------------------
-- Table structure for ppr_perfiles_produccion
-- ----------------------------
DROP TABLE IF EXISTS `ppr_perfiles_produccion`;
CREATE TABLE `ppr_perfiles_produccion` (
  `ppr_id` int(11) NOT NULL AUTO_INCREMENT,
  `ppr_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `ppr_genero` enum('A','F','M') CHARACTER SET latin1 NOT NULL DEFAULT 'A',
  `ppr_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`ppr_id`),
  UNIQUE KEY `nombre` (`ppr_descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ppr_perfiles_produccion
-- ----------------------------
INSERT INTO `ppr_perfiles_produccion` VALUES ('1', 'Alergia', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('2', 'Polimedicado/a', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('3', 'Politransfundido/a', 'A', '1');
INSERT INTO `ppr_perfiles_produccion` VALUES ('4', 'Nro. de Gestas', 'A', '1');

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productocategoria_id` int(11) DEFAULT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_producto` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '0',
  `ultimo_costo` decimal(10,2) DEFAULT NULL,
  `stock_punto_reposicion` decimal(10,2) DEFAULT NULL,
  `stock_minimo` decimal(10,2) DEFAULT NULL,
  `stock_maximo` decimal(10,2) DEFAULT NULL,
  `control_calidad` int(4) DEFAULT NULL,
  `proveedor_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `productocategoria_id` (`productocategoria_id`) USING BTREE,
  KEY `coidigo` (`codigo`) USING BTREE,
  KEY `nombre_producto` (`nombre_producto`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of productos
-- ----------------------------
INSERT INTO `productos` VALUES ('1', '1', 'REAC001', 'Reactivo para Laboratorio A1', '1', '230.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('2', '2', 'GUAN002', 'Guantes de Latex por 100 Und.', '1', '300.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('3', '1', 'FILT2221', 'FIltro para Laboratoio  A33', '1', '1200.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('4', '2', 'GUANIT', 'Guantes de Nitrilo x 100 und.', '1', '500.00', null, null, null, null, null, null, null);
INSERT INTO `productos` VALUES ('5', '1', 'Cloro5', 'Bidon de cloro x 5lts.', '1', '150.00', null, null, null, null, null, '2017-03-03 11:53:27', '1');
INSERT INTO `productos` VALUES ('6', '3', 'BB1', 'Reacitvo B1', '1', '100.00', null, null, null, null, null, '2017-06-01 12:35:01', '1');
INSERT INTO `productos` VALUES ('7', '3', 'BB22', 'Reacitvo B22', '1', '122.00', null, null, null, null, null, '2017-06-01 12:35:24', '1');

-- ----------------------------
-- Table structure for productos_categorias
-- ----------------------------
DROP TABLE IF EXISTS `productos_categorias`;
CREATE TABLE `productos_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(100) CHARACTER SET latin1 NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nombre:_categoria` (`nombre_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of productos_categorias
-- ----------------------------
INSERT INTO `productos_categorias` VALUES ('1', 'Insumos', '1');
INSERT INTO `productos_categorias` VALUES ('2', 'Material Descartable', '1');
INSERT INTO `productos_categorias` VALUES ('3', 'Reactivos', '1');
INSERT INTO `productos_categorias` VALUES ('4', 'Limpieza', '1');

-- ----------------------------
-- Table structure for serologia
-- ----------------------------
DROP TABLE IF EXISTS `serologia`;
CREATE TABLE `serologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `don_id` int(11) NOT NULL,
  `sifilis` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brucelosis` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chagas` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hepatitisB` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hepatitisC` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hiv` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htlv` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `validado` int(11) DEFAULT NULL,
  `usu_id_valida` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of serologia
-- ----------------------------
INSERT INTO `serologia` VALUES ('1', '48', null, null, 'Reactivo', 'Reactivo', 'Reactivo', 'Reactivo', 'No reactivo', '2015-11-18 11:09:13', '1', null, null);
INSERT INTO `serologia` VALUES ('2', '50', null, null, 'REACTIVO', 'REACTIVO', 'REACTIVO', 'REACTIVO', 'NO REACTIVO', '2015-11-18 12:07:11', '1', null, null);
INSERT INTO `serologia` VALUES ('3', '57', null, null, 'REACTIVO', 'REACTIVO', 'REACTIVO', 'REACTIVO', 'NO REACTIVO', '2015-11-18 12:08:12', '1', null, null);
INSERT INTO `serologia` VALUES ('4', '58', null, null, 'REACTIVO', 'REACTIVO', 'REACTIVO', 'REACTIVO', null, '2015-11-18 13:10:19', '1', null, null);
INSERT INTO `serologia` VALUES ('5', '59', null, null, 'REACTIVO', 'REACTIVO', 'NO REACTIVO', 'REACTIVO', 'REACTIVO', '2015-11-18 13:10:20', '1', null, null);
INSERT INTO `serologia` VALUES ('6', '60', null, null, 'NO REACTIVO', 'NO REACTIVO', 'NO REACTIVO', 'NO REACTIVO', 'NO REACTIVO', '2015-11-18 13:10:21', '1', null, null);
INSERT INTO `serologia` VALUES ('7', '62', 'Reactivo', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', '2015-11-20 11:51:58', '85', '1', '85');
INSERT INTO `serologia` VALUES ('8', '65', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', '2015-11-23 12:39:23', '85', '1', '85');
INSERT INTO `serologia` VALUES ('9', '62', 'Reactivo', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', '2015-11-24 11:12:55', '85', null, null);
INSERT INTO `serologia` VALUES ('10', '62', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', '2015-11-24 11:13:41', '85', null, null);
INSERT INTO `serologia` VALUES ('11', '62', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', '2015-11-24 11:13:56', '85', null, null);
INSERT INTO `serologia` VALUES ('12', '62', 'Reactivo', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', '2015-11-24 11:14:11', '85', '1', '85');
INSERT INTO `serologia` VALUES ('13', '66', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', '2015-11-26 08:08:04', '1', '1', '1');
INSERT INTO `serologia` VALUES ('14', '62', 'Reactivo', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', 'Sin Determinar', '2015-11-26 13:32:19', '85', null, null);
INSERT INTO `serologia` VALUES ('15', '49', null, null, 'REACTIVO', 'REACTIVO', null, 'REACTIVO', null, '2016-02-15 09:50:44', '1', null, null);
INSERT INTO `serologia` VALUES ('16', '51', null, null, 'REACTIVO', null, null, 'REACTIVO', 'REACTIVO', '2016-02-15 09:50:45', '1', null, null);
INSERT INTO `serologia` VALUES ('17', '68', null, null, null, 'REACTIVO', 'REACTIVO', null, null, '2016-02-15 09:50:46', '1', null, null);
INSERT INTO `serologia` VALUES ('18', '70', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', 'No reactivo', '2016-03-14 11:05:37', '1', null, null);

-- ----------------------------
-- Table structure for sol_solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `sol_solicitudes`;
CREATE TABLE `sol_solicitudes` (
  `sol_id` int(11) NOT NULL AUTO_INCREMENT,
  `sol_cod` char(32) CHARACTER SET latin1 NOT NULL,
  `sol_fh_ini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sol_fh_fin` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `per_id` char(20) CHARACTER SET latin1 NOT NULL,
  `efe_id` int(11) DEFAULT NULL,
  `usu_creacion` int(11) NOT NULL,
  `tdo_id` int(11) DEFAULT NULL,
  `afe_id` int(11) DEFAULT NULL,
  `ocu_id` int(11) NOT NULL,
  `usu_cierre` int(11) DEFAULT NULL,
  `mot_id` int(11) DEFAULT '0',
  `motivos_diferimiento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sol_diferido` tinyint(4) DEFAULT NULL,
  `sol_estado` tinyint(1) NOT NULL,
  `cph` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `plaqueta` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`sol_id`),
  KEY `per_id` (`per_id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sol_solicitudes
-- ----------------------------
INSERT INTO `sol_solicitudes` VALUES ('7', '18420151112112325', '2015-11-12 11:19:24', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('8', '18420151112112417', '2015-11-12 11:20:17', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('9', '18420151112112440', '2015-11-12 11:20:40', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('10', '18420151112114100', '2015-11-12 11:37:00', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '4', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('11', '18420151112115544', '2015-11-12 11:51:43', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '4', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('12', '18420151112124650', '2015-11-12 12:42:50', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('13', '18420151112124740', '2015-11-12 12:43:40', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('14', '18420151112124812', '2015-11-12 12:44:11', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('15', '18420151112124931', '2015-11-12 12:45:31', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('16', '18420151112125039', '2015-11-12 12:46:39', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('17', '18420151112125051', '2015-11-12 12:46:50', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('18', '18420151112125116', '2015-11-12 12:47:16', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('19', '18420151112125148', '2015-11-12 12:47:48', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('20', '18420151112125203', '2015-11-12 12:48:02', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('21', '18420151112125620', '2015-11-12 12:52:19', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('22', '18420151112125649', '2015-11-12 12:52:48', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('23', '18420151112125717', '2015-11-12 12:53:16', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('24', '18420151112125854', '2015-11-12 12:54:53', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('25', '18420151112125914', '2015-11-12 12:55:13', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('26', '18420151112125931', '2015-11-12 12:55:31', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('27', '18420151112125942', '2015-11-12 12:55:41', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('28', '18420151113084859', '2015-11-13 08:44:55', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('29', '18420151113084908', '2015-11-13 08:45:04', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('30', '18420151113085044', '2015-11-13 08:46:40', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('31', '18420151113085129', '2015-11-13 08:47:26', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('32', '18420151113085213', '2015-11-13 08:48:09', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('33', '18420151113085536', '2015-11-13 08:51:33', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('34', '18420151113090031', '2015-11-13 08:56:28', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('35', '18420151113090033', '2015-11-13 08:56:29', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('36', '18420151113090034', '2015-11-13 08:56:31', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('37', '18420151113090117', '2015-11-13 08:57:13', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('38', '18420151113090137', '2015-11-13 08:57:34', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('39', '18420151113090212', '2015-11-13 08:58:08', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('40', '18420151113090247', '2015-11-13 08:58:43', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('41', '18420151113090259', '2015-11-13 08:58:55', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('42', '18420151113091139', '2015-11-13 09:07:35', '0000-00-00 00:00:00', '4', '184', '1', '1', '4', '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('43', '18420151113091204', '2015-11-13 09:08:01', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('44', '18420151113091358', '2015-11-13 09:09:54', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('45', '18420151113091522', '2015-11-13 09:11:19', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('46', '18420151113091530', '2015-11-13 09:11:26', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('47', '18420151113091601', '2015-11-13 09:11:57', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('48', '18420151113091609', '2015-11-13 09:12:05', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('49', '18420151113091643', '2015-11-13 09:12:39', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('50', '18420151113093740', '2015-11-13 09:33:36', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('51', '18420151113094625', '2015-11-13 09:42:21', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('52', '18420151113102918', '2015-11-13 10:25:14', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('53', '18420151113102956', '2015-11-13 10:25:52', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('54', '18420151113103022', '2015-11-13 10:26:18', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('55', '18420151113103250', '2015-11-13 10:28:46', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('56', '18420151113103956', '2015-11-13 10:35:52', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('57', '18420151113110606', '2015-11-13 11:02:02', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('58', '18420151113110718', '2015-11-13 11:03:14', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('59', '18420151113110833', '2015-11-13 11:04:29', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('60', '18420151113111230', '2015-11-13 11:08:26', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('61', '18420151113112039', '2015-11-13 11:16:35', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('62', '18420151113112958', '2015-11-13 11:25:54', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('63', '18420151113113055', '2015-11-13 11:26:51', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('64', '18420151113125219', '2015-11-13 12:48:15', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('65', '18420151113125309', '2015-11-13 12:49:05', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('66', '18420151113125701', '2015-11-13 12:52:57', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('67', '18420151113125823', '2015-11-13 12:54:19', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('68', '18420151113125922', '2015-11-13 12:55:18', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '2', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('69', '18420151113130017', '2015-11-13 12:56:13', '2015-11-13 12:56:13', '4', '184', '1', '2', null, '2', '1', '0', null, null, '0', null, null);
INSERT INTO `sol_solicitudes` VALUES ('70', '18420151113130056', '2015-11-13 12:56:52', '2015-11-13 12:56:52', '4', '184', '1', '2', null, '2', '1', '0', null, null, '0', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('71', '18420151113130129', '2015-11-13 12:57:25', '2015-11-13 12:57:25', '4', '184', '1', '2', null, '2', '1', '0', null, null, '0', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('72', '18420151113130246', '2015-11-13 12:58:42', '2015-11-13 12:58:42', '4', '184', '1', '2', null, '2', '1', '0', null, null, '0', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('73', '18420151113130338', '2015-11-13 12:59:34', '2015-11-13 12:59:34', '4', '184', '1', '2', null, '2', '1', '0', null, null, '0', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('74', '18420151113130533', '2015-11-13 13:01:28', '2015-11-13 13:01:29', '4', '184', '1', '2', null, '2', '1', '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('75', '18420151113130635', '2015-11-13 13:02:30', '2015-11-13 13:02:31', '4', '184', '1', '2', null, '2', '1', '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('76', '18420151113130711', '2015-11-13 13:03:07', '2015-11-13 13:03:07', '4', '184', '1', '2', null, '2', '1', '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('77', '18420151113130847', '2015-11-13 13:04:43', '2015-11-13 13:04:43', '4', '184', '1', '2', null, '2', '1', '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('78', '18420151113130939', '2015-11-13 13:05:35', '2015-11-13 13:05:35', '4', '184', '1', '2', null, '2', '1', '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('79', '18420151116091407', '2015-11-16 09:09:54', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('80', '18420151116091515', '2015-11-16 09:11:02', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('81', '18420151116092532', '2015-11-16 09:21:19', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('82', '18420151116101449', '2015-11-16 10:10:36', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('83', '18420151116101838', '2015-11-16 10:14:25', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('84', '18420151116102834', '2015-11-16 10:24:21', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('85', '18420151116103324', '2015-11-16 10:29:11', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('86', '18420151116103433', '2015-11-16 10:30:19', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('87', '18420151116103554', '2015-11-16 10:31:41', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('88', '18420151116103750', '2015-11-16 10:33:36', '2016-10-31 17:14:20', '4', '184', '1', '2', null, '9', '1', '0', null, null, '4', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('89', '18420151116103818', '2015-11-16 10:34:04', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('90', '18420151116103858', '2015-11-16 10:34:45', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('91', '18420151116103911', '2015-11-16 10:34:57', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('92', '18420151116104151', '2015-11-16 10:37:37', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('93', '18420151116104223', '2015-11-16 10:38:09', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('94', '18420151116104315', '2015-11-16 10:39:02', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('95', '18420151116104332', '2015-11-16 10:39:18', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('96', '18420151116110755', '2015-11-16 11:03:42', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('97', '18420151116110820', '2015-11-16 11:04:06', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('98', '18420151116110838', '2015-11-16 11:04:24', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('99', '18420151116110849', '2015-11-16 11:04:36', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('100', '18420151116110924', '2015-11-16 11:05:11', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('101', '18420151116110948', '2015-11-16 11:05:35', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('102', '18420151116111412', '2015-11-16 11:09:59', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('103', '18420151116111446', '2015-11-16 11:10:33', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '10', null, '0', null, null, '5', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('104', '18420151117085739', '2015-11-17 08:53:26', '2015-11-17 08:56:37', '4', '184', '1', '2', null, '1', '1', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('105', '18420151117104347', '2015-11-17 10:39:34', '2015-11-17 10:39:48', '4', '184', '1', '2', null, '1', '1', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('106', '18420151117121014', '2015-11-17 12:06:00', '0000-00-00 00:00:00', '1', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('107', '18420151117124645', '2015-11-17 12:42:32', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('108', '18420151117125429', '2015-11-17 12:50:16', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('109', '18420151117130017', '2015-11-17 12:56:04', '0000-00-00 00:00:00', '4', '184', '1', null, null, '1', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('110', '18420151117130033', '2015-11-17 12:56:19', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('111', '18420151117130203', '2015-11-17 12:57:49', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('112', '18420151117130229', '2015-11-17 12:58:15', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('113', '18420151117131132', '2015-11-17 13:07:18', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('114', '18420151117131601', '2015-11-17 13:11:47', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('115', '18420151117131722', '2015-11-17 13:13:08', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('116', '18420151117131917', '2015-11-17 13:15:03', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('117', '18420151117132618', '2015-11-17 13:22:04', '0000-00-00 00:00:00', '4', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('118', '18420151118122501', '2015-11-18 12:20:46', '0000-00-00 00:00:00', '6', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('119', '18420151118122600', '2015-11-18 12:21:45', '0000-00-00 00:00:00', '6', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('120', '18420151118122614', '2015-11-18 12:21:58', '0000-00-00 00:00:00', '6', '184', '1', '2', null, '10', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('121', '65820151120094706', '2015-11-20 09:47:06', '0000-00-00 00:00:00', '8', '658', '89', '1', '4', '3', null, '0', null, null, '4', '0', '1');
INSERT INTO `sol_solicitudes` VALUES ('122', '78520151120103004', '2015-11-20 10:30:04', '2015-11-20 11:25:39', '5', '785', '85', '2', null, '1', '85', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('123', '78520151120112635', '2015-11-20 11:26:35', '2015-11-20 11:27:30', '5', '785', '85', '2', null, '1', '85', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('124', '65820151120121814', '2015-11-20 12:18:15', '0000-00-00 00:00:00', '8', '658', '89', '2', null, '1', null, '0', null, null, '1', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('125', '18420151123084142', '2015-11-23 08:37:12', '2016-09-26 12:49:05', '6', '184', '1', '2', null, '1', '1', '0', null, null, '4', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('126', '78520151123123715', '2015-11-23 12:37:16', '2015-11-23 12:38:01', '1', '785', '85', '2', null, '1', '85', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('127', '78520151123130139', '2015-11-23 13:01:40', '2015-11-23 13:02:29', '1', '785', '85', '2', null, '1', '85', '1', null, '12', '2', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('128', '78520151123130552', '2015-11-23 13:05:52', '0000-00-00 00:00:00', '7', '785', '85', '2', null, '1', null, '0', null, null, '4', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('129', '18420151126080442', '2015-11-26 08:04:43', '2015-11-26 08:06:59', '9', '184', '1', '2', null, '1', '1', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('130', '18420160122090801', '2016-01-22 09:08:01', '2016-01-22 09:09:21', '1', '184', '1', '2', null, '1', '1', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('131', '18420160215094628', '2016-02-15 09:38:20', '0000-00-00 00:00:00', '1', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('132', '18420160309084429', '2016-03-09 08:44:29', '2017-08-29 11:47:13', '4', '184', '1', '2', null, '1', '1', '5', null, '0', '2', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('133', '124920160309091454', '2016-03-09 09:14:54', '2016-03-23 11:54:36', '11', '1249', '90', '2', null, '11', '90', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('134', '18420160314105616', '2016-03-14 10:46:54', '0000-00-00 00:00:00', '1', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('135', '18420160314105940', '2016-03-14 10:50:18', '0000-00-00 00:00:00', '12', '184', '1', '2', null, '9', null, '0', null, null, '5', null, null);
INSERT INTO `sol_solicitudes` VALUES ('136', '65720160318101356', '2016-03-18 10:13:57', '0000-00-00 00:00:00', '13', '657', '91', '2', null, '1', null, '0', null, null, '1', '1', '1');
INSERT INTO `sol_solicitudes` VALUES ('137', '18420160328114034', '2016-03-28 11:40:34', '2016-09-26 13:27:12', '1', '184', '1', '2', null, '1', '1', '0', null, null, '4', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('138', '78520160926163723', '2016-09-26 16:37:23', '0000-00-00 00:00:00', '4', '785', '1', '2', null, '9', null, '0', null, null, '1', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('139', '78520161028171858', '2016-10-28 17:18:58', '0000-00-00 00:00:00', '14', '785', '1', '2', null, '4', null, '0', null, null, '1', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('140', '78520161222113403', '2016-12-22 11:34:03', '2016-12-22 11:42:40', '2', '785', '1', '2', null, '9', '1', '0', null, null, '4', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('141', '78520170609083131', '2017-06-09 08:31:31', '2017-06-09 09:03:12', '21', '785', '1', '2', null, '4', '1', '0', null, null, '5', '0', '0');
INSERT INTO `sol_solicitudes` VALUES ('142', '78520170807104009', '2017-08-07 10:40:09', '0000-00-00 00:00:00', '21', '785', '1', '3', null, '1', null, '0', null, null, '1', '0', '0');

-- ----------------------------
-- Table structure for stock
-- ----------------------------
DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `efector_id` int(11) DEFAULT NULL,
  `donacion_id` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_descarte` datetime DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fecha_vencimiento` datetime DEFAULT NULL,
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `usu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `donacion_id` (`donacion_id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `hemocomponente_id` (`hemocomponente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock
-- ----------------------------
INSERT INTO `stock` VALUES ('1', '184', '69', '1', '2016-10-12', null, '1', 'A', '+', null, '0', '600.000', '450.000', '2016-03-30 16:31:00', '0', null, null);
INSERT INTO `stock` VALUES ('2', '184', '70', '2', '2016-10-13', '0000-00-00 00:00:00', '1', '0', '+', null, '0', '500.000', '320.000', '2017-03-30 16:31:00', '0', '', null);

-- ----------------------------
-- Table structure for stock_descartes
-- ----------------------------
DROP TABLE IF EXISTS `stock_descartes`;
CREATE TABLE `stock_descartes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `efector_id` int(11) DEFAULT NULL,
  `donacion_id` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_descarte` datetime DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fecha_vencimiento` datetime DEFAULT NULL,
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `donacion_id` (`donacion_id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `hemocomponente_id` (`hemocomponente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_descartes
-- ----------------------------
INSERT INTO `stock_descartes` VALUES ('1', '184', '69', '1', '2016-10-12', null, '1', 'A', '+', null, '0', '600.000', '450.000', '2016-03-30 16:31:00', '0', null, null);
INSERT INTO `stock_descartes` VALUES ('2', '184', '70', '2', '2016-10-13', '0000-00-00 00:00:00', '1', '0', '+', null, '0', '500.000', '320.000', '2017-03-30 16:31:00', '0', '', null);

-- ----------------------------
-- Table structure for stock_descartes_renamed_y_no_se_x_cual
-- ----------------------------
DROP TABLE IF EXISTS `stock_descartes_renamed_y_no_se_x_cual`;
CREATE TABLE `stock_descartes_renamed_y_no_se_x_cual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT '0',
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factor` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupo_inverso` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `irradiado` tinyint(4) DEFAULT NULL COMMENT 'Si fue irradiado',
  `peso_produccion` decimal(10,3) DEFAULT NULL COMMENT 'Peso registrado en produccion',
  `cantidad` decimal(10,3) DEFAULT NULL COMMENT 'Peso de lo que se usó.',
  `fraccionado` tinyint(4) DEFAULT NULL COMMENT 'Si fue fraccionado',
  `observaciones` text COLLATE utf8_unicode_ci,
  `fecha_consolidacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `fecha_consolidacion` (`fecha_consolidacion`),
  KEY `fecha` (`fecha`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_descartes_renamed_y_no_se_x_cual
-- ----------------------------
INSERT INTO `stock_descartes_renamed_y_no_se_x_cual` VALUES ('1', null, '285', '1', 'A', '+', null, '0', '600.000', '0.000', '0', null, null);

-- ----------------------------
-- Table structure for stock_insumos_movimientos
-- ----------------------------
DROP TABLE IF EXISTS `stock_insumos_movimientos`;
CREATE TABLE `stock_insumos_movimientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `tipomovimientoajuste_id` int(11) DEFAULT NULL,
  `pedido_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(16,2) DEFAULT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  `fecha_consolidado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`),
  KEY `pedido_id` (`pedido_id`),
  KEY `tipomovimientoajuste_id` (`tipomovimientoajuste_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_insumos_movimientos
-- ----------------------------
INSERT INTO `stock_insumos_movimientos` VALUES ('26', '2017-05-18', '785', '1', '1', '1', '5.00', 'L1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('27', '2017-05-18', '785', '1', '1', '2', '6.00', 'L2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('28', '2017-05-18', '785', '1', '1', '3', '7.00', 'L3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('29', '2017-05-18', '785', '1', '1', '4', '8.00', 'L4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('30', '2017-05-18', '785', '1', '1', '5', '9.00', 'L5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('31', '2017-05-19', '785', '1', '1', '1', '2.00', 'AA1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('32', '2017-05-19', '785', '1', '1', '2', '2.00', 'AA2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('33', '2017-05-19', '785', '1', '1', '3', '2.00', 'AA3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('34', '2017-05-19', '785', '1', '1', '4', '2.00', 'AA4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('35', '2017-05-19', '785', '1', '1', '5', '2.00', 'AA5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('36', '2017-05-19', '785', '1', '1', '1', '3.00', 'M1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('37', '2017-05-19', '785', '1', '1', '2', '3.00', 'M2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('38', '2017-05-19', '785', '1', '1', '3', '3.00', 'M3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('39', '2017-05-19', '785', '1', '1', '4', '3.00', 'M4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('40', '2017-05-19', '785', '1', '1', '5', '3.00', 'M5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('41', '2017-05-19', '785', '1', '1', '1', '3.00', 'M1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('42', '2017-05-19', '785', '1', '1', '2', '3.00', 'M2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('43', '2017-05-19', '785', '1', '1', '3', '3.00', 'M3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('44', '2017-05-19', '785', '1', '1', '4', '3.00', 'M4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('45', '2017-05-19', '785', '1', '1', '5', '3.00', 'M5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('46', '2017-05-19', '785', '1', '1', '1', '3.00', 'M1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('47', '2017-05-19', '785', '1', '1', '2', '3.00', 'M2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('48', '2017-05-19', '785', '1', '1', '3', '3.00', 'M3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('49', '2017-05-19', '785', '1', '1', '4', '3.00', 'M4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('50', '2017-05-19', '785', '1', '1', '5', '3.00', 'M5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('51', '2017-05-19', '785', '1', '1', '1', '4.00', 'B1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('52', '2017-05-19', '785', '1', '1', '2', '4.00', 'B2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('53', '2017-05-19', '785', '1', '1', '3', '4.00', 'B3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('54', '2017-05-19', '785', '1', '1', '4', '4.00', 'B4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('55', '2017-05-19', '785', '1', '1', '5', '4.00', 'B5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('56', '2017-05-19', '785', '1', '1', '1', '4.00', 'B1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('57', '2017-05-19', '785', '1', '1', '2', '4.00', 'B2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('58', '2017-05-19', '785', '1', '1', '3', '4.00', 'B3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('59', '2017-05-19', '785', '1', '1', '4', '4.00', 'B4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('60', '2017-05-19', '785', '1', '1', '5', '4.00', 'B5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('61', '2017-05-19', '785', '1', '1', '1', '1.00', 'D1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('62', '2017-05-19', '785', '1', '1', '2', '1.00', 'D2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('63', '2017-05-19', '785', '1', '1', '3', '1.00', 'D3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('64', '2017-05-19', '785', '1', '1', '4', '1.00', 'D4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('65', '2017-05-19', '785', '1', '1', '5', '1.00', 'D5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('66', '2017-05-19', '785', '1', '1', '1', '2.00', 'C1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('67', '2017-05-19', '785', '1', '1', '2', '2.00', 'C2', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('68', '2017-05-19', '785', '1', '1', '3', '2.00', 'C3', '2017-05-26 15:07:16');
INSERT INTO `stock_insumos_movimientos` VALUES ('69', '2017-05-19', '785', '1', '1', '4', '2.00', 'C4', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('70', '2017-05-19', '785', '1', '1', '5', '2.00', 'C5', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('71', '2017-05-22', '785', '1', '3', '1', '10.00', 'PRUEBA 1 LINEA 1', '2017-05-26 15:07:15');
INSERT INTO `stock_insumos_movimientos` VALUES ('72', '2017-05-22', '785', '1', '3', '4', '10.00', 'PRUEBA 1 LINEA 2', '2017-05-26 15:07:17');
INSERT INTO `stock_insumos_movimientos` VALUES ('73', '2017-05-26', '785', '1', '2', '1', '10.00', 'CUUUUU', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('74', '2017-05-26', '785', '1', '2', '1', '3.00', 'LALALA LANS', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('75', '2017-05-26', '785', '1', '3', '1', '3.00', 'GGG', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('76', '2017-05-26', '785', '1', '3', '4', '4.00', 'HHHHHH', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('77', '2017-05-26', '785', '1', '2', '1', '2.00', 'GGHHEE', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('78', '2017-05-26', '785', '1', '2', '1', '2.00', 'GGHHEE', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('79', '2017-05-26', '785', '1', '1', '1', '3.00', 'R1', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('80', '2017-05-26', '785', '1', '1', '2', '4.00', 'R2', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('81', '2017-05-26', '785', '1', '1', '3', '5.00', 'R3', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('82', '2017-05-26', '785', '1', '1', '4', '6.00', 'R4', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('83', '2017-05-26', '785', '1', '1', '5', '7.00', 'R5', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('84', '2017-05-26', '785', '1', '3', '1', '2.00', 'A1', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('85', '2017-05-26', '785', '1', '3', '4', '2.00', 'A2', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('86', '2017-05-26', '785', '1', '3', '1', '2.00', 'A1', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('87', '2017-05-26', '785', '1', '3', '4', '2.00', 'A2', '2017-05-26 16:33:46');
INSERT INTO `stock_insumos_movimientos` VALUES ('88', '2017-05-26', '785', '1', '2', '1', '5.00', 'TEST 5', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('89', '2017-05-26', '785', '1', '2', '1', '5.00', 'TEST 5', '2017-05-26 16:33:45');
INSERT INTO `stock_insumos_movimientos` VALUES ('90', '2017-06-01', '785', '1', '6', '1', '300.00', 'recibimos la mitad', '2017-06-01 11:56:43');
INSERT INTO `stock_insumos_movimientos` VALUES ('91', '2017-06-13', '785', '1', '2', '1', '2.00', 'Unidades basicas de control', '2017-07-07 11:31:06');
INSERT INTO `stock_insumos_movimientos` VALUES ('92', '2017-06-30', '785', '2', '0', '5', '-2.00', 'ajuste por robo', '2017-07-07 11:31:08');
INSERT INTO `stock_insumos_movimientos` VALUES ('93', '2017-06-30', '785', '1', '0', '4', '3.00', 'Mal contado', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('94', '2017-06-30', '785', '1', '0', '4', '12.00', 'AJUSTE EN POSITIVO', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('95', '2017-06-30', '785', '3', '0', '4', '-3.00', 'SSSSSS', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('96', '2017-06-30', '785', '3', '0', '4', '-3.00', 'SSSSSS', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('97', '2017-07-03', '785', '2', '0', '4', '-3.00', 'Por vencimiento', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('98', '2017-07-03', '785', '2', '0', '4', '-3.00', 'Vencimiento', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('99', '2017-07-05', '785', '3', '0', '4', '8.00', 'pppppppp', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('100', '2017-07-05', '785', '1', '0', '4', '1.00', 'dsfsdf', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('101', '2017-07-05', '785', '1', '0', '4', '1.00', 'dsfsdf', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('102', '2017-07-05', '785', '1', '0', '4', '1.00', 'asd', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('103', '2017-07-05', '785', '1', '0', '4', '1.00', 'asdasd', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('104', '2017-07-05', '785', '1', '0', '4', '2.00', 'asdasda', '2017-07-07 11:31:07');
INSERT INTO `stock_insumos_movimientos` VALUES ('105', '2017-07-05', '785', '1', '0', '1', '1.00', 'asdasdas', '2017-07-07 11:31:06');

-- ----------------------------
-- Table structure for stock_insumos_movimientos_consolidados
-- ----------------------------
DROP TABLE IF EXISTS `stock_insumos_movimientos_consolidados`;
CREATE TABLE `stock_insumos_movimientos_consolidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `producto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha` (`fecha`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stock_insumos_movimientos_consolidados
-- ----------------------------
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('1', '2017-05-26 15:07:15', '785', '1', '37.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('2', '2017-05-26 15:07:16', '785', '2', '28.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('3', '2017-05-26 15:07:16', '785', '3', '29.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('4', '2017-05-26 15:07:17', '785', '4', '40.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('5', '2017-05-26 15:07:17', '785', '5', '31.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('6', '2017-05-26 16:33:45', '785', '1', '37.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('7', '2017-05-26 16:33:45', '785', '2', '4.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('8', '2017-05-26 16:33:45', '785', '3', '5.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('9', '2017-05-26 16:33:46', '785', '4', '14.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('10', '2017-05-26 16:33:46', '785', '5', '7.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('11', '2017-06-01 11:56:43', '785', '1', '300.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('12', '2017-07-07 11:31:06', '785', '1', '3.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('13', '2017-07-07 11:31:07', '785', '4', '17.00');
INSERT INTO `stock_insumos_movimientos_consolidados` VALUES ('14', '2017-07-07 11:31:08', '785', '5', '-2.00');

-- ----------------------------
-- Table structure for stock_movimientos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `stock_movimientos_detalle`;
CREATE TABLE `stock_movimientos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `don_id` int(11) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `abo` varchar(5) DEFAULT NULL,
  `rh` varchar(1) DEFAULT NULL,
  `irradiado` int(1) DEFAULT NULL,
  `cantidad` decimal(10,3) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `observaciones` text,
  PRIMARY KEY (`id`),
  KEY `don_id` (`don_id`),
  KEY `stock_id` (`stock_id`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `fecha` (`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_movimientos_detalle
-- ----------------------------

-- ----------------------------
-- Table structure for stock_solicitudes
-- ----------------------------
DROP TABLE IF EXISTS `stock_solicitudes`;
CREATE TABLE `stock_solicitudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(4) DEFAULT NULL COMMENT '1 = Pendiente (pedido/solicitud abierta)\r\n2 = Comprometida por un envio (respuesta a una solicitud)\r\n3 = Pendiente Recepción  (respondida con un cantidad y pendiente de ingreso)\r\n4 = Cerrada   (cerrada / finalizada)\r\n5 = Descartada',
  `fecha` datetime DEFAULT NULL,
  `efector_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `responde_efector_id` int(11) DEFAULT NULL,
  `responde_fecha` datetime DEFAULT NULL,
  `responde_usuario_id` int(11) DEFAULT NULL,
  `hemocomponente_id` int(11) DEFAULT NULL,
  `grupo` varchar(5) DEFAULT NULL,
  `factor` varchar(1) DEFAULT NULL,
  `grupo_inverso` varchar(5) DEFAULT NULL,
  `irradiado` int(1) DEFAULT NULL,
  `fenotipo` varchar(50) DEFAULT NULL,
  `prioridad` int(4) DEFAULT NULL COMMENT '1 Baja, 2 Media, 3 Alta, 4 Urgente',
  `cantidad_solicitada` decimal(10,3) DEFAULT NULL,
  `cantidad_entregada` decimal(10,3) DEFAULT NULL,
  `observaciones` text,
  `donacion_id` int(11) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `cierre_usuario_id` int(11) DEFAULT NULL,
  `cierre_fecha` datetime DEFAULT NULL,
  `cierre_observaciones` text,
  PRIMARY KEY (`id`),
  KEY `estado` (`estado`),
  KEY `fecha` (`fecha`),
  KEY `efector_id` (`efector_id`),
  KEY `fecha_vencimiento` (`fecha_vencimiento`),
  KEY `hemocomponente_id` (`hemocomponente_id`),
  KEY `prioridad` (`prioridad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stock_solicitudes
-- ----------------------------
INSERT INTO `stock_solicitudes` VALUES ('1', '1', '2016-11-01 11:38:22', '184', '3', '2016-11-05 11:38:42', '0', '0000-00-00 00:00:00', '0', '3', 'A', '-', null, '1', null, '3', '20.000', null, null, '1111', null, null, null, null);
INSERT INTO `stock_solicitudes` VALUES ('2', '3', '2017-01-13 08:18:09', '184', '3', '2017-01-28 08:18:21', '0', '0000-00-00 00:00:00', '0', '5', 'AB', '+', null, '0', null, '1', '7.000', '3.000', 'anular la parte superior', '2222', null, null, null, null);
INSERT INTO `stock_solicitudes` VALUES ('3', '3', '2017-01-15 08:56:45', '184', '3', '2017-01-27 08:56:57', '0', '0000-00-00 00:00:00', '0', '4', '0', '+', null, '0', null, '2', '5.000', '5.000', 'verificar correctamente', '3333', null, null, null, null);
INSERT INTO `stock_solicitudes` VALUES ('4', '1', '2017-03-01 00:00:00', '785', '1', '2017-06-24 09:13:36', '0', null, null, '5', '0', '+', null, '1', null, null, '1.000', null, 'AAAA', null, '2017-03-22 10:59:50', null, null, null);

-- ----------------------------
-- Table structure for tdo_tipos_donaciones
-- ----------------------------
DROP TABLE IF EXISTS `tdo_tipos_donaciones`;
CREATE TABLE `tdo_tipos_donaciones` (
  `tdo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tdo_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tdo_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tdo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tdo_tipos_donaciones
-- ----------------------------
INSERT INTO `tdo_tipos_donaciones` VALUES ('1', 'Aferesis', '1');
INSERT INTO `tdo_tipos_donaciones` VALUES ('2', 'Donante', '1');
INSERT INTO `tdo_tipos_donaciones` VALUES ('3', 'Pre-Donante', '1');

-- ----------------------------
-- Table structure for tipos_movimentos_stock
-- ----------------------------
DROP TABLE IF EXISTS `tipos_movimentos_stock`;
CREATE TABLE `tipos_movimentos_stock` (
  `tipo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_descripcion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tipo_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tipo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipos_movimentos_stock
-- ----------------------------
INSERT INTO `tipos_movimentos_stock` VALUES ('1', 'Ingreso en Almacen', '1');
INSERT INTO `tipos_movimentos_stock` VALUES ('2', 'Egreso de Almacen', '1');
INSERT INTO `tipos_movimentos_stock` VALUES ('3', 'Egreso por Vencimiento', '1');
INSERT INTO `tipos_movimentos_stock` VALUES ('4', 'Egreso por Descarte', '1');

-- ----------------------------
-- Table structure for ____stock_insumos_detalle
-- ----------------------------
DROP TABLE IF EXISTS `____stock_insumos_detalle`;
CREATE TABLE `____stock_insumos_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insumo_id` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `proucto_id` int(11) DEFAULT NULL,
  `cantidad` decimal(10,2) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `notas` text COLLATE utf8_unicode_ci,
  `fecha_alta` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `insumos_id` (`insumo_id`) USING BTREE,
  KEY `productos_id` (`proucto_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ____stock_insumos_detalle
-- ----------------------------
