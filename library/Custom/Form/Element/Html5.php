 <?php

    /** Zend_Form_Element_Xhtml */
    require_once 'Zend/Form/Element/Xhtml.php';


    class Custom_Form_Element_Html5 extends Zend_Form_Element_Xhtml
    {
        public $helper = 'formHtml5';
    }