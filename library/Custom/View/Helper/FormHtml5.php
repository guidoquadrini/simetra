<?php


    /**
    * Abstract class for extension
    */
    require_once 'Zend/View/Helper/FormElement.php';


    /**
    * Helper to generate an "Html5" element
    *
    */
    class Custom_View_Helper_FormHtml5 extends Zend_View_Helper_FormElement
    {

        public function formHtml5($name, $value = null, $attribs = null)
        {
            $info = $this->_getInfo($name, $value, $attribs);
            extract($info); // name, value, attribs, options, listsep, disable

            // build the element
            $disabled = '';
            if ($disable) {
                // disabled
                $disabled = ' disabled="disabled"';
            }

            // XHTML or HTML end tag?
            $endTag = ' />';
            if (($this->view instanceof Zend_View_Abstract) && !$this->view->doctype()->isXhtml()) {
                $endTag= '>';
            }

            $xhtml = '<input'
                    . ' type="' . (($attribs['type'])?($this->view->escape($attribs['type'])):'text') . '"'
                    . ' name="' . $this->view->escape($name) . '"'
                    . ' id="' . $this->view->escape($id) . '"'
                    . ' value="' . $this->view->escape($value) . '"'
                    . $disabled
                    . $this->_htmlAttribs($attribs)
                    . $endTag;

            return $xhtml;
        }
    }
