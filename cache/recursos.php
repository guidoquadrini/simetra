<?php
return array (
  'modules' => 
  array (
    'acceso' => 
    array (
      'efector' => 
      array (
        1 => 'asociar',
        3 => 'defecto',
        4 => 'defectoajax',
        0 => 'index',
        2 => 'remover',
      ),
      'recursos' => 
      array (
        2 => 'agregar',
        1 => 'assoc',
        5 => 'build-controller',
        4 => 'build-modules',
        6 => 'buildactions',
        0 => 'index',
        3 => 'remover',
      ),
      'roles' => 
      array (
        1 => 'add',
        2 => 'del',
        3 => 'edit',
        0 => 'index',
        4 => 'manage',
      ),
      'usuarios' => 
      array (
        2 => 'add',
        8 => 'agregar',
        7 => 'assoc',
        4 => 'cambiarpassword',
        12 => 'control-credenciales-usuario',
        11 => 'control-permisos',
        3 => 'edit',
        0 => 'index',
        6 => 'manage',
        5 => 'procesocambiarpassword',
        10 => 'proyecto',
        9 => 'quitar',
        1 => 'ver',
      ),
    ),
    'banco' => 
    array (
      'donaciones' => 
      array (
        1 => 'bolsa',
        0 => 'index',
      ),
      'informes' => 
      array (
        2 => 'imprimir-donaciones',
        3 => 'imprimir-inmunologia',
        1 => 'inmunologia',
        0 => 'pacientes',
      ),
      'inmunologia' => 
      array (
        2 => 'busqueda',
        5 => 'exportar',
        3 => 'importar',
        0 => 'index',
        4 => 'mostrar-exportar',
        6 => 'mostrar-importar',
        7 => 'mostrar-tratar-envio',
        8 => 'mostrar-validar',
        10 => 'planillapdf',
        1 => 'registrar',
        9 => 'verpdf',
      ),
      'preproduccion' => 
      array (
        0 => 'index',
        2 => 'producir-mayores',
        1 => 'producir-menores',
        3 => 'producir-simulacion',
      ),
      'produccion' => 
      array (
        11 => 'agregar-hemo',
        1 => 'confirmar',
        14 => 'descartar',
        13 => 'descarte',
        2 => 'eliminar-row',
        9 => 'enviar',
        6 => 'envios',
        7 => 'historial',
        0 => 'index',
        3 => 'informe',
        8 => 'irradiar',
        10 => 'pdf-envio',
        5 => 'stock',
        4 => 'validar',
        12 => 'verpdf',
        15 => 'verpdfenvio',
      ),
      'recepcion' => 
      array (
        7 => 'buscar',
        5 => 'buscar-items-envio',
        0 => 'index',
        6 => 'modificar',
        4 => 'mostrar-registrar-item',
        1 => 'recepcion',
        2 => 'recepcion-multiple',
        3 => 'ver',
      ),
      'serologia' => 
      array (
        3 => 'busqueda',
        6 => 'exportar',
        7 => 'exportar-manual',
        1 => 'formulario-ingreso-manual',
        0 => 'index',
        8 => 'informe',
        2 => 'registrar',
        4 => 'upload',
        9 => 'validar',
        5 => 'visualizador',
      ),
    ),
    'efector' => 
    array (
      'aferesis' => 
      array (
        7 => 'agregar-datos',
        4 => 'agregar-muni',
        10 => 'constancia',
        5 => 'descartar',
        1 => 'estado',
        11 => 'fes',
        8 => 'fesconstancia',
        3 => 'finalizar',
        6 => 'form-fes',
        0 => 'index',
        9 => 'tickets',
        2 => 'ver',
      ),
      'entrevistas' => 
      array (
        1 => 'alta',
        2 => 'formulario-entrevista',
        3 => 'grabar',
        0 => 'index',
        4 => 'verpdf',
      ),
      'envios' => 
      array (
        1 => 'alta',
        8 => 'buscar',
        5 => 'cantidaddonacionesenvio',
        4 => 'enviodetallemodal',
        9 => 'imprimir',
        0 => 'index',
        7 => 'mostrar-editar',
        2 => 'registrar',
        3 => 'ver',
        6 => 'verpdf',
      ),
      'examenes' => 
      array (
        2 => 'cambiar-estado',
        3 => 'codigos',
        1 => 'formulario-examen',
        0 => 'index',
      ),
      'extraccion' => 
      array (
        5 => 'agregar-muni',
        13 => 'cambiar-estado',
        12 => 'cod-tubuladura-fue-utilizado',
        10 => 'constancia',
        6 => 'descartar',
        4 => 'extraccion-multiple',
        8 => 'fesconstancia',
        3 => 'finalizar',
        7 => 'form-fes',
        1 => 'formulario-extraccion',
        11 => 'formularioautoexclusion',
        0 => 'index',
        9 => 'tickets',
        2 => 'ver',
      ),
      'personas' => 
      array (
        1 => 'alta',
        9 => 'buscar-persona',
        3 => 'detalle',
        7 => 'get-calles',
        5 => 'get-localidades',
        6 => 'get-paises',
        10 => 'get-permiso-donacion-por-perid',
        8 => 'historico-donaciones',
        0 => 'index',
        4 => 'modificacion',
        2 => 'sicap-to-dni',
      ),
      'solicitudes' => 
      array (
        6 => 'alta',
        5 => 'carga-rapida',
        11 => 'constancia',
        7 => 'crear',
        2 => 'crear-carga-rapida',
        8 => 'crear-sin-control',
        4 => 'editar',
        1 => 'eliminar',
        3 => 'eliminarsolicitud',
        9 => 'form-nueva-donacion',
        14 => 'formulario',
        10 => 'grabar-nueva-donacion',
        15 => 'imprimir',
        12 => 'imprimir-solicitud',
        13 => 'imprimirpdf',
        0 => 'index',
      ),
    ),
    'gestion' => 
    array (
      'aferesis' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'almacenes' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'bolsas' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'bolsasmarcas' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'bolsastipos' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'centralizado' => 
      array (
        1 => 'alta-lote',
        7 => 'cambiar-estado',
        9 => 'create-lote',
        8 => 'eliminar-lote',
        2 => 'envio',
        6 => 'generacion',
        10 => 'imprimir-lote',
        0 => 'index',
        3 => 'recepcion',
        11 => 'registrar-envio-de-lote',
        12 => 'registrar-recepcion-de-lote',
        4 => 'solicitud',
        5 => 'validar-codigo-utilizable',
      ),
      'cuestionariopreguntascategorias' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'determinaciones' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        4 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'donaciones' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'hemocomponentes' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'motivosdescartes' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'motivosdiferidos' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'motivosrechazos' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'ocupaciones' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'perfilesproduccion' => 
      array (
        1 => 'agregar',
        3 => 'cambiar-estado',
        0 => 'index',
        2 => 'modificar',
      ),
      'plantillascuestionario' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
        4 => 'nuevo',
      ),
      'tipodonacion' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
    ),
    'paciente' => 
    array (
      'inmunotransfusional' => 
      array (
        2 => 'alta',
        3 => 'editar',
        0 => 'index',
        1 => 'listar',
      ),
      'ordentransfusional' => 
      array (
        3 => 'alta',
        4 => 'editar',
        8 => 'hemocomponentecompatibilizados',
        6 => 'hemocomponentelistar',
        7 => 'hemocomponentereservados',
        0 => 'index',
        1 => 'listar',
        5 => 'reaccionadversa',
        2 => 'resultadosinmuno',
      ),
    ),
    'promocion' => 
    array (
    ),
    'soporte' => 
    array (
      'centralizados' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
        4 => 'nuevo',
      ),
    ),
    'stock' => 
    array (
      'descartes' => 
      array (
        2 => 'confirmar',
        8 => 'descartar',
        1 => 'descartarstock',
        7 => 'descarte',
        5 => 'historial',
        0 => 'index',
        3 => 'informe',
        4 => 'validar',
        6 => 'verpdf',
      ),
      'insumos' => 
      array (
        10 => 'agregar',
        2 => 'confirmar',
        13 => 'descartar',
        12 => 'descarte',
        8 => 'enviar',
        5 => 'envios',
        6 => 'historial',
        0 => 'index',
        3 => 'informe',
        7 => 'irradiar',
        9 => 'pdf-envio',
        1 => 'pedido',
        4 => 'validar',
        11 => 'verpdf',
      ),
      'pedidosinsumos' => 
      array (
        5 => 'actualizarenvio',
        7 => 'actualizarrecepcion',
        3 => 'alta',
        9 => 'cancelar',
        8 => 'cerrar',
        4 => 'enviar',
        0 => 'index',
        1 => 'listar',
        2 => 'pedido',
        6 => 'recepcion',
        10 => 'verpdf',
        11 => 'verpedido',
      ),
      'pedidosinsumosdetalle' => 
      array (
        1 => 'get-detalle-by-id',
        0 => 'index',
      ),
      'pedidosinsumoslog' => 
      array (
        0 => 'index',
      ),
      'pedidosinsumosmovimientos' => 
      array (
        3 => 'ajuste',
        1 => 'consolidar',
        0 => 'index',
        4 => 'procesoajuste',
        2 => 'procesoconsolidar',
      ),
      'pedidosinsumosmovimientosconsolidados' => 
      array (
        0 => 'index',
        1 => 'verpdfstockexistencia',
      ),
      'productos' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
        4 => 'nuevo',
      ),
      'productoscategorias' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
      'solicitudes' => 
      array (
        4 => 'alta',
        5 => 'cerrar',
        2 => 'grabar',
        0 => 'index',
        6 => 'informe',
        8 => 'pdf-envio',
        7 => 'pdf-solicitud',
        3 => 'recepcionar',
        1 => 'respondersolicitud',
        9 => 'verpdf',
      ),
      'stock' => 
      array (
        0 => 'index',
        1 => 'verpdf',
      ),
      'tiposmovimientosstock' => 
      array (
        1 => 'agregar',
        3 => 'eliminar',
        0 => 'index',
        2 => 'modificar',
      ),
    ),
  ),
);
