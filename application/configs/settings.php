<?php
return array(
    'baseurl' => '/',
    'pry_nombre' => 'Sistema de Medicina Transfucional',
    'pry_id' => '3',
    'pry_version' => '3.7.1',
    'codename' => 'simetra',
    'nombre_legado' => 'banco',
    'db_banco' => 'dsalud_banco',
    'db_front' => 'dsalud_front',
    'db_hmi2' => 'dsalud_hmi2',
    'db_sicap' => 'dsalud_sicap',
    'dbusers' => 'dsalud_front',
    'db_mod_sims' => 'dsalud_mod_sims',
    'prefijo_tabla' => 'd'
);
