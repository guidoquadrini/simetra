<div>
  <input type="hidden" id="iddonacion" value="<?php echo isset($donacion)?$donacion->id:"";?>">
  <input type="hidden" id="accion" value="<?php echo isset($cod_tipo_accion)?$cod_tipo_accion:"";?>">
  <input type="hidden" id="peso" value="<?php echo isset($peso)?$peso:"";?>">
  <h2 style="margin-top:3px;">Items Env&iacute;os:</h2>
  
  <p style="text-align:center;padding:10px;" class="ui-state-highlight">Se dispone a <strong>
  <?php echo isset($tipo_accion)?$tipo_accion:"";?></strong> la donación:
  <strong>
  <?php 
    if (isset($donacion)) {
       echo $donacion->cod_efector.$donacion->nro;
    }
  ?>
  </strong>
  con peso: <strong><?php echo isset($peso)?$peso:"";?></strong> (Kg.)
   </p>
   <p>
    ¿Est&aacute; seguro?
   </p>
   <div id="botonera_opciones" style="padding:4px;margin-top:3px;border-top: 1px solid #CCCCCC;" class="ui-helper-clearfix">
	<button id="btnRegistrarItem">Aceptar</button>	
</div>
   <div id="haciendo_llamado" style="display:none">
     <table align="center">
           <td><strong>Registrando Item...</strong></td>
           <td><img id="resultado_registro" src="css/verificando.gif"></td>
     </table>
   </div>
   <div id="detalle_cajon" style="display:none;width:100%;text-align:center">
     <table align="center">
      <td valign="middle"><strong>Colocar Donaci&oacute;n en Caj&oacute;n color:</strong></td>
      <td>  
         <div id="cajon" style="margin:5 auto;width:80px;height:80px;border:1px solid #CCCCCC;"></div>        
      </td>
     </table>
   </div>
   <div id="detalle_rechazo" style="display:none;width:100%;text-align:center">
     <table align="center">
      <td><strong>La Donaci&oacute;n se rechaz&oacute; correctamente!</strong></td>
     </table>
   </div>
</div>

<script type="text/javascript">

$("#btnRegistrarItem").button({icons: {primary: "ui-icon-check"}});

$("#btnRegistrarItem").click(function() {
      $(this).hide();
      $("#haciendo_llamado").show();

      //Registrar estado de recepcion del item
      $.post("ctrdonaciones/registrarEstadoRecepcion",
             {
               donacion: $("#iddonacion").val(),
               accion: $("#accion").val(),
               peso: $("#peso").val()
             },
             function (datos) {
                if (datos.estado=="Ok") {
                  $("#resultado_registro").attr("src","css/checkazul.jpeg");
                  if (datos.tipoaccion==1) {
		          $("#cajon").css("background-color",datos.color_cajon);
		          $("#detalle_cajon").show();
                  } else {
                          $("#detalle_rechazo").show();
                  }
                  //Actualizar el estado de la fila de la donacion
                  link=$("a.registrarItem[rel='" + $("#iddonacion").val() + "']");
                  trPadre = link.parent().parent();
                  tdAccion = trPadre.find("td").eq(2).children("select");
                  tdPeso = trPadre.find("td").eq(3).children("input");
                  tdOpciones = trPadre.find("td").eq(5);
                  //Deshabilitar edicion
                  tdAccion.attr("disabled","diabled");
                  tdPeso.attr("readonly","readonly"); 
                  //Poner Botones de Edicion
                  tdOpciones.html('<a href="#" rel="'+ $("#iddonacion").val() + '" class="edicionItem">Editar Donaci&oacute;n</a>');
                } else {
                  $("#resultado_registro").attr("src","css/falloverificacion.jpeg")
                }
             },
             "json"
             );
});

</script>
