<div style="width:100%;">
<div class="ui-widget-header" style="padding:5px;">
  <h2>Validar Bolsa Hemocomponente:</h2>
</div>
<div class="ui-widget-content" style="width:100%;padding:15px auto;">
  <table align="center">
    <tr>
       <td align="left">Bolsa:</td>
       <td align="left">
          <input id="bolsa" type="text" size="12" style="text-align:right">
       </td>
       <td>
          <button id="btnAceptar">Validar</button>
       </td>
    </tr>
  </table>
</div>
</div>
<script type="text/javascript">

$("#btnAceptar").button({icons: {primary: "ui-icon-check"}});

$("#btnAceptar").click(function () {
            if ($("#bolsa").val()!="") {
              hacerLlamado();
            } else {
              alert("Se debe completar el campo Bolsa!");
            }

});

$("#bolsa").keydown(function (e) {
           if (e.which == 13) {
            if ($("#bolsa").val()!="") {
              hacerLlamado();
            } else {
              alert("Se debe completar el campo Bolsa!");
            }
           }
});

function hacerLlamado () {
                 var auxlink = $("<a id=\"iframe\" href=\"ctrdonaciones/mostrarValidacion/\">");
                refAuxlink = auxlink;
                auxlink.fancybox({
                              "autoDimensions":true,
                              "scrolling":"no",
                              onComplete: function() {
                                 $("#fancybox-wrap").css("top","5px");
                               }        
                              });
                auxlink.trigger("click");

}

</script>
