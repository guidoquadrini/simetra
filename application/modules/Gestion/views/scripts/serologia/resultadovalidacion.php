<div style="width:400px;">
<div class="ui-widget-header" style="padding:5px;">
  <h2>Resultado Validaci&oacute;n Bolsa:</h2>
</div>
<div class="ui-widget-content" style="width:100%;">
  <table align="center">
    <tr>
       <td align="left">Serolog&iacute;a:</td>
       <td align="left">
            Ok/Not Ok/En Proceso
       </td>
    </tr>
    <tr>
      <td colspan="2"> Detalles:</td>
    </tr>
    <tr>
       <td align="left">Inmunolog&iacute;a:</td>
       <td align="left">
            Ok/Not Ok/En Proceso
       </td>
    </tr>
    <tr>
      <td colspan="2"> Detalles:</td>
    </tr>
  </table>
</div>
<div class="ui-widget-content" style="padding:5px;">
   <button id="btnImprimir">Imprimir Etiqueta</button>
   <button id="btnCancelar">Cancelar</button>
</div>
</div>
<script type="text/javascript">

$("#btnImprimir").button({icons: {primary: "ui-icon-check"}});
$("#btnCancelar").button({icons: {primary: "ui-icon-closethick"}});

</script>
