<div class="ui-widget-header ui-corner-all" style="padding: 5px;margin: 5px auto;text-align:left;">
	<span class="tituloModulo">:: Exportar An&aacute;lisis a Laboratorio ::</span>
</div>
<div class="ui-widget ui-widget-header ui-corner-tl ui-corner-tr" style="height:20px;text-align:center;padding-top:6px;padding-left:5px;">
	Filtro de Datos:
</div>
<div class="ui-widget ui-widget-content ui-corner-bl ui-corner-br" style="margin-bottom:10px;padding:5px;">
	<div>
		<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
		<tr>
		<td align="left">
		<table cellpadding="0" cellspacing="8" border="0" align="center">
			<tr>
				<td>
					<span class="textoCampoIng">Efector:</span>
					<select id="efectores">
						<option value="-1">Todos</option>
						<?php
							if (isset($efectores)) {
								foreach ($efectores as $efector) {
									echo '<option value="'.$efector->id.'">'.$efector->nombre.'</option>';
								}
							}
						?>
					</select>
				</td>
				<td>
					<span class="textoCampoIng">Desde:</span>
					<input type="text" id="fdesde" class="campoTextoIng ui-widget-content ui-corner-all" size="12" style="text-align:center"></td>
				<td>
					<span class="textoCampoIng">Hasta:</span>
					<input type="text" id="fhasta" class="campoTextoIng ui-widget-content ui-corner-all" size="12" style="text-align:center"></td>
				<td>
					<button id="btnExportar" style="margin-top:10px;">Exportar</button>
				</td>
			</tr>
		</table>
	</div>
</div>

<div id="diagInfo" title="">
	<table border="0" width="100%">
		<tr>
			<td><img src="css/imagenes/cargando1.gif"><td>
			<td><span id="textoAccion"></span></td>
		</tr>
	</table>
</div>
<script type="text/javascript" src="js/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="js/modelo/preproduccion/filtropreproduccion.js"></script>
<script type="text/javascript">
	$("#btnExportar").button({icons: {primary: "ui-icon-circle-arrow-e"}});

	$("#btnExportar").click(function() {
		if ($.trim($("#fdesde").val())=='' && $.trim($("#fhasta").val())=='' ) {
			alert("Se debe seleccionar al menos un fecha!");
			return;
		}

		$("#diagInfo").dialog("option","title","Exportando An&aacute;lisis:");
		exportacion = {
			"efector": $("#efectores option:selected").val() ,
			"fdesde": $("#fdesde").val(),
			"fhasta": $("#fhasta").val()
		};
		cadena = JSON.stringify(exportacion);

		//Mostrar el cuadro de espera
		$("#diagInfo").dialog("open");
		//Hacer post
		$.post("ctrserologia/exportar",
		{
			exportacion: cadena
		}
		);



	});


	$("#diagInfo").dialog({
		modal: true,
		autoOpen: false,
		width: 200
	});


	$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
	$("#fdesde").datepicker($.datepicker.regional[ "es" ]);
	$("#fhasta").datepicker($.datepicker.regional[ "es" ]);


</script>
