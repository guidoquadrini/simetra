<style>
    #tbl_descarte td,#tbl_descarte th {
        font-size: 1em !important;
        text-align: center;
    }
    .modal.large {
    width: 80%; /* respsonsive width */
    margin-left:-40%; /* width/2) */ 
    .modal-body{
        padding-left: 50px !important;
    }
}
</style>

<div class="span9 offset1">
    <div class="span12" style="text-align: right; margin-bottom: 20px;">    
        <a href="#modal_descarte_producto" role="button" data-toggle="modal" class="btn btn-primary"><span class="ion ion-plus-circled"></span> Agregar producto para descarte</a>
        
    </div>

    <table  class="table table-bordered table-striped" id="tbl_descarte">
        <thead>
            <tr class="btn-info btn-small btn-inverse">
                <th colspan="2">Donacion</th>
                <th>Hemocomponente</th>
                <th>Peso</th>
                <th>Irradiado</th>
                <th>Motivo de descarte</th>
                <th>Fecha de Vencimiento</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><input type="checkbox"/></td>
                <td>HPRC000001</td>
                <td>Plaqueta</td>
                <td>300<span> grs.</span></td>
                <td><span class="badge badge-info">NO</span></td>
                <td>Por vencimiento</td>
                <td>06/06/2016</td>            
            </tr>
            <tr>
                <td><input type="checkbox"/></td>
                <td>HPRC000001</td>
                <td>Plaqueta</td>
                <td>300<span> grs.</span></td>
                <td><span class="badge badge-info">NO</span></td>
                <td>Por vencimiento</td>
                <td>06/06/2016</td>            
            </tr>
            <tr>
                <td><input type="checkbox"/></td>
                <td>HPRC000001</td>
                <td>Plaqueta</td>
                <td>300<span> grs.</span></td>
                <td><span class="badge badge-info">NO</span></td>
                <td>Por vencimiento</td>
                <td>06/06/2016</td>               
            </tr>
            <tr>
                <td><input type="checkbox"/></td>
                <td>HPRC000001</td>
                <td>Plaqueta</td>
                <td>300<span> grs.</span></td>
                <td><span class="badge badge-info">NO</span></td>
                <td>Por vencimiento</td>
                <td>06/06/2016</td>            
            </tr>
        </tbody>
        </thead>
    </table>
</div>
<div class="span11" style="text-align: right;">
    <button class="btn btn-warning"><span class="ion ion-refresh"></span> Refrescar</button>
    <button class="btn btn-danger"><span class="ion ion-flame"></span> Descartar</button>
</div>


<!-- Modal -->
<div id="modal_descarte_producto" class="modal hide fade" tabindex="-1" 
     role="dialog" aria-labelledby="modal_descarte_producto_label" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3 id="modal_descarte_producto_label">Descartar Hemocomponente</h3>
  </div>
  <div class="modal-body">
    <h4>Formulario de Descarte</h4>
    <label>Donacion:</label><input id="nro_donacion" name="nro_donacion" type="text" class="form-control"/>
    <label>Hemocomponente:</label>
    <select id="cbo_motivo" name="id_motivo" class="form-control">
    <option></option>
    <option></option>
    </select>  
    
    <label>Peso:</label><input id="peso" name="peso" class="form-control uneditable-input"/>
    <label>Motivo:</label>
    <select class="form-control">
    <option></option>
    <option></option>
    </select>  
    <label>Observacion:</label><textarea id="observacion" name="observacion" class="form-control"></textarea>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
    <button class="btn btn-primary">Agregar al listado</button>
  </div>
</div>