<!--Estilo para debug-->
<!--style>#transferencia > div{border:1px solid red;}</style>
<!--Fin estilo para debug-->

<style>
    .resumen{
        background-color: rgba(250,247,211,.5);
        box-shadow: 0.1em 0.1em 0.1em rgba(200,200,200,.7);
        padding: 10px;
    }
</style>

<div class="span4">
    <h4 class="badge badge-info" style="border-radius: 0px;width:100%;">Stock Actual</h4>
    <table class="table ">
        <thead>
            <tr>
                <th>Hemocomponente</th>
                <th width="60px;">Stock</th>
                <th width="60px;">Restantes</th>                
            </tr>   
        </thead>
        <tbody>
            <!--?php foreach($this->hemocomponentes as $i => $v): ?-->
            <tr class="stock_hemocomponente">
                <td>Concentrado G   l�bulos Rojos</td>
                <td><span class="badge badge-warning pull-right ">10</span></td>
                <td><span class="badge badge-info pull-right ">1</span></td>
            </tr>
            <tr class="stock_hemocomponente">
                <td>Crioprecipitado</td>
                <td><span class="badge badge-warning pull-right ">10</span></td>
                <td><span class="badge badge-info pull-right ">1</span></td>
            </tr>
            <tr class="stock_hemocomponente">
                <td>Plaquetas</td>
                <td><span class="badge badge-warning pull-right ">10</span></td>
                <td><span class="badge badge-info pull-right ">1</span></td>
            </tr>
            <tr class="stock_hemocomponente">
                <td>Plasma Fresco</td>
                <td><span class="badge badge-warning pull-right ">10</span></td>
                <td><span class="badge badge-info pull-right ">1</span></td>
            </tr>
            <tr class="stock_hemocomponente">
                <td>Plasma Modificado</td>
                <td><span class="badge badge-success pull-right ">10</span></td>
                <td><span class="badge badge-info pull-right ">0</span></td>
            </tr>
            <tr class="stock_hemocomponente">
                <td>Plasma Normal</td>
                <td><span class="badge badge-warning pull-right ">10</span></td>
                <td><span class="badge badge-info pull-right ">1</span></td>
            </tr>            
        </tbody>
    </table>
</div>
<div class="span4 ">
    <h4 class="badge badge-info" style="border-radius: 0px;width:100%;">Transferir</h4>
    <label class="span4">Destino: </label>
    <select id="cbo_destino" name="id_efector_destino" class="form-control select2 span7">
        <option></option>        
    </select>
    <label class="span4">Hemocomponente: </label>
    <select id="cbo_hemocomponente" name="id_hemocomponente" class="form-control select2 span7">
        <option></option>        
    </select>    

    <table>
        <tr>
            <td><label class="control-label" for="id_cantidad">Cantidad:</label></td>
            <td><input type="number" id="id_cantidad" min="0" value="2" class="form-comtrol" /></td>
            <td style="padding-left: 20px;"><label class="control-label" for="es_irradiado">Ver irradiados: </label></td>
            <td style="padding-bottom: 9px;"><input type="checkbox" id="es_irradiado" class="form-comtrol" checked/></td>

        </tr>
    </table>


    <table class="table">
        <thead>
            <tr>
                <th colspan="2" style="text-align: center;">Donacion</th>
                <th width="50px;">Irradiado</th>                
            </tr>   
        </thead>
        <tbody>            
            <tr class="stock_hemocomponente">
                <td><input type="checkbox" checked></td>
                <td>HPRC000001</td>
                <td><span class="badge badge-warning">SI</span></td>                
            </tr>
            <tr class="stock_hemocomponente">
                <td><input type="checkbox" checked></td>
                <td>HPRC000002</td>
                <td><span class="badge badge-warning">SI</span></td>                
            </tr>
            <tr class="stock_hemocomponente">
                <td><input type="checkbox"></td>
                <td>HPRC000003</td>
                <td><span class="badge badge-info">NO</span></td>                
            </tr>
            <tr class="stock_hemocomponente">
                <td><input type="checkbox"></td>
                <td>HPRC000004</td>
                <td><span class="badge badge-info">NO</span></td>                
            </tr>

        </tbody></table>
</div>
<div class="span3">
    <h4 class="badge badge-info" style="border-radius: 0px;width:100%;">Transferencia en Curso</h4>
    <div class="span12 resumen" >
        <div class="input-prepend">    
            <span class="add-on span3">Origen.</span>
            <input type="text" value="Centro de Sangre - Rosario" class="uneditable-input"/>
        </div>
        <div class="input-prepend">    
            <span class="add-on span3">Destino:</span>        
            <input type="text" value="Hospital Roque Seans Pe�a" class="uneditable-input"/>
        </div>
        <div class="control-group">        
            <label class="control-label">Detalle:</label>
            <table class="table">
                <tr>
                    <th>Don.</th>
                    <th>Hemo.</th>
                    <th>Cant.</th>
                </tr>
                <tr>
                    <td>HRPC000001</td>
                    <td>Plaqueta</td>
                    <td>200<span class="hemo_unidad"> grs.</span></td>
                </tr>
                <tr>
                    <td>HRPC000001</td>
                    <td>Plaqueta</td>
                    <td>200<span class="hemo_unidad"> grs.</span></td>
                </tr>
                <tr>
                    <td>HRPC000001</td>
                    <td>Plaqueta</td>
                    <td>200<span class="hemo_unidad"> grs.</span></td>
                </tr>   
            </table>
        </div>
        <div style="text-align: right;">
            <button class="btn btn-warning">Cancelar</button>
            <button class="btn btn-primary">Realizar Transferencia</button>
        </div>
    </div>  
</div>


