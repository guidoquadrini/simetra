
                        <div class="span5">
                            <h5>Stock Actual Total</h5>
                            <table class="table table-striped table-bordered table-hover table-condense">
                                <thead>
                                    <tr class="btn-info btn-small btn-inverse">
                                        <th>Hemocomponente</th>
                                        <th width="90px;">Stock Actual</th>
                                        <th width="100px;">Donaciones Fraccionadas</th>
                                        <th width="100px;">Stock Transferible</th>
                                    </tr>   
                                </thead>
                                <tbody>
                                    <!--?php foreach($this->hemocomponentes as $i => $v): ?-->
                                    <tr class="stock_hemocomponente">
                                        <td>Concentrado G   l�bulos Rojos</td>
                                        <td><span class="badge badge-warning pull-right ">10</span></td>
                                        <td><span class="badge badge-info pull-right ">1</span></td>
                                        <td><span class="badge badge-success pull-right ">9</span></td>
                                    </tr>
                                    <tr class="stock_hemocomponente">
                                        <td>Crioprecipitado</td>
                                        <td><span class="badge badge-warning pull-right ">10</span></td>
                                        <td><span class="badge badge-info pull-right ">1</span></td>
                                        <td><span class="badge badge-success pull-right ">9</span></td>
                                    </tr>
                                    <tr class="stock_hemocomponente">
                                        <td>Plaquetas</td>
                                        <td><span class="badge badge-warning pull-right ">10</span></td>
                                        <td><span class="badge badge-info pull-right ">1</span></td>
                                        <td><span class="badge badge-success pull-right ">9</span></td>
                                    </tr>
                                    <tr class="stock_hemocomponente">
                                        <td>Plasma Fresco</td>
                                        <td><span class="badge badge-warning pull-right ">10</span></td>
                                        <td><span class="badge badge-info pull-right ">1</span></td>
                                        <td><span class="badge badge-success pull-right ">9</span></td>
                                    </tr>
                                    <tr class="stock_hemocomponente">
                                        <td>Plasma Modificado</td>
                                        <td><span class="badge badge-success pull-right ">10</span></td>
                                        <td><span class="badge badge-info pull-right ">0</span></td>
                                        <td><span class="badge badge-success pull-right ">10</span></td>
                                    </tr>
                                    <tr class="stock_hemocomponente">
                                        <td>Plasma Normal</td>
                                        <td><span class="badge badge-warning pull-right ">10</span></td>
                                        <td><span class="badge badge-info pull-right ">1</span></td>
                                        <td><span class="badge badge-success pull-right ">9</span></td>
                                    </tr>
                                    <!--?php endforeach; ?-->
                                </tbody>
                            </table>
                        </div>
                        <div class="span3">
                            <h5>Stock de Donaciones por Hemocomponente [<span id="lst_don_hemo"></span>]</h5>
                            <table class="table table-striped table-bordered table-hover table-condense">
                                <thead>
                                    <tr class="btn-info btn-small btn-inverse">
                                        <th>Codigo de Donacion</th>                                        
                                    </tr>   
                                </thead>
                                <tbody>				            
                                    <tr class="list_donacion"><td>Seleccione un hemocomponente para ver las donaciones que componen el stock.</td></tr>                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="span3 detalle-donacion">
                            <h5>Detalle Estado Donacion [<span id="titulo_detalle_donacion"></span>]</h5>
                            <div class="span12">

                                <table class="table table-striped table-bordered table-hover table-condense">
                                    <thead>
                                        <tr class="btn-inverse btn-small">
                                            <th width="80px;"><i class="ion ion-waterdrop" title="Hemocomponente"> </i> Hemocomp.</th>
                                            <th width="50px;" title="Peso de Produccion">Producido</th>
                                            <th width="50px;" title="Cantidad Utilizada">Utilizada</th>
                                            <th width="50px;" title="Fecha de Vencimiento"><i class="ion ion-calendar" ></i></th>				                                                      
                                            <th width="25px;" title="Irradiado"><i class="ion ion-nuclear" ></i></th>
                                            <th title="�El hemocomponente fue fraccionado?">
                                                <span class="step size-32"><i class="ion ion-network"></i>Fraccionado</span>
                                            </th>				              
                                        </tr>   
                                    </thead>
                                    <!--tbody>				            
                                        <tr>
                                            <td>Plaqueta</td>      
                                            <td>600 grs.</td>
                                            <td>200 grs.</td>
                                            <td>06/11/2016</td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>                            
                                        </tr>
                                        <tr>
                                            <td>Globulos Rojos</td>      
                                            <td>600 grs.</td>
                                            <td>200 grs.</td>
                                            <td>06/11/2016</td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                        </tr>
                                        <tr>
                                            <td>Crio Presipitado</td>      
                                            <td>600 grs.</td>
                                            <td>200 grs.</td>
                                            <td>06/11/2016</td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                        </tr>
                                        <tr>
                                            <td>Pasta Dental</td>      
                                            <td>600 grs.</td>
                                            <td>200 grs.</td>
                                            <td>06/11/2016</td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                            <td><input class="form-control" type="checkbox" checked="checked" disabled="disabled"></td>
                                        </tr>
                
                                    </tbody-->
                                    <tbody><tr><td colspan="6">No hay datos para mostrar. Seleccione una donacion para ver su detalle.</td></tr></tbody>
                                </table>
                            </div>
                        </div>