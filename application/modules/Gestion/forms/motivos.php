<?php

class Gestion_forms_motivos extends Zend_Form {

	public function init(){
		$this->addElement($this->createElement('text','mot_descripcion')
		->setRequired(true)
		->setLabel('Nombre')
		->setAttribs(array(
			'class' => 'input-xxlarge',
			'maxlength' => 100,
			'required' => 'required',
		)));

		$this->addElement($this->createElement('text', 'dias')
			->setLabel('Dias')
			->setRequired(false)
			->setAttribs(array(
				'type' => 'number',
				'maxlenght' => 10,
			))
		);

		$this->addElement($this->createElement('hidden','mot_cat'));
	}
}
