<?php

class Gestion_forms_bolsasmarcas extends Zend_Form {

	public function init(){
		$this->addElement($this->createElement('text','marca_bolsa')
		->setLabel('Marca')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-xxlarge',
			'maxlength' => 50
		))
		->setRequired(true));

                 
	}
}