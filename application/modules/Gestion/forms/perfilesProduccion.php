<?php

class Gestion_forms_perfilesProduccion extends Zend_Form {

	public function init(){
		$this->addElement($this->createElement('text','ppr_descripcion')
		->setLabel('Nombre Perfil')
		->setAttribs(array(
			'required' => 'required',
			'maxlength' => 40
		))
		->setRequired(true));

		$this->addElement($this->createElement('radio','ppr_genero')
		->setLabel('Genero Especifico')
		->setSeparator('')
		->addMultiOptions(array(
			'A' => 'Ambos',
			'M' => 'Masculino',
			'F' => 'Femenino',
		)));
	}
}