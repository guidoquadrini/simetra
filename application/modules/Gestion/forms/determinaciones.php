<?php

class Gestion_forms_determinaciones extends Zend_Form {

	public function init(){

		$cod = $this->createElement('text','cod')
		->setLabel('Código Laboratorio')
		->setAttrib('required','required')
        ->setAttrib('maxlength',"10")
		->setRequired(true);

		$descripcion = $this->createElement('text','descripcion')
		->setLabel('Nombre')
		->setAttrib('required','required')
        ->setAttrib('maxlength',"50")
		->setRequired(true);
                
        $enfermedades = new Banco_models_Enfermedades;       
        $enfermedades_list = $enfermedades->getPairs();
        $this->addElement($this->createElement('select', 'enf_id')
                        ->setLabel('Enfermedad')
                        ->setAttrib('required', 'required')
                        ->addMultiOptions($enfermedades_list)
                        ->setRequired(true)); 
        
        

        $def = $this->createElement('radio','def')
        ->setLabel('Tamizaje por defecto')
        ->setSeparator('')
        ->addMultiOptions(array(
            1 => 'Si',
            0 => 'No'
        ))
        ->setRequired('true');
        

		$this->addElements(array($cod,$descripcion,$enf_id,$def));
	}
}
