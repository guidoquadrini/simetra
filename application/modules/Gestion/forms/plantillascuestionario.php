<?php

class Gestion_forms_plantillascuestionario extends Zend_Form {

    public function init() {


        $this->addElement($this->createElement('select', 'estado')
                        ->setLabel('Estado')
                        ->setAttribs(array(
                            'title' => 'Defina el estado',
                        ))
                        ->addMultiOptions(array(
                            '0' => 'Selecione un estado',
                            '0' => 'Inactiva',
                            '1' => 'Activa'
                        ))
                        ->setRequired(true)
        );



        $this->addElement($this->createElement('text', 'version')
                        ->setLabel('Versión')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 20
                        ))
                        ->setRequired(true));



        $this->addElement($this->createElement('text', 'titulo')
                        ->setLabel('Título')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 255
                        ))
                        ->setRequired(true));

        
        
           $this->addElement($this->createElement('text', 'observacion')
                        ->setLabel('Observaciones')
                        ->setAttribs(array(
                            'maxlength' => 255
                        ))
                        ->setRequired(false));
     
       

    }

}
