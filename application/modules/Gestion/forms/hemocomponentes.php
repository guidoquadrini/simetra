<?php

class Gestion_forms_hemocomponentes extends Zend_Form {

	public function init(){
		$nombre = $this->createElement('text','hem_descripcion')
		->setLabel('Nombre')
		->setAttrib('required','required')
		->setRequired(true);

		$dias_aptitud = $this->createElement('text','hem_dias_aptitud')
		->setLabel('Dias Aptitud')
		->setAttrib('required','required')
		->setRequired(true);

		$hem_temp = $this->createElement('text','hem_temp')
		->setLabel('Temperatura de almacenamiento')
		->setAttrib('required','required')
        ->setAttrib('maxlength','12')
        ->setRequired(true);
	
		$hem_peso_min = $this->createElement('text','hem_peso_min')
		->setLabel('Peso min')
		->setAttrib('required','required')
		->setRequired(true);
	
		$hem_peso_max = $this->createElement('text','hem_peso_max')
		->setLabel('Peso max')
		->setAttrib('required','required')
		->setRequired(true);
                
                $hem_valor_referencia = $this->createElement('text','hem_valor_referencia')
		->setLabel('Valor de Referencia')
		->setAttrib('required','required')
		->setRequired(true);

		$this->addElements(array($nombre,$dias_aptitud, $hem_temp, $hem_peso_min, $hem_peso_max, $hem_valor_referencia));
	}
}
