<?php

class Gestion_forms_aferesis extends Zend_Form {

	public function init(){
		$nombre = $this->createElement('text','afe_descripcion')
		->setLabel('Nombre')
		->setAttrib('required','required')
		->setRequired(true);

		$this->addElements(array($nombre));
	}
}