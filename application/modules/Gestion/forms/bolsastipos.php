<?php

class Gestion_forms_bolsastipos extends Zend_Form {

	public function init(){
		$this->addElement($this->createElement('text','bolsa_tipo')
		->setLabel('Nombre del Tipo de Bolsa')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-large',
			'maxlength' => 50
		))
		->setRequired(true));

		$this->addElement($this->createElement('text','peso_minimo')
		->setLabel('Peso Mínimo')
		->setAttribs(array(
//			'required' => 'required',
			'class' => 'input-sm',                                     
			'maxlength' => 10
		))                                         
		->setRequired(true));                
                

		$this->addElement($this->createElement('text','peso_maximo')
		->setLabel('Peso Máximo')
		->setAttribs(array(
//			'required' => 'required',
			'class' => 'input-sm',
			'maxlength' => 10
		))
		->setRequired(true));                  
 
		$this->addElement($this->createElement('text','observaciones')
		->setLabel('Observaciones')
		->setAttribs(array(
			'class' => 'input-xxlarge',
			'maxlength' => 255
		))
		);                
                
	}
}