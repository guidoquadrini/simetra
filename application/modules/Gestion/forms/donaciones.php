<?php

class Gestion_forms_donaciones extends Zend_Form {

	public function init(){
		$nombre = $this->createElement('text','tdo_descripcion')
		->setLabel('Nombre')
		->setAttrib('required','required')
		->setRequired(true);

		$this->addElements(array($nombre));
	}
}