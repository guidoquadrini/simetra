<?php

class Gestion_forms_cuestionariopreguntascategorias extends Zend_Form {

	public function init(){
               
        $this->addElement($this->createElement('select', 'estado')
                        ->setLabel('Estado')
                        ->setAttribs(array(
                            'title' => 'Defina el estado',
                        ))
                        ->addMultiOptions(array(
                            '0' => 'Selecione un estado',
                            '0' => 'Inactiva',
                            '1' => 'Activa'
                        ))
                        ->setRequired(true)
        );            
            
        
        
		$this->addElement($this->createElement('text', 'orden')
			->setLabel('Orden Nro.')
			->setRequired(false)
			->setAttribs(array(
				'type' => 'number',
				'maxlenght' => 10,
			))
		);
                
        
		$this->addElement($this->createElement('text','nombre')
		->setLabel('Nombre')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-xxlarge',
			'maxlength' => 255
		))
		->setRequired(true));   
                
                
                
            
		$this->addElement($this->createElement('text','descripcion')
		->setLabel('Descripcion')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-xxlarge',
			'maxlength' => 50
		))
		->setRequired(true));                
                 
	}
}