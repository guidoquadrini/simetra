<?php

class Gestion_forms_tipodonacion extends Zend_Form {

	public function init(){
               
		$this->addElement($this->createElement('text','descripcion')
		->setLabel('Descripcion')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-xxlarge',
			'maxlength' => 50
		))
		->setRequired(true));                
                 
	}
}