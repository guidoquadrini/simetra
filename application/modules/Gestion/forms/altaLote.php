<?php

class Gestion_forms_altalote extends Zend_Form {

    public function init() {

        $view = $this->getView();
        $view->addHelperPath(APPLICATION_PATH . '/../library/Custom/View/Helper/', 'Custom_View_Helper');

        $nroLote = new Custom_Form_Element_Html5('nroLote');
        $nroLote->setRequired(true)
                ->setLabel('Identificación del Lote (autogenerado):')
                ->setAttribs(array('type' => 'number',
                    'class' => '',                    
                    'disabled' => 'true'
        ));
        $this->addElement($nroLote);

        $cnt_codigos = new Custom_Form_Element_Html5('cnt_codigos');
        $cnt_codigos->setRequired(true)
                ->setLabel('Numero de donaciones que poseera el lote:')
                ->setAttribs(array('type' => 'number',
                    'minlenght' => 1,
                    'maxlenght' => 1000,
                    'step' => 10
        ));
        $this->addElement($cnt_codigos);
               
        $this->addElement(
                $this->createElement('select', 'cbo_efector')
                        ->setLabel('Seleccione el efector a quien se le enviara el Lote:')
                        ->setRegisterInArrayValidator(false)                       
                        ->setRequired(true)
                        ->setAttribs(array(
                            'title' => 'Seleccione un efector...',                            
                        ))
        );
    }

}
