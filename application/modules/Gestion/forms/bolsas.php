<?php
//TODO: Refactorear nombres, bolsas => conservantes_bolsas
class Gestion_forms_bolsas extends Zend_Form {

	public function init(){
		$this->addElement($this->createElement('text','tipo_bolsa')
		->setLabel('Nombre de la solución')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-xxlarge',
			'maxlength' => 40
		))
		->setRequired(true));
                
                
                 $this->addElement($this->createElement('text', 'dias')
                        ->setLabel('Días de conservación')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 10
                        ))
                        ->setRequired(true));       
                 
	}
}