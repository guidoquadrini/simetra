<?php

class Gestion_forms_ocupaciones extends Zend_Form {

	public function init(){
		$this->addElement($this->createElement('text','ocu_descripcion')
		->setLabel('Nombre')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-xxlarge',
			'maxlength' => 90
		))
		->setRequired(true));
	}
}