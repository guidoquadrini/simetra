<?php

class Gestion_models_Hemocomponentes extends Zend_Db_Table_Abstract {

    protected $_name = 'hem_hemocomponentes';
    protected $_primary = 'hem_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('hem_id', 'hem_descripcion'))
                                ->where('hem_estado=?', 1)
                                //		->where('hem_esperado=?',1)
                                ->order('hem_descripcion'));
    }

    public function getPairsMenores() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('hem_id', 'hem_descripcion'))
                                ->where('hem_estado=?', 1)
                                ->where('hem_esperado=?', 1)
                                ->order('hem_descripcion'));
    }

    public function getPairsMayores() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('hem_id', 'hem_descripcion'))
                                ->where('hem_estado=?', 1)
                                ->where('hem_caducidad=?', 0)
                                ->order('hem_descripcion'));
    }

    public function getPairsPerfil($hem_perfil) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)
                ->reset('columns')->columns('hem_id')
                ->where('hem_estado=?', 1)
        //		->where('hem_esperado=?',1)
        ;

        if (!empty($hem_perfil))
            $query = $query->where('hem_id NOT IN (?)', $hem_perfil);

        $query = $query->order('hem_id');
        //var_dump($query->__toString());
        return $this->fetchAll(($query))->toArray();
    }

    public function getTemps($hem_id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('hem_temp_min', 'hem_temp_max'))
                ->where('hem_id = ?', $hem_id);

        return $this->fetchRow($query)->toArray();
    }

    public function getTempAndDesc($hem_id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('hem_temp', 'hem_descripcion'))
                ->where('hem_id = ?', $hem_id);

        return $this->fetchRow($query)->toArray();
    }

    public function getTempDescDias($hem_id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('hem_temp', 'hem_descripcion', 'hem_dias_aptitud'))
                ->where('hem_id = ?', $hem_id);

        return $this->fetchRow($query)->toArray();
    }

    public function getPesos($hem_id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('hem_peso_min', 'hem_peso_max'))
                ->where('hem_id = ?', $hem_id);

        return $this->fetchRow($query)->toArray();
    }

    public function getDias($hem_id) {
        $query = $this->select(true)
                ->reset('columns')->columns('hem_dias_aptitud')
                ->where('hem_id = ?', $hem_id);

        return $this->fetchRow($query)->toArray();
    }

    
    public function getById($hem_id) {
        $query = $this->select(true)
                 ->reset('columns')->columns(array('hem_id', 'hem_descripcion'))
                ->where('hem_id = ?', $hem_id);

        return $this->fetchRow($query)->toArray();
    }    
    
    
}
