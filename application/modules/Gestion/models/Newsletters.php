<?php

class Gestion_models_Newsletters extends Zend_Db_Table_Abstract {

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->order('ocu_descripcion'));
    }

}

/*
 * Este modelo trabaja con la tabla Newsletter
 * 
 
 CREATE TABLE Newsletter(
    id int NOT NULL AUTO_INCREMENT,
    mensaje varchar(max),
    fec_creacion datetime,
    fec_envio datetime,
    estado,
    PRIMARY KEY id
    
  );
 
 
 CREATE TABLE Newsletter_Destinatarios(
    id_Newsletter int primary key,
    id_Persona int,
    email varchar(120)
  );
 
  
  
  
 
 * * *  
 */