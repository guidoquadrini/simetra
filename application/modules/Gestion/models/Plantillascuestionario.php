<?php

class Gestion_models_Plantillascuestionario extends Zend_Db_Table_Abstract {

    protected $_name = 'plantillas_cuestionario';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'nombre'))
                                ->order('orden'));
    }

    
    public function getPlantillaActiva(){
        return $this->fetchAll($this->select(true)
                                ->where('estado=?', 1)
                                ->order('id')
                                ->limit(1))->current();        
    }
    
    
        
}
