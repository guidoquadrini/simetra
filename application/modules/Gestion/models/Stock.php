<?php

class Gestion_models_Stock extends Zend_Db_Table_Abstract {

    protected $_name = 'stock';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getStockPorEfector($efector) {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                //->reset('columns')->columns(array('tdo_id','tdo_descripcion'))
                                ->where('efector_id=?', $efector)
                                ->order('tdo_descripcion'));
    }

}
