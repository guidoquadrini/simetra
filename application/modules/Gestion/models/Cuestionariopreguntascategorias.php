<?php

class Gestion_models_Cuestionariopreguntascategorias extends Zend_Db_Table_Abstract {

    protected $_name = 'cuestionario_preguntas_categorias';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'nombre'))
                                ->order('orden'));
    }
    
    public function obetenerTodoOrdenadoPorOrden() {
        return $this->getDefaultAdapter()->fetchAll($this->select(true)
                                ->order('orden'));
    }

    
    public function getByID($id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array(
                    'id',
                    'nombre',
                    'estado')
                )
                ->where('id = ?', $id);

        return $this->fetchRow($query)->toArray();
    }
    
}
