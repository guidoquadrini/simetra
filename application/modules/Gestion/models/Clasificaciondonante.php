<?php

class Gestion_models_Clasificaciondonante extends Zend_Db_Table_Abstract {

    protected $_name = 'clasificacion_donante';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'descripcion'))
                                //	->where('def=?',1)
                                ->order('id'));
    }

    public function getClasificacionDonanteFiltrado() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'descripcion'))
                                ->where('id >?', 1)
                                ->order('descripcion'));
    }

}
