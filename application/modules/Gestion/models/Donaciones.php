<?php

class Gestion_models_Donaciones extends Zend_Db_Table_Abstract {

	protected $_name = 'tdo_tipos_donaciones';
	protected $_primary = 'tdo_id';

        public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs(){
		return $this->getDefaultAdapter()->fetchPairs($this->select(true)
			->reset('columns')->columns(array('tdo_id','tdo_descripcion'))
			->where('tdo_estado=?',1)
			->order('tdo_descripcion'));
	}
}