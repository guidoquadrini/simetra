<?php

class Gestion_models_Bolsastipos extends Zend_Db_Table_Abstract {
    //TODO: Incorporar Tabla Tipo_donacion/Bolsas y a bolsas agregar estado y demas campos.
    protected $_name = 'bolsas_tipos';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'bolsa_tipo'))
                                //	->where('def=?',1)
                                ->order('id'));
    }

    public function getTiposBolsaPorTipoExtraccion($tipo_extraccion = null) {
        switch ($tipo_extraccion) {//Tipo de Extraccion (tabla tdo_tipos_donaciones
            case 1: //Aferesis
            case 3: //Toma Muestra
                return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                        ->reset('columns')->columns(array('id', 'bolsa_tipo'))
                        ->join('')
                                        ->where('id = ?', 7) //Exclusion de Bolsas Para Aferesis
                                        ->order('id'));


            case 2: //Sangre Notmal
                return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                        ->reset('columns')->columns(array('id', 'bolsa_tipo'))
                                        ->where('id <> ?', 7) //Exclusion de Bolsas Para Aferesis
                                        ->order('id'));

            default:
                return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                        ->reset('columns')->columns(array('id', 'bolsa_tipo'))
                                        ->order('id'));
        }
    }

}
