<?php

class Gestion_models_Plantillascuestionariodetalle extends Zend_Db_Table_Abstract {

    protected $_name = 'plantillas_cuestionario_detalle';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'nombre'))
                                ->order('orden'));
    }

    
    public function getCategoriasActivasPreguntas($plantilla_id){
        $query = $this->select()
                ->distinct()
                ->from($this, array('cuestionario_pregunta_categoria_id'))
                ->where('planilla_cuestionario_id = ?', $plantilla_id);
        return $query->query()->fetchAll();        
        
    }

    
    public function getPreguntasPorPlantillaPorCategoria($plantilla_id, $categoria_id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array('id','orden', 'pregunta', 'estado'))
                                ->where('planilla_cuestionario_id =?', $plantilla_id)
                                ->where('cuestionario_pregunta_categoria_id =?', $categoria_id)
                                ->order('orden'));        
    }        
        
}
