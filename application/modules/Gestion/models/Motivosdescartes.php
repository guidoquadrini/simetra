<?php

/*
 * Estados de los motivos
 * 0:Invactivo
 * 1:Activo
 */

class Gestion_models_Motivosdescartes extends Zend_Db_Table_Abstract {

    protected $_name = 'mot_descartes';
    protected $_primary = 'mot_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('mot_id', 'mot_descripcion'))
                                ->where('mot_estado=?', 1)
                                ->order('mot_descripcion'));
    }


    public function getDescripcion($id) {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('mot_descripcion'))
                                ->where('mot_id=?', $id)
                                ->where('mot_estado=?', 1));
    }
    
    //Se sostiene funcion por compatibilidad
    public function getDesc($id) {
        return $this->getDescripcion($id);
    }
    
}
