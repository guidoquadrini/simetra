<?php

class Gestion_models_Bolsasmarcas extends Zend_Db_Table_Abstract {

    protected $_name = 'bolsas_marcas';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'marca_bolsa'))
                                //	->where('def=?',1)
                                ->order('id'));
    }

    
    
        
}
