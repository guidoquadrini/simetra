<?php

class Gestion_models_Ocupaciones extends Zend_Db_Table_Abstract {

    protected $_name = 'ocu_ocupaciones';
    protected $_primary = 'ocu_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->order('ocu_descripcion'));
    }

}
