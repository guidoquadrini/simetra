<?php

class Gestion_models_PerfilesProduccion extends Zend_Db_Table_Abstract {

    protected $_name = 'ppr_perfiles_produccion';
    protected $primary = 'ppr_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('ppr_id', 'ppr_descripcion'))
                                ->where('ppr_estado=?', 1)
                                ->order('ppr_descripcion'));
    }

    //para detalle de donaciones anteriores
    public function getDesc($id) {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('ppr_descripcion'))
                                ->where('ppr_id=?', $id));
    }

    public function hacePlaquetas($perfil) {
        foreach ($perfil->hemocomponentes as $hemocomponente) {
            if ($hemocomponente->id == 2) {
                return true;
            }
        }
        return false;
    }

    private function devHemocomponentesParaEliminar($r) {
        $resultado = array();
        for ($i = 0; $i < count($r); $i++) {
            $e = $bandera = 0;
            while ($bandera == 0 && $e < count($this->hemocomponentes)) {
                if ($r[$i]['idhemocomponente'] == $this->hemocomponentes[$e]->id) {
                    $bandera = 1;
                }
                $e++;
            }
            if ($bandera == 0) {
                $resultado[] = $r[$i]['idhemocomponente'];
            }
        }
        return $resultado;
    }

    private function devHemocomponentesParaAgregar($r) {
        $resultado = array();
        foreach ($this->hemocomponentes as $item) {
            $e = $bandera = 0;
            while ($bandera == 0 && $e < count($r)) {
                if ($r[$e]['idhemocomponente'] == $item->id) {
                    $bandera = 1;
                }
                $e++;
            }
            if ($bandera == 0) {
                $resultado[] = $item->id;
            }
        }
        return $resultado;
    }

}
