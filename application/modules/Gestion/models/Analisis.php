<?php

class Gestion_models_Analisis extends Zend_Db_Table_Abstract {

	protected $_name = 'analisis';
	protected $_primary = 'ana_id';

        public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;        
        parent::__construct();
    }
    
	public function getPairs(){
		return $this->getDefaultAdapter()->fetchPairs($this->select(true)
			->reset('columns')->columns(array('ana_id','descripcion'))
		//	->where('def=?',1)
			->order('descripcion'));
	}
}
