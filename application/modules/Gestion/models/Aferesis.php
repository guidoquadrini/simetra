<?php

class Gestion_models_Aferesis extends Zend_Db_Table_Abstract {

	protected $_name = 'afe_tipos_aferesis';
	protected $_primary = 'afe_id';

        public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;        
        parent::__construct();
    }
    
	public function getPairs(){
		return $this->getDefaultAdapter()->fetchPairs($this->select(true)
			->reset('columns')->columns(array('afe_id','afe_descripcion'))
			->where('afe_estado=?',1)
			->order('afe_descripcion'));
	}
}