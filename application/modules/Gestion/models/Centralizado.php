<?php

class Gestion_models_Centralizado extends Zend_Db_Table_Abstract {

    protected $_name = 'centralizados';
    protected $_primary = 'don_id';

    
    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;        
        parent::__construct();
    }
    
    
    public function getByNro($don_nro) {
        $query = $this->select()
                ->from($this)
                ->where('don_nro=?', $don_nro);
        return $this->fetchRow($query);
    }
    
   
    public function ObtenerRegistrosDelLote($NroLote) {
        return $this->fetchAll($this->select(true)
                                ->where('nroLote = ?', $NroLote)
        );
    }

    public function getPendientes() {
        return $this->fetchAll($this->select(true)
                                ->where('lote_estado = 1')
        );
    }

    public function getByLote($nroLote) {
        $query = $this->select()
                
                ->from($this)
                ->where('nroLote=?', $nroLote);
        return $this->fetchRow($query);
    }

    public function getNuevoNroLote() {
        $vRet = $this->getUltimoNroLote()->nroLote;
        if ($vRet == null) {
            return 1;
        }
        $vRet++;
        return $vRet;
    }

    public function getUltimoNroLote() {
        return $this->fetchAll($this->select(true)
                                ->order('nroLote DESC')
                                ->limit(1))->current();
    }

    public function newIndex($efector, $year) {
        
        //Busca el id_don mas grande para el efector y el a�o
        $maximo = $this->fetchAll(
                        $this->select()
                                ->from($this, array(new Zend_Db_Expr("MAX(don_id) AS max_don_id")))
                                ->where('efe_id=?', $efector)
                                ->where(new Zend_Db_Expr('substring(don_nro, 5,2)') . ' = ?', $year)
                )->current();
        
        //Si no existe registro para este efector para este a�o, debe generarse el primero.
        if (!is_null($maximo['max_don_id'])) {


            //Recupera el registro de la donacion mas grande para ese efector ese a�o.
            $row = $this->fetchAll(
                            $this->select()
                                    ->from($this)
                                    ->where('don_id=?', $maximo['max_don_id']))->current();
            //Toma la secuencia y le suma uno.

            $NroSecuencia = substr($row['don_nro'], 6, 6);
            $NroSecuencia ++;
            //Agrega 0 a la izquierda del nro.
            while (strlen($NroSecuencia) < 6) {
                $NroSecuencia = '0' . (string) $NroSecuencia;
            }
        } else {
            $NroSecuencia = '000001';
        }

        $vRet = $year . $NroSecuencia;
        //Retorna un string con el formato YY + Nro de Secuencia. Ej A�o 2015 Nro de Secuencia 000001 => 15000001
        return $vRet;
    }

    public function pendientesDeEnvio() {
        $estado = 1; //Estado Impreso
        $query = $this->select()
                ->from($this, array('nroLote', new Zend_Db_Expr('MIN(don_nro)  as NroDesde'), new Zend_Db_Expr('MAX(don_nro) as NroHasta'), 'efe_id'))
                ->where('lote_estado = ?', $estado)
                ->group('nroLote');
        return $query->query()->fetchAll();
    }

    public function pendientesDeRecepcion($idEfector) {
        $estado = array(2,3); //Estado Enviado
        $query = $this->select()
                ->from($this, array('nroLote', new Zend_Db_Expr('MIN(don_nro)  as NroDesde'), new Zend_Db_Expr('MAX(don_nro) as NroHasta'),'lote_estado'))
                ->where('lote_estado IN (?)', $estado)
                ->where('efe_id = ?', $idEfector)
                ->group('nroLote')
                ->order('nroLote DESC');
        return $query->query()->fetchAll();
    }

    public function getPendientesImpresion() {
        $estado = 0; //Estado Generado
        $query = $this->select()
                ->distinct()
                ->from($this, array('nroLote'))
                ->where('lote_estado = ?', $estado);
        return $query->query()->fetchAll();
    }

    public function getCabeceras($nroLote_selected, $efe_selected, $estado_selected, $page, $perPage){
        
     $query =  $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                    'nroLote','efe_id',
                    new Zend_Db_Expr('MIN(don_nro)  as NroDesde'), 
                    new Zend_Db_Expr('MAX(don_nro) as NroHasta'),
                    'don_estado','lote_estado','usu_inicio'
                    ))
     //           ->where("nroLote = ?",$nroLote_selected)
                ->order('nroLote DESC')
                ->group('nroLote')
                ->limitPage($page, $perPage);
        
     if ($nroLote_selected !== NULL && $nroLote_selected !== ""){   
         $query->where("nroLote = ? ",$nroLote_selected);
     }
     if ($efe_selected[0] !== ''){
         $query->where('efe_id IN (?)', $efe_selected);
     }
     if ($estado_selected !== NULL && $estado_selected !== '') {
         $query->where('lote_estado = ? ', $estado_selected);
     }
    return $query->query()->fetchAll();
     
    }
    
    public function countGetCabeceras(){
        
     return count($this->fetchAll($this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                    'nroLote','efe_id',
                    new Zend_Db_Expr('MIN(don_nro)  as NroDesde'), 
                    new Zend_Db_Expr('MAX(don_nro) as NroHasta'),
                    'don_estado','lote_estado','usu_inicio'
                    ))                
                ->order('nroLote DESC')
                ->group('nroLote')
                ));
    }
    
    public function tamLote($nroLote) {
        $query = $this->select()
                ->from($this)
                ->where('nroLote = ?', $nroLote);
        return count($query->query()->fetchAll());
    }

}
