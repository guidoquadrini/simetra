<?php

class Gestion_PerfilesproduccionController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_PerfilesProduccion();
		$resultados = $model->fetchAll(null,'ppr_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
		$form = new Gestion_forms_perfilesProduccion();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_PerfilesProduccion();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	*
	*/
	public function modificarAction(){
		$id = (int)$this->getRequest()->getParam('ppr_id');
		$form = new Gestion_forms_perfilesProduccion();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$modelo = new Gestion_models_PerfilesProduccion();
				$modelo->find($id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$modelo = new Gestion_models_PerfilesProduccion();
			$form->populate($modelo->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	*
	*/
	public function cambiarEstadoAction(){
		$id = (int)$this->getRequest()->getParam('ppr_id');
		$st = (int)$this->getRequest()->getParam('st');

		$model = new Gestion_models_PerfilesProduccion();
		$model->find($id)->current()
			->setFromArray(array('ppr_estado' => $st))->save();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}
