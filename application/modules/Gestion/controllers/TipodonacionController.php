<?php

class Gestion_TipodonacionController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_Tipodonacion();
                $resultados = $model->fetchAll(null,'descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*/
	public function agregarAction(){
		$form = new Gestion_forms_tipodonacion();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Tipodonacion();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*/

	public function modificarAction(){
		$id = $this->getRequest()->getParam('id');
		$form = new Gestion_forms_tipodonacion();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Tipodonacion();
				$model->find($id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Tipodonacion();
			$form->populate($model->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	public function eliminarAction(){
		$id = $this->getRequest()->getParam('id');

		$model = new Gestion_models_Tipodonacion();
		$model->find($id)->current()->delete();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}