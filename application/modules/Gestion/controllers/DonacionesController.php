<?php

class Gestion_DonacionesController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_Donaciones();
		$resultados = $model->fetchAll(null,'tdo_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
		$form = new Gestion_forms_donaciones();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Donaciones();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*
	*/
	public function modificarAction(){
		$tdo_id = $this->getRequest()->getParam('tdo_id');
		$form = new Gestion_forms_donaciones();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Donaciones();
				$model->find($tdo_id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Donaciones();
			$form->populate($model->find($tdo_id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	public function cambiarEstadoAction(){
		$id = (int)$this->getRequest()->getParam('tdo_id');
		$st = (int)$this->getRequest()->getParam('st');

		$model = new Gestion_models_Donaciones();
		$model->find($id)->current()
			->setFromArray(array('tdo_estado' => $st))->save();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}
