<?php

class Gestion_CentralizadoController extends BootPoint {

    public function indexAction() {
        
    }

    public function altaLoteAction() {
        
    }

    public function envioAction() {
        $modelCen = new Gestion_models_Centralizado();
        $PendientesDeEnvio = $modelCen->pendientesDeEnvio();
        $cache = $this->getInvokeArg('bootstrap')->getResource('cache');

        if (!$efectores = $cache->load('efectores')) {
            $modelEfec = new Acceso_models_Efectores();
            $efectores = $modelEfec->getPairs();
            $cache->save($efectores, 'efectores');
        }

        for ($i = 0; $i < count($PendientesDeEnvio); $i++) {
            $PendientesDeEnvio[$i] = array_merge($PendientesDeEnvio[$i], array('Efector' => $efectores[$PendientesDeEnvio[$i]['efe_id']]));
        }

        $this->view->Pendientes = $PendientesDeEnvio;
    }

    public function recepcionAction() {
        //"metodo recepcionAction en controladora de centralizado"
        $modelCen = new Gestion_models_Centralizado();
        $idEfector = $this->efector_activo['id_efector'];
        $PendientesDeEnvio = $modelCen->pendientesDeRecepcion($idEfector);
        $this->view->Pendientes = $PendientesDeEnvio;
    }

    public function solicitudAction() {
        //No se implementa. 
    }

    public function validarCodigoUtilizableAction() {
        if (is_null($this->getRequest()->getParam('codigo', null))) {
            $response = array('st' => 'NOK', 'mensaje' => 'Debe ingresar un Código Centralizado Valido.');
            $this->_helper->json($response);
        }
        $codigo = $this->getRequest()->getParam('codigo');
        $mCentralizado = new Gestion_models_Centralizado();
        $row_Centralizado = $mCentralizado->getByNro($codigo);

        if (is_null($row_Centralizado)) {
            $response = array('st' => 'NOK', 'mensaje' => 'Código de donación Inexistente.');
        }
        if ($row_Centralizado->don_estado == 2) {
            $response = array('st' => 'NOK', 'mensaje' => 'Código de donación ya utilizado.');
        }
        if ($response == null) {
            $response = array('st' => 'OK', 'mensaje' => '', 'efector' => $row_Centralizado->efe_id);
        }

        $this->_helper->json($response);
    }

    public function generacionAction() {
        $efe_selected = $this->getRequest()->getParam('cbo_efectores');
        $estado_selected = $this->getRequest()->getParam('cbo_estados');
        $nroLote_selected = $this->getRequest()->getParam('fld_nroLote');
        
        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();

        //Efectores asociados al usuario.
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);
        
        if ($efe_selected === null || $efe_selected === '' ) {
            foreach ($efes_usu as $e) {
                $efes_ids[] = $e['id_efector'];
            }
        } else {
            $efes_ids[] = $efe_selected;
        }

        $modelCen = new Gestion_models_Centralizado();

        $cache = $this->getInvokeArg('bootstrap')->getResource('cache');

        if (!$efectores = $cache->load('efectores')) {
            $modelEfec = new Acceso_models_Efectores();
            $efectores = $modelEfec->getPairs();

            $cache->save($efectores, 'efectores');
        }
        //Definicion de filas por pagina
        $perPage = 10;
        
        $lotes = $modelCen->getCabeceras($nroLote_selected, $efes_ids, $estado_selected, $this->page, $perPage);

        if (sizeof($lotes) > 0) {
            $total = $modelCen->countGetCabeceras();
            $paginas = ceil($total / $perPage);
        }

        $mUsuario = new Acceso_models_Usuarios();
        //$lotes = $lotes->toArray();
        foreach ($lotes as &$lote) {
            $lote['tam_lote'] = $modelCen->tamLote($lote['nroLote']);
            $lote['usuario_genero'] = $mUsuario->find($lote['usu_inicio'])->current()->usu_usuario;
        };
               
        //Paso de datos a la vista.
        $this->view->efe_selected = $efe_selected;
        $this->view->nroLote_selected = $nroLote_selected;
        $this->view->estado_selected = $estado_selected;
        $this->view->paginas = $paginas;
        $this->view->mensaje = $this->getRequest()->getParam('mensaje', null);
        $this->getRequest()->setParam('mensaje', '');
        $this->view->efectores = $efes_usu;
        $this->view->lotes = $lotes;
    }

    /**
     * Cambiar estado de un lote.
     * (0) Lote Generado - (1) Lote Impreso - (2) Lote Enviado - (3) Lote Recibido 
     */
    public function cambiarEstadoAction() {
        
        $st = (int) $this->getRequest()->getParam('st');
        $nroLote = (int) $this->getRequest()->getParam('nro_lote');
        $estado = (int) $this->getRequest()->getParam('estado');
        
        $vRet = false;
        
        try {
            $vRet = $this->cambiaEstadoLote($nroLote, $estado);
        
            
        } catch (Exception $exc) {
            
            $vRet = false;
            
        }
        
        if ($vRet == TRUE) {
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'nok'));
        }
    }

    public function cambiaEstadoLote($nroLote, $estado) {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $vRet = null;
        
        $modelCen = new Gestion_models_Centralizado();
        
        $data = array('lote_estado' => ($estado));
        try {
            $modelCen->update($data, "nroLote = {$nroLote}");
            $vRet = TRUE;
        } catch (Exception $exc) {
            $vRet = FALSE;
        }
        return $vRet;
    }

    public function eliminarLoteAction() {
        $st = (int) $this->getRequest()->getParam('st');
        $nroLote = (int) $this->getRequest()->getParam('nro_lote');
        $modelCen = new Gestion_models_Centralizado();

        $vRet = false;
        try {
            $vRet = $modelCen->delete(array('nroLote = ?' => $nroLote));
        } catch (Exception $exc) {
            $vRet = false; //TODO: Add Registry to System LOG
        }
        if ($vRet) {
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'nok'));
        }
    }

    protected $formAltaLote;

    public function createLoteAction() {

        //Levanta el form de alta de lote
        $this->formAltaLote = new Gestion_forms_altalote();
        $form = &$this->formAltaLote;

        //Verifico que se haya realizado un posteo.
        if ($this->getRequest()->isPost()) {

            //Tomo los datos posteados.
            $post = $this->getRequest()->getPost();

            if ($form->isValid($post)) {
                $values = $form->getValues();
                //Obtener parametros del request            
                $cant = (int) $this->getRequest()->getParam('cnt_codigos');
                $efector = $this->getRequest()->getParam('cbo_efector');
                $modEfectores = new Efector_models_EfectoresParametros();
                $efector_selecionado = $modEfectores->getParametros($efector);

                //Se levanta el modelo de Centralizado
                $modeloCodCen = new Gestion_models_Centralizado();

                $nroLote = $modeloCodCen->getNuevoNroLote();
                $loteEstado = 0;
                /* Estados postibles: (0)Generado | (1)Impreso | (2)Enviado | (3)Utilizado */

                $codigos = array();
                for ($i = 0; $i < $cant; $i++) {
                    $cod = $efector_selecionado['nombre_corto'] . 'C';
                    $index = sprintf("%06d", $modeloCodCen->newIndex($efector, date('y')));
                    $codigo = $cod . $index;
                    $codigos[] = $codigo;

                    //Armo Arreglo para grabar el registro.
                    $Registro = array(
                        'nroLote' => $nroLote,
                        'efe_id' => $efector,
                        'don_nro' => $codigo,
                        'usu_inicio' => $this->auth->getIdentity()->usu_id,
                        'don_estado' => 1,
                        'lote_estado' => $loteEstado
                    );
                    $modeloCodCen->createRow($Registro)->save();
                }
                //Redireccionar al listado
                $this->_helper->redirector('generacion', $this->_request->controller, $this->_request->module, array('mensaje' => 'El lote fue generado exitosamente.'));
            } else {//Validacion NO existosa
                $form->populate($form->getValues());
            }
        }

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();

        //Efectores asociados al usuario.
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);
        $modelCen = new Gestion_models_Centralizado();
        $NuevoNroLote = $modelCen->getNuevoNroLote();

        $cache = $this->getInvokeArg('bootstrap')->getResource('cache');

        if (!$efectores = $cache->load('efectores')) {
            $modelEfec = new Acceso_models_Efectores();
            $efectores = $modelEfec->getPairs();

            $cache->save($efectores, 'efectores');
        }

//        $pendientes = $modelCen->getPendientesImpresion();
//
//        for ($i = 0; $i < count($pendientes); $i++) {
//            $pendientesImpresion[$i] = array(
//                'nroLote' => $pendientes[$i]['nroLote'],
//                'tamLote' => $modelCen->tamLote($pendientes[$i]['nroLote']),
//                'destLote' => $efectores[$modelCen->getByLote($pendientes[$i])['efe_id']]
//            );
//        }
        //Paso de datos a la vista.
        $this->view->efectores = $efes_usu;

        $this->view->campo_forms = array(
            'cbo_efector' => 'Efector',
            'nro_copias' => 'Numero de copias',
            'nroLote' => 'Numero de lote',
            'cnt_codigos' => 'Cantidad de copias'
        );
        $id_efector = array_column($efes_usu, 'id_efector');
        $nom_efector = array_map('utf8_encode', array_column($efes_usu, 'nomest'));
        $efectores_asociados = array_combine($id_efector, $nom_efector);
        $form->cbo_efector->addMultiOptions($efectores_asociados);

        $form->setDefault("nroLote", $NuevoNroLote);
        $this->view->nroLote = $NuevoNroLote;
        $form->setDefault("nro_copias", 10);

        //Inicializacion de datos para la creacion de un lote.
        $this->view->form = $form;
        $this->renderScript('centralizado/create-lote.phtml');
    }

    // Codigo de Barras:Genera codigo de barras en base a parametro -> don_nro 
    private function codigos($don_nro) {
        //$don_nro = 'HPRC15000001';
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $pdf = new Zend_Pdf();

        $x = 290;
        $y = 70;
        $top = 25;
        $leftA = 6;
        $leftB = 155;

        $page = new Zend_Pdf_Page($x, $y);
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/arial.ttf');

        $CNT_CODIGOS_POR_DONACION = 1;
        for ($i = 1; $i <= $CNT_CODIGOS_POR_DONACION; $i++) {
            $page = new Zend_Pdf_Page($x, $y);
            $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/arial.ttf');
            $page->setFont($font, 8);
            $pdf->pages[] = $page;
        }

        for ($i = 0; $i < $CNT_CODIGOS_POR_DONACION; $i++) {
            /* Codigo de barra A */
            $barcodeOptions = array(
                'font' => APPLICATION_PATH . '/arial.ttf',
                'text' => $don_nro,
                'fontSize' => 12,
                'barThickWidth' => 3,
                'barThinWidth' => 1,
                'stretchText' => true
            );
            $barcodeRender = array(
                'topOffset' => $top,
                'leftOffset' => $leftA
            );
            Zend_Barcode::factory('lima', 'pdf', $barcodeOptions, $barcodeRender
            )->setResource($pdf, $i)->draw();
            $barcodeRender['leftOffset'] = $leftB;

            /* Codigo de barra B - Igual a A excepto en el leftOffset */
            Zend_Barcode::factory(
                    'cod', 'pdf', $barcodeOptions, $barcodeRender
            )->setResource($pdf, $i)->draw();
        }

        return $pdf;
    }

    private function procesar_datos_lote($registros) {


        $count = 0;
        foreach ($registros as $registro) {
            $contingencia = (strlen($registro->don_nro) == 12) ? true : false;
            //12 Cantidad de caracteres que tiene un codigo centralizado.
            if ($count == 0) {
                $min = substr($registro->don_nro, 6, 6);
                $max = substr($registro->don_nro, 6, 6);
                $count ++;
                $efector = substr($registro->don_nro, 0, 3);
                $ciclo = substr($registro->don_nro, 4, 2);
                continue;
            }
            $var = substr($registro->don_nro, 6, 6);
            if ($var < $min) {
                $min = $var;
            }
            if ($var > $max) {
                $max = $var;
            }
        }
        $vRet = [
            'Inicio' => $min,
            'Fin' => $max,
            'Efector' => $efector,
            'Ciclo' => $ciclo,
            'Contingencia' => $contingencia
        ];
        return $vRet;
    }

    public function imprimirLoteAction() {
        $st = $this->getRequest()->getParam('st');
        $NroLote = $this->getRequest()->getParam('nroLote');
        if ((bool) $this->cambiaEstadoLote($NroLote, 1)) {
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'nok'));
        }


        $datos = $this->procesar_datos_lote($RegistrosLote);

        /* Estructura Json a Enviar. */
//        {"Codigos":
//            [
//                {"Cantidad":3,"Efector":"HPR","Contingencia":true,"Ciclo":16,"Inicio":1,"Fin":1},
//                {"Cantidad":1,"Efector":"HCH","Contingencia":false,"Ciclo":16,"Inicio":7,"Fin":8}
//            ]
//        }

        $this->_helper->json(array('st' => 'ok', 'datos' => ($enviar)));
    }

    public function registrarEnvioDeLoteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $st = $this->getRequest()->getParam('st');
        $NroLote = $this->getRequest()->getParam('nroLote');

        if ((bool) $this->cambiaEstadoLote($NroLote, 2)) {
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'nok'));
        }
    }

    public function registrarRecepcionDeLoteAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $st = $this->getRequest()->getParam('st');
        $NroLote = $this->getRequest()->getParam('nroLote');

        if ((bool) $this->cambiaEstadoLote($NroLote, 3)) {
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'nok'));
        }
    }

}
