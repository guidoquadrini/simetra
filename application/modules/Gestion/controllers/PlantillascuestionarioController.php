<?php

class Gestion_PlantillascuestionarioController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_Plantillascuestionario();
		$resultados = $model->fetchAll(null,'titulo')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
		$form = new Gestion_forms_Plantillascuestionario();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Plantillascuestionario();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*
	*/
	public function modificarAction(){
		$id = $this->getRequest()->getParam('id');
		$form = new Gestion_forms_plantillascuestionario();
                
                if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Plantillascuestionario();
				$model->find($id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Plantillascuestionario();
			$form->populate($model->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

    public function eliminarAction(){
        $id = $this->getRequest()->getParam('id');

        $model = new Gestion_models_Plantillascuestionario();
        $model->find($id)->current()->delete();

        $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
        $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
    }   
    
    
    
    	/**
	* Nuevo (viene del formulario)
	*
	*/
	public function nuevoAction(){
            
            $modelo = new Gestion_models_Plantillacuestionario();
            
            $modelo->createRow(array(
                        'titulo' => $this->getRequest()->getParam('titulo'),
                        'estado' => $this->getRequest()->getParam('estado'),
                        'version' => $this->getRequest()->getParam('version'),
                        'observacion' => $this->getRequest()->getParam('observacion'),
                        'usuario_creacion_id' => $this->auth->getIdentity()->usu_id,
                        'fecha_creacion' => new Zend_Db_Expr('NOW()')
                    ))->save();

            $this->_helper->json(array('st' => 'ok'));             

	}
        
}

