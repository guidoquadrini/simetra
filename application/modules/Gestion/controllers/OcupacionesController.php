<?php

class Gestion_OcupacionesController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_Ocupaciones();
		$resultados = $model->fetchAll(null,'ocu_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*/
	public function agregarAction(){
		$form = new Gestion_forms_ocupaciones();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Ocupaciones();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*/

	public function modificarAction(){
		$ocu_id = $this->getRequest()->getParam('ocu_id');
		$form = new Gestion_forms_ocupaciones();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Ocupaciones();
				$model->find($ocu_id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Ocupaciones();
			$form->populate($model->find($ocu_id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	public function eliminarAction(){
		$ocu_id = $this->getRequest()->getParam('ocu_id');

		$model = new Gestion_models_Ocupaciones();
		$model->find($ocu_id)->current()->delete();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}