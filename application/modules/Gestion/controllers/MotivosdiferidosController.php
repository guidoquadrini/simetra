<?php

class Gestion_MotivosdiferidosController extends BootPoint {

	/** Listar **/
	public function indexAction(){
		$model = new Gestion_models_Motivos();
		$resultados = $model->fetchAll("mot_cat='DIF'",'mot_descripcion')->toArray();

		$this->view->resultados = $resultados;
		$this->view->form_title = 'Motivos Diferidos';

		$this->renderScript('motivos/index.phtml');
	}

	/** Alta **/
	public function agregarAction(){
		$form = new Gestion_forms_motivos();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Motivos();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		$form->mot_cat->setValue('DIF');

		$this->view->form = $form;
		$this->view->form_title = 'Motivos Diferidos';
		$this->view->form_desc = 'Alta';
		$this->view->controller = $this->_request->controller;

		$this->renderScript('motivos/form.phtml');
	}

	/** Modificar **/
	public function modificarAction(){
		$mot_id = $this->getRequest()->getParam('mot_id');
		$form = new Gestion_forms_motivos();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Motivos();
				$model->find($mot_id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Motivos();
			$form->populate($model->find($mot_id)->current()->toArray());
		}
		$form->mot_cat->setValue('DIF');

		$this->view->form = $form;
		$this->view->form_title = 'Motivos Diferidos';
		$this->view->form_desc = 'Modificar';
		$this->view->controller = $this->_request->controller;

		$this->renderScript('motivos/form.phtml');
	}

	public function cambiarEstadoAction(){
		$id = (int)$this->getRequest()->getParam('mot_id');
		$st = (int)$this->getRequest()->getParam('st');

		$model = new Gestion_models_Motivos();
		$model->find($id)->current()
			->setFromArray(array('mot_estado' => $st))->save();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}
