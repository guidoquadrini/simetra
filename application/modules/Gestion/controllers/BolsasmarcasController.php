<?php

class Gestion_BolsasmarcasController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_Bolsasmarcas();
		$resultados = $model->fetchAll(null,'marca_bolsa')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*/
	public function agregarAction(){
		$form = new Gestion_forms_bolsasmarcas();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Bolsasmarcas();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*/

	public function modificarAction(){
		$id = $this->getRequest()->getParam('id');
		$form = new Gestion_forms_bolsasmarcas();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Bolsasmarcas();
				$model->find($id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Bolsasmarcas();
			$form->populate($model->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	public function eliminarAction(){
		$id = $this->getRequest()->getParam('id');

		$model = new Gestion_models_Bolsasmarcas();
		$model->find($id)->current()->delete();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}