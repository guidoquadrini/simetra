<?php

class Gestion_HemocomponentesController extends BootPoint {

	/** Listar Hemocomponentes **/
	public function indexAction(){
		$model = new Gestion_models_Hemocomponentes();
		$resultados = $model->fetchAll(null,'hem_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/** Alta Hemocompoente **/
	public function agregarAction(){
		$form = new Gestion_forms_hemocomponentes();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Hemocomponentes();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/** Modificar Hemocomponente **/
	public function modificarAction(){
		$hem_id = $this->getRequest()->getParam('hem_id');
		$form = new Gestion_forms_hemocomponentes();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Hemocomponentes();
				$model->find($hem_id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Hemocomponentes();
			$form->populate($model->find($hem_id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	public function cambiarEstadoAction(){
		$id = (int)$this->getRequest()->getParam('hem_id');
		$st = (int)$this->getRequest()->getParam('st');

		$model = new Gestion_models_Hemocomponentes();
		$model->find($id)->current()
			->setFromArray(array('hem_estado' => $st))->save();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}
