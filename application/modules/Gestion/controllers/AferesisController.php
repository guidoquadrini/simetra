<?php

class Gestion_AferesisController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Gestion_models_Aferesis();
		$resultados = $model->fetchAll(null,'afe_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
		$form = new Gestion_forms_aferesis();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Aferesis();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*
	*/
	public function modificarAction(){
		$afe_id = $this->getRequest()->getParam('afe_id');
		$form = new Gestion_forms_aferesis();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Gestion_models_Aferesis();
				$model->find($afe_id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Gestion_models_Aferesis();
			$form->populate($model->find($afe_id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	public function cambiarEstadoAction(){
		$id = (int)$this->getRequest()->getParam('afe_id');
		$st = (int)$this->getRequest()->getParam('st');

		$model = new Gestion_models_Aferesis();
		$model->find($id)->current()
			->setFromArray(array('afe_estado' => $st))->save();

		$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
		$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
	}
}
