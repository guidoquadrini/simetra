<?php

class Gestion_BolsastiposController extends BootPoint {

    /**
     * Listar
     *
     */
    public function indexAction() {
        $model = new Gestion_models_Bolsastipos();
        $resultados = $model->fetchAll(null, 'bolsa_tipo')->toArray();

        $this->view->resultados = $resultados;
    }

    /**
     * Alta
     */
    public function agregarAction() {
        
        $form = new Gestion_forms_bolsastipos();
        
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            if ($form->isValid($post)) {
                $model = new Gestion_models_Bolsastipos();
                $model->createRow($form->getValues())->save();

                $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
                $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
            }
        }

        $this->view->form = $form;
        $this->view->form_desc = 'Alta';

        $this->renderScript("{$this->_request->controller}/form.phtml");
    }

    /**
     * Modificar
     */
    public function modificarAction() {
        $id = $this->getRequest()->getParam('id');
        $form = new Gestion_forms_bolsastipos();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            if ($form->isValid($post)) {
                $model = new Gestion_models_Bolsastipos();
                $model->find($id)->current()
                        ->setFromArray($form->getValues())->save();

                $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
                $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
            }
        } else {
            $model = new Gestion_models_Bolsastipos();
            $form->populate($model->find($id)->current()->toArray());
        }

        $this->view->form = $form;
        $this->view->form_desc = 'Modificar';

        $this->renderScript("{$this->_request->controller}/form.phtml");
    }

    public function eliminarAction() {
        
        $id = $this->getRequest()->getParam('id');
        $modelDonaciones_Ext = new Efector_models_DetallesDonaciones();
        $donaciones = $modelDonaciones_Ext->fetchAll('tipo_bolsa = ' . $id);
        
        if ($donaciones->count() == 0 ) {
            $model = new Gestion_models_Bolsastipos();
            $model->find($id)->current()->delete();
            $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
            $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
        }
    }

}
