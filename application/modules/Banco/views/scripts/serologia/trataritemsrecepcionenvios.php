<div class="ui-widget-header ui-corner-all" style="padding: 5px;margin: 5px auto;text-align:left;">
<span class="tituloModulo">:: Detalle de Envio Nro. <?php echo isset($envio)?sprintf("%06d",$envio->nro):"";?> (<?php echo isset($envio)?$envio->efector->nombre:"";?>) - Total Donaciones: <?php echo isset($cantidadItems)?$cantidadItems:"";?> ::</span>
</div>
<div class="ui-widget ui-widget-content ui-corner-bl ui-corner-br" style="margin-bottom:10px;padding:5px;">	
            <div>
               <table align="left">
                <tr>
                 <td style="font-size:20px;font-weigth:bold;">
                   Buscar Donaci&oacute;n:
                 </td>
                 <td align="left">
                   <input id="txtFiltro" type="text" size="10" style="text-align:right;">
                 </td>
                 <td align="left">
                   <button id="btnBuscar">Filtrar</button>
                 </td>
                 <tr>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="tablaVisorItemsEnvios">
             <thead><tr>
               <th>id</th>
	       <th>Donaci&oacute;n</th>
	       <th>Fecha Ext.</th>
               <th>Acci&oacute;n</th>
               <th>Peso (Gr.)</th>
               <th>Observaciones</th>
               <th></th>
             </tr></thead>
            </table>
        </div>
        
</div>
<div class="ui-widget-content ui-corner-all" style="padding:4px;">
 <button id="btnCancelar">Volver a Env&iacute;os</button>

</div>
<div id="diagSolicitud" title="Estado de Solicitud:">
   <iframe id="iframeEstadoSolicitud" style="width:95%;height:95%;border: 0px;" src=""></iframe>	
</div>
<script type="text/javascript" src="js/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="js/modelo/recepcionenvios/filtroenvios.js"></script>
<script type="text/javascript">

$("#btnCancelar").button({icons: {primary: "ui-icon-closethick"}});
$("#btnBuscar").button({});

$("#btnBuscar").click(function() {
        laTabla.fnDraw();

});


   $("#btnCancelar").click(function () {
        rutear("ctrrecepcionenvios");
   });

   var laTabla=$("#tablaVisorItemsEnvios").dataTable({
		"bJQueryUI": true,
		"bServerSide": true,
                "bScrollInfinite": true,
                "iScrollLoadGap": 10, 
                "sDom":'t',
                "bInfo": false,
	        "bFilter": false,
		"sAjaxSource": "ctrrecepcionenvios/buscarItemsEnvio",
		"oLanguage": {
			"sLengthMenu": "Mostrando _MENU_ filas por página",
			"sZeroRecords": "No se encontraron registros",
			"sInfo": "Mostrando _START_ hasta _END_ de _TOTAL_ filas",
			"sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
			"sInfoFiltered": "(filtrados de _MAX_ registros)"
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "envio", "value": <?php echo isset($envio)?$envio->id:"''";?> } );
                        aoData.push( { "name": "donacion", "value": $("#txtFiltro").val()} );
			$.ajax( {
				"dataType": "json",
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			} );
		},
		"sScrollY": "250px",
		"fnDrawCallback": function() {
			filas=laTabla.fnGetNodes();
			if (filas.length>0) {
				$(filas[0]).addClass("row_selected");
			}
		},
                "aoColumns": [
                  {"bVisible":false},
                  {"sWidth":"90px"},
                  {"sWidth":"150px"},
                  {"sWidth":"90px"},
                  {"sWidth":"90px"},
                  null,
                  null
                ]
	});



$("#btnBuscarSolicitud").button({icons: {primary: "ui-icon-search"}});
$("#btnBuscarSolicitud").click(function() {
		laTabla.fnDraw();
});



$("#codsolicitud").keydown(function (e) {
           if (e.which == 13) {
            if ($("#codsolicitud").val()!="") {
                mostrarTratarDonaciones($("#codsolicitud").val());
            } else {
                alert("Se debe completar el codigo de donacion");
                return;
            }
           } 
});




$("#tablaVisorDonaciones tbody").click(function(event) {
		$(laTabla.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass("row_selected");
		});
		$(event.target.parentNode).addClass("row_selected");
	});


	function obtenerSeleccionados( tabla ) {
		var aReturn = new Array();
		var aTrs = tabla.fnGetNodes();
		for ( var i=0 ; i<aTrs.length ; i++ ) {
			if ( $(aTrs[i]).hasClass("row_selected") ) {
				aReturn.push( aTrs[i] );
			}
		}
		return aReturn;
	}

$(".registrarItem").die("click");
$(".registrarItem").live("click",function(e){
    e.preventDefault();
    trPadre = $(this).parent().parent();
    tdAccion = trPadre.find("td").eq(2).children("select");
    tdPeso = trPadre.find("td").eq(3).children("input");
  
    if (tdAccion.val()==-1) {
       alert("Se debe seleccionar una accion para la Donacion");
       return;
    }    

    if ($.trim(tdPeso.val())=='') {
       alert("Se debe completar el peso de la Donacion");
       return;
    } else {
       //Verificar formato de decimal para el peso
       peso = parseInt(tdPeso.val());
       if (peso<500 || peso>700) {
          if (tdAccion.val()==1) {
             alert("Error: La donacion no cumple con los limites de peso!");
             return;
          }
       }
    }

    //Hacer la llamada
    opciones = "donacion/" + $(this).attr("rel") + "/";
    opciones = opciones + "accion/" + tdAccion.val() + "/";
    opciones = opciones + "peso/" + tdPeso.val();
    auxlink = $("<a id=\"iframe\" href=\"ctrrecepcionenvios/mostrarRegistrarItem/" + opciones +"\">");
    auxlink.fancybox({
		    "autoDimensions":true,
		    "scrolling":"no"
            });
    auxlink.trigger("click");
    
});

$(".edicionItem").die("click");
$(".edicionItem").live("click",function(e){
    e.preventDefault();
    trPadre = $(this).parent().parent();
    tdAccion = trPadre.find("td").eq(2).children("select");
    tdPeso = trPadre.find("td").eq(3).children("input");
    tdOpciones = trPadre.find("td").eq(5);
    //Habilitar edicion
    tdAccion.attr("disabled",false);
    tdPeso.removeAttr('readonly'); 
    //Poner Botones de Edicion
    opciones = '<a href="#" rel="'+ $(this).attr('rel') + '" class="aceptarEdicionItem">Aceptar</a>';
    opciones = opciones + '&nbsp;<a href="#" rel="'+ $(this).attr('rel') + '" class="cancelarEdicionItem">Cancelar</a>';
    tdOpciones.html(opciones);
});

$(".aceptarEdicionItem").die("click");
$(".aceptarEdicionItem").live("click",function(e){
    e.preventDefault();
    trPadre = $(this).parent().parent();
    tdAccion = trPadre.find("td").eq(2).children("select");
    tdPeso = trPadre.find("td").eq(3).children("input");
  
    if (tdAccion.val()==-1) {
       alert("Se debe seleccionar una accion para la Donacion");
       return;
    }    

    if ($.trim(tdPeso.val())=='') {
       alert("Se debe completar el peso de la Donacion");
       return;
    } else {
       //Verificar formato de decimal para el peso
       peso = parseInt(tdPeso.val());
       if (peso<500 || peso>700) {
          if (tdAccion.val()==1) {
             alert("Error: La donacion no cumple con los limites de peso!");
             return;
          }
       }
    }

    //Hacer la llamada
    opciones = "donacion/" + $(this).attr("rel") + "/";
    opciones = opciones + "accion/" + tdAccion.val() + "/";
    opciones = opciones + "peso/" + tdPeso.val();
    auxlink = $("<a id=\"iframe\" href=\"ctrrecepcionenvios/mostrarRegistrarItem/" + opciones +"\">");
    auxlink.fancybox({
		    "autoDimensions":true,
		    "scrolling":"no"
            });
    auxlink.trigger("click");
    
});

$(".cancelarEdicionItem").die("click");
$(".cancelarEdicionItem").live("click",function(e){
    e.preventDefault();
    trPadre = $(this).parent().parent();
    tdAccion = trPadre.find("td").eq(2).children("select");
    tdPeso = trPadre.find("td").eq(3).children("input");
    tdOpciones = trPadre.find("td").eq(5);
    //Deshabilitar edicion
    tdAccion.attr("disabled","diabled");
    tdPeso.attr("readonly","readonly"); 
    //Poner Botones de Edicion
    tdOpciones.html('<a href="#" rel="'+ $(this).attr('rel') + '" class="edicionItem">Editar Donaci&oacute;n</a>');
    
});


/*
function mostrarTratarSolicitud(codigo) {
         var auxlink = $("<a id=\"iframe\" href=\"ctrexamenmedico/tratarSolicitud/solicitud/" + codigo +"\">");
                refAuxlink = auxlink;
                auxlink.fancybox({
                              "autoDimensions":true,
                              "scrolling":"no",
                              onComplete: function() {
                                 $("#fancybox-wrap").css("top","5px");
                               }        
                              });
          auxlink.trigger("click");
}
*/

function mostrarTratarLectura(donacion) {
          var auxlink = $("<a id=\"iframe\" href=\"ctrformularioa/tratarDonacion/donacion/" + donacion +"\">");
                refAuxlink = auxlink;
                auxlink.fancybox({
                              "autoDimensions":true,
                              "scrolling":"no",
                              onComplete: function() {
                                 $("#fancybox-wrap").css("top","5px");
                               }        
                              });
          auxlink.trigger("click");
}



</script>
