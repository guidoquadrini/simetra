<div style="width:400px;">
	<input type="hidden" id="iddonacion" value="<?php echo isset($iddonacion)?$iddonacion:"";?>">
	<input type="hidden" id="nrobolsa" value="<?php echo isset($nrobolsa)?$nrobolsa:"";?>">
	<div class="ui-widget-header">
		<h3>Resultado Producci&oacute;n:</h3>
		<h3>Donaci&oacute;n: <?php echo isset($donacion)?$donacion:"";?></h3>
		<h3>Hemocomponente: <?php echo isset($hemocomponente)?$hemocomponente:"";?></h3>
	</div>
	<div class="ui-widget-content" style="width:100%;">
		<table align="center">
			<tr>
				<td align="left"><strong>Resultado:</strong></td>
				<td align="left">
					<select id="resultado">
						<option value="-1"></option>
						<option value="2">Producido Ok</option>
						<option value="3">Descarte</option>
					</select>
				</td>
			</tr>
			<tr id="filaDescarte" style="display:none;">
				<td align="left"><strong>Motivo:</strong></td>
				<td align="left">
					<select id="lstDescarte">
						<option value="-1"></option>
						<option value="1">Bolsa Rota</option>
						<option value="2">Bolsa Contaminada</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align="left"><strong>Peso:</strong></td>
				<td align="left">
					<input id="peso" type="text" size="5" style="text-align:right;">
				</td>
			</tr>
			<tr>
				<td valign="top" align="left"><strong>Obs.</strong></td>
				<td align="left"><textarea style="width:200px;height:70px;"></textarea>
			</tr>
		</table>
	</div>
	<div class="ui-widget-content" style="padding:5px;border-top: 1px solid #CCCCCC;">
		<button id="btnAceptar">Aceptar</button>
		<table id="haciendo_llamado" style="display:none" align="center">
			<tr>
				<td><strong>Registando...</strong></td>
				<td><img id="resultado_registro" src="css/verificando.gif"></td>
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">

	$("#btnAceptar").button({icons: {primary: "ui-icon-check"}});

	$("#resultado").change(function () {
		if ($("#resultado option:selected").val()==3) {
			$("#filaDescarte").show();
		} else {
			$("#filaDescarte").hide();
		}
	});

	$("#btnAceptar").click(function () {
		//Verificar
		if ($("#resultado option:selected").val()=="-1") {
			alert("Se debe seleccion una accion!");
			return;
		}

		if ($.trim($("#peso").val())=='') {
			alert("Se debe completar el peso del Hemocomponente!");
			return;
		}

		$(this).hide();
		$("#haciendo_llamado").show();

		$.post("ctrproduccion/asignarResultadoHemocomponente",
		{
			iddonacion: $("#iddonacion").val(),
			estado: $("#resultado option:selected").val(),
			peso: $("#peso").val(),
			nrobolsa: $("#nrobolsa").val()
		},
		function (datos) {
			if (datos.estado=="Ok") {
				$("#resultado_registro").attr("src","css/checkazul.jpeg");
				laTabla.fnDraw();
			} else {
				$("#resultado_registro").attr("src","css/falloverificacion.jpeg")
			}
		},
		"json");
	});

</script>
