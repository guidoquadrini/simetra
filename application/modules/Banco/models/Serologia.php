<?php

class Banco_models_Serologia extends Zend_Db_Table_Abstract {

    protected $_name = 'serologia';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getDatosDonacion($don_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)
                ->where('don_id = ?', $don_id)
                ->order('id DESC');

        return $this->fetchAll($query)->current();
    }
    
    

}
