<?php

class Banco_models_Analisis extends Zend_Db_Table_Abstract {

    protected $_name = 'analisis';
    protected $_primary = 'ana_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getAnalisisByEnf($enf_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)
                ->where('enf_id = ?', $enf_id);

        return $this->fetchAll($query)->current()->toArray();
    }
    
    public function getDescripcionByCodLab($codAna) {
        $query = $this->select(true)
                ->reset('columns')
                ->columns(array('descripcion'))
                ->where('cod = ?', $codAna);
        return $this->fetchRow($query);
    }

    public function getDefaults() {
        $query = $this->select(true)
                ->reset('columns')
                ->columns(array('cod'))
                ->where('def = ?', 1);

        return $this->fetchAll($query);
    }

    public function getCodByID($ids) {
        $query = $this->select(true)
                ->reset('columns')
                ->columns(array('cod'))
                ->where('ana_id IN (?)', $ids);

        return $this->fetchAll($query)->toArray();
    }

    public function getCodEnf($cod_ana) {
        $query = $this->select(true)
                ->reset('columns')
                ->columns(array('enf_id'))
                ->where('cod = ?', $cod_ana);

        return $this->fetchRow($query);
    }

    public function getNomAna() {
        $query = $this->select(true)
                ->reset('columns')
                ->columns(array('cod', 'descripcion'))
                ->where(1);

        return $this->fetchAll($query)->toArray();
    }

}
