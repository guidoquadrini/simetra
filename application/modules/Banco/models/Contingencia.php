<?php

class Banco_models_Contingencia extends Zend_Db_Table_Abstract {

    protected $_name = 'contingencia';
    protected $_primary = 'don_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByNro($don_nro) {
        $query = $this->select()
                ->from($this)
                ->where('don_nro=?', $don_nro);

        return $this->fetchRow($query);
    }

    public function newIndex($efector, $year) {
        $query = $this->fetchAll(
                $this->select()
                        ->from($this, array(new Zend_Db_Expr('IFNULL(max(CONVERT(MID(don_nro,7),UNSIGNED INTEGER)),0) + 1 as nuevo')))
                        ->where('efe_id=?', $efector)
                        ->where('MID(don_nro,5,2) =?', $year)
        );

        return $query->current()->nuevo;
    }

}
