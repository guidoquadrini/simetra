<?php

class Banco_models_EnviosProduccion extends Zend_Db_Table_Abstract {

    protected $_name = 'envios_produccion';
    protected $_primary = 'env_prod_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByIdProd($prod_id) {
        $query = $this->select(true)
                ->where('prod_id = ?', $prod_id);

        return $this->fetchAll($query)->current();
    }

    public function getByEstado($estado, $page, $perPage, $efe, $nro = null, $fecha = null, $efe_dest = null) {

        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(fecha_env,"%d/%m/%Y %H:%i") AS hora_salida',
            'DATE_FORMAT(fecha_rec,"%d/%m/%Y %H:%i") AS hora_llegada'
        ));

        if ($efe_dest != null)
            $query = $query->where("efe_destino = ?", $efe_dest);

        if ($fecha != null)
            $query = $query->where("fecha_env LIKE '{$fecha}%'");

        if ($nro != null)
            $query = $query->where("env_prod_id = ?", $nro);

        $query = $query->where("efe_origen IN (?) OR efe_destino IN (?)", $efe);

        $query = $query->where('estado IN (?)', $estado)
                ->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getDestinoFecha($id) {
        $query = $this->select(true)
                ->setIntegrityCheck('false')
                ->reset('columns')
                ->columns(array('efe_destino', 'fecha_env'))
                ->where("env_prod_id = ?", $id)
        ;

        return $this->fetchAll($query)->toArray();
    }

}
