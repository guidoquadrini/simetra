<?php

class Banco_models_Determinaciones extends Zend_Db_Table_Abstract {

    protected $_name = 'determinaciones';
    protected $_primary = 'det_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByDonID($don_id, $page = 1, $perPage = 10, $estado, $fecha = null) {
        $query = $this->select(true)
                ->where('don_id = ?', $don_id)
                ->where('estado = ?', $estado);

        if ($fecha != null)
            $query = $query->where("fecha_exp LIKE '{$fecha}%'");

        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getDeter($don_id = null, $page = 1, $perPage = 10, $estado, $fecha = null) {
        $query = $this->select(true)
                ->where('estado = ?', $estado);

        if ($fecha != null)
            $query = $query->where("fecha_exp LIKE '{$fecha}%'");

        if ($don_id != null)
            $query = $query->where('don_id = ?', $don_id);

        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getByIdAndAna($id, $ana) {
        return $this->fetchRow($this->select(true)
                                ->setIntegrityCheck(false)
                                ->where('don_id = ?', $id)
                                ->where('cod_ana = ?', $ana)
        );
    }
    
    public function getDetermiancionesByDonId($don_id) {
        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)
                                ->where('don_id = ?', $don_id)                                
        );
    }


    public function getByEstado($page = 1, $perPage = 10, $estado) {
        $query = $this->select(true)
                ->where('estado = ?', $estado);

        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function obtenerUltimaDetermiacionPorEnfermedadPorDonacion($don_id, $enf_id){
              
        $query = $this->select(true)
                ->setIntegrityCheck(false)
                ->join('analisis', 'analisis.cod = determinaciones.cod_ana', array('enf_id'))
                ->where('don_id = ?', $don_id)
                ->where('analisis.enf_id = ?', $enf_id)
                ->order('fecha_imp DESC');

        
        return $this->fetchAll($query)->current();
    }
    
    public function obtenerDeterminacionesPorEnfermedadPorDonacion($don_id, $enf_id){
              
        $query = $this->select(true)
                ->setIntegrityCheck(false)
                ->join('analisis', 'analisis.cod = determinaciones.cod_ana', array('enf_id'))
                ->where('don_id = ?', $don_id)
                ->where('analisis.enf_id = ?', $enf_id)
                ->order('fecha_imp DESC');

        
        return $this->fetchAll($query);
    }
    
    
}
