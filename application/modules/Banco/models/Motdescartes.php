<?php

class Banco_models_Motdescartes extends Zend_Db_Table_Abstract {

    protected $_name = 'mot_descartes';
    protected $_primary = 'mot_id';

    /* archivo */

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getMotDescartes() {

        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)->reset('columns')
                                ->columns(array(
                                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                    'mot_id',
                                    'mot_descripcion'
                                ))
                                ->order('mot_descripcion'));
    }

}

?>
