<?php

class Banco_models_Packsproduccion extends Zend_Db_Table_Abstract {

    protected $_name = 'detalles_preproduccion';
    protected $_primary = 'dpr_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPreProduccionList($donaciones) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('hem_id', 'COUNT(*) as cantidad'))
                ->where('don_id IN (?)', $donaciones)
                ->group('hem_id');
        return $this->getDefaultAdapter()->fetchPairs($query);
    }

    public function getCantPlaquetas() {
        return $this->fetchAll($this->select(true)->where('hem_id=?', 2))->count();
    }

    public function getCantCrio() {
        return $this->fetchAll($this->select(true)->where('hem_id=?', 6))->count();
    }

    public function agregarPackaPreProduccion($donacion, $tipo_pack) {
        switch ($tipo_pack) {
            case 1:
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",3)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",4)";
                $this->bd->insert($sql);
                break;
            case 2:
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",3)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",1)";
                $this->bd->insert($sql);
                break;
            case 3:
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",3)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",2)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",1)";
                $this->bd->insert($sql);
                break;
            case 4:
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",3)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",5)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donacion->id . ",6)";
                $this->bd->insert($sql);
                break;
        }
    }

    function devPacksConCrio($cr) {
        $resultados = array();
        $sql = "SELECT don_id FROM detalles_preproduccion";
        if ($criterio != '') {
            $sql .= " WHERE " . $criterio->devCriterio();
        }
        $r = $this->bd->select($sql);
        if (count($r) > 0) {
            for ($i = 0; $i < count($r); $i++) {
                $resultados[] = $r[$i]['id'];
            }
        }
        return $resultados;
    }

    function pasarPacksaPlaquetas($donaciones) {
        if (count($donaciones) > 0) {
            for ($i = 0; $i < count($r); $i++) {
                $sql = "DELETE FROM detalles_preproduccion WHERE don_id=" . $donaciones[$i];
                $this->bd->hacer_operacion($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donaciones[$i] . ",3)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donaciones[$i] . ",2)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donaciones[$i] . ",1)";
                $this->bd->insert($sql);
            }
        }
    }

    function devPacksConPlaquetas($criterio = '') {
        $resultados = array();
        $sql = "SELECT don_id FROM detalles_preproduccion";
        if ($criterio != '') {
            $sql.=" WHERE " . $criterio->devCriterio();
        }

        $r = $this->bd->select($sql);
        if (count($r) > 0) {
            for ($i = 0; $i < count($r); $i++) {
                $resultados[] = $r[$i]['don_id'];
            }
        }
        return $resultados;
    }

    function pasarPacksaCrio($donaciones) {
        if (count($donaciones) > 0) {
            for ($i = 0; $i < count($donaciones); $i++) {
                $sql = "DELETE FROM detalles_preproduccion WHERE don_id=" . $donaciones[$i];
                $this->bd->hacer_operacion($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donaciones[$i] . ",3)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donaciones[$i] . ",5)";
                $this->bd->insert($sql);
                $sql = "INSERT INTO detalles_preproduccion VALUES (" . $donaciones[$i] . ",6)";
                $this->bd->insert($sql);
            }
        }
    }

    function pasaraProduccion() {
        //Verificar si existen elementos para pasar
        $total = $this->contar();
        if ($total == 0)
            return true;

        $cadenaLabo = "";
        $sql = "SELECT * FROM detalles_preproduccion";
        $r = $this->bd->select($sql);
        for ($i = 0; $i < count($r); $i++) {
            $donacion = new donacion($r[$i]['don_id']);
            $hemocomponente = new hemocomponente($r[$i]['hem_id']);
            $sql = "INSERT INTO detalles_produccion VALUES (''," . $donacion->don_id . "," . $hemocomponente->hem_id . ",";
            $sql .= $hemocomponente->nrobolsa . ",'" . date("Y-m-d H:i:s") . "','0000-00-00',1,'',0,0)";
            $this->bd->insert($sql);

            $cadenaLabo .= "M," . $donacion->cod_efector . $donacion->nro . ",";
            $cadenaLabo .= $donacion->solicitud->persona->apellido . " " . $donacion->solicitud->persona->nombres . "\n";
        }
        //		$sql = "TRUNCATE TABLE detalles_preproduccion";
        //		$this->bd->hacer_operacion($sql);
        //Grabar archivo para laboratorio
        $nomArchivo = date("Ymdhis") . ".txt";
        $a = fopen("./salidaLabo/" . $nomArchivo, "w");
        fwrite($a, $cadenaLabo);
        fclose($a);
        return true;
    }

}
