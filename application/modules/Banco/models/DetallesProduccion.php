<?php

class Banco_models_DetallesProduccion extends Zend_Db_Table_Abstract {

    protected $_name = 'detalles_produccion';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getStockList() {
        $query = $this->select(true)
                ->reset('columns')->columns(array('idhemocomponente', 'COUNT(*) as cantidad'))
                ->where('estado = ?', 2)
                ->where('validado = ?', 1)
                ->group('idhemocomponente');

        return $this->getDefaultAdapter()->fetchPairs($query);
    }

    public function getStockListByEfe($efes_ids) {
        $query = $this->select(true)->setIntegrityCheck(false)
                ->reset('columns')->columns(array('idhemocomponente', 'COUNT(*) as cantidad'))
                ->where('estado = ?', 2)
                ->where('validado = ?', 1)
                ->group('idhemocomponente');

        $query = $query->where("don_donaciones.efe_id IN (?)", $efes_ids);

        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');

        return $this->getDefaultAdapter()->fetchPairs($query);
    }

    // 27.08.2014 agregada condición 'validado = 1'
    public function getStockListByIDs($don_ids) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('idhemocomponente', 'COUNT(*) as cantidad'))
                ->where('estado = ?', 2)
                ->where('validado = ?', 1)
                ->where('iddonacion IN (?)', $don_ids)
                ->group('idhemocomponente');

        return $this->getDefaultAdapter()->fetchPairs($query);
    }

    public function getProducidos() {
        $query = $this->select(true)
                ->reset('columns')->columns(array('idhemocomponente', 'peso', 'hem_temp', 'ffin_prod', 'iddonacion'))
                ->where('estado = ?', 2);

        return $this->fetchAll($query)->toArray();
    }

    public function getByDonID($don_id, $estados) {
        $query = $this->select(true)
                ->reset('columns')->columns(array('idhemocomponente', 'estado', 'env_prod_id', 'detalle', 'ffin_prod'))
                ->where('iddonacion = ?', $don_id)
                ->where('estado IN (?)', $estados);

        return $this->fetchAll($query)->toArray();
    }

    public function getByEnv($env_id) {
        $query = $this->select(true)
                ->where('env_prod_id = ?', $env_id);

        return $this->fetchAll($query)->toArray();
    }

    public function getByID($id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array(
                    'id',
                    'idhemocomponente',
                    'peso',
                    'hem_temp',
                    'ffin_prod',
                    'iddonacion',
                    'detalle')
                )
                ->where('id = ?', $id);

        return $this->fetchRow($query)->toArray();
    }

    public function getEnviosParaProduccion($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(fini_prod,"%H:%i") as hora_llegada',
            'DATE_FORMAT(fini_prod,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), fini_prod) as tiempo_envio',
            'don_donaciones.don_nro as don_nro',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
            'don_donaciones.don_fh_extraccion as fh_extraccion',
        ));


        if ($fecha != null)
            $query = $query->where("fini_prod LIKE '{$fecha}%'");

        if ($don_id != null)
            $query = $query->where("don_donaciones.don_nro =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("don_donaciones.efe_id = ?", $efe_id);

        if ($todos != null)
            $query = $query->where('estado =?', $todos);

        $query = $query->order('detalles_produccion.fini_prod');
        $query = $query->order('detalles_produccion.iddonacion');
        $query = $query->order('detalles_produccion.estado');
        $query = $query->order('detalles_produccion.idhemocomponente');
        $query = $query->joinInner('hem_hemocomponentes', ' detalles_produccion.idhemocomponente=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getEnviosParaProduccionByEfe($fecha, $page = 1, $perPage = 10, $don_nro = null, $efe_id = null, $todos = false, $hem_id = null) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(fini_prod,"%H:%i") as hora_llegada',
            'DATE_FORMAT(fini_prod,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), fini_prod) as tiempo_envio',
            'don_donaciones.don_nro as don_nro',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
            'don_donaciones.don_fh_extraccion as fh_extraccion',
        ));

        if ($hem_id != null)
            $query = $query->where("idhemocomponente = ?", $hem_id);

        if ($fecha != null)
            $query = $query->where("fini_prod LIKE '{$fecha}%'");

        if ($don_nro != null)
            $query = $query->where("don_donaciones.don_nro =?", $don_nro);

        if ($efe_id != null)
            $query = $query->where("don_donaciones.efe_id IN (?)", $efe_id);

        if ($todos != null)
            $query = $query->where('estado =?', $todos);

        $query = $query->order('detalles_produccion.fini_prod');
        $query = $query->order('detalles_produccion.iddonacion');
        $query = $query->order('detalles_produccion.estado');
        $query = $query->order('detalles_produccion.idhemocomponente');

        $query = $query->joinInner('hem_hemocomponentes', 'detalles_produccion.idhemocomponente = hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function obtenerResultadoParaInformeIndex($fecha, $page = 1, $perPage = 10, $don_nro = null, $efe_id = null, $estado = false, $hem_id = null) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            //  new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'detalles_produccion.id as detalle_produccion_id',
            'detalles_produccion.peso as peso',
            'detalles_produccion.detalle as detalle',
            'detalles_produccion.ffin_prod',
            'detalles_produccion.validado',
            #'serologia.validado as serologia_validado',
            'don_donaciones.don_id as don_id',
            'don_donaciones.don_nro as don_nro',
            'don_donaciones.don_fh_extraccion as fh_extraccion',
            'DATE_FORMAT(fini_prod,"%d/%m/%Y %H:%i") as fecha_recepcion',
            'DATE_FORMAT(fini_prod,"%H:%i") as hora_recepcion',
            'TIMEDIFF(NOW(), fini_prod) as tiempo_envio',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
        ));

        //FILTRADO
        if ($hem_id != null) {
            $query = $query->where("idhemocomponente = ?", $hem_id);
        }
        if ($fecha != null) {
            $query = $query->where("fini_prod LIKE '{$fecha}%'");
        }
        if ($don_nro != null) {
            $query = $query->where("don_donaciones.don_nro = ?", $don_nro);
        }
        if ($efe_id != null) {
            $query = $query->where("don_donaciones.efe_id IN (?)", $efe_id);
        }
        if ($estado != null) {
            $query = $query->where('estado = ?', $estado);
        }

        //CRITERIO DE ORDENAMIENTO ORDENAMIENTO
        $query = $query->order('detalles_produccion.fini_prod');
        $query = $query->order('detalles_produccion.iddonacion');
        $query = $query->order('detalles_produccion.estado');
        $query = $query->order('detalles_produccion.idhemocomponente');

        //JOINS OTRAS TABLAS
        $query = $query->joinInner('hem_hemocomponentes', 'detalles_produccion.idhemocomponente=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');
        $query = $query->joinInner('serologia', ' serologia.don_id=don_donaciones.don_id', 'validado as serologia_validado');
        $query = $query->limitPage($page, $perPage);

        $retorno = $this->fetchAll($query);

        return $retorno;
    }

    public function getParaDescarte($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $estados, $hem_id = null) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(fini_prod,"%H:%i") as hora_llegada',
            'DATE_FORMAT(fini_prod,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), fini_prod) as tiempo_envio',
            'don_donaciones.don_nro as don_nro',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
            'don_donaciones.don_fh_extraccion as fh_extraccion',
        ));

        if ($hem_id != null)
            $query = $query->where("idhemocomponente = ?", $hem_id);

        if ($fecha != null)
            $query = $query->where("fini_prod LIKE '{$fecha}%'");

        if ($don_id != null)
            $query = $query->where("don_donaciones.don_nro =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("don_donaciones.efe_id IN (?)", $efe_id);

        if ($estados != null)
            $query = $query->where('estado IN (?)', $estados);

        $query = $query->order('detalles_produccion.fini_prod');
        $query = $query->order('detalles_produccion.iddonacion');
        $query = $query->order('detalles_produccion.estado');
        $query = $query->order('detalles_produccion.idhemocomponente');
        $query = $query->joinInner('hem_hemocomponentes', ' detalles_produccion.idhemocomponente=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    //igual al método anterior sólo que checkea validado = 1
    public function getBolsaParaEnvio($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                    'DATE_FORMAT(fini_prod,"%H:%i") as hora_llegada',
                    'DATE_FORMAT(fini_prod,"%d/%m/%Y %H:%i") as fecha_envio',
                    'TIMEDIFF(NOW(), fini_prod) as tiempo_envio',
                    'don_donaciones.don_nro as don_nro',
                    'hem_hemocomponentes.hem_descripcion as hem_descripcion',
                    'don_donaciones.don_fh_extraccion as fh_extraccion',
                ))
                ->where('validado = ?', 1);


        if ($fecha != null)
            $query = $query->where("fini_prod LIKE '{$fecha}%'");

        if ($don_id != null)
            $query = $query->where("don_donaciones.don_nro =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("don_donaciones.efe_id = ?", $efe_id);

        if ($todos != null)
            $query = $query->where('estado =?', $todos);

        $query = $query->order('detalles_produccion.fini_prod');
        $query = $query->order('detalles_produccion.iddonacion');
        $query = $query->order('detalles_produccion.estado');
        $query = $query->order('detalles_produccion.idhemocomponente');
        $query = $query->joinInner('hem_hemocomponentes', ' detalles_produccion.idhemocomponente=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getBolsaParaEnvioByEfe($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false, $hem_id = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                    'env_prod_id',
                    'DATE_FORMAT(fini_prod,"%H:%i") as hora_llegada',
                    'DATE_FORMAT(fini_prod,"%d/%m/%Y %H:%i") as fecha_envio',
                    'TIMEDIFF(NOW(), fini_prod) as tiempo_envio',
                    'don_donaciones.don_nro as don_nro',
                    'hem_hemocomponentes.hem_descripcion as hem_descripcion',
                    'don_donaciones.don_fh_extraccion as fh_extraccion',
                ))
                ->where('validado = ?', 1);

        if ($hem_id != null)
            $query = $query->where("idhemocomponente = ?", $hem_id);

        if ($fecha != null)
            $query = $query->where("fini_prod LIKE '{$fecha}%'");

        if ($don_id != null)
            $query = $query->where("don_donaciones.don_nro =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("don_donaciones.efe_id IN (?)", $efe_id);

        if ($todos != null)
            $query = $query->where('estado =?', $todos);

        $query = $query->order('detalles_produccion.fini_prod');
        $query = $query->order('detalles_produccion.iddonacion');
        $query = $query->order('detalles_produccion.estado');
        $query = $query->order('detalles_produccion.idhemocomponente');
        $query = $query->joinInner('hem_hemocomponentes', ' detalles_produccion.idhemocomponente=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones', ' detalles_produccion.iddonacion=don_donaciones.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

}
