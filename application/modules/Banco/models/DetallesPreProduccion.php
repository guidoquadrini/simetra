<?php

/**
 * Estados posibles
 * 1:
 * 2:
 */

class Banco_models_DetallesPreProduccion extends Zend_Db_Table_Abstract {

    protected $_name = 'detalles_preproduccion';
    protected $_primary = 'dpr_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function listDonacionesPendientes() {
        return $this->fetchAll($this->select(true)
        ->setIntegrityCheck(false)->reset('columns')
        ->columns(array('don_id'))
        ->where('estado = ?',1)
        ->where('tipo_prod = ?', 1)
        ->group('don_id'))->toArray();
    }

	public function getPreProduccionByID($id) {
		return $this->fetchAll($this->select(true)
		->where('estado = ?', 1)
		->where('don_id = ?', $id));
	}

    public function getPreProduccionList($donaciones) {
        $query = $this->select(true)
            ->reset('columns')->columns(array('hem_id','COUNT(*) as cantidad'))
            ->where('don_id IN (?)',$donaciones)
            ->group('hem_id');

        return $this->getDefaultAdapter()->fetchPairs($query);
    }

    
    public function listDonacionesAProducir($donaciones) {
		return $this->fetchAll($this->select(true)
		->where('estado = ?',1)
		->where('tipo_prod = ?', 1)
		->where('don_id IN (?)', $donaciones));
	}
	
	public function listFechas() {
		$query = $this -> select()
				->from(array('don_donaciones'), array())
				->join(array('env_envios'), 'don_donaciones.despacho_id=env_envios.env_id', array('don_donaciones.don_fh_extraccion', 'env_envios.env_fh', 'env_envios.env_id'));
		$query->setIntegrityCheck(false);
		return $this->fetchAll($query);
	}

	//07/08/2014 se agregó AND p.tipo_prod=1
	public function listDonacionesPend() {
		$query = $this->select()
				->distinct()
				->setIntegrityCheck(false)->reset('columns')
				->from(array('d'=>'don_donaciones'), array('d.don_fh_recepcion','d.don_id', 'd.don_nro', 'd.don_fh_extraccion', 'TIME_TO_SEC( TIMEDIFF( d.don_fh_recepcion, d.don_fh_extraccion ) ) as tiempo')) 
				->join(array('p'=>'detalles_preproduccion'), 'd.don_id=p.don_id AND p.estado=1 AND p.tipo_prod=1', array());
		return $this->fetchAll($query);
	}
	public function listDonacionesPendByEfe($efes_ids) {
		$query = $this->select()
				->distinct()
				->setIntegrityCheck(false)->reset('columns')
				->from(array('d'=>'don_donaciones'), array('d.don_fh_recepcion','d.don_id', 'd.don_nro', 'd.don_fh_extraccion', 'TIME_TO_SEC( TIMEDIFF( d.don_fh_recepcion, d.don_fh_extraccion ) ) as tiempo')) 
				->join(array('p'=>'detalles_preproduccion'), 'd.don_id=p.don_id AND p.estado=1 AND p.tipo_prod=1', array());
				
		$query = $query->where("d.efe_id IN (?)",$efes_ids);
		
		return $this->fetchAll($query);
	}
	
	public function getHemoAndNroDon() {
		$query = $this->select()
				->setIntegrityCheck(false)->reset('columns')
				->from(array('p'=>'detalles_preproduccion'), array())
				->join(array('d'=>'don_donaciones'),'p.don_id=d.don_id', array('d.don_nro', 'd.don_id'))
				->join(array('e'=>'env_envios'), 'd.despacho_id=e.env_id', array())
				->where('p.estado = ?',1)
				->where('p.tipo_prod = ?', 1)
				->where('p.hem_id = ?', 2)
				->where('TIME_TO_SEC( TIMEDIFF( d.don_fh_recepcion, e.env_fh ) ) < ?', 28800);
		return $this->fetchAll($query)->toArray();
	}
}
