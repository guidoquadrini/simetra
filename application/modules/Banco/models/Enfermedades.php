<?php

class Banco_models_Enfermedades extends Zend_Db_Table_Abstract {

    protected $_name = 'enfermedades';
    protected $_primary = 'enf_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('enf_id', 'descripcion'))
                                //	->where('def=?',1)
                                ->order('descripcion'));
    }    
    
    public function getNomByCod($enf_id) {
        $query = $this->select(true)
                ->reset('columns')
                ->columns(array('descripcion'))
                ->where('enf_id = ?', $enf_id);

        return $this->fetchRow($query)->toArray();
    }

}
