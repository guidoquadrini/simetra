<?php

class Banco_models_HemocomponentesPerfiles extends Zend_Db_Table_Abstract {

    protected $_name = 'hemocomponentesxperfilprod';
    protected $_primary = 'hem_fil_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }
    
    public function listHemoByPerfiles($perfiles) {
        return $this->fetchAll($this->select(true)
        ->setIntegrityCheck(false)->reset('columns')
        ->columns('idhemocomponente')
        ->where('idperfil IN (?)',$perfiles)
        ->order('idperfil'))->toArray();
    }    

}
