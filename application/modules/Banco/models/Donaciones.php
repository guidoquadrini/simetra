<?php

/**
 * Estados de Donacion
 *
 * 1 alta
 * 2 aceptar
 * 4 rechazar
 * 3 diferir
 * 5 aceptaform
 * 6 rechazaform
 * 5 pasar a prod
 * 6 rechazar prod
 * 7 aceptar proceso
 * 8 no aceptar
 *
 */
class Banco_models_Donaciones extends Zend_Db_Table_Abstract {

    protected $_name = 'don_donaciones';
    protected $_primary = 'don_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByEstado($estado, $page, $perPage, $fecha = null, $don_nro = null) {

        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(don_fh_inicio,"%d/%m/%Y %H:%i") AS hora_llegada',
            'DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") AS hora_extracion'
        ));

        if ($fecha != null)
            $query = $query->where("don_fh_inicio LIKE '{$fecha}%'");

        if ($don_nro != null)
            $query = $query->where("don_nro=?", $don_nro);

        $query = $query->where('don_estado IN (?)', $estado)
                ->order('don_fh_inicio')
                ->joinLeft('per_personas as p', 'don_donaciones.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                ->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    /**
     * Filtrado de Donaciones por Efector, Estado, Fecha y Codigo de Donacion (Codigo de Barras)
     * @param type $efe:  arreglo de efectores.
     * @param type $estado: arreglo de estado de la donaciones
     * @param type $page: Nro de Pagina
     * @param type $perPage: Nro de Registros por pagina
     * @param type $fechaDesde: Filtrado por fecha formato(Y-m-d)
     * @param type $fechaHasta: Filtrado por fecha formato(Y-m-d)
     * @param type $don_nro: Filtrado por Codigo de Donacion (Codigo de Barras)
     */
    public function getByEstadoAndEfe($efe, $estado = null, $page, $perPage, $fechaDesde = null, $fechaHasta = null, $don_nro = null) {

        $fecha = (new DateTime($fecha))->format('Y-m-d');

        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'don_id',
            'don_nro',
            'don_estado',
            'per_id',
            'sol_id',
            'DATE_FORMAT(don_fh_inicio,"%d/%m/%Y %H:%i") AS hora_llegada',
            'DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") AS hora_extracion'
        ));
        
        if ($fechaDesde != null) {
            $fechaDesde_formateada = (new DateTime($fechaDesde))->format('Y-m-d H:i:s');
            $query = $query->where("don_fh_inicio >= ?", $fechaDesde_formateada);
        }

        if ($fechaHasta != null) {
            $fechaHasta_formateada = (new DateTime($fechaHasta))->format('Y-m-d H:i:s');
            $query = $query->where("don_fh_inicio <= ?", $fechaHasta_formateada);
        }


        if ($don_nro != null)
            $query = $query->where("don_nro = ?", $don_nro);

        if ($efe != null)
            $query = $query->where("efe_id IN (?)", $efe);


        if ($estado == null) {
            $query = $query->where('don_estado IN (?)', array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        } elseif ($estado == '*') { 
            $query = $query->where('don_estado NOT IN(?)', array(100, 102));
        } else {
            $query = $query->where("don_estado IN (?)", $estado);
        }
        $query = $query->order('don_fh_inicio')
                ->joinLeft('per_personas as p', 'don_donaciones.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                ->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getByEstadoEfeFecha($efe, $estado = null, $page, $perPage, $fechaDesde = null, $fechaHasta = null, $don_nro = null, $dni = null) {
                   #getByEstadoEfeFecha(663 , null          , 1    , 18      , 17-12-2017        , 19-12-2018        , CRHC18000015          , null)
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(don_fh_inicio,"%d/%m/%Y %H:%i") AS hora_llegada',
            'DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") AS hora_extracion'
        ));

        if ($fechaDesde != null)
            $query = $query->where("don_fh_extraccion >= ?", $fechaDesde);

        if ($fechaHasta != null) {
            $fechaHasta = date('Y-m-d', strtotime($fechaHasta)) . ' 23:59:59';
            $query = $query->where("don_fh_extraccion <= ?", $fechaHasta);
        }

        if ($don_nro != null)
            $query = $query->where("don_nro = ?", $don_nro);

        if ($efe != null)
            $query = $query->where("efe_id IN (?)", $efe);

        if ($estado != null)
            $query = $query->where("don_estado = ?", $estado);
        else
            $query = $query->where('don_estado IN (?)', array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        $query = $query->order('don_fh_inicio')
                ->joinLeft('per_personas as p', 'don_donaciones.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                ->limitPage($page, $perPage);

        if ($dni != null)
            $query = $query->where("p.per_nrodoc = ?", $dni);

        return $this->fetchAll($query);
    }

    public function getExtracciones($efe, $estado = null, $page, $perPage, $fechaDesde = null, $fechaHasta = null, $don_nro = null, $persona_id = null, 
            $tipo_extraccion = null) {
 
        
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(don_fh_inicio,"%d/%m/%Y %H:%i") AS hora_llegada',
            'DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") AS hora_extracion'
        ));

        if ($fechaDesde != null) {
            $fechaDesde_formateada = (new DateTime($fechaDesde))->format('Y-m-d H:i:s');
            $query = $query->where("don_fh_inicio >= ?", $fechaDesde_formateada);
        }

        if ($fechaHasta != null) {
            $fechaHasta_formateada = (new DateTime($fechaHasta))->format('Y-m-d H:i:s');
            $query = $query->where("don_fh_inicio <= ?", $fechaHasta_formateada);
        }
        
        if ($don_nro != null) {
            $query = $query->where("don_nro = ?", $don_nro);
        }

        if ($efe != null) {
            $query = $query->where("don_donaciones.efe_id IN (?)", $efe);
        }

        if ($estado != null) {
            $query = $query->where("don_estado = ?", $estado);
        } else {
            $query = $query->where('don_estado IN (?)', array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        }
        
        $query = $query->order('don_fh_inicio')
                ->joinLeft('per_personas as p', 'don_donaciones.per_id = p.per_id', array('per_apellido', 'per_nombres'))
                ->joinLeft('sol_solicitudes as s', 'don_donaciones.sol_id = s.sol_id', array('tdo_id', 'afe_id'))
                ->joinLeft('don_donaciones_ext as donaciones_extraccion', 'don_donaciones.don_id = donaciones_extraccion.don_id', array('ext_id', 'tipo_extraccion'))
                ->limitPage($page, $perPage);

        if ($tipo_extraccion != null) {
            $query = $query->where("donaciones_extraccion.tipo_extraccion = " . $tipo_extraccion);
        }
        
        if ($persona_id != null) {
            $query = $query->where("p.per_id = ?", $persona_id);
        }

        return $this->fetchAll($query);
    }

    public function getRegistro($id) {
        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)
                                ->columns(array(
                                    'DATE_FORMAT(don_fh_inicio,"%d/%m/%Y %H:%i") AS hora_llegada',
                                    'DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") AS hora_extraccion'
                                ))
                                ->where('don_id=?', $id)
                )->current();
    }

    public function getByDni($dni) {
        return $this->fetchAll($this->select(true)->where('per_id=?', $dni));
    }

    public function getDonacionesHistorialParaHistorial($per_id) {
        return $this->fetchAll($this->select(true)->reset('columns')
                                ->columns(array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'), 'DATEDIFF(NOW(),don_fh_inicio) as dias_desde'))
                                ->where('per_id=?', $per_id)
                                ->where('don_estado <> ?', '1') //Donacion Recien Creada
                                ->where('don_estado <> ?', '102') //Eliminacion Administrativa
                                ->where('don_estado <> ?', '101') //Cancelacion Administrativa
                                ->order('don_fh_inicio DESC'))->current();
    }
    
     public function getDonacionesHistorialParaHistorialNotCurrent($per_id) {
        return $this->fetchAll($this->select(true)->reset('columns')
                                ->columns(array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'), 'DATEDIFF(NOW(),don_fh_inicio) as dias_desde'))
                                ->where('per_id=?', $per_id)
                                ->where('don_estado <> ?', '1') //Donacion Recien Creada
                                ->where('don_estado <> ?', '102') //Eliminacion Administrativa
                                ->where('don_estado <> ?', '101') //Cancelacion Administrativa
                                ->order('don_fh_inicio DESC'));
    }

    public function getBySolID($sol_id) {
        return $this->fetchRow($this->select(true)->where('sol_id=?', $sol_id));
    }

    public function getSinDespacho() {
        return $this->fetchAll($this->select(true)
                                //->setIntegrityCheck(false)->reset('columns')->columns(array())
                                ->where('despacho_id IS NULL')
                                ->where('don_estado IN (?)', 1)
                                ->order('don_fh_inicio')
                        //->joinLeft('personas','don_donaciones.per_dni=personas.nrodoc',array('apellido','nombres'))
        );
    }

    public function getSinDespachoByEfe($efe) {
        return $this->fetchAll($this->select(true)
                                ->where('despacho_id IS NULL')
                                ->where('don_estado IN (?)', array(105,3,4))
                                ->where('efe_id = ?', $efe)
                                ->order('don_fh_inicio')
        );
    }

    public function getDespacho($env_id) {

        return $this->fetchAll($this->select(true)
                                //->columns('DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") as fh_muestra')
                                ->columns('per_id, DATE_FORMAT(don_fh_extraccion,"%d/%m/%Y %H:%i") as fh_muestra')
                                ->where('despacho_id=?', $env_id)
                                ->order('don_fh_inicio')
        );
    }

    public function getDonacionesPorEnvio($env_id) {

        return $this->fetchAll($this->select(true)
                                ->where('despacho_id=?', $env_id)
                                ->order('don_fh_inicio')
        );
    }

    public function getUltimaByDni($per_id) {
        return $this->fetchAll($this->select(true)->reset('columns')
                                ->columns(array(new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'), 'DATEDIFF(NOW(),don_fh_inicio) as dias_desde'))
                                ->where('per_id=?', $per_id)
                                ->where('don_estado <> ?', '1') //Donacion Recien Creada
                                ->where('don_estado <> ?', '102') //Eliminacion Administrativa
                                ->where('don_estado <> ?', '101') //Cancelacion Administrativa
                                ->order('don_fh_inicio DESC'))->current();
    }

    /* Este metodo trae la ultima donacion que se realizo la Persona por su DNI */

    public function getUltimaByDni2($dni) {
//   		$sql = "SELECT * FROM don_donaciones as don "
//                . "left join per_personas as p on p.per_id = don.per_id "
//                . "WHERE p.per_nrodoc = '$dni' "
//                . "order by don.don_fh_inicio desc "
//                . "limit 1";
//        
//       $stmt = new Zend_Db_Statement_Mysqli(self::$_defaultDb, $sql);       
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->from('don_donaciones as don')
                ->joinLeft('per_personas as p', 'don_donaciones.per_id=p.per_id')
                ->where('p.per_nrodoc = ?', $dni)
                ->order('don.don_fh_inicio DESC');

        return $this->fetchAll($query)->current();
    }

    public function newIndex($efector, $year) {
        $query = $this->fetchAll($this->select()
                        ->from($this, array(new Zend_Db_Expr('IFNULL(max(CONVERT(SUBSTRING(don_nro,6),UNSIGNED INTEGER)),0) + 1 as nuevo')))
                        ->where('efe_id=?', $efector)
                        ->where('SUBSTRING(don_nro,4,2) =?', $year)
        );

        return $query->current()->nuevo;
    }

    public function obtenerDonacionExtendida($id){
        
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->from('don_donaciones as don')
                ->joinLeft('don_donaciones_ext as don_ext', 'don_ext.don_id = don.don_id')
                ->where('don.don_id = ?', $id)
                ;

        return $this->fetchAll($query)->current();
        
    }
    
    public function getByID($id) {
        $query = $this->select(true)
                ->reset('columns')->columns(array(
                    'don_id',
                    'efe_id',
                    'per_id',
                    'don_nro',
                    'don_fh_extraccion',
                    'sol_id')
                )
                ->where('don_id = ?', $id);

        return $this->fetchRow($query)->toArray();
    }

    public function getID($don_nro) {
        $query = $this->select(true)
                ->reset('columns')->columns('don_id')
                ->where('don_nro = ?', $don_nro);
        
        return $this->fetchRow($query);
    }

    public function getIdByNroDonacion($don_nro) {
        $query = $this->select(true)
                ->reset('columns')->columns('don_id')
                ->where('don_nro = ?', $don_nro);

        return $this->fetchRow($query)->toArray();
    }

    public function getNro($don_id) {
        $query = $this->select(true)
                ->reset('columns')->columns('don_nro')
                ->where('don_id = ?', $don_id);

        return $this->fetchRow($query);
    }

    public function getByMuni($cod_muni) {
        $query = $this->select(true)
                ->reset('columns')->columns('don_id')
                ->where('cod_muni = ?', $cod_muni);

        return $this->fetchRow($query)->toArray();
    }

    public function getNroDonacionBySolicitud($id) {
        $query = $this->select(true)
                ->reset('columns')->columns('don_nro')
                ->where('sol_id = ?', $id);

        return $this->fetchRow($query)->toArray();
    }

}
