<?php

class Banco_models_Inmunologia extends Zend_Db_Table_Abstract {

    protected $_name = 'inmunologia';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getDatosDonacion($don_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)
                ->where('don_id = ?', $don_id)
                ->order('id DESC');

        return $this->fetchAll($query)->current();
    }

    public function getDatosEntreFechas($fechaDesde, $fechaHasta) {
        $query = $this->select(true)
                ->setIntegrityCheck(false);
        if ($fechaDesde != null)
            $query = $query->where("timestamp >= ?", $fechaDesde);

        if ($fechaHasta != null) {
            $fechaHasta = date('Y-m-d', strtotime($fechaHasta)) . ' 23:59:59';
            $query = $query->where("timestamp <= ?", $fechaHasta);
        }
        //return $this->fetchRow($query)->toArray();
        return $this->fetchAll($query)->toArray();
    }

}
