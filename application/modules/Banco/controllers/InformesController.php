<?php

class Banco_InformesController extends BootPoint {

    public function pacientesAction() {
        
        $fechaDesde = $this->getRequest()->getParam('from', null);
        $fechaHasta = $this->getRequest()->getParam('to', null);
        $don_id = $this->getRequest()->getParam('don_id');
        $doc = $this->getRequest()->getParam('doc');
        $efector_id = $this->getRequest()->getParam('efe_id');
        $estado = $this->getRequest()->getParam('estado');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($fechaDesde)){  
        $fechaDesde = (new DateTime())->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        
        }
            
        if (is_null($fechaHasta)) {
            $fechaHasta = (new DateTime())->setTime(23, 59, 59)->format('Y-m-d H:i:s');
        }
        $fechaDesde = '2017-12-19 00:00:00';
        
        $perPage = 18;

        $modeloDon = new Banco_models_Donaciones();
        $modeloPer = new Efector_models_Personas();
        $modeloPerSicap = new Efector_models_PersonasSicap();
        $modeloSicap = new Efector_models_Sicap();
        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloSer = new Banco_models_Serologia();
                $modeloSerEnf = new Banco_models_Serologiasenfermedades();

        $provincias = $modeloSicap->getProvincias();
        $localidades = $modeloSicap->getLocalidadesPairs(82);

        $resultados = $modeloDon->getByEstadoEfeFecha($efector_id, $estado, $this->page, $perPage, $fechaDesde, $fechaHasta, $don_id, $doc);
        
        if (sizeof($resultados) > 0) {
            $total = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        foreach ($resultados as $i => $v) {
            $sicap_ids[] = $v['sicap_id'];
            $producidos[] = $modeloPro->getByDonID($v['don_id'], array(1, 2, 3, 4, 5));
            $row_serologia = $modeloSer->getDatosDonacion($v['don_id']);
            $serologia[$v['don_id']]['cabecera'] = $row_serologia;
            $serologia[$v['don_id']]['detalle'] = array_column($modeloSerEnf->fetchAll('serologias_id = ' . $row_serologia->id)->toArray(), 'determinacion');
            
            $inmuno[$v['don_id']] = $modeloInmuno->getDatosDonacion($v['don_id']);
        }
        if ($sicap_ids) {
            //$personas = $modeloPerSicap->getByIDs($sicap_ids);
        }

        foreach ($resultados as $i => $v) {
//            $personas[$i] = $modeloPerSicap->find($v['sicap_id'])->current();
//            $per_sicap[$i] = $modeloPerSicap->find($v['sicap_id'])->current();
        }

        $modeloEnvProd = new Banco_models_EnviosProduccion();
        $modeloEfe = new Acceso_models_Efectores();

        if ($producidos != null && sizeof($producidos) > 0) {
            foreach ($producidos[0] as $i => $v) {
                if ($v['env_prod_id']) {
                    $efe_fecha = $modeloEnvProd->getDestinoFecha($v['env_prod_id']);
                    $efe = $modeloEfe->getNom($efe_fecha[0]['efe_destino']);
                    $producidos[0][$i]['efe_destino'] = $efe[0]['nomest'];
                    $producidos[0][$i]['fecha_env'] = $efe_fecha[0]['fecha_env'];
                }
            }
        }

        $this->view->inmuno = $inmuno;
        $this->view->serologia = $serologia;
        $this->view->localidades = $localidades;
        $this->view->provincias = $provincias;
        $this->view->estado = $estado;
        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->don_id = $don_id;
        $this->view->doc = $doc;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
        $this->view->personas = $per_sicap;
        $this->view->producidos = $producidos;
    }

    public function inmunologiaAction() {
        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');

        if ($fechaDesde == '' || $fechaHasta == '') {
            $fechaHasta = date('Y-m-d');
            $fechaDesde = date('Y-m-d');
        }

        $perPage = 18;

        $modeloDon = new Banco_models_Donaciones();
        $modeloInmuno = new Banco_models_Inmunologia();
        $resultados = $modeloInmuno->getDatosEntreFechas($fechaDesde, $fechaHasta);
        foreach ($resultados as $clave => $valor) {
            $donacion = $modeloDon->getByID($valor['don_id']);
            $valor['don_nro'] = $donacion['don_nro'];
            $informe[] = $valor;
        }

        if (sizeof($resultados) > 0) {
            $total = $modeloInmuno->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->informe = $informe;
    }

    public function imprimirDonacionesAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /* Para saber desde dónde se llama */
        $flag = $this->getRequest()->getParam('flag');
        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');
        $nro = $this->getRequest()->getParam('nro');
        $doc = $this->getRequest()->getParam('doc');
        $efector_id = $this->getRequest()->getParam('efe_id');
        $estado = $this->getRequest()->getParam('estado');
        $tipo = $this->getRequest()->getParam('tipo');

        if ($estado == "0")
            $estado = null;
        elseif ($estado == "-1")
            $estado = null;

        if ($fechaDesde == '' || $fechaHasta == '') {
            $fechaHasta = date('Y-m-d');
            $fechaDesde = date('Y-m-d', strtotime("-30 days"));
        }

        $perPage = 18;

        $modeloDon = new Banco_models_Donaciones();
        $modeloPer = new Efector_models_Personas();
        $modeloPerSicap = new Efector_models_PersonasSicap();
        $modeloSicap = new Efector_models_Sicap();
        $modeloPro = new Banco_models_DetallesProduccion();

        $provincias = $modeloSicap->getProvincias();
        $localidades = $modeloSicap->getLocalidadesPairs(82);

        if ($flag == 'ext') {
            $efe_id = $this->efector_activo['id_efector'];
            $resultados = $modeloDon->getExtracciones($efe_id, $estado, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, $tipo);
        } else {
            $resultados = $modeloDon->getByEstadoEfeFecha($efector_id, $estado, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc);
        }
        
        if (sizeof($resultados) > 0) {
            $total = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
            $resultados = $resultados->toArray();
        }

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $encoding = 'UTF-8';
        $x = 385;
        $y = $page->getHeight();

        $page
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                ->setFont($font, 17)
                ->drawText("Informe de Donaciones", 170, $y - 40, $encoding)
        ;

        if ($fechaDesde) {
            $from = date("d-m-Y", strtotime($fechaDesde));
            $to = date("d-m-Y", strtotime($fechaHasta));
            $page
                    ->setFont($font, 12)
                    ->drawText("Entre  " . $from . "  y  " . $to, 30, $y - 85, $encoding)
            ;
        }

        $page
                ->setFont($font, 12)
                ->drawText("Nº de donación", 30, $y - 115, $encoding)
                ->drawText("Efector", 130, $y - 115, $encoding)
                ->drawText("Estado", 340, $y - 115, $enconding)
                ->drawText("Observaciones", 425, $y - 115)
                ->drawLine(30, $y - 120, 575, $y - 120)
        ;

        if ($flag = 'ext') {
            $page
                    ->setFont($font, 12)
                    ->drawText("Fecha exámen", 240, $y - 115, $encoding);
        } else {
            $page
                    ->setFont($font, 12)
                    ->drawText("Fecha extracción", 240, $y - 115, $encoding);
        }


        $h = 0;
        foreach ($resultados as $i => $v) {
            $page
                    ->setFont($font, 10)
                    ->drawText($v['don_nro'], 30, $y - 140 - $h, $encoding)
                    ->drawText(sprintf("%15.35s", $this->view->cache_efectores[$v['efe_id']]), 130, $y - 140 - $h, $encoding)
                    ->drawText($this->view->estados_donaciones[$v['don_estado']], 340, $y - 140 - $h, 'Windows-1250')
                    ->drawText(sprintf("%15.35s", $v['observaciones']), 425, $y - 140 - $h, $encoding)
            ;

            if ($flag == 'ext') {
                $page
                        ->drawText(date('d-m-Y', strtotime($v['don_fh_inicio'])), 255, $y - 140 - $h, $encoding);
            } else {
                $page
                        ->drawText(date('d-m-Y', strtotime($v['don_fh_extraccion'])), 255, $y - 140 - $h, $encoding);
            }

            $h = $h + 20;
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    public function imprimirInmunologiaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /* Para saber desde donde se llama */

        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');


        if ($fechaDesde == '' || $fechaHasta == '') {
            $fechaHasta = date('Y-m-d');
            $fechaDesde = date('Y-m-d', strtotime("-30 days"));
        }

        $perPage = 18;

        $modeloDon = new Banco_models_Donaciones();
        $modeloInmuno = new Banco_models_Inmunologia();
        $resultados = $modeloInmuno->getDatosEntreFechas($fechaDesde, $fechaHasta);
        foreach ($resultados as $clave => $valor) {
            $donacion = $modeloDon->getByID($valor['don_id']);
            $valor['don_nro'] = $donacion['don_nro'];
            $informe[] = $valor;
        }

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $encoding = 'UTF-8';
        $x = 385;
        $y = $page->getHeight();

        $page
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                ->setFont($font, 17)
                ->drawText("Informe de Inmunología", 170, $y - 40, $encoding)
        ;

        if ($fechaDesde) {
            $from = date("d-m-Y", strtotime($fechaDesde));
            $to = date("d-m-Y", strtotime($fechaHasta));
            $page
                    ->setFont($font, 12)
                    ->drawText("Entre  " . $from . "  y  " . $to, 30, $y - 85, $encoding)
            ;
        }

        $page
                ->setFont($font, 12)
                ->drawText("Nro. donación", 30, $y - 115, $encoding)
                ->drawText("Grupo", 110, $y - 115, $encoding)
                ->drawText("Factor", 150, $y - 115, $enconding)
                ->drawText("Rastreo", 200, $y - 115)
                ->drawText("Grupo Inverso", 250, $y - 115)
                ->drawText("Fecha proc.", 335, $y - 115)
                ->drawText("DU", 440, $y - 115)
                ->drawText("CDE", 465, $y - 115)
                ->drawText("Fenotipo", 500, $y - 115)
                ->drawLine(30, $y - 120, 575, $y - 120)
        ;


        $h = 20;
        foreach ($informe as $i => $v) {
            $page
                    ->setFont($font, 10)
                    ->drawText($v['don_nro'], 30, $y - 115 - $h)
                    ->drawText($v['grupo'], 120, $y - 115 - $h)
                    ->drawText($v['factor'], 165, $y - 115 - $h)
                    ->drawText($v['rastreo'], 200, $y - 115 - $h)
                    ->drawText($v['grupo_inverso'], 280, $y - 115 - $h)
                    ->drawText($v['timestamp'], 335, $y - 115 - $h)
                    ->drawText($v['du'], 445, $y - 115 - $h)
                    ->drawText($v['cde'], 475, $y - 115 - $h)
                    ->drawText($v['fenotipo'], 500, $y - 115 - $h)
            ;

            $h = $h + 20;
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

}
