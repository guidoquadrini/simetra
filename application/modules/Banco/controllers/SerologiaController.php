<?php

class Banco_SerologiaController extends BootPoint {

    public function indexAction() {
        /**
         * Buscar Pendientes
         */
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');

        if ($this->getRequest()->getParam('estado') == "todos") {
            $estado = true;
            $this->view->estado = "todos";
        } else {
            $estado = false;
            $this->view->estado = "pendientes";
        }

        $perPage = 12;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modelo = new Efector_models_Envios();
        $modeloInm = new Banco_models_Serologia();

        //Obtiene todas las donaciones sin serologia para los efectores del usuario.
        $resultados = $modelo->getDonacionesSinSerologiaAndEfe($fecha, $this->page, $perPage, $nro, $efes_ids, $estado);
        $this->view->busqueda = $fecha;

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        // En el caso que haya datos ya cargados para esa donación, los obtenemos.
        $donacion = array();

        foreach ($resultados as $i => $v)
            $donacion[$i] = $modeloInm->getDatosDonacion($v['donacion_id']);


        $modeloEnf = new Banco_models_Enfermedades();
        $modeloAna = new Banco_models_Analisis();

        $enfermedades = $modeloEnf->fetchAll()->toArray();
        $analisis = $modeloAna->fetchAll()->toArray();

        $this->view->errores = $this->getRequest()->getParam('errores', null);
        $this->view->enfermedades = $enfermedades;
        $this->view->analisis = $analisis;
        $this->view->donacion = $donacion;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
    }

    public function formularioIngresoManualAction() {
        $params = $this->getAllParams();
        $don_id = $params['don_id'];

        $modeloDonaciones = new Banco_models_Donaciones();

        $donacion = $modeloDonaciones->fetchRow('don_id = ' . $don_id);
        $per_id = $donacion['per_id'];

        $modeloPersonas = new Efector_models_Personas();
        $persona = $modeloPersonas->fetchRow('per_id = ' . $per_id);
        $codigo_autogenerado = $modeloPersonas->getCodigoAutogenerado($per_id);



        //TODO: revisar si vale la pena seguir con la siguiente estructura.
        $modeloInmunologia = new Banco_models_Serologia();
        //$donacion_determinaciones = $modeloInmunologia->getDatosDonacion($don_id);

        $modeloDeterminaciones = new Banco_models_Determinaciones();
        $donacion_determinaciones = $modeloDeterminaciones->getDetermiancionesByDonId($don_id);


        $modeloEnf = new Banco_models_Enfermedades();
        $this->view->enfermedades = $modeloEnf->fetchAll();

        $modeloAna = new Banco_models_Analisis();
        $this->view->analisis = $modeloAna->fetchAll()->toArray();

        $this->view->donacion = $donacion;
        $this->view->persona = $persona;

        $this->view->donacion_determinaciones = $donacion_determinaciones;

        $this->view->cabecera = array(
            'codigo_donacion' => $donacion->don_nro,
            'codigo_autogenerado' => $codigo_autogenerado,
            'fecha_extraccion' => $doancion->don_fh_extraccion
        );
    }

    public function registrarAction() {

        $pDetermiaciones = $this->getRequest()->getParam('params');

        $modeloDon = new Banco_models_Donaciones();
        $modeloSero = new Banco_models_Serologia();
        $modeloPer = new Efector_models_Personas();
        $modeloDet = new Banco_models_Determinaciones();

        $don_id = $this->getRequest()->getParam('don_id');
        $row = $modeloSero->fetchRow("don_id = " . $don_id);

        if ($don_id != null && $row == null) {
            $sero_id = $modeloSero->createRow(array(
                        'don_id' => $don_id,
                        'timestamp' => new Zend_Db_Expr('NOW()'),
                        'usuario_id' => $this->auth->getIdentity()->usu_id,
                    ))->save();

            $row = $modeloSero->find($sero_id)->toArray();
        }

        /**
         * REGISTRO DE NUEVOS RESULTADOS (DETERMINACIONES)
         * Se cargan las nuevas determianciones vinculadas a la donacion.
         */
        foreach ($pDetermiaciones as $determinacion) {
            $row_determinacion = array(
                'don_id' => $don_id,
                'cod_ana' => $determinacion['codigo-analisis'],
                'fecha_imp' => new Zend_Db_Expr('NOW()'),
                'usu_imp_id' => $this->auth->getIdentity()->usu_id,
                'resultado' => $determinacion['determinacion'],
                'estado' => 1,
                'tipo_ingreso' => 'MANUAL'
            );
            $newRow = $modeloDet->createRow($row_determinacion)->save();
        }

        /**
         * AJUSTE TBL:SEROLOGIAS-ENFERMEDADES
         * El siguiente metodo realiza una actualizacion de la tabla SEROLOGIAS_ENFERMEDADES.
         * Su importancia radica en que la validacion se realiza viendo los resultados de esta tabla.
         */
        $this->actualizarSerologia($row->id);

        /**
         * ACTUALIZACION CAMPO ULTIMA SEROLOGIA 
         * Se procede a actualizar el campo ult_serologia en la tabla personas con el valor de el ID de serologia.
         * Este proceso facilita los trabajos de busqueda aunque requiere eventual actualizacion.
         */
        foreach ($pDetermiaciones as $determinacion) {
            if (($determinacion == "REACTIVO") || ($determinacion == "DETECTABLE")) {

                //Busco persona y marco ult_serologia 
                $don = $modeloDon->find($don_id)->current();
                $per = $modeloPer->find($don->per_id)->current();
                $values = array(
                    'ult_serologia' => $row->id
                );
                $per->setFromArray($values)->save();
                break;
            }
        }

        /** ACTUALIZAR ESTADO VALIDADO A NO VALIDADO
         * Siempre que se registre un nuevo cambio entorno a las determinacione la validacion del hemocomponente queda nulo.
         */
        $this->cambiarEstadoValidacion($row->id, 0);






        $this->_helper->json(array('st' => 'ok'));
    }

    public function busquedaAction() {

        $this->view->visualizando = true;
    }

    /* Acción de importación de archivo de serología
     * 
     * */

    public function uploadAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->setDestination(APPLICATION_PATH . '/modules/Banco/temp');

        $modeloDet = new Banco_models_Determinaciones();
        $modeloSer = new Banco_models_Serologia();
        $modeloDon = new Banco_models_Donaciones();

        try {
            if ($upload->receive()) {
                $arc = $upload->getFileInfo();
            }
            $archivo = file($arc['archivo']['tmp_name']);
            //	Zend_Debug::dump($upload->getFileInfo());	
        } catch (Zend_File_Transfer_Exception $e) {
            echo $e->message();
        }

        /* Más validaciones sobre el archivo */
        if ($archivo) {//Si el archivo existe...
            foreach ($archivo as $v) {//Tomo el archivo y recorro las filas...
                $rows[] = explode(",", $v); //Convierto la cadena de caracteres en un arreglo utilizando como delimitador las comas.
            }

            //(Hay 16 campos por fila)
            foreach ($rows as $r) {//Tomo cada fila y recorro sus posiciones...
                $donaciones_codigo[] = rtrim(str_replace('"', '', $r[1]));
                $codigos[] = rtrim(str_replace('"', '', $r[2]));
                $descripcion[] = rtrim(str_replace('"', '', $r[4]));
                $resultados[] = rtrim(str_replace('"', '', $r[5]));
            }

            foreach ($donaciones_codigo as $donacion_codigo) {
                $don_id = $modeloDon->getID($donacion_codigo);
                $donaciones_id[] = $don_id['don_id'];
            }

            /* Analizar caso de múltiples rows (análisis repetido, se permite?) */
            foreach ($donaciones_id as $i => $id) {
                $fecha_imp = new Zend_Db_Expr('NOW()'); //Fecha y hora actual.
                $estado = 1;
                $usu_imp_id = $this->auth->getIdentity()->usu_id;

                /* 1.Checkeo si existe la donación
                 * 2.A.Si existe, sigo el proceso
                 * 2.B. Si no existe, no inserto en tabla de determinaciones
                 * */

                $existe = !is_null($modeloDon->find($id)->current());

                if ($existe) {

                    $modeloAna = new Banco_models_Analisis();
                    $idenf = $modeloAna->getCodEnf($codigos[$i]);

                    /* 1. Me fijo si el código es BB (bolsa bloqueada)
                     * 1.A. Si es BB, analizar el texto del resultado, que en vez de estar en la posición 5, está en la 6
                     * 1.B.1. Si no es BB, busco la determinación por id_don y código de det
                     * 2. Actualizo si es que no existe resultado o si es "sin det"
                     * 3. Si no existe, inserto sin datos de exportación
                     * */

                    if ($codigos[$i] == 'BB') {

                        // Se descarta por codigo BB.';
                    } else if (!$idenf) {

                        /** Sólo en este caso inserto el nombre del estudio en base de datos en funcion de lo que dice el archivo. 
                         * En el resto de los casos se obtiene de base de datos. 
                         * */
                        $row_determinacion = array(
                            'don_id' => $donaciones_id[$i],
                            'cod_ana' => $codigos[$i],
                            'fecha_imp' => $fecha_imp,
                            'usu_imp_id' => $usu_imp_id,
                            'resultado' => $resultados[$i],
                            'estado' => $estado,
                            'tipo_ingreso' => 'INTERFAZ'
                        );

                        $newRow = $modeloDet->createRow($row_determinacion)->save();
                    } else {

                        //Obtener por ID Y Codigo de Analisis la Determinacion del modelo determinaciones.
                        $rowDet = $modeloDet->getByIdAndAna($id, $codigos[$i]);

                        /**  1. Si ya existe una DETERMINACION con ese don_id y cod_ana
                         *  2.A. Inserto otra fila con la nueva información
                         *  2.B. Actualizo la determinacion con la nueva informacion.
                         * */
                        if ($rowDet) {//Si existe la determinacion consulta su ESTADO

                            /** ESTADOS POSIBLES EN RESULTADOS
                             * REACTIVO - NO REACTIVO - SIN ANALIZAR - INDETERMINADO - NO DETECTABLE - DETECTABLE
                             */

                            if ((strtoupper($rowDet->resultado) == 'REACTIVO') || (strtoupper($rowDet->resultado) == 'NO REACTIVO') || (strtoupper($rowDet->resultado) == 'INDETERMINADO') || (strtoupper($rowDet->resultado) == 'NO DETECTABLE') || (strtoupper($rowDet->resultado) == 'DETECTABLE')) {

                                
                                
                                $row_determinacion = array(
                                    'don_id' => $donaciones_id[$i],
                                    'cod_ana' => $codigos[$i],
                                    'fecha_imp' => $fecha_imp,
                                    'usu_imp_id' => $usu_imp_id,
                                    'resultado' => $resultados[$i],
                                    'estado' => $estado,
                                    'tipo_ingreso' => 'INTERFAZ'
                                );
                                $newRow = $modeloDet->createRow($row_determinacion)->save();
                                
                            } else {//Este es el caso en que se reciba un valor no reconocido por simetra

                                $values = array(
                                    'fecha_imp' => $fecha_imp,
                                    'usu_imp_id' => $usu_imp_id,
                                    'resultado' => $resultados[$i],
                                    'estado' => $estado,
                                    'tipo_ingreso' => 'INTERFAZ'
                                );
                                $rowDet->setFromArray($values)->save();
                            }
                        } else {//Si NO exite la determinacion la CREA.
                            $row_determinacion = array(
                                'don_id' => $donaciones_id[$i],
                                'cod_ana' => $codigos[$i],
                                'fecha_imp' => $fecha_imp,
                                'usu_imp_id' => $usu_imp_id,
                                'resultado' => $resultados[$i],
                                'estado' => $estado,
                                'tipo_ingreso' => 'INTERFAZ'
                            );
                            $newRow = $modeloDet->createRow($row_determinacion)->save();
                        }
                    }

                    /* Análisis de resultados para insertar en la tabla serología */
                    $modeloSer = new Banco_models_Serologia();
                    $rowSer = $modeloSer->getDatosDonacion($id);

                    if ($rowSer == null) {
                        $newRow = $modeloSer->createRow(array(
                                    'don_id' => $id,
                                    'timestamp' => new Zend_Db_Expr('NOW()'),
                                    'usuario_id' => $this->auth->getIdentity()->usu_id
                                ))->save();
                    }
                } else {
                    /* Se descarta porque no existe don id */
                }
            }
                        
            $serologia_id = $modeloSer->fetchRow("don_id = " . $don_id->don_id)->id;
            
            $this->cambiarEstadoValidacion($serologia_id, 0);
            
            $this->actualizarSerologia($serologia_id);

            $this->_helper->redirector('visualizador');
        }
    }

    public function visualizadorAction() {
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $estado = $this->getRequest()->getParam('estado');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 1;

        $perPage = 18;

        $modeloDet = new Banco_models_Determinaciones();
        $modeloAna = new Banco_models_Analisis();
        $mDonaciones = new Banco_models_Donaciones();
        if ($nro || $fecha) {
            $don_id = $mDonaciones->getID($nro);
            $resultados = $modeloDet->getDeter($don_id, $this->page, $perPage, 1, $fecha);
            $total = sizeof($resultados);
        } else {
            $resultados = $modeloDet->getByEstado($this->page, $perPage, 1);
            $total = $modeloDet->getDefaultAdapter()->fetchOne('SELECT COUNT(*) AS count FROM determinaciones WHERE estado = 1');
        }
        $result_procesados = null;
        if (sizeof($resultados) > 0) {
            $paginas = ceil($total / $perPage);

            foreach ($resultados as $resultado) {

                $nro_don = $mDonaciones->getNro($resultado->don_id)->don_nro;

                if ($nro_don == null) {
                    $nro_don = "Atencion! Error.";
                }
                $temp = $resultado->toArray();
                $temp['nro_don'] = $nro_don;
                $result_procesados[] = $temp;
            }
        }

        $analisis = $modeloAna->getNomAna();

        foreach ($analisis as $a)
            $nomAna[$a["cod"]] = $a["descripcion"];

        $this->view->busqueda = $fecha;
        $this->view->analisis = $nomAna;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->resultados = $result_procesados;
        $this->view->paginas = $paginas;
    }

    public function exportarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $exportado = $this->getRequest()->getParam('exportado');
        $exportar = explode(",", $exportado);
        $nom = str_replace(",", "", $exportado);

        $modeloEfecParam = new Efector_models_EfectoresParametros();
        $modeloDon = new Banco_models_Donaciones();
        $modeloPersonas = new Efector_models_Personas();
        $modeloEnf = new Banco_models_Enfermedades();
        $modeloAna = new Banco_models_Analisis();
        $modeloDet = new Banco_models_Determinaciones();

        $ana_def = $modeloAna->getDefaults()->toArray();
        $datos_efector = $modeloEfecParam->getParametros($this->efector_activo['id_efector']);


        //Busco y guardo los IDs de los donantes
        foreach ($exportar as $v) {
            $donaciones[$v] = $modeloDon->getRegistro($v);
            $per_ids[$v] = $donaciones[$v]->per_id;
        }

        foreach ($per_ids as $i => $v) {
            $personas[$i] = $modeloPersonas->getByID($v);
        }

        $tipo_doc = array(
            0 => 'DNR',
            1 => 'DNI',
            2 => 'LC',
            3 => 'LE',
            4 => 'CI',
            5 => 'OTR',
            6 => 'PAS',
            7 => 'IND'
        );

        $file = fopen(APPLICATION_PATH . "/modules/Banco/temp/exportado-" . $nom . ".txt", 'w');
        $encabezado = '"M"';
        $n = "N"; //campo sin especificar
        $obs = "Sin observaciones";
        $cod_med = $datos_efector['medico_matricula'];
        $servicio = $datos_efector['servicio_id'];
        $peticion = "H";
        $obs_ana = "";

        foreach ($exportar as $v) {
            $per_id = $per_ids[$v];
            $puerta = $this->efector_activo['id_efector'];
            $id = $donaciones[$v]->don_nro;
            /* Cabecera             
             * "M": SIGNIFICA QUE LOS DATOS CORRESPONDEN A LA CABECERA DE LA ORDEN.
             * "H": SIGNIFICA QUE LOS DATOS CORRESPONDEN A LAS PETICIONES DE ANALISIS. SI UNA ORDEN TIENE MAS DE UN AN�LISIS, SE AGREGA UN REGISTRO CON LOS MISMOS PARAMETROS DEBAJO.
             * ORI + NUM: CORRESPONDEN A LA IDENTIFICACION ALFANUMERICA DEL PACIENTE.
             * "N" (EL CAMPO QUE NO TIENE TITULO): SE REPITE EN TODAS LAS CABECERAS, NO TIENE NINGUNA UTILIDAD. 

             * Tipo(M o H), 
             * ORI(Codigo de Efector)
             * NUM        
             * APELLIDO NOMBRE                       
             * SEX   
             * FECH.NAC.        
             * OBS.GRALES DE LA ORDEN                   
             * FECH.INGRESO 
             * COD.MED.  
             * HIS.CLINICA  
             * TIPO  
             * NRO.DOC. 
             * SERV   
             * TELEFONO          
             * DIRECCION                      
             * NIM (N�mero Interno de Muestra) 
             */


            fwrite($file, $encabezado . ',');
            fwrite($file, '"' . $this->efector_activo['id_efector'] . '"' . ',');
            fwrite($file, '"' . $id . '"' . ',');
            fwrite($file, '"' . str_pad($personas[$v]->per_apellido . " " . $personas[$v]->per_nombres, 40) . '",');
            fwrite($file, '"' . $personas[$v]->per_sexo . '",');
            fwrite($file, '"' . $personas[$v]->fecha_nac . '",');
            fwrite($file, '"' . $n . '",'); //Sin especificar
            fwrite($file, '"' . $obs . '",');
            fwrite($file, '"' . date('d/m/Y', strtotime($donaciones[$v]->don_fh_inicio)) . '",');
            fwrite($file, '"' . $cod_med . '",');
            fwrite($file, '"' . $personas[$v]->per_id . '",');
            fwrite($file, '"' . $tipo_doc[$personas[$v]->per_tipodoc] . '",');
            fwrite($file, '"' . $personas[$v]->per_nrodoc . '",');
            fwrite($file, '"' . $servicio . '",');
            fwrite($file, '"' . str_pad($personas[$v]->telefono, 15) . '",');
            fwrite($file, '"' . str_pad($personas[$v]->calle . '  ' . $personas[$v]->numero, 30) . '",');
            fwrite($file, '"' . str_pad($donaciones[$v]->don_id, 28) . '"');

            fwrite($file, PHP_EOL);

            /*
              DOC.
              Ejemplo: "M","B",866533,"PEREZ IGNACIA","F","01/01/1999","N","OBSERVACIONES",
             *       "14/05/2009","CODPROF","1234567890","D","14669216","02","TELEFONO",
             *       "DIRECCION","I106251"
             * 
             * ORI  
             * NUM   
             * COD.ANA.       
             * 9 LINEAS CORRESPONDIENTES A OBSERVACIONES PARTICULARES DE ANALISIS
             * 
              Ejemplo: "H","B",866533,"902","1","2","3","4","5","6","7","8","9","N"
             *  

             * 
             */
            /* Peticiones de análisis */
            foreach ($ana_def as $i => $a) {
                fwrite($file, '"' . $peticion . '",');
                fwrite($file, '"' . $puerta . '"' . ',');
                fwrite($file, '"' . $id . '",');
                fwrite($file, '"' . str_pad($a['cod'], 8) . '",');
                for ($i = 1; $i < 9; $i++) {
                    fwrite($file, '"' . str_pad($obs_ana, 40) . '"' . ',');
                }
                fwrite($file, '"' . str_pad($obs_ana, 40) . '"');
                fwrite($file, ',"N"');
                fwrite($file, PHP_EOL);

                /* Inserción de campos en tabla determinaciones (exportadas) */
                $det_id = $modeloDet->createRow(array(
                            'cod_ana' => $a['cod'],
                            'don_id' => $donaciones[$v]->don_id,
                            'fecha_exp' => new Zend_Db_Expr('NOW()'),
                            'usu_exp_id' => $this->auth->getIdentity()->usu_id,
                            'estado' => 1
                        ))->save();
            }
            fwrite($file, PHP_EOL);
        }

        fclose($file);

        $path = APPLICATION_PATH . "/modules/Banco/temp/exportado-" . $nom . ".txt";

        if (file_exists($path)) {
            header('Content-Description: File Transfer');
            header('Content-Type: text/plain');
            header('Content-Disposition: attachment; filename=' .'solicitud.txt');//. basename($path));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);
            exit;
        }
    }

    public function exportarManualAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $analisis_string = $this->getRequest()->getParam('analisis');
        $don_id = $this->getRequest()->getParam('iddon');

        $analisis = explode(",", $analisis_string);

        $modeloAna = new Banco_models_Analisis();

        $realizarAna = $modeloAna->getCodByID($analisis);

        $modeloEfecParam = new Efector_models_EfectoresParametros();
        $modeloDon = new Banco_models_Donaciones();
        $modeloPersonas = new Efector_models_Personas();
        $modeloDet = new Banco_models_Determinaciones();
        $datos_efector = $modeloEfecParam->getParametros($this->efector_activo['id_efector']);

        $donacion = $modeloDon->getRegistro($don_id);
        $per_id = $donacion->per_id;

        $persona = $modeloPersonas->getByID($per_id);


        $tipo_doc = array(
            0 => DNR,
            1 => DNI,
            2 => LC,
            3 => LE,
            4 => CI,
            5 => OTR,
            6 => PAS,
            7 => IND
        );
        $file = fopen(APPLICATION_PATH . "/modules/Banco/temp/exportado-manual-" . $don_id . ".txt", w);
        $encabezado = '"M"';
        $n = "N"; //campo sin especificar
        $obs = "Sin observaciones"; //"N";
        $cod_med = $datos_efector['medico_matricula'];
        $servicio = $datos_efector['servicio_id'];
        $peticion = "H";
        $obs_ana = "";

        $puerta = $this->efector_activo['id_efector'];
        $id = $donacion[$v]->don_nro;

        fwrite($file, $encabezado . ',');
        fwrite($file, '"' . $puerta . '"' . ',');
        fwrite($file, '"' . $id . '"' . ',');
        fwrite($file, '"' . str_pad($persona->per_apellido . " " . $persona->per_nombres, 40) . '",');
        fwrite($file, '"' . $persona->per_sexo . '",');
        fwrite($file, '"' . $persona->fecha_nac . '",');
        fwrite($file, '"' . $n . '",');
        fwrite($file, '"' . $obs . '",');
        fwrite($file, '"' . date('d/m/Y', strtotime($donacion->don_fh_inicio)) . '",');
        fwrite($file, '"' . $cod_med . '",');
        fwrite($file, '"' . $persona->per_id . '",');
        fwrite($file, '"' . $tipo_doc[$persona->per_tipodoc] . '",');
        fwrite($file, '"' . $persona->per_nrodoc . '",');
        fwrite($file, '"' . $servicio . '",');
        fwrite($file, '"' . str_pad($persona->telefono, 15) . '",');
        fwrite($file, '"' . str_pad($persona->calle . '  ' . $persona->numero, 30) . '",');
        fwrite($file, '"' . str_pad($donacion->don_id, 28) . '"');

        fwrite($file, PHP_EOL);

        /* Peticiones de análisis */
        foreach ($realizarAna as $i => $a) {
            fwrite($file, '"' . $peticion . '",');
            fwrite($file, '"' . $puerta . '"' . ',');
            fwrite($file, '"' . $id . '",');
            fwrite($file, '"' . str_pad($a['cod'], 8) . '",');
            for ($i = 1; $i < 9; $i++) {
                fwrite($file, '"' . str_pad($obs_ana, 40) . '"' . ',');
            }
            fwrite($file, '"' . str_pad($obs_ana, 40) . '"');
            fwrite($file, ',"N"');
            fwrite($file, PHP_EOL);

            /* Inserción de campos en tabla determinaciones (exportadas) */
            $det_id = $modeloDet->createRow(array(
                        'cod_ana' => $a['cod'],
                        'don_id' => $donacion->don_id,
                        'fecha_exp' => new Zend_Db_Expr('NOW()'),
                        'usu_exp_id' => $this->auth->getIdentity()->usu_id,
                        'estado' => 1
                    ))->save();
        }
        fwrite($file, PHP_EOL);

        fclose($file);

        $path = APPLICATION_PATH . "/modules/Banco/temp/exportado-manual-" . $don_id . ".txt";

        if (file_exists($path)) {
            header('Content-Description: File Transfer');
            header('Content-Type: text/plain');
            header('Content-Disposition: attachment; filename=' . 'solicitud.txt');//. basename($path));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);
            exit;
        }
    }

    public function informeAction() {
        /**
         * Buscar Pendientes
         */
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $don_id = $this->getRequest()->getParam('id');

        if ($don_id) {
            $modeloDon = new Banco_models_Donaciones();
            $num = $modeloDon->getNro($don_id);
            if ($nro)
                $nro = $num->don_nro;
        }

        $estado = true;

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e) {
            $efes_ids[] = $e['id_efector'];
        }
        /* Fin efectores usuario */

        //Levanto los modelos que voy a utilizar
        $modeloEnvios = new Efector_models_Envios();
        $modeloSerologia = new Banco_models_Serologia();
        $modeloEnfermedades = new Banco_models_Enfermedades();
        $modeloAnalisis = new Banco_models_Analisis();
        $modeloSeroEnf = new Banco_models_Serologiasenfermedades();
        $modeloDeterminaciones = new Banco_models_Determinaciones();

        //Obtengo los catalogos de cada modelo (Enfermedades y Analisis)
        $enfermedades = $modeloEnfermedades->fetchAll()->toArray();
        $analisis = $modeloAnalisis->fetchAll()->toArray();

        //Busco donaciones sin serologia.
        $resultados = $modeloEnvios->getDonacionesSinSerologiaAndEfe($fecha, $this->page, $perPage, $nro, $efes_ids, $estado);

        //Paginado
        if (sizeof($resultados) > 0) {
            $total = $modeloEnvios->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        //Obtenemos los datos que puedan estar cargados la donacion. (Datos dentro de la grilla)
        $donacion = array();

        if (sizeof($resultados) > 0) {
            $resultados = $resultados->toArray();
        }

        //[RESULTADOS] CONTIENE LAS DONACIONES A VISUALIZAR.
        foreach ($resultados as &$item) {

            //Obtengo el ID de serologia para la donacion.
            $sero_id = $modeloSerologia->fetchRow("don_id = " . $item['donacion_id'])->id;
            //Actualizo cada serologia enfermedad de la donacion.
            $this->actualizarSerologia($sero_id);

            //Obtengo los resultados de Serologias_Enfermedades para cada Donacion.
            $rows_serologias_enfermedades = $modeloSeroEnf->fetchAll('serologias_id = ' . $sero_id)->toArray();
            //Agrego un Campo al REGISTRO DE RESULTADOS con la coleccion de determinaciones que se le hicieron a la donacion.

            foreach ($rows_serologias_enfermedades as &$enf) {
                //Buscar todas las determinaciones para esa enfermedad vinculadas a la donacion.
                $enf['determianciones_detalle'] = $modeloDeterminaciones->obtenerDeterminacionesPorEnfermedadPorDonacion($item['don_id'], $enf['enfermedades_id']);
            }
            $item['determinacion'] = $rows_serologias_enfermedades;

//Bloquear serologia para que no se pueda modificar nuevamente ya que esta validada en habilitaciones.
            //buscar en detalle produccion todas los hemos para la don_id
            //buscar si al menos una tiene validacion en true
            //caso positivo bloquear = true else false
            $modeloDetallesProduccion = new Banco_models_DetallesProduccion();
            $rows_detallesProduccion = $modeloDetallesProduccion->fetchAll('iddonacion = ' . $item['donacion_id']);
            $item['bloquear_serologia'] = in_array(true, array_column($rows_detallesProduccion->toArray(), 'validado'));
        }

        $this->view->rows_serologias_enfermedades = $rows_serologias_enfermedades;


        $this->view->busqueda = $fecha;
        $this->view->enfermedades = $enfermedades;
        $this->view->analisis = $analisis;
        $this->view->donacion = $donacion;
        $this->view->fecha = $fecha;
//        $this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
    }

    public function validarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $hemos = $this->getRequest()->getParam('hemo');

        $modeloSer = new Banco_models_Serologia();

        $values = array(
            'validado' => 1,
            'usu_id_valida' => $this->auth->getIdentity()->usu_id,
        );

        if ($hemos == '') {
            $this->_helper->json(array('st' => 'vacio'));
        } else {
            foreach ($hemos as $h) {
                $row = $modeloSer->find($h)->current();
                $modeloSer->find($h)->current()->setFromArray($values)->save();
            }
            $this->_helper->json(array('st' => 'ok'));
        }
    }

    public function actualizarSerologia($serologias_id) {

        $catalogoEnfermedades = (new Banco_models_Enfermedades())->fetchAll();
        $catalogoAnalisis = (new Banco_models_Analisis())->fetchAll();
        $modeloSerologia = new Banco_models_Serologia();
        $serologia = $modeloSerologia->fetchRow('id = ' . $serologias_id);
        $modeloSerologiaEnfermedad = new Banco_models_Serologiasenfermedades();

        $modeloDeterminaciones = new Banco_models_Determinaciones();

        foreach ($catalogoEnfermedades as $enfermedad) {

            $ultimaDetPorEnfDon = $modeloDeterminaciones->obtenerUltimaDetermiacionPorEnfermedadPorDonacion($serologia->don_id, $enfermedad->enf_id);

            //Ajuste para cuando no se realizo ningun estudio en sobre la muestra.

            $resultado = ($ultimaDetPorEnfDon->resultado == null) ? 'SIN ANALIZAR' : $ultimaDetPorEnfDon->resultado;

            //ROW_ENFERMEDAD: Buscar registro de la enfermedad para la serologia_id
            $row_enfermedad = $modeloSerologiaEnfermedad->fetchRow(array(
                'serologias_id = ' . $serologias_id,
                "enfermedades_id = " . $enfermedad->enf_id
            ));

            if ($row_enfermedad == null) {//Crea Registro Enfermedad
                $row = array(
                    'serologias_id' => $serologias_id,
                    'enfermedades_id' => $enfermedad->enf_id,
                    'determinacion' => $resultado
                );
                $row_enfermedad = $modeloSerologiaEnfermedad->createRow($row)->save();
            } else {//Actualiza Enfermedad
                $row_enfermedad->setFromArray(array('determinacion' => $resultado))->save();
            }
        }
    }

    /*     * Cambiar el estado de la validacion
     * 
     * @param type $serologia_id codigo de identificacion de la serologia.
     * @param type $estado estado de la serologia. 1:Validada - 0:No validada
     */

    public function cambiarEstadoValidacion($serologia_id, $estado) {

        $modeloSer = new Banco_models_Serologia();

        $values = array(
            'validado' => $estado,
            'usu_id_valida' => $this->auth->getIdentity()->usu_id,
        );

        $row_serologia = $modeloSer->fetchrow("id = " . $serologia_id)->setFromArray($values)->save();
    }

}
