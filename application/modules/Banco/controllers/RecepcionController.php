<?php

class Banco_RecepcionController extends BootPoint {

    public function preDispatch() {

        $cache = $this->getInvokeArg('bootstrap')->getResource('cache');

        $modeloMotDescartes = new Banco_models_Motdescartes();

        $motDescartes = $modeloMotDescartes->getMotDescartes();

        $cache->save($motDescartes, 'mot_descartes');

        $this->view->mot_descartes = $cache->load('mot_descartes');
    }

    public function indexAction() {

        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efe_id = $this->getRequest()->getParam('efe_id');
        $estado = $this->getRequest()->getParam('estado');

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modelo = new Efector_models_Envios();
        $resultados = $modelo->getByFechaWithDonacionesByEfe($fecha, $this->page, $perPage, $nro, $efe_id, $estado);

        $this->view->busqueda = $fecha;

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }
        if (empty($estado))
            $estado = 1;

        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efe_id = $efe_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
    }

    public function recepcionAction() {

        $don_id = (int) $this->getRequest()->getParam('don_id');
        $mot_id = (int) $this->getRequest()->getParam('mot_id');
        $don_peso = $this->getRequest()->getParam('pesaje');
        $don_temp = $this->getRequest()->getParam('temp');
        $pro_estado = $this->getRequest()->getParam('pro_estado');
        $recepcion_bolsa = $this->getRequest()->getParam('recepcion_bolsa');
        $recepcion_muestra = $this->getRequest()->getParam('recepcion_muestra');
        $recepcion_cuestionario = $this->getRequest()->getParam('recepcion_cuestionario');
        $recepcion_autoexclusion = $this->getRequest()->getParam('recepcion_autoexclusion');

        if ($recepcion_bolsa) {
            if ($don_peso >= 300 && $don_peso <= 900) {
                if ($recepcion_bolsa == null) {
                    $recepcion_bolsa = 0;
                }

                if ($recepcion_muestra == null) {
                    $recepcion_muestra = 0;
                }

                if (!$don_temp) {
                    $don_temp = 0;
                }

                $values = array('don_estado' => 10,
                    'pro_estado' => $pro_estado,
                    'recepcion_bolsa' => $recepcion_bolsa,
                    'recepcion_muestra' => $recepcion_muestra,
                    'recepcion_cuestionario' => $recepcion_cuestionario,
                    'recepcion_autoexclusion' => $recepcion_autoexclusion,
                    'don_peso' => $don_peso,
                    'don_temp' => $don_temp,
                    'don_fh_recepcion' => new Zend_Db_Expr('NOW()'),
                    'don_usu_recepcion' => $this->auth->getIdentity()->usu_id
                );

                $modeloDon = new Banco_models_Donaciones();
                $donacion = $modeloDon->find($don_id)->current();
                $donacion->setFromArray($values)->save();

                /* Si fue aceptada, insertamos en pre-produccion *
                 * *********************************************** */
                if ($pro_estado == 1)
                    $this->agregarAPreProduccion($don_id, $donacion->ppr_id);

                $this->_helper->json(array('st' => 'ok'));
            }

            else {
                $this->_helper->json(array('st' => 'pesonok'));
            }
        } elseif ($recepcion_muestra && !$recepcion_bolsa) {
            if ($recepcion_bolsa == null)
                $recepcion_bolsa = 0;

            $don_peso = 0;
            $don_temp = 0;

            if ($recepcion_muestra == null)
                $recepcion_muestra = 0;

            $values = array('don_estado' => 10,
                'pro_estado' => $pro_estado,
                'recepcion_bolsa' => $recepcion_bolsa,
                'recepcion_muestra' => $recepcion_muestra,
                'recepcion_muestra' => $recepcion_cuestionario,
                'recepcion_muestra' => $recepcion_autoexclusion,
                'don_peso' => $don_peso,
                'don_temp' => $don_temp,
                'don_fh_recepcion' => new Zend_Db_Expr('NOW()'),
                'don_usu_recepcion' => $this->auth->getIdentity()->usu_id
            );

            $modeloDon = new Banco_models_Donaciones();
            $donacion = $modeloDon->find($don_id)->current();
            $donacion->setFromArray($values)->save();

            /* NO se inserta en pre producción dado que sólo *
             *  se recepcionó la bolsa                        *
             * *********************************************** */

            $this->_helper->json(array('st' => 'ok'));
        }
    }

    public function recepcionMultipleAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $envios = $this->getRequest()->getParam('envios');
        $temp = (int) $this->getRequest()->getParam('temp');
        $peso = (int) $this->getRequest()->getParam('peso');

        if ($peso < 300 || $peso > 900) {
            $this->_helper->json(array('st' => 'nok'));
        }

        $modeloDon = new Banco_models_Donaciones();

        $pro_estado = 1; //Estado Produccion

        $values = array(
            'don_estado' => 10,
            'pro_estado' => $pro_estado,
            'recepcion_bolsa' => 1,
            'recepcion_muestra' => 1,
            'don_peso' => $peso,
            'don_temp' => $temp,
            'don_fh_recepcion' => new Zend_Db_Expr('NOW()'),
            'don_usu_recepcion' => $this->auth->getIdentity()->usu_id
        );

        /* Para cada envío, y para cada donación dentro de ese envío */
        foreach ($envios as $e) {
            $donaciones = $modeloDon->getDonacionesPorEnvio($e);

            foreach ($donaciones as $d) {
                $d->setFromArray($values)->save();

                $this->agregarAPreProduccion($d->don_id, $d->ppr_id);
            }
        }

        $this->_helper->json(array('st' => 'ok'));
    }

    private function agregarAPreProduccion($don_id, $ppr_id) {
        $modeloPre = new Banco_models_DetallesPreProduccion();

        $modeloHemoPer = new Banco_models_HemocomponentesPerfiles();

        $perfiles = explode(",", $ppr_id);

        if ($ppr_id != null && $ppr_id != "") {
            $hemo = $modeloHemoPer->listHemoByPerfiles(explode(",", $ppr_id));

            $hemos = array();
            foreach ($hemo as $i => $v)
                $hemos[] = $v["idhemocomponente"];

            /* Listo hemocomponentes quitando los no permitidos por el perfil */
            $modeloHem = new Gestion_models_Hemocomponentes();
            $hem = $modeloHem->getPairsPerfil($hemos);

            foreach ($hem as $i) {
                $pre_id = $modeloPre->createRow(array(
                            'don_id' => $don_id,
                            'hem_id' => $i['hem_id'],
                            'estado' => 1,
                            'usuario_id' => $this->auth->getIdentity()->usu_id,
                            'timestamp' => new Zend_Db_Expr('NOW()'),
                            'tipo_prod' => 1
                        ))->save();
            }
        } else {
            $modeloHem = new Gestion_models_Hemocomponentes();
            $hemo = $modeloHem->getPairs();
            foreach ($hemo as $i => $v) {
                $pre_id = $modeloPre->createRow(array(
                            'don_id' => $don_id,
                            'hem_id' => $i,
                            'estado' => 1,
                            'usuario_id' => $this->auth->getIdentity()->usu_id,
                            'timestamp' => new Zend_Db_Expr('NOW()'),
                            'tipo_prod' => 1
                        ))->save();
            }
        }

        return;
    }

    public function verAction() {
        $modeloHemoPer = new Banco_models_HemocomponentesPerfiles();

        $env_id = (int) $this->getRequest()->getParam('env_id');
        $env_nro = (int) $this->getRequest()->getParam('env_nro');

        $perPage = 18;

        $modelo = new Efector_models_Envios();

        $resultados = $modelo->getDonacionesEnvio($this->page, $perPage, $env_id);
        $this->view->busqueda = $fecha;

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $this->view->fecha = $fecha;
        $this->view->nro = $env_nro;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
    }

    public function mostrarRegistrarItemAction() {
        if (isset($this->helperSistema->parametros['donacion']) && isset($this->helperSistema->parametros['accion']) &&
                isset($this->helperSistema->parametros['peso'])
        ) {
            $donacion = new donacion((int) $this->helperSistema->parametros['donacion']);
            if ($this->helperSistema->parametros['accion'] == 1) {
                $tipo_accion = "Aceptar";
            } else {
                $tipo_accion = "Rechazar";
            }
            $this->helperSistema->template->donacion = $donacion;
            $this->helperSistema->template->tipo_accion = $tipo_accion;
            $this->helperSistema->template->cod_tipo_accion = $this->helperSistema->parametros['accion'];
            $this->helperSistema->template->peso = $this->helperSistema->parametros['peso'];
            $this->helperSistema->template->mostrar("recepcionenvios/recepcionarItem");
        } else {
            echo 'Error obteniendo parametros!';
        }
    }

    public function buscarItemsEnvioAction() {
        //parametro envio
        $helperDT = new dataTablesHelper($this->helperSistema->parametros);
        $envio = new envio((int) $this->helperSistema->parametros['envio']);

        //Si se paso una donacion
        if (isset($this->helperSistema->parametros['donacion']) && trim($this->helperSistema->parametros['donacion']) != '') {
            $cr = new Criterio('CONCAT(donaciones.cod_efector,donaciones.nro)', Criterio::IGUAL, "'" . $this->helperSistema->parametros['donacion'] . "'");
            $items = $envio->devItems($cr);
        } else {
            $items = $envio->devItems();
        }

        $arrItems = array();

        //Combo seleccion de accion para item
        $combo = '<select class="tipoaccion" @resultado@>';
        $combo .= '<option value="-1"></option>';
        $combo .= '<option value="1">Aceptar</option>';
        $combo .= '<option value="2">Rechazar</option>';
        $combo .= '</select>';

        //Input para ingreso de paso
        $inpPeso = '<input type="text" @resultado@ size="5" value="@valor_peso@" style="text-align:right;">';

        if (count($items) > 0) {
            foreach ($items as $item) {
                if ($item->estado == 5) {
                    $op = '<a href="#" rel="' . $item->id . '" class="registrarItem">Registrar Donaci&oacute;n</a>';
                    $combo = str_replace('@resultado@', '', $combo);
                    $inpPeso = str_replace('@resultado@', '', $inpPeso);
                    $inpPeso = str_replace('@valor_peso@', '', $inpPeso);
                } else {
                    $op = '<a href="#" rel="' . $item->id . '" class="edicionItem">Editar Donaci&oacute;n</a>';
                    $combo = str_replace('@resultado@', 'disabled', $combo);
                    //De acuerdo al estado de la donacion seleccion algun item del combo
                    $cadena = 'value="' . ($item->estado - 6) . '"';
                    $cadena_seleccionada = 'value="' . ($item->estado - 6) . '" selected ';
                    $combo = str_replace($cadena, $cadena_seleccionada, $combo);
                    $inpPeso = str_replace('@resultado@', 'readonly', $inpPeso);
                    //Poner el valor del peso en el input
                    $inpPeso = str_replace('@valor_peso@', $item->peso, $inpPeso);
                }
                $arrItems [] = array(
                    $item->id,
                    $item->solicitud->efector_origen->cod . $item->nro,
                    $item->ffin_extraccion,
                    $combo,
                    $inpPeso,
                    $item->observaciones,
                    $op
                );
            }
        }
        $total = count($arrItems);
        header('Content-Type: application/json');
        echo $helperDT->crearSalida1($total, $total, json_encode($arrItems));
    }

    public function modificarAction() {
        //determinacion
        $objPasado = json_decode($this->helperSistema->parametros['determinacion']);
        $odeterminacion = new determinacion();
        $this->igualarObjeto($objPasado, $odeterminacion, "M");
        if ($odeterminacion->actualizar()) {
            echo '{"estado":"ok"}';
        } else {
            echo '{"estado":"notok"}';
        }
    }

    public function buscarAction() {
        $helperDT = new dataTablesHelper($this->helperSistema->parametros);

        $cr = new Criterio();
        if (!isset($this->helperSistema->parametros["mini"])) {
            if ($helperDT->criterioBusqueda != "") {
                $helper = new helpers();
                $cr->agregarCriterio("1", Criterio::IGUAL, "1");
                /*
                  Aca se deben definir los criterios de busqueda:
                  Por ejemplo:
                  $cr->agregarAND(new Criterio("determinaciones.id",Criterio::CONTENGA,"'%".$helperDT->criterioBusqueda->id."%'"));
                 */
                if (trim($helperDT->criterioBusqueda->nro) != '') {
                    $cr->agregarAND(new Criterio("envios.nro", Criterio::IGUAL, "'" . (int) $helperDT->criterioBusqueda->nro . "'"));
                }
                if ($helperDT->criterioBusqueda->efector != -1) {
                    $cr->agregarAND(new Criterio("envios.idefector", Criterio::IGUAL, "'" . $helperDT->criterioBusqueda->efector . "'"));
                }
                if (trim($helperDT->criterioBusqueda->fdesde) != '') {
                    $cr->agregarAND(new Criterio('date(envios.fecha)', Criterio::MAYOR_IGUAL, "'" . $helper->formatearFecha(2, $helperDT->criterioBusqueda->fdesde) . "'"));
                }
                if (trim($helperDT->criterioBusqueda->fhasta) != '') {
                    $cr->agregarAND(new Criterio('date(envios.fecha)', Criterio::MENOR_IGUAL, "'" . $helper->formatearFecha(2, $helperDT->criterioBusqueda->fhasta) . "'"));
                }
                if ($helperDT->criterioBusqueda->estado != -1) {
                    $cr->agregarAND(new Criterio("envios.estado", Criterio::IGUAL, "'" . $helperDT->criterioBusqueda->estado . "'"));
                }
            }
        } else {

            if ($helperDT->textoFiltro != '') {
                $cr->agregarCriterio("1", Criterio::DISTINTO, "1");
                /*
                  Aca se deben definir los criterios de busqueda:
                  Por ejemplo:
                  $cr->agregarOR(new Criterio("determinaciones.id",Criterio::CONTENGA,"'%".$helperDT->textoFiltro."%'"));
                  $cr->agregarOR(new Criterio("determinaciones.cod",Criterio::CONTENGA,"'%".$helperDT->textoFiltro."%'"));
                  $cr->agregarOR(new Criterio("determinaciones.nombre",Criterio::CONTENGA,"'%".$helperDT->textoFiltro."%'"));
                  $cr->agregarOR(new Criterio("determinaciones.estado",Criterio::CONTENGA,"'%".$helperDT->textoFiltro."%'"));
                 */
            }
        }
        $crCuenta = clone $cr;
        $cr->agregarOrden($helperDT->nroColOrden, $helperDT->dirOrden);
        $cr->agregarLimite($helperDT->desde, $helperDT->cantidad);
        $oenvios = new envios();
        if (!isset($this->helperSistema->parametros["mini"])) {
            echo $helperDT->crearSalida($oenvios->contar(), $oenvios->contar($crCuenta), $oenvios->devRecepcionEnviosDT($cr));
        } else {
            echo $helperDT->crearSalida($odeterminaciones->contar(), $odeterminaciones->contar($crCuenta), $odeterminaciones->devdeterminacionesMiniDT($cr));
        }
    }

}
