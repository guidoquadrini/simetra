<?php

class Banco_ProduccionController extends BootPoint {

    public function indexAction() {
        /**
         * Buscar Pendientes
         */
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $hem_id = $this->getRequest()->getParam('hem_id');
        $estado = $this->getRequest()->getParam('estado');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 1; /* Por defecto busca los pendientes */

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();

        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modelo = new Banco_models_DetallesProduccion();
        //llamada con efes_ids en vez de efector_id
        $resultados = $modelo->getEnviosParaProduccionByEfe($fecha, $this->page, $perPage, $nro, $efes_ids, $estado, $hem_id);
        $this->view->busqueda = $fecha;

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $modeloHemocomponentes = (new Gestion_models_Hemocomponentes())->fetchAll();

        $this->view->catalogo_hemocomponentes = $modeloHemocomponentes;
        $this->view->hem = $hem_id;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
    }

    public function confirmarAction() {

        $id = $this->getRequest()->getParam('id');
        $temp = (int) $this->getRequest()->getParam('temp');
        $peso = (int) $this->getRequest()->getParam('peso');
        $modelo = new Banco_models_DetallesProduccion();
        $modeloHem = new Gestion_models_Hemocomponentes();
        $modeloDetallesDon = new Efector_models_DetallesDonaciones();
        $modeloBolsas = new Efector_models_Bolsas();

        $row = $modelo->find($id)->current()->toArray();
        $pesos = $modeloHem->getPesos($row['idhemocomponente']);
        $dias = $modeloHem->getDias($row['idhemocomponente']);

        /* Si es CGR, se calcula vencimiento en base a bolsa */
        if ($row['idhemocomponente'] == 3) {
            $id_bolsa = $modeloDetallesDon->getTipoBolsa($row['iddonacion']);
            $bolsa = $modeloBolsas->find($id_bolsa)->current()->toArray();
            $vencimiento = date('Y-m-d H:i:s', strtotime($row['ffin_prod'] . '+' . $bolsa["dias"] . ' days'));
            ;
        } else
            $vencimiento = date('Y-m-d H:i:s', strtotime($row['ffin_prod'] . '+' . $dias["hem_dias_aptitud"] . ' days'));


        if (($peso >= $pesos['hem_peso_min']) && ($peso <= $pesos['hem_peso_max'])) {
            $values = array(
                'detalle' => $this->getRequest()->getParam('detalle'),
                'peso' => $peso,
                'hem_temp' => $temp,
                'estado' => $this->getRequest()->getParam('estado'),
                'ffin_prod' => new Zend_Db_Expr('NOW()'),
                'f_vencimiento' => $vencimiento,
                'usuario_id' => $this->auth->getIdentity()->usu_id,
            );
            $modelo->find($id)->current()->setFromArray($values)->save();
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'pesonok'));
        }
    }

    /** Eliminar Fila Index * */
    public function eliminarRowAction() {
        $hem_prod_id = $this->getRequest()->getParam('id');
        $don_nro = $this->getRequest()->getParam('don_nro');
        $hem_id = (int) $this->getRequest()->getParam('hemocomponente_id');
        $modeloDon = new Banco_models_Donaciones();
        $don_id = $modeloDon->getID($don_nro);

        $modeloProduccion = new Banco_models_DetallesProduccion();
        $modeloPreproduccion = new Banco_models_DetallesPreProduccion();

        $where_produccion = 'id =  ' . $hem_prod_id
        ;

        $where_preproduccion = array(//Filtrado por donacion y hemocomponente para la donacion.
            'don_id' => $don_id,
            'hem_id' => $hem_id
        );

        $values = array(//Estado a actualizar.
            'estado' => 1
        );

        try {
            //Eliminar Hemocomponentes de Produccion        
            $modeloProduccion->delete($where_produccion);
            // Actualizar Estado de Hemocomponentes de Preproduccion.
            $row_preproduccion = $modeloPreproduccion->fetchAll($where_preproduccion)->current();
            $row_preproduccion->setFromArray($values)->save();
            $this->_helper->json(array('st' => 'ok'));
        } catch (Exception $exc) {
            $this->_helper->json(array(
                'st' => 'nok',
                'mensaje' => $exc->getTraceAsString()
            ));
        }
    }

    /** INFORME DE VALIDACION PARA HEMOCOMPONENTES PRODUCIDOS
     *  PARAMETROS DE FILTRADO
     *  FECHA: fecha de inicio de produccion
     *  NRO: CODIGO DE BARRA DE LA DONACION
     *  EFECTOR_ID: Efector para el que se revisara la produccion.
     *  HEM_ID: Hemocomponente por el que filtrara la informacion.
     *  ESTADO: Estado por el que se filtrara la informacion. DEFAULT = 0
     */
    public function informeAction() {

        /** Parametros de filtrado * */
        $fecha = $this->getRequest()->getParam('fecha', null);
        $don_nro = $this->getRequest()->getParam('nro', null);
        $efector_id = $this->getRequest()->getParam('efector_id', null);
        $hem_id = $this->getRequest()->getParam('hem_id', null);

        /* Por defecto busca registros con estado PENDIENTE */
        $estado = $this->getRequest()->getParam('estado', 1);
        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modeloSer = new Banco_models_Serologia();
        $modelo = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();

        /** PROCEDIMIENTO
         * 1.BUSCAR HEMOCOMPONENTES PRODUCIDOS USANDO FILTROS EN TABLA ENVIOS
         * 2.BUSCAR DATOS DEL HEMOCOMP. EN PRODUCCION
         * 3.BUSCAR DATOS DE LA DONACION
         * 4.BUSCAR DATOS DE INMUNO PARA LA DONACION
         * 5.BUSCAR DATOS DE SEROLOGIA PARA CADA HEMOCOMP Y PRESENTAR RESULTADO UNICO.
         * 
         */
        $producidos = $modelo->obtenerResultadoParaInformeIndex($fecha, $this->page, $perPage, $don_nro, $efes_ids, 2, $hem_id);

        $modeloSerologiaEnfermedades = new Banco_models_Serologiasenfermedades();

        $resultados = array();
        foreach ($producidos as $item) {
            $id = $item['detalle_produccion_id'];
            $resultados[$id] = $item->toArray();
            
            $rows_inmuno = $modeloInmuno->getDatosDonacion($item->don_id);
            if ($rows_inmuno != null){
                $resultados[$id]['inmunologia'] = $modeloInmuno->getDatosDonacion($item->don_id)->toArray();
            }else{
                $resultados[$id]['inmunologia'] = null;
            }
            
            
            $row_serologia = $modeloSer->getDatosDonacion($item->don_id);
            $rows_serologia = $modeloSerologiaEnfermedades->fetchAll('serologias_id = ' . $row_serologia->id)->toArray();
            
            $conformidad_serologia = true;
            foreach ($rows_serologia as $serologia_item) {

                $rSero = $serologia_item['determinacion'];

                $aSeroOK = array("NO REACTIVO", "NO DETECTABLE");
                
                $conformidad_serologia = $conformidad_serologia && in_array($rSero, $aSeroOK);

                }
            /** Conformidad_Serologica
             * TRUE: En caso que se pueda utilizar
             * FALSE: En caso que este cualquier cosa fuera de "NO REACTIVO", "NO DETECTABLE"
             */
                        $resultados[$id]['serologia'] = (bool)$conformidad_serologia;
                        //TODO: usar rows serologia pero quitar el to array, ver antes el impacto.
                        $resultados[$id]['serologia_detalle'] = $modeloSerologiaEnfermedades->fetchAll('serologias_id = ' . $row_serologia->id);
                        
        }
        
//        echo "<pre>";
//        var_dump($rows_serologia);
//        echo "</pre>";
//        die();        

        /** CONJUNTO DE RESULTADO
         * RESULTADO = PRODUCCIONe + DONACIONe + HEMOe + SEROLOGIA + INMUNO
         * 
         * ESTRUCTURAS DE DATOS
         * PRODUCCIONe = ID_PRODUCTO + PROD_OBS
         * DONACIONe = DON_ID+ FH_EXTRACCION + VENCIMIENTO + ESTADO VALIDACION
         * HEMOe = ID_HEMO + DESC_HEMO + PESO
         */
        $this->view->busqueda = $fecha;
        $this->view->enfermedades = (new Banco_models_Enfermedades())->getPairs();
        $this->view->hem = $hem_id;
        $this->view->serologia = $serologia; //REVISAR QUE ESTE VALIDADA
        $this->view->inmuno = $inmuno;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $don_nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
        

        
        
        
    }

    public function validarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $hemos = $this->getRequest()->getParam('hemo');
        $irrad = $this->getRequest()->getParam('irrad');

        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloSer = new Banco_models_Serologia();
        $modeloInmuno = new Banco_models_Inmunologia();

        $values = array(
            'validado' => 1,
            'usu_id_valida' => $this->auth->getIdentity()->usu_id,
        );

        if ($hemos == '') {
            $this->_helper->json(array('st' => 'vacio'));
        } else {
            foreach ($hemos as $h) {
                $row = $modeloPro->find($h)->current();
                $serologia[$h] = $modeloSer->getDatosDonacion($row->iddonacion)->toArray();
                $inmuno[$h] = $modeloInmuno->getDatosDonacion($row->iddonacion)->toArray();

                /* Si está analizada la serología, es "No reactiva" en todos sus campos,
                 * y tiene analizada la inmunología, entonces valido
                 * (ya se valida del lado del cliente también, a la hora de mostrar el checkbox)
                 * */
                if (!is_null($serologia[$h]) && !in_array("Reactivo", $serologia[$h]) && !in_array("Sin Determinar", $serologia[$h]) && !is_null($inmuno[$h]))
                    $modeloPro->find($h)->current()->setFromArray($values)->save();
            }
            $this->_helper->json(array('st' => 'ok'));
        }
    }

    public function stockAction() {
        /**
         * Buscar Pendientes -- listo
         * IMPORTANTE:
         * Buscar sólo hemocomponentes con serología no reactiva e 
         * inmunología ya analizada -- no se debería poder producir de ese modo
         * 
         * Agregar filtro de  grupo y factor
         * 
         */
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $grupo = $this->getRequest()->getParam('grupo');
        $factor = $this->getRequest()->getParam('factor');
        $estado = $this->getRequest()->getParam('estado');
        $tab = $this->getRequest()->getParam('tab');
        $hem_id = $this->getRequest()->getParam('hem_id');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 2; /* Por defecto busca los pendientes */

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modeloSer = new Banco_models_Serologia();
        $modelo = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloHem = new Gestion_models_Hemocomponentes();

        $hemocomponentes = $modeloHem->getPairs();

        $stock = $modelo->getStockListByEfe($efes_ids);

        $this->view->busqueda = $fecha;

        $producidos = $modelo->getBolsaParaEnvioByEfe($fecha, $this->page, $perPage, $nro, $efes_ids, 2, $hem_id);

        if (sizeof($producidos) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        foreach ($producidos as $i => $v)
            $serologia[$v['don_id']] = $modeloSer->getDatosDonacion($v['don_id']);

        foreach ($producidos as $i => $v)
            $inmuno[$v['don_id']] = $modeloInmuno->getDatosDonacion($v['don_id']);

        /* Si pasa parámetro de grupo y factor, modifico ->resultados
         *  y ->stock
         * */
        if ($factor != '') {
            foreach ($producidos as $i => $v) {
                if ((($inmuno[$v['don_id']]->grupo) == $grupo) && (($inmuno[$v['don_id']]->factor) == $factor)) {
                    $filtro[$i] = $v;
                    $ids[] = $v['don_id'];
                }
            }
            if ($ids)
                $this->view->stock = $modelo->getStockListByIDs($ids);

            $this->view->resultados = $filtro;
        }
        else {
            $this->view->resultados = $producidos;
            $this->view->stock = $stock;
        }

        $this->view->grupo = $grupo;
        $this->view->factor = $factor;
        $this->view->tab = $tab;
        $this->view->serologia = $serologia;
        $this->view->inmuno = $inmuno;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->hemocomponentes = $hemocomponentes;
        $this->view->paginas = $paginas;
    }

    public function enviosAction() {

        /**
         * Buscar Pendientes
         */
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $estado = $this->getRequest()->getParam('estado');
        $hem_id = $this->getRequest()->getParam('hem_id');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 2; /* Por defecto busca los pendientes */

        $perPage = 18;

        $modelo = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloSer = new Banco_models_Serologia();
        $modeloEnvProd = new Banco_models_EnviosProduccion();

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $producidos = $modelo->getBolsaParaEnvioByEfe($fecha, $this->page, $perPage, $nro, $efes_ids, $estado, $hem_id);
        if (sizeof($producidos) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        foreach ($producidos as $i => $v) {
            $inmuno[$v['don_id']] = $modeloInmuno->getDatosDonacion($v['don_id']);
            $serologia[$v['don_id']] = $modeloSer->getDatosDonacion($v['don_id']);
            //	$env_prod[$v['don_id']] = $modeloEnvProd->getByIdProd($v['id']);
            $env_prod[$v['don_id']] = $modeloEnvProd->find($v['env_prod_id'])->current();
        }

        $modeloUsu = new Acceso_models_Usuarios();

        if ($env_prod) {
            foreach ($env_prod as $i => $v) {
                if ($v->usu_env) {
                    $usu = $modeloUsu->find($v->usu_env)->current();
                    $v->usu_env = $usu['usu_usuario'];
                }
                if ($v->usu_rec) {
                    $usu = $modeloUsu->find($v->usu_rec)->current();
                    $v->usu_rec = $usu['usu_usuario'];
                }
            }
        }

        $this->view->hem_id = $hem_id;
        $this->view->serologia = $serologia;
        $this->view->busqueda = $fecha;
        $this->view->inmuno = $inmuno;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $producidos;
        $this->view->paginas = $paginas;
        $this->view->datos_env = $env_prod;
    }

    public function historialAction() {
        /**
         * Buscar Pendientes
         */
        $fechaEnv = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $estado = $this->getRequest()->getParam('estado');
        $efe_dest = (int) $this->getRequest()->getParam('efe_id');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 4; /* Por defecto busca los sólo enviados */

        $perPage = 18;

        $modelo = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloSer = new Banco_models_Serologia();
        $modeloEnvProd = new Banco_models_EnviosProduccion();
        $modeloUsu = new Acceso_models_Usuarios();

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        /* Historial de envíos */
        $enviados = $modeloEnvProd->getByEstado(array(4, 5), $this->page, $perPage, $efes_ids, $nro, $fechaEnv, $efe_dest);

        if (sizeof($enviados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        if ($enviados) {
            foreach ($enviados as $i => $v) {
                if ($v->usu_env) {
                    $usu = $modeloUsu->find($v->usu_env)->current();
                    $v->usu_env = $usu['usu_usuario'];
                }
                if ($v->usu_rec) {
                    $usu = $modeloUsu->find($v->usu_rec)->current();
                    $v->usu_rec = $usu['usu_usuario'];
                }
            }
        }

        $this->view->enviados = $enviados;

        $this->view->busqueda = $fecha;
        $this->view->estado = $estado;
        $this->view->fecha = $fechaEnv;
        $this->view->nro = $nro;
        $this->view->paginas = $paginas;
    }

    public function irradiarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $ids_string = $this->getRequest()->getParam('irrad');
        $irrad = explode(",", $ids_string);

        $modeloPro = new Banco_models_DetallesProduccion();

        $values = array(
            'irradiado' => 1
        );

        if ($irrad) {
            foreach ($irrad as $i => $v) {
                $modeloPro->find($v)->current()->setFromArray($values)->save();
            }
            $this->_helper->json(array('st' => 'ok'));
        } else {
            $this->_helper->json(array('st' => 'nok'));
        }
    }

    public function enviarAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bolsas = $this->getRequest()->getParam('bolsas');
        $efe_id = $this->getRequest()->getParam('efector');
        $otro = $this->getRequest()->getParam('otro');
        $obs = $this->getRequest()->getParam('obs');

        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloEnvProd = new Banco_models_EnviosProduccion();

        if ($bolsas == '') {
            $this->_helper->json(array('st' => 'vacio'));
        } else {
            if ($efe_id) {
                $val = array(
                    'efe_origen' => $this->efector_activo['id_efector'],
                    'efe_destino' => $efe_id,
                    'fecha_env' => new Zend_Db_Expr('NOW()'),
                    'usu_env' => $this->auth->getIdentity()->usu_id,
                    'estado' => 4, //enviado
                    'observaciones' => $obs
                );
            } else {
                $val = array(
                    'efe_origen' => $this->efector_activo['id_efector'],
                    'efe_destino' => $this->efector_activo['id_efector'],
                    'otro_destino' => $otro,
                    'fecha_env' => new Zend_Db_Expr('NOW()'),
                    'usu_env' => $this->auth->getIdentity()->usu_id,
                    'estado' => 4, //enviado
                    'observaciones' => $obs
                );
            }
            $env_id = $modeloEnvProd->createRow($val)->save();

            foreach ($bolsas as $b) {
                $row_prod = $modeloPro->find($b)->current();
                $values = array(
                    'estado' => 4, //enviado a efector
                    'env_prod_id' => $env_id
                );

                $modeloPro->find($b)->current()->setFromArray($values)->save();
            }

            $this->_helper->json(array('st' => 'ok', 'id' => $env_id));
        }
    }

    public function pdfEnvioAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $env_id = (int) $this->getRequest()->getParam('id');

        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloHem = new Gestion_models_Hemocomponentes();
        $modeloEnvProd = new Banco_models_EnviosProduccion();
        $modelUsu = new Acceso_models_Usuarios();
        $modeloEfec = new Acceso_models_Efectores();
        $modeloDon = new Banco_models_Donaciones();

        $env_prod = $modeloEnvProd->find($env_id)->current();
        $donaciones = $modeloPro->getByEnv($env_id);
        $usuarioEnv = $modelUsu->find($env_prod['usu_env'])->current();
        $efe_origen = $modeloEfec->find($env_prod['efe_origen']);
        $efe_destino = $modeloEfec->find($env_prod['efe_destino']);

        if ($env_prod['usu_rec'])
            $usuarioRec = $modelUsu->find($env_prod['usu_rec'])->current()->toArray();

        foreach ($donaciones as $i => $v) {
            $hemo[$i] = $modeloHem->getTempDescDias($v['idhemocomponente']);
            $inmuno[$i] = $modeloInmuno->getDatosDonacion($v['iddonacion']);
            $vencimiento[$i] = date('d-m-Y H:i:s', strtotime($v['ffin_prod'] . '+' . $hemo[$i]['hem_dias_aptitud'] . ' days'));
            $don[$i] = $modeloDon->getRegistro($v['iddonacion']);
            $extraccion[$i] = date('d-m-Y', strtotime($don[$i]->hora_extraccion));
        }

        $Detalle = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $encoding = 'UTF-8';
        $x = 385;
        $y = $page->getHeight();

        $cache_env_prod = array(
            'env_prod_id' => 'ID Envío',
            'prod_id' => 'ID Producción',
            'efe_origen' => 'Efector orígen',
            'efe_destino' => 'Efector destino',
            'otro_destino' => 'Otro destino',
            'fecha_env' => 'Fecha envío',
            'fecha_rec' => 'Fecha recepción',
            'usu_env' => 'Usuario envío',
            'usu_rec' => 'Usuario recepción',
            'estado' => 'Estado',
            'observaciones' => 'Observaciones',
        );

        $h = 20;

        if ($env_prod) {
            $page
                    ->setFont($font, 16)
                    ->drawText("Envío de hemocomponentes", 30, $y - 20, $encoding)
                    ->setFont($font, 11)
                    ->drawText($cache_env_prod['env_prod_id'] . ' : ' . $env_prod['env_prod_id'], 30, $y - 40, $encoding)
                    ->drawText($cache_env_prod['efe_origen'] . ' : ' . sprintf("%15.30s", $efe_origen->current()->nomest), 30, $y - 60, $encoding)
                    ->drawText($cache_env_prod['efe_destino'] . ' : ' . sprintf("%15.30s", $efe_destino->current()->nomest), 30, $y - 80, 'ISO-8859-1')
                    ->drawText($cache_env_prod['otro_destino'] . ' : ' . $env_prod['otro_destino'], 330, $y - 40, $encoding)
                    ->drawText($cache_env_prod['fecha_env'] . ' : ' . date('d-m-Y H:i:s', strtotime($env_prod['fecha_env'])), 330, $y - 60, $encoding)
                    ->drawText($cache_env_prod['usu_env'] . ' : ' . $usuarioEnv['usu_usuario'], 330, $y - 80, $encoding)
            ;
            
            if ($env_prod['fecha_rec'] != "0000-00-00 00:00:00") {
                $page
                        ->drawText($cache_env_prod['fecha_rec'] . ' : ' . date('d-m-Y H:i:s', strtotime($env_prod['fecha_rec'])), 400, $y - 40, $encoding)
                        ->drawText($cache_env_prod['usu_rec'] . ' : ' . $usuarioRec['usu_usuario'], 400, $y - 60, $encoding)
                        ->drawText($cache_env_prod['observaciones'] . ' : ' . $env_prod['observaciones'], 400, $y - 80, $encoding)
                ;
            } else {
                $page
                        ->drawText($cache_env_prod['observaciones'] . ' : ' . sprintf("%15.45s", $env_prod['observaciones']), 30, $y - 100, $encoding)
                ;
            }
        }

        $page
                ->setFont($font, 15)
                ->drawText("Detalle", 30, $y - 140, $encoding)
                ->setFont($font, 11)
                ->drawText("Hemocomponente", 30, $y - 160, $encoding)
                ->drawText("Grupo", 175, $y - 160, $encoding)
                ->drawText("Factor", 225, $y - 160, $encoding)
                ->drawText("Peso", 275, $y - 160, $encoding)
                ->drawText("Extracción", 320, $y - 160, $encoding)
                ->drawText("Vencimiento", 390, $y - 160, $encoding)
                ->drawText("Observaciones", 475, $y - 160, $encoding)
                ->setFont($font, 10)
        ;
        $h = 0;
        foreach ($donaciones as $i => $v) {
            if ($v['irradiado'])
                $donaciones[$i]['detalle'] = 'Irradiado - ' . $v['observaciones'];

            $page
                    ->drawText($hemo[$i]['hem_descripcion'], 30, $y - 180 - $h, 'ISO-8859-1')
                    ->drawText($inmuno[$i]['grupo'], 180, $y - 180 - $h, $encoding)
                    ->drawText($inmuno[$i]['factor'], 230, $y - 180 - $h, $encoding)
                    ->drawText($donaciones[$i]['peso'], 280, $y - 180 - $h, $encoding)
                    ->drawText($extraccion[$i], 320, $y - 180 - $h, $encoding)
                    ->drawText(date('d-m-Y', strtotime($vencimiento[$i])), 390, $y - 180 - $h, $encoding)
                    ->drawText(sprintf("%15.20s", $donaciones[$i]['detalle']), 475, $y - 180 - $h, $encoding)
            ;
            $h = $h + 20;
        }

        $pdf->pages[] = $page;
        
        header('Content-Type: application/pdf');
        
        echo $pdf->render();
    }

    public function agregarHemoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cod = $this->getRequest()->getParam('cod');
        $hem_id = (int) $this->getRequest()->getParam('hemo');

        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloPre = new Banco_models_DetallesPreProduccion();
        $modeloDon = new Banco_models_Donaciones();

        //A partir del código de donación, obtengo el ID de donación
        $condicion = !($modeloDon->getID($cod) == NULL);
        if ($condicion) {
            $donacion = $modeloDon->getID($cod);
            //ver que hemos no sea null
            $hemos = $modeloPre->getPreProduccionByID($donacion['don_id']);
            foreach ($hemos as $i => $v)
                $filtro[] = $v->hem_id;
            //correción por versión php
            $condicion2 = ($filtro == NULL);
            if ($condicion2) {
                $this->_helper->json(array('st' => 'nohay'));
            } else {
                $values = array('estado' => 2,
                    'timestamp' => new Zend_Db_Expr('NOW()'),
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'tipo_prod' => 2);
                $values_manual = array('estado' => 1,
                    'timestamp' => new Zend_Db_Expr('NOW()'),
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'tipo_prod' => 2);

                //Si el hemocomponente solicitado está en los disponibles, se crea
                //en producción y se pasa a estado 2 en preproducción
                if (in_array($hem_id, $filtro)) {
                    foreach ($hemos as $h) {
                        if (($h->hem_id) == $hem_id) {
                            $h->setFromArray($values)->save();
                        } else {
                            $h->setFromArray($values_manual)->save();
                        }
                    }

                    // paso a producción
                    $prod_id = $modeloPro->createRow(array(
                                'iddonacion' => $donacion['don_id'],
                                'idhemocomponente' => $hem_id,
                                'fini_prod' => new Zend_Db_Expr('NOW()'),
                                'estado' => 1
                            ))->save();

                    $this->_helper->json(array('st' => 'ok'));
                } else {
                    $this->_helper->json(array('st' => 'noesta'));
                }
            }
        } else {
            $this->_helper->json(array('st' => 'nodon'));
        }
    }

    /*
     * ETIQUETON
     *  
     */
    
    public function verpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = (int) $this->getRequest()->getParam('id');

        $modeloSer = new Banco_models_Serologia();
        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloDon = new Banco_models_Donaciones();

        $modeloHem = new Gestion_models_Hemocomponentes();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloDeterminaciones = new Banco_models_Determinaciones();
        $modeloAnalisis = new Banco_models_Analisis();

        $catSolucionesConservadoras = (new Gestion_models_Bolsas())->fetchAll();
        $donacion = $modeloPro->getByID($id);

        $dona = $modeloDon->obtenerDonacionExtendida($donacion['iddonacion']);

        //$dona = $modeloDon->getByID($donacion['iddonacion']);
        $hem = $modeloHem->getDias($donacion['idhemocomponente']);
        $hemo = $modeloHem->getTempAndDesc($donacion['idhemocomponente']);


        
        $determinaciones = $modeloDeterminaciones->getDetermiancionesByDonId($dona['don_id']);
        $determinacionAnalisis = [];

        foreach ($determinaciones as $determinacion) {
            $determinacionAnalisis[] = [
                'cod' => $determinacion['cod_ana'],
                'descripcion' => $modeloAnalisis->getDescripcionByCodLab($determinacion['cod_ana'])['descripcion'],
                'resultado' => ($determinacion['resultado'] !== NULL) ? $determinacion['resultado'] : 'ERROR'
            ];
        }

        $serologia = $modeloSer->getDatosDonacion($donacion['iddonacion']);

        $inmuno = $modeloInmuno->getDatosDonacion($donacion['iddonacion']);

        $x = 385;
        $y = 385;

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page($x, $y);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $font_bold = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $encoding = 'UTF-8';
		
		// rectangulo determinaciones
        //$page->drawRectangle(15, 375, 375, 15, Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        
        $hemocomponente_nombre = $hemo['hem_descripcion'];
        $fecha_extraccion = date('d-m-Y', strtotime($dona['don_fh_extraccion']));
        $fecha_vencimiento = date('d-m-Y', strtotime($donacion['ffin_prod'] . '+' . $hem['hem_dias_aptitud'] . ' days'));
   
        if ($inmuno['factor'] == '+') {
            $inmuno_factor = "POS";
        } else {
            $inmuno_factor = "NEG";
        }

        $page->setFont($font, 8);


        /** INICIO GRUPO FACTOR * */
        $page->setFont($font, 42);
        $page->drawText($inmuno['grupo'], 250, $y - 60, $encoding);
        $page->setFont($font, 22);
		$page->setFont($font_bold, 26);
        $page->drawText($inmuno_factor, 315, $y - 55, $encoding);
        /** FIN GRUPO FACTOR * */
        
        /** ROTULADO DE CELDAS * */
        $page->setFont($font, 12);
        $page->drawText('COD. DE DONACION', 30, 360, $encoding);
        
       
	    $page->setFont($font, 20);
	    $page->drawText($hemocomponente_nombre, 30, 280, $encoding);
        #$page->drawText('Tipo de Hemocomponente:', 30, 300, $encoding);
        
        
//        $page->setFont($font, 10);
//        $page->drawText('Codigo de Donación', 30, 300, $encoding);

        

        /** INICIO LAYOUT DEL ETIQUETÓN * */
		// -- RECTANGULO LATERAL DERECHO
		
        // -- $page->drawRectangle(240, 315, 375, 375, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO GRUPO FACTOR
        //$page->drawRectangle(240, 270, 375, 315, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO FECHA EXTRACCION
        //$page->drawRectangle(240, 225, 375, 270, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO FECHA VENCIMIENTO
        

		// RECTANGULO SUPERIOR INMUNOLOGIA
		$page->drawRectangle(240, 150, 380, 275, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO INMUNOLOGIA

		// RECTANGULO INFERIOR INMUNOLOGIA
        $page->drawRectangle(240, 150, 380, 225, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO INMUNOLOGIA

		
		// -- RECTANGULO LATERAL IZQUIERDO
        // -- $page->drawRectangle(15, 315, 240, 375, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO ID HEMOCOMPONENTE
        //$page->drawRectangle(15, 270, 240, 315, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO TIPO HEMOCOMPONENTE
        //$page->drawRectangle(15, 225, 240, 270, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO CODIGO DE DONACION
        // -- $page->drawRectangle(15, 160, 240, 225, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO DATOS BOLSA






        /** INICIO DATOS BOLSA * */
        $page->setFont($font, 10);
/*         $page->drawText("Temp. almacenamiento: ", 30, 215, $encoding);
        $page->drawText($hemo['hem_temp'] . ' ºC', 150, 215, $encoding);

        $page->drawText("Volumen: ", 30, 205, $encoding);
        $page->drawText($donacion['peso'] . ' ml.', 150, 205, $encoding); */

        $page->drawText("Tubuladura: ", 10, 215, $encoding);
        $page->drawText($dona['nroTubuladura'], 130, 215, $encoding);

        $page->drawText("Cod. Autogenerado: ", 10, 205, $encoding);
        $page->drawText('MGQ1099912', 130, 205, $encoding);

        $page->drawText("Extracción: ", 10, 185, $encoding);
        $page->drawText($fecha_extraccion, 130, 185, $encoding);
		
        $page->drawText("Vencimiento: ", 10, 175, $encoding);
        $page->drawText($fecha_vencimiento, 130, 175, $encoding);		

/*         $page->drawText("Anticoagulante: ", 30, 175, $encoding);
        $page->drawText($solucion, 150, 175, $encoding); */

        $page->drawText("Venc. descartable: ", 10, 155, $encoding);
        $page->drawText(date('d-m-Y', strtotime($dona['fecha_vencimiento_bolsa'])), 130, 155, $encoding);

        /** FIN DATOS BOLSA * */
		
		
		
        /** RECTANGULO DERECHO INICIO DATOS INMUNOLOGIA * */
		
		// INMUNOLOGIA SUPERIOR
        $page->drawText("Volumen: ", 245, 265, $encoding);
        $page->drawText($donacion['peso'] . ' ml.', 335, 265, $encoding);

        $solucion = null;
        foreach ($catSolucionesConservadoras as $solucionConservadora) {
            if ($solucionConservadora['id'] == $dona['bolsas_id']) {
                $solucion = $solucionConservadora['tipo_bolsa'];
            }
        }
		
        $page->drawText("Anticoagulante: ", 245, 255, $encoding);
        $page->drawText($solucion, 335, 255, $encoding);

        $page->drawText("T. Almacenamiento: ", 245, 245, $encoding);
        $page->drawText($hemo['hem_temp'] . ' ºC', 335, 245, $encoding); 


		// INMUNOLOGIA INFERIOR
        $page->drawText("Acs. Irreg: ", 245, 215, $encoding);
        $page->drawText($inmuno['rastreo'], 335, 215, $encoding);

        switch ($inmuno['cde']) {
            case '+':$cde = 'POS';
                break;
            case '-':$cde = 'NEG';
                break;
            default:$cde = 'SIN DATO';
                break;
        }
        $page->drawText("CDE: ", 245, 205, $encoding);
        $page->drawText($cde, 335, 205, $encoding);

        $du = ($inmuno['du'] == '+') ? 'POS' : 'NEG';
        $page->drawText("D: ", 245, 195, $encoding);
        $page->drawText($du, 335, 195, $encoding);

        switch ($inmuno['kell']) {
            case '+':$kell = 'POS';
                break;
            case '-':$kell = 'NEG';
                break;
            default:$kell = 'SIN DATO';
                break;
        }
        $page->drawText("Sistema Kell: ", 245, 185, $encoding);
        $page->drawText($kell, 335, 185, $encoding);

        /** RECTANGULO DERECHO FIN DATOS INMUNOLOGIA * */
        
        
		
		
		
/*         $page->setFont($font, 8);
        $page->drawText("Extracción:", 30, 235, $encoding);
        $page->drawText($fecha_extraccion, 80, 235, $encoding);        

        $page->drawText("Vencimiento:", 265, 235, $encoding);
        $page->drawText($fecha_vencimiento, 320, 235, $encoding); */

        
     
		// RECTANGULO INFERIOR DETERMINACIONES
        $page->drawRectangle(5, 145, 380, 30, Zend_Pdf_Page::SHAPE_DRAW_STROKE); //RECTANGULO INMUNOLOGIA   
        
        /** INICIO RESULTADOS SEROLOGICOS * */
        $page->drawText('TAMIZAJE DE INFECCIONES TRANSMISIBLES POR TRANSFUSION', 20, 130, $encoding);

        if ($serologia != null) {
            $posicion = 80;
            $tot_determiacinoes = sizeof($determinacionAnalisis);
            $count = 0;
            $col = 1;
            foreach ($determinacionAnalisis as $resultado) {
                $tab = ($count < floor($tot_determiacinoes / 2)) ? 0 : 180;

                $page->setFont($font, 6);
                $page->drawText($resultado['descripcion'] . ":", 20 + $tab, $y - 190 - $posicion, $encoding);
                if (($resultado['resultado'] == 'REACTIVO') || ($resultado['resultado'] == 'DETECTABLE')) {
                    $page->setFont($font_bold, 6);
                    $page->drawText($resultado['resultado'], 145 + $tab, $y - 190 - $posicion, $encoding);
                } else {
                    $page->drawText($resultado['resultado'], 145 + $tab, $y - 190 - $posicion, $encoding);
                }

                $posicion = $posicion + 6;
                if ($col == 1 && $count >= floor($tot_determiacinoes / 2)) {
                    $col++;
                    $posicion = 80;
                }

                $count ++;
            }
        }
        /** FIN RESULTADOS SEROLOGICOS * */
        //Asignacion de Hoja al PDF
        $pdf->pages[] = $page;

        /** INICIO CODIGOS DE BARRA - VARIOS * */
//        $grupoFactor = $inmuno['grupo'] . "/" . $inmuno_factor;
//        Zend_Barcode::factory('code128', 'pdf', array('font' => APPLICATION_PATH . '/arial.ttf', 
//                    'text' => $grupoFactor, 
//                    'drawText' => false, 
//                    'barHeight' => 30,
//                    'fontSize' => 20, 
//                    'barThickWidth' => 3, 
//                    'barThinWidth' => 2, 
//                    'stretchText' => false
//                ), array('topOffset' => 45, 'leftOffset' => 267))->setResource($pdf)->draw();

        
		        // DONACION NUMERO
					Zend_Barcode::factory('code128', 'pdf', 
		             array('font' => APPLICATION_PATH . '/arial.ttf', 
                    'text' => $dona['don_nro'],
                    'barHeight' => 38,
                    'fontSize' => 40, 
                    'barThickWidth' => 22, 
                    'barThinWidth' => 3, 
                    'stretchText' => false, 
                    'factor' => 1.0
                ), array('topOffset' => 33, 'leftOffset' => 18 ))->setResource($pdf)->draw();
        
        
		
		
		
/*         // ID UNICO DE HEMOCOMPONENTE
        Zend_Barcode::factory('code128', 'pdf', 
		             array('font' => APPLICATION_PATH . '/arial.ttf', 
                    'text' => $donacion['idhemocomponente'],  // $donacion['id'],
                    'barHeight' => 39,      // altura del grafico de barras
                    'fontSize' => 33    , 
                    'barThickWidth' => 22,  // grosor de las barras
                    'barThinWidth' => 5,    // separacion entre barras
                    'stretchText' => false, 
                    'withQuietZones' => true, 
                    'factor' => 0.9
                ), array('topOffset' => 33, 'leftOffset' => 18 ))->setResource($pdf)->draw();
        
         */

//        Zend_Barcode::factory('code128', 'pdf', array('font' => APPLICATION_PATH . '/arial.ttf', 'text' => $dona['don_nro'], 'barHeight' => 30,
//            'fontSize' => 18, 'barThickWidth' => 12, 'barThinWidth' => 3, 'stretchText' => false, 'withQuietZones' => true, 'factor' => 0.9
//                ), array('topOffset' => 135, 'leftOffset' => 25))->setResource($pdf)->draw();


/* 
		// NUMERO DE DONACION
        Zend_Barcode::factory('code128', 'pdf', 
					array('font' => APPLICATION_PATH . '/arial.ttf', 
                    'text' => $dona['don_nro'], 
                    'barHeight' => 38,
                    'fontSize' => 46, 
                    'barThickWidth' => 22, 
                    'barThinWidth' => 4, 
                    'stretchText' => false, 
                    'factor' => 1.10
                ), array('topOffset' => 80, 'leftOffset' => 25))->setResource($pdf)->draw();        
        
		 */
		
		
		
        // ID HEMOCOMPONENTE
//        Zend_Barcode::factory('code128', 'pdf', array('font' => APPLICATION_PATH . '/arial.ttf', 'text' => $donacion['idhemocomponente'], 'barHeight' => 20,
//            'fontSize' => 14, 'barThickWidth' => 9, 'barThinWidth' => 3, 'stretchText' => false, 'factor' => 1.06
//                ), array('topOffset' => 90, 'leftOffset' => 25))->setResource($pdf)->draw();

        
        
        // FECHA EXTRACCION
//        Zend_Barcode::factory('code128', 'pdf', array('font' => APPLICATION_PATH . '/arial.ttf', 'text' => date('d-m-Y', strtotime($dona['don_fh_extraccion'])), 'barHeight' => 30,
//            'fontSize' => 16, 'barThickWidth' => 6, 'barThinWidth' => 2, 'stretchText' => false, 'factor' => 0.8
//                ), array('topOffset' => 90, 'leftOffset' => 250))->setResource($pdf)->draw();

        // FECHA VENCIMIENTO
//        Zend_Barcode::factory('code128', 'pdf', array('font' => APPLICATION_PATH . '/arial.ttf', 'text' => $vencimiento, 'barHeight' => 30,
//            'fontSize' => 22, 'barThickWidth' => 6, 'barThinWidth' => 2, 'stretchText' => false, 'factor' => 0.8
//                ), array('topOffset' => 135, 'leftOffset' => 250))->setResource($pdf)->draw();
        
        
        
        
        /** FIN CODIGOS DE BARRA - VARIOS * */
        header('Content-Type: application/pdf');
        echo $pdf->render();
    }
    
    
    
    
    

    /* Pantalla que lista para descarte de hemocomponentes */

    public function descarteAction() {
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $hem_id = $this->getRequest()->getParam('hem_id');
        $estado = $this->getRequest()->getParam('estado');

        if ($estado == 0 || is_null($estado)) {
            $estado = 0;
            $array_estado = array(2, 4, 5, 6);
        }

        if ($estado == 3)
            $array_estado = array(3);

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modelo = new Banco_models_DetallesProduccion();
        $resultados = $modelo->getParaDescarte($fecha, $this->page, $perPage, $nro, $efes_ids, $array_estado, $hem_id);

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $this->view->hem = $hem_id;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
    }

    /* Acción que descarta hemocomponentes */

    public function descartarAction() {
        $id = $this->getRequest()->getParam('id');

        $modeloPro = new Banco_models_DetallesProduccion();
        $values = array(
            'estado' => 3
        );
        $row = $modeloPro->find($id)->current()->setFromArray($values)->save();

        $this->_helper->json(array('st' => 'ok'));
    }

    /*     * *********************************************************
     *  *****     Ver formulario de envío en PDF     ************
     * ******************************************************** */

    public function verpdfenvioAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $fechaEnv = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $estado = $this->getRequest()->getParam('estado');
        $efe_dest = (int) $this->getRequest()->getParam('efe_id');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 4; /* Por defecto busca los sólo enviados */


        $modelo = new Banco_models_DetallesProduccion();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloSer = new Banco_models_Serologia();
        $modeloEnvProd = new Banco_models_EnviosProduccion();
        $modeloUsu = new Acceso_models_Usuarios();

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $perPage = 999;

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        /* Historial de envíos */
        $enviados = $modeloEnvProd->getByEstado(array(4, 5), $this->page, $perPage, $efes_ids, $nro, $fechaEnv, $efe_dest);

        if (sizeof($enviados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        if ($enviados) {
            foreach ($enviados as $i => $v) {
                if ($v->usu_env) {
                    $usu = $modeloUsu->find($v->usu_env)->current();
                    $v->usu_env = $usu['usu_usuario'];
                }
                if ($v->usu_rec) {
                    $usu = $modeloUsu->find($v->usu_rec)->current();
                    $v->usu_rec = $usu['usu_usuario'];
                }
            }
        }


        if ($estado == 0) {
            $estado_reporte = "Todos";
        } elseif ($estado == 4) {
            $estado_reporte = "Enviados";
        } elseif ($estado == 5) {
            $estado_reporte = "Recibidos";
        }


        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8'; //'ISO-8859-1'
        $y = $page->getHeight();
        $xi = 0;
        $titulo = 'Planilla de Envios - ' . $estado_reporte;
        $efector = $this->efector_activo['nomest'];
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha Reporte: ' . date("d-m-Y");

        $page
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                ->setFont($font, 14)
                ->drawText($titulo, 100, $y - 25, $encoding)
                ->setFont($font, 11)
                ->drawText($dateRPT, 100, $y - 45, $encoding)
                ->drawText($efector, 350, $y - 45, $encoding)
                ->setFont($font, 11)

                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea columna1            
                ->drawLine(90, $y - 55, 90, $y - 500) // Vertical fecha envio
                ->drawLine(225, $y - 55, 225, $y - 500) // Vertical efector envio
                ->drawLine(350, $y - 55, 350, $y - 500) // Vertical efector destino
                ->drawLine(450, $y - 55, 450, $y - 500) // Vertical fecha de recepcion
                ->drawLine(560, $y - 55, 560, $y - 500) // Vertical usuario recepecion           
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical observaciones final  
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)
                // titulos columnas                      
                ->drawText('Fecha Envio', 20, $y - 70, $encoding)
                ->drawText('Orígen', 100, $y - 70, $encoding)
                ->drawText('Destino', 250, $y - 70, $encoding)
                ->drawText('Recepción', 355, $y - 70, $encoding)
                ->drawText('Usuario Recep.', 470, $y - 70, $encoding)
                ->drawText('Observaciones', 605, $y - 70, $encoding)
        ;

        foreach ($enviados as $i => $v) {

            $modelEfec = new Acceso_models_Efectores();
            $efectores = $modelEfec->getPairs();

            $efector_envio = $efectores[$v['efe_origen']];
            $efector_recibe = $efectores[$v['efe_destino']];

            $fecha_envio = date('d-m-Y', strtotime($v['fecha_env']));

            if ($v->fecha_rec == '0000-00-00 00:00:00') {
                $fecha_recepcion = '';
            } else {
                $fecha_recepcion = date('d-m-Y', strtotime($v['fecha_rec']));
            }

            $usuario_envio = $v['usu_env'];
            $usuario_recibe = $v['usu_rec'];
            $observaciones = $v['observaciones'];


            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 8)
                        ->drawText($fecha_envio, 20, $y - 90 - $h, $encoding)
                        ->drawText($efector_envio, 100, $y - 90 - $h, $encoding)
                        ->drawText($efector_recibe, 230, $y - 90 - $h, $encoding)
                        ->drawText($fecha_recepcion, 355, $y - 90 - $h, $encoding)
                        ->drawText($usuario_recibe, 230, $y - 70 - $h, $encoding)
                        ->drawText($observaciones, 700, $y - 90 - $h, $encoding) //hacer break
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 8)
                        ->drawText($fecha_envio, 20, $y - 90 - $h, $encoding)
                        ->drawText($efector_envio, 100, $y - 90 - $h, $encoding)
                        ->drawText($efector_recibe, 230, $y - 90 - $h, $encoding)
                        ->drawText($fecha_recepcion, 355, $y - 90 - $h, $encoding)
                        ->drawText($usuario_recibe, 230, $y - 70 - $h, $encoding)
                        ->drawText($observaciones, 700, $y - 90 - $h, $encoding) //hacer break
                ;
                $h2 = $h2 + 20;
            }
        }


        $pdf->pages[] = $page;


        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

}
