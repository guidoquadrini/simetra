<?php

class Banco_PreproduccionController extends BootPoint {
    
    //Pantalla para la Gestion de la PRE Produccion
    public function indexAction() {
        $modeloHem = new Gestion_models_Hemocomponentes();
        $modeloPro = new Banco_models_DetallesProduccion();
        $modeloPre = new Banco_models_DetallesPreProduccion();

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);
        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        /* Chequear qué stock visualizar para el CRH  */
        $stock = $modeloPro->getStockListByEfe($efes_ids);
        $donaciones = $modeloPre->listDonacionesPendientes();
        $hem = $modeloHem->fetchAll();
        $dona = $modeloPre->listDonacionesPendByEfe($efes_ids);
        $filter = array();

        /* Valores de caducidad */
        $caducidades = array();
        foreach ($hem as $h) {
            $caducidades[] = $h['hem_caducidad'];
        }

        foreach ($donaciones as $don) {
            $filter[] = $don['don_id'];
        }

        /* Para mostrar en vista hemocomponentes de menos de 8hs */
        $hemocomponentes = array();
        $hemocomponentes = $modeloHem->getPairsMenores();

        /* Para mostrar en vista hemocomponentes de más de 8hs */
        $hem_sincaducidad = array();
        $hem_sincaducidad = $modeloHem->getPairsMayores();

        /* IDs de donaciones menores y mayores al tiempo de caducidad */
        $menores = array();
        $mayores = array();

        /* Hemocomponente de referencia para tiempo de caducidad */
        $referencia = 1;

        /* Comparar fechas (en segundos)
         * 28800 segundos = 60 * 60 * 8 = 8 horas
         */

        foreach ($dona as $d) {
            if ($d['tiempo'] < 28800) {
                $menores[] = $d['don_id'];
            } else {
                $mayores[] = $d['don_id'];
            }
        }

        /* Nro de donación y hemocomponentes de esa donación */
        $hemoPorNro = $modeloPre->getHemoAndNroDon();
        $this->view->hemoNro = $hemoPorNro;

        $resultadosMenores = array();
        if (count($menores) != 0) {
            $resultadosMenores = $modeloPre->getPreProduccionList($menores);
        }

        $resultadosMayores = array();
        if (count($mayores) != 0) {
            $resultadosMayores = $modeloPre->getPreProduccionList($mayores);
        }

        $this->view->resultadosMenores = $resultadosMenores;
        $this->view->resultadosMayores = $resultadosMayores;
        $this->view->stock = $stock;
        $this->view->hemocomponentes = $hemocomponentes;
        //tomo de referencia un hemocomponente
        $this->view->caducidades = $caducidades[$referencia];
        $this->view->donacionesMayores = $mayores;
        $this->view->donacionesMenores = $menores;
        $this->view->hem_sincaducidad = $hem_sincaducidad;
    }

    //Nueva acción para donaciones con tiempo menor a -
    //Produce hemocomponentes PF(1), PLT(2) y CGR(3) -- si disponibles
    public function producirMenoresAction() {
        $filtro = $this->getRequest()->getParam('donacionesMenores');


        if ($filtro == '') {
            $this->_helper->json(array('st' => 'nok'));
        } else {
            $modeloPre = new Banco_models_DetallesPreProduccion();
            $donaciones = $modeloPre->listDonacionesAProducir($filtro);

            $modeloPro = new Banco_models_DetallesProduccion();

            $values = array('estado' => 2,
                'timestamp' => new Zend_Db_Expr('NOW()'),
                'usuario_id' => $this->auth->getIdentity()->usu_id);

            foreach ($donaciones as $don) {

                $don->setFromArray($values)->save();

                if ($don->hem_id >= 1 && $don->hem_id <= 3) {
                    $prod_id = $modeloPro->createRow(array(
                                'iddonacion' => $don->don_id,
                                'idhemocomponente' => $don->hem_id,
                                'fini_prod' => new Zend_Db_Expr('NOW()'),
                                'estado' => 1
                            ))->save();
                }
            }

            $this->_helper->json(array('st' => 'ok'));
        }
    }

    //Nueva acción para donaciones con tiempo mayor a -
    //Produce CGR(3) y PN(4) -- si disponibles
    public function producirMayoresAction() {
        $filtro = $this->getRequest()->getParam('donacionesMayores');

        if ($filtro == '') {
            $this->_helper->json(array('st' => 'nok'));
        } else {
            $modeloPre = new Banco_models_DetallesPreProduccion();
            $donaciones = $modeloPre->listDonacionesAProducir($filtro);

            $modeloPro = new Banco_models_DetallesProduccion();

            $values = array('estado' => 2,
                'timestamp' => new Zend_Db_Expr('NOW()'),
                'usuario_id' => $this->auth->getIdentity()->usu_id);

            foreach ($donaciones as $don) {

                $don->setFromArray($values)->save();
                if ($don->hem_id >= 3 && $don->hem_id <= 4) {
                    $prod_id = $modeloPro->createRow(array(
                                'iddonacion' => $don->don_id,
                                'idhemocomponente' => $don->hem_id,
                                'fini_prod' => new Zend_Db_Expr('NOW()'),
                                'estado' => 1
                            ))->save();
                }
            }

            $this->_helper->json(array('st' => 'ok'));
        }
    }

    /* Producir CGR(3), CRIO(5) y PM(6) */
    public function producirSimulacionAction() {
        $donNros = $this->getRequest()->getParam('simulacion');

        if ($donNros == '') {
            $this->_helper->json(array('st' => 'nok'));
        } else {

            $modeloPre = new Banco_models_DetallesPreProduccion();
            /* En la 'simulacion' están los nro_don 
             * A partir de eso, obtengo los don_id
             * */
            $don = $modeloPre->getHemoAndNroDon();
            $filtro = array();
            foreach ($don as $i => $d) {
                if (in_array($d['don_nro'], $donNros)) {
                    $filtro[] = $d['don_id'];
                }
            }

            $donaciones = $modeloPre->listDonacionesAProducir($filtro);

            $modeloPro = new Banco_models_DetallesProduccion();

            $values = array('estado' => 2,
                'timestamp' => new Zend_Db_Expr('NOW()'),
                'usuario_id' => $this->auth->getIdentity()->usu_id);

            foreach ($donaciones as $don) {
                $don->setFromArray($values)->save();
            }


            /* Si la simulación es siempre de la misma forma, se crean:
             * CGR
             * CRIO
             * PM
             * */

            foreach ($filtro as $id) {
                /* CGR */
                $prod_id = $modeloPro->createRow(array(
                            'iddonacion' => $id,
                            'idhemocomponente' => 3,
                            'fini_prod' => new Zend_Db_Expr('NOW()'),
                            'estado' => 1
                        ))->save();

                /* CRIO */
                $prod_id = $modeloPro->createRow(array(
                            'iddonacion' => $id,
                            'idhemocomponente' => 5,
                            'fini_prod' => new Zend_Db_Expr('NOW()'),
                            'estado' => 1
                        ))->save();

                /* PM */
                $prod_id = $modeloPro->createRow(array(
                            'iddonacion' => $id,
                            'idhemocomponente' => 6,
                            'fini_prod' => new Zend_Db_Expr('NOW()'),
                            'estado' => 1
                        ))->save();
            }
            $this->_helper->json(array('st' => 'ok'));
        }
    }

}
