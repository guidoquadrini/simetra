<?php

class BootPoint extends Zend_Controller_Action {

    public $pryId; // = 3; //banco

    public function init() {

        $registry = Zend_Registry::getInstance();
        $this->pryId = $registry->config->pry_id;

        $this->auth = Zend_Auth::getInstance();
        if (!$this->auth->hasIdentity()) {
            return;
        }

        $this->initCache();

        $this->page = (int) $this->getRequest()->getParam('pagina');
        if ($this->page < 1)
            $this->page = 1;

        $acl = $this->view->navigation()->getAcl();
        if (!empty($this->auth->getIdentity()->efector_activo)) {
            $this->efector_activo = $this->auth->getIdentity()->efector_activo;

            $modeloParam = new Efector_models_EfectoresParametros();

            $param = $modeloParam->find($this->efector_activo['id_efector']);
            if (count($param) > 0)
                $this->efector_activo_cod = $param->current()->nombre_corto;
            else
                $this->efector_activo_cod = "---";

            $this->view->efector_activo = $this->auth->getIdentity()->efector_activo;
            $this->view->efectores_activos = $this->auth->getIdentity()->efectores;
            if (!$acl->has('banco')) {
                $acl->addResource('banco');
            }
            //$acl->deny($acl->rol,'banco');
        } else {
            if (!$acl->has('efector')) {
                $acl->addResource('efector');
            }
            $acl->deny($acl->rol, 'efector');
        }
        $this->view->usuario = $this->auth->getIdentity();

        $this->setAclPages($acl);
        $this->settings = $this->getInvokeArg('bootstrap')->getResource('settings');
    }

    /**
     * Cacheo de variables
     *
     */
    private function initCache() {
    $cache = $this->getInvokeArg('bootstrap')->getResource('cache');
    if (!$config = $cache->load('mainconfig')) {
//          $cliente = new Zend_Rest_Client("http://{$_SERVER['SERVER_NAME']}".$this->view->baseUrl()."/rest/sistema/index/");
//          $efectores = $cliente->efectoreshttp()->get();
//          $nodos = $cliente->nodos()->get();


        $modelCat = new Stock_models_Productoscategorias();

        $modelAfe = new Gestion_models_Aferesis();
        $modelAna = new Gestion_models_Analisis();
        $modelDon = new Gestion_models_Donaciones();
        $modelMot = new Gestion_models_Motivos();
        $modelPer = new Gestion_models_PerfilesProduccion();
        $modelTipoDon = new Gestion_models_Tipodonacion();


        $config = array(
            'aferesis' => $modelAfe->getPairs(),
            'analisis' => $modelAna->getPairs(),
            'donaciones' => $modelDon->getPairs(),
            'perfilesprod' => $modelPer->getPairs(),
            'diferidos' => $modelMot->getPairs('DIF'),
            'rechazos' => $modelMot->getPairs('REC'),
            'descarte' => $modelMot->getPairs('DES')
        );
        $cache->save($config, 'mainconfig');
    }

    if (!$ocupaciones = $cache->load('ocupaciones')) {
        $modelOcu = new Gestion_models_Ocupaciones();
        $ocupaciones = $modelOcu->getPairs();
        $cache->save($ocupaciones, 'ocupaciones');
    }

    if (!$tipodonacion = $cache->load('tipodonacion')) {
        $modelTipoDonacion = new Gestion_models_Tipodonacion();
        $tipodonacion = $modelTipoDonacion->getPairs();
        $cache->save($tipodonacion, 'tipodonacion');
    }

    if (!$tipo_donacion = $cache->load('tipo_donacion')) {
        $modelTipoDonacion = new Gestion_models_Tipodonacion();
        $tipo_donacion = $modelTipoDonacion->getTipoDonacionFiltrado();
        $cache->save($tipo_donacion, 'tipo_donacion');
    }

    if (!$efectores = $cache->load('efectores')) {
        $modelEfec = new Acceso_models_Efectores();
        $efectores = $modelEfec->getPairs();
        $cache->save($efectores, 'efectores');
    }

    if (!$provincias = $cache->load('provincias')) {
        $modelProvincias = new Efector_models_Sicap();
        $provincias = $modelProvincias->getProvincias();
        $cache->save($provincias, 'provincias');
    }

    if (!$clasificacion_donante = $cache->load('clasificacion_donante')) {
        $modelClasificacionDonante = new Gestion_models_Clasificaciondonante();
        $clasificacion_donante = $modelClasificacionDonante->getClasificacionDonanteFiltrado();
        $cache->save($clasificacion_donante, 'clasificacion_donante');
    }

    if (!$tdo_tipos_donaciones = $cache->load('tdo_tips_donaciones')) {
        $modelTdoTiposDonaciones = new Gestion_models_TdoTiposDonaciones();
        $tdo_tipos_donaciones = $modelTdoTiposDonaciones->getPairs();
        $cache->save($tdo_tipos_donaciones, 'tdo_tips_donaciones');
    }



    if (!$LocalidadesSantaFe = $cache->load('localidades_santafe')) {

        $modeloEfectorSICAP = new Efector_models_Sicap();
            $catLocalidades = $modeloEfectorSICAP->getLocalidades('82'); //Localidades de Santa Fe
            $LocalidadesSantaFe = [];
            foreach ($catLocalidades as $localidad) {
                $LocalidadesSantaFe[$localidad['claveloc']] = utf8_encode($localidad['nomloc']);
            }
            $LocalidadesSantaFe['99999'] = "OTRA LOCALIDAD";
            $cache->save($LocalidadesSantaFe, 'localidades_santafe');
        }
        if (!$paises_pair = $cache->load('paises')) {
            $cat_paises = (array) (new Efector_models_Paises())->getAll();
            $id_pais = array_column($cat_paises, 'pais_iso3');
            $nom_pais = array_map('utf8_encode', array_column($cat_paises, 'pais_nombre'));
            $paises_pair = array_combine($id_pais, array_map('utf8_encode', $nom_pais));
            $cache->save($paises_pair, 'paises');
        }
        
        if (!$motivos_descartes = $cache->load('motivos_descartes')) {
            $modelo_descartes = new Gestion_models_Motivosdescartes();
            $motivos_descartes = $modelo_descartes->getPairs();
            $cache->save($motivos_descartes, 'descartes');
        }
        

        $this->view->cache_paises = $paises_pair;
        $this->view->cache_tipo_donacion = $tipo_donacion;
        $this->view->cache_tdo_tipos_donaciones = $tdo_tipos_donaciones;
        $this->view->cache_clasificacion_donante = $clasificacion_donante;
        $this->view->cache_provincias = $provincias;
        $this->view->cache_localidades_santafe = $LocalidadesSantaFe;
        $this->view->cache_efectores = $efectores;
        $this->view->cache_aferesis = $config['aferesis'];
        $this->view->cache_analisis = $config['analisis'];
        $this->view->cache_donaciones = $config['donaciones'];
        $this->view->cache_diferidos = $config['diferidos'];
        $this->view->cache_rechazos = $config['rechazos'];
        $this->view->cache_descartes = $motivos_descartes; // $config['descarte'];
        $this->view->cache_perfilesprod = $config['perfilesprod'];
        $this->view->cache_ocupaciones = $ocupaciones;

        $this->view->cache_estados_motivos = array(
            '0' => 'Inactivo',
            '1' => 'Activo'
        );

        $this->view->cache_grupos_sa = array(
            'AB+' => 'AB+',
            'AB-' => 'AB-',
            'A+' => 'A+',
            'A-' => 'A-',
            'B+' => 'B+',
            'B-' => 'B-',
            'O+' => 'O+',
            'O-' => 'O-',
        );

        $this->view->cache_meses = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );

        $this->view->cache_dias = array(
            '01' => 'Domingo',
            '02' => 'Lunes',
            '03' => 'Martes',
            '04' => 'Miercoles',
            '05' => 'Jueves',
            '06' => 'Viernes',
            '07' => 'Sabado'
        );
        $this->view->estados_donaciones = array(
            1 => 'Alta y Pendiente Entrevista',
            2 => 'Aceptar Para Extraccion',
            3 => 'Diferir Donante',
            4 => 'Rechazar Donante',
            5 => 'Pasar a produccion',
            6 => 'Rechazar Produccion',
            7 => 'Aceptar Proceso',
            8 => 'No Aceptar - Descarte definitivo',
            100 => 'Cancelacion Administrativa',
            101 => 'Rechazo Admnistrativo',
            102 => 'Eliminacion Administrativa',
            104 => 'Pendiente Examen Medico',
            105 => 'Pendiente Envio',
            106 => 'Pendiente Recepcion'
        );

//        $this->view->estados_donaciones = array(
//            1 => 'Finaliza',
//            3 => 'Anulada',
//            4 => 'Extracción',
//            5 => 'pasar a prod',
//            6 => 'rechazar prod',
//            7 => 'aceptar proceso',
//            8 => 'Descarte definitivo',
//            9 => 'Enviada',
//            10 => 'Aceptar recepción',
//            11 => 'Descarte parcial'
//        );
        $this->view->estados_solicitudes = array(
            0 => 'Cerrada sin problemas - Anulada', //'Anulada'
            1 => 'Aceptada', //Creada
            2 => 'Diferida',
            3 => 'Rechazada',
            4 => 'Extracción', //Pendiente Extraccion
            5 => 'Enviada', //Pasa a Donacion.?????
            6 => 'Interrumpida'
        );

        /* Para tabla don_donaciones */
        $this->view->estados_produccion = array(
            0 => 'Enviada',
            1 => 'Recibida',
            2 => 'Descartada parcialmente',
            3 => 'Descartada'
        );

        /* Para tabla detalles_produccion */
        $this->view->estados_seguimiento_prod = array(
            0 => 'No definido',
            1 => 'Pendiente',
            2 => 'Producido',
            3 => 'Descartado',
            4 => 'Enviado',
            5 => 'Recibido'
        );

        /* Para tabla detalles_preproduccion */
        $this->view->estado_preproduccion = array(
            1 => 'Pendiente',
            2 => 'Aceptada'
        );

        $this->view->cache_generos = array(
            'A' => 'Ambos',
            'F' => 'Femenino',
            'M' => 'Masculino',
        );
        
        //TODO: Eliminar esta cache. Se levanta al momneto de utilizarse. La busqueda no marco usos. Volver a revisar y eliminar.
        $this->view->cache_serologia = array(
            'id' => 'ID Serología',
            'don_id' => 'ID Donante',
            'sifilis' => 'Sífilis',
            'brucelosis' => 'Brucelosis',
            'chagas' => 'Chagas',
            'hepatitisB' => 'Hepatitis B',
            'hepatitisC' => 'Hepatitis C',
            'hiv' => 'HIV',
            'htlv' => 'HTLV',
            'timestamp' => 'Fecha de registro',
            'usuario_id' => 'Usuario'
        );

        /* Para tabla envios_produccion  */
        $this->view->cache_est_env_prod = array(
            1 => 'Validado para envío',
            4 => 'Enviado a efector'
        );

        $this->view->cache_env_prod = array(
            'env_prod_id' => 'ID Envío',
            'prod_id' => 'ID Producción',
            'efe_origen' => 'Efector origen',
            'efe_destino' => 'Efector destino',
            'otro_destino' => 'Otro destino',
            'fecha_env' => 'Fecha envío',
            'fecha_rec' => 'Fecha recepción',
            'usu_env' => 'Usuario envío',
            'usu_rec' => 'Usuario recepción',
            'estado' => 'Estado',
            'observaciones' => 'Observaciones'
        );

        /** Cache de hemocomponentes deprecada **/
        $this->view->cache_hemo = array(
            1 => 'Plasma fresco',
            2 => 'Plaquetas',
            3 => 'Concentrado glóbulos rojos',
            4 => 'Plasma normal',
            5 => 'Crioprecipitado',
            6 => 'Plasma modificado'
        );

        $this->view->cache_efectores_reducido = array(
            663 => 'HOSP DR JUAN BAUTISTA ALBERDI',
            659 => 'HOSP INTENDENTE GABRIEL CARRASCO',
            662 => 'MATERNIDAD MARTIN',
            657 => 'HOSP DE NIÑOS VILELA',
            658 => 'HOSP DE EMERGENCIAS DR CLEMENTE ALVAREZ',
            1249 => 'HOSPITAL DR. ROQUE SAENZ PE�A',
            181 => 'HOSP DE NIÑOS ZONA NORTE',
            167 => 'HOSP EVA PERON',
            313 => 'SAMCO GRANADEROS A CABALLO',
            434 => 'HOSP SAN CARLOS',
            522 => 'SAMCO GENERAL SAN MARTIN - FIRMAT',
            331 => 'SAMCO SAN JORGE',
            323 => 'SAMCO EL TREBOL',
            183 => 'HOSP PROVINCIAL CENTENARIO',
            184 => 'HOSP PROVINCIAL'
        );

        $this->view->cache_tipo_doc = array(
//            0 => 'DNR',
            1 => 'DNI',
//            2 => 'LC',
//            3 => 'LE',
//            4 => 'CI',
//            5 => 'OTR',
            6 => 'PAS',
//            7 => IND
        );
    }

    public function getTipoDocumentoPorNombre($tipo_documento) {
        foreach ($this->view->cache_tipo_doc as $ID => $NOMBRE) {
            if ($NOMBRE === $tipo_documento) {
                return $ID;
            }
        }
        return ($tipo_documento === NULL) ? 1 : $tipo_documento;
    }

    /**
     * ACL y Paginas disponibles
     *
     */
    private function setAclPages($acl) {
        $paginas = $this->view->navigation()->getContainer()->toArray();
        foreach ($paginas as $i => $page) {
            if (!empty($page['pages'])) {
                foreach ($page['pages'] as $a => $b) {
                    if ($b['type'] != 'Zend_Navigation_Page_Mvc') {
                        continue;
                    }
                    if ($acl->has($b['module'])) {
                        if ($acl->isAllowed($acl->rol, $b['module']))
                            continue;
                        else {
                            continue;
                        }
                    }
                    if ($acl->has($b['module'] . ':' . $b['controller'])) {
                        if (is_null($b['action']))
                            $b['action'] = 'index';
                        if ($acl->isAllowed($acl->rol, $b['module'] . ':' . $b['controller'], $b['action']))
                            continue;
                        else {
                            continue;
                        }
                    }
                    unset($paginas[$i]['pages'][$a]);
                }
            }
            if ($page['type'] != 'Zend_Navigation_Page_Mvc') {
                if ($page['class'] == 'dropdown' && empty($paginas[$i]['pages'])) {
                    unset($paginas[$i]);
                }
                continue;
            }
            if ($acl->has($page['module'])) {
                if ($acl->isAllowed($acl->rol, $page['module']))
                    continue;
                else {
                    continue;
                }
            }
            if ($acl->has($page['module'] . ':' . $page['controller'])) {
                if ($acl->isAllowed($acl->rol, $page['module'] . ':' . $page['controller']))
                    continue;
                else {
                    continue;
                }
            }
            unset($paginas[$i]);
        }
        $this->view->navigation()->getContainer()->setPages($paginas);
    }

    public function ultimaSQL() {
        return Zend_Db_Table_Abstract::getDefaultAdapter()->getProfiler()->getLastQueryProfile()->getQuery();
    }
}
    
    if (! function_exists('array_column')) {
        function array_column(array $input, $columnKey, $indexKey = null) {
            $array = array();
            foreach ($input as $value) {
                if ( !array_key_exists($columnKey, $value)) {
                    trigger_error("Key \"$columnKey\" does not exist in array");
                    return false;
                }
                if (is_null($indexKey)) {
                    $array[] = $value[$columnKey];
                }
                else {
                    if ( !array_key_exists($indexKey, $value)) {
                        trigger_error("Key \"$indexKey\" does not exist in array");
                        return false;
                    }
                    if ( ! is_scalar($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not contain scalar value");
                        return false;
                    }
                    $array[$value[$indexKey]] = $value[$columnKey];
                }
            }
            return $array;
        }
    }
    

