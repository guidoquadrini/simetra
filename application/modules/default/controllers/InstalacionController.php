<?php

class InstalacionController extends BootPoint
{

	public function indexAction()
    {
		$success = array();
		$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/db.ini",APPLICATION_ENV);
		$settings = new Zend_Config(require APPLICATION_PATH . "/configs/settings.php",APPLICATION_ENV);

		$params = array(
			'host' => $config->host,
			'username' => $config->username,
			'password' => $config->password,
			'dbname'   => 'dsalud_front',
			'charset'  => 'utf8',
			'profiler' => false
		);
		$db = Zend_Db::factory('Mysqli',$params);

		/**
		* buscar proyecto en tabla
		* crear registro
		*/
		$proyecto = $db->fetchOne($db->select()->from('proyectos','pry_id')->where('pry_id=?',$this->pryId));
		if(!$proyecto){
			$db->insert('proyectos',array(
				'pry_id' => $this->pryId,
				'pry_nombre' => 'Banco de Sangre',
				'pry_version' => '1.0',
				'pry_url' => $this->view->url(array('controller'=>'')),
				'pry_activo' => 1,
				'pry_actualizacion' => new Zend_Db_Expr('NOW()'),
				'codename' => 'bcosangr'
			));
			$proyecto = $db->lastInsertId();
			$success[] = "Creando Proyecto <b>bcosangre</b>";
		}

		/**
		* buscar usuario Administrador en tabla
		* crear registro para este proyecto
		*/
		$usuario = $db->fetchOne($db->select()->from('usuarios','usu_id')->where('usu_usuario=?','adminbanco'));
		if(!$usuario){
			$db->insert('usuarios',array(
				'usu_usuario' => 'adminbanco',
				'usu_nombre' => 'Administrador',
				'usu_clave' => md5('admin'),
				'usu_activo' => 1
			));
			$usuario = $db->lastInsertId();
			$success[] = "Creando Usuario <b>adminbanco</b>";
		}

		$usuarioProyecto = $db->fetchOne($db->select()->from('usuarios_proyectos','upy_id')
			->where('usu_id=?',$usuario)
			->where('pry_id=?',$this->pryId));
		if(!$usuarioProyecto){
			$db->insert('usuarios_proyectos',array(
				'pry_id' => $this->pryId,
				'usu_id' => $usuario,
				'pry_activo' => 1,
				'pry_actualizacion' => new Zend_Db_Expr('NOW()'),
			));
			$usuarioProyecto = $db->lastInsertId();
			$success[] = "Asignando Usuario a Proyecto";
		}

		/**
		* buscar roles de este proyecto
		* crearlos
		*/
		$rol = $db->fetchOne($db->select()->from('roles','rol_id')
			->where('rol_nombre=?','Administrador')
			->where('pry_id=?',$proyecto));
		if(!$rol){
			$db->insert('roles',array(
				'rol_nombre' => 'Administrador',
				'pry_id' => $this->pryId,
                'rol_activo' => 1
			));
			$rol = $db->lastInsertId();
			$success[] = "Creando Rol <b>Administrador</b>";
		}

        $usuarioRol = $db->fetchOne($db->select()->from('usuarios_roles','rol_id')
            ->where('usu_id=?',$usuario)
            ->where('rol_id=?',$rol));
        if(!$usuarioRol){
            $db->insert('usuarios_roles',array(
                'usu_id' => $usuario,
                'rol_id' => $rol,
                'usr_activo' => 1,
            ));
            $usuarioRol = $db->lastInsertId();
            $success[] = "Asignando Rol a Usuario";
        }

		/**
		* buscar recursos de este proyecto
		* crearlos
		*/
		$controllers = $this->getInvokeArg('bootstrap')->getResource('front')->getControllerDirectory();
		$modules = array_keys($controllers);
		foreach($modules as $v){
			if(!in_array($v,array('default','rest'))){
				$db->update('roles_recursos',array('rec_activo' => 0),
					"pry_id={$this->pryId} AND rol_id={$rol} AND module='{$v}' AND (controller!='' OR action!='')");

				$sql = "INSERT INTO roles_recursos (pry_id,rol_id,module,controller,action,rec_activo) "
				. "VALUES ({$this->pryId},{$rol},'{$v}','','',1) ON DUPLICATE KEY UPDATE rec_activo=1";
				$db->query($sql);
			}
		}
		$success[] = "Recursos asignados a Rol";

		$this->view->mensajes = $success;
	}

	public function confirmar()
    {

	}
}
