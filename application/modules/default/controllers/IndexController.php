<?php

class IndexController extends BootPoint {

    public function indexAction() {
        //cargar info de efector y mostrarla
        if ($this->auth->getIdentity()->efector != '') {
            
        }
        $this->view->version = $this->getInvokeArg('bootstrap')->getResource('settings');
    }

    public function logoutAction() {
        $this->auth->clearIdentity();
        $this->_redirect('/');
    }

    public function loginAction() {
        if ($this->auth->hasIdentity()) {
            $this->_redirect('/');
        }
        $form = new default_forms_inicio();
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            if ($form->isValid($post)) {
                $values = $form->getValues();

                $adapter = $this->getInvokeArg('bootstrap')->getResource('auth');
                $adapter->setIdentity($values['username'])->setCredential($values['password']);

                $autenticar = $this->auth->authenticate($adapter);
                if ($autenticar->isValid()) {
                    /*
                      $cliente = new Zend_Rest_Client($this->view->serverUrl().$this->view->baseUrl()."/rest/usuarios/usuario");
                      $get = $cliente->auth($values['username'],$values['password'])->get();
                      $obj = json_decode(json_encode($get->auth)); #fix Xml2Obj
                      if(is_null($obj->response)){
                      $usu_id = (int)$obj->usuario->usu_id;
                      chequear si esta en el proyecto * REST
                     */

                    $resultado = $adapter->getResultRowObject(null, 'usu_password');
                    $usu_id = $resultado->usu_id;

                    $model = new Acceso_models_Usuarios();
                    $pertenece = $model->getPertenece($usu_id, $this->pryId);
                    if (!$pertenece) {
                        $this->auth->clearIdentity();
                        $form->username->addErrorMessage('Este Usuario no esta asignado al proyecto')->markAsError();
                    } else {
                        $roles = $model->getRoles($usu_id, $this->pryId);
                        if (empty($roles)) {
                            $this->auth->clearIdentity();
                            $form->username->addErrorMessage('Este Usuario no tiene Roles asociados')->markAsError();
                        } else {
                            $cache = $this->getInvokeArg('bootstrap')->getResource('cache');
                            $cache->remove('acl_' . $usu_id);

                            $resultado->roles = array_keys($roles);

                            /*                             * * Efectores ** */
                            $efectores = $model->getEfectores($usu_id, $this->pryId);
                            if (!empty($efectores)) {
                                $resultado->efectores = $efectores;
                                foreach ($efectores as $v) {
                                    //armar array de efectores
                                    $resultado->efector_activo = $v;
                                    if ($v['efe_defecto'] == 1) {
                                        $resultado->efector_activo = $v;
                                        break;
                                    }
                                }
                            }
                            $this->auth->getStorage()->write($resultado);
                            $this->_helper->redirector($this->_request->action, $this->_request->controller, $this->_request->module);
                        }
                    }
                }
                $form->username->markAsError();
            }
        }

        $this->view->form = $form;
    }

    public function cambiarEfectorActivoAction() {
        //Parametros:usu_id
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Obtengo el ID de usuario del request.
        $usu_id = $this->getRequest()->getParam('usu_id');

        $adapter = $this->getInvokeArg('bootstrap')->getResource('auth');
        $adapter->setIdentity($values['username'])->setCredential($values['password']);
        $modelUsuarios = new Acceso_models_Usuarios();
        $resultado = (object) $modelUsuarios->fetchRow('usu_id= ' . $usu_id)->toArray();

        #$autenticar = $this->auth->authenticate($adapter);
        #$resultado = $adapter->getResultRowObject(null, 'usu_password');
        $usu_id = $resultado->usu_id;

        //$model = new Acceso_models_Usuarios();
        //$pertenece = $model->getPertenece($usu_id, $this->pryId);
        $roles = $modelUsuarios->getRoles($usu_id, $this->pryId);


        $cache = $this->getInvokeArg('bootstrap')->getResource('cache');
        $cache->remove('acl_' . $usu_id);
        $resultado->roles = array_keys($roles);


        $efectores = $modelUsuarios->getEfectores($usu_id, $this->pryId);
        if (!empty($efectores)) {
            $resultado->efectores = $efectores;
            foreach ($efectores as $v) {
                //armar array de efectores
                $resultado->efector_activo = $v;
                if ($v['efe_defecto'] == 1) {
                    $resultado->efector_activo = $v;
                    break;
                }
            }
            $this->auth->getStorage()->write($resultado);
        }
    }

    public function sinPermisoAction() {
        if ($this->getRequest()->isXmlHttpRequest())
            $this->view->layout()->disableLayout();
    }

}
