<?php

class Zend_View_Helper_Unir extends Zend_View_Helper_Abstract {

	/**
	* Une valores como 2,3,4 dado un array de indice,valor
	*
	*/

	public function unir($arreglo,$valores){
		if(empty($arreglo)) return;
		if($valores == 'ALL')
			return implode(', ',array_values($arreglo));

		if(!is_array($valores))
			$valores = explode(",",$valores);
		if(empty($valores)) return;

		foreach($arreglo as $i => $v){
			if(!in_array($i,$valores))
				unset($arreglo[$i]);
		}

		return implode(", ",array_values($arreglo));
	}
}