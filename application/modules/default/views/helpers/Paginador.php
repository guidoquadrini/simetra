<?php

class Zend_View_Helper_Paginador extends Zend_View_Helper_Abstract {

	public function paginador($paginas = 1){
		if($paginas < 2)
			return;
		$front = Zend_Controller_Front::getInstance();
		$routeName = $front->getRouter()->getCurrentRouteName();
		$request = $front->getRequest();

		$page = 1;
		if($request->isGet()){
			$page = (int)$request->getParam('pagina');
			if($page < 1) $page = 1;
		}

		if($routeName == 'default')
			$baseUrl = $front->getBaseUrl().'/'.$this->getParams($request->getParams());
		else
			$baseUrl = $front->getBaseUrl().$this->view->Url(array(),$routeName, true);

		$_actual = $page;
		$retorno = '';
		$margen = 10;
		if($page > $margen && $page <= $paginas){
			$inicio = $_actual-$margen;
			$offset = $margen+$_actual;
		}
		else{
			$inicio = $_actual = 1;
			$offset = $margen*2;
		}
		for($i=$inicio;$i<$offset;$i++){
			if($i > $paginas){ break; }
			if($i == $page){ $retorno .= "<li class=\"active\"><span>{$i}</span></li>"; }
			elseif($i == 1) { $retorno .= "<li><a href=\"{$baseUrl}\">{$i}</a></li>"; }
			else { $retorno .= "<li><a href=\"{$baseUrl}/pagina/{$i}\">{$i}</a></li>"; }
		}

		if($page > 1)
			$s = "<li><a href=\"{$baseUrl}/pagina/".($page-1)."\">&laquo; Anterior</a></li>";
		else
			$s = '<li class="disabled"><span>&laquo; Anterior</span></li>';

		if($page > 1)
			$s .= "<li><a href=\"{$baseUrl}\">&laquo; Primeros</a></li>";
		else
			$s .= '<li class="disabled"><span>&laquo; Primeros</span></li>';

		$s .= $retorno;
		if($page < $paginas)
			$s .= "<li><a href=\"{$baseUrl}/pagina/{$paginas}\">Ultimos &raquo;</a></li>";
		else
			$s .= '<li class="disabled"><span>Ultimos &raquo;</span></li>';
		if($page+1 <= $paginas)
			$s .= "<li><a href=\"{$baseUrl}/pagina/".($page+1)."\">Siguiente &raquo;</a></li>";
		else
			$s .= '<li class="disabled"><span>Siguiente &raquo;</span></li>';

		return '<div class="pagination"><ul>'.$s.'</ul></div>';
	}

	private function getParams($params){
		$modulo = ($params['module'] == 'default') ? '' : $params['module'].'/';
		$s = "{$modulo}{$params['controller']}/{$params['action']}";
		unset($params['action'],$params['controller'],$params['module'],$params['pagina']);

		foreach($params as $i => $v)
			if($v != '')
				$s .= "/{$i}/".preg_replace('#/#','_',$v);
		return $s;
	}

}
