<?php

class Zend_View_Helper_Permite extends Zend_View {

	public function permite($module,$controller=null,$action=null){
		return $this->navigation()->getAcl()->permite($module,$controller,$action);
	}
}