<?php

class Zend_View_Helper_Niceage {

	/**
	* Date ingles
	*
	* @param mixed $date
	* $date = "2009-03-04 17:45";
	* $result = nicetime($date); // 2
	*/
	public function niceage($date){
		if(empty($date)) {
			return "No date provided";
		}

		$periods = array("segundo", "minuto", "hora", "d&iacute;a", "semana", "mes", "a&ntilde;o");
		$lengths = array("60","60","24","7","4.35","12","10");

		$now = time();
		$unix_date = strtotime($date);

		// check validity of date
		if(empty($unix_date)){
			return "Bad date";
		}
		// is it future date or past date
		if($now > $unix_date){
			$difference = $now - $unix_date;
		} else {
			$difference = $unix_date - $now;
		}

		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}

		$difference = floor($difference);

		if($difference != 1){
			if($j == 5) $periods[$j] .= "e";
			$periods[$j] .= "s";
		}
		return "{$difference} {$periods[$j]}";
	}
}