<?php

class Zend_View_Helper_Datepicker extends Zend_View_Helper_Abstract {

	public function datepicker(){
		$this->view->headLink()->appendStylesheet($this->view->baseUrl().'/themes/smoothness/jquery-ui-1.10.3.custom.min.css');
		$this->view->headScript()
		->appendFile($this->view->baseUrl().'/themes/v1/js/jquery-ui-1.10.3.custom.min.js')
		->appendFile($this->view->baseUrl().'/themes/v1/js/datepicker.js');
	}
}