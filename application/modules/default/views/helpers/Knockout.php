<?php

class Zend_View_Helper_Knockout extends Zend_View_Helper_Abstract {

	public function knockout(){
		$this->view->headScript()->prependFile($this->view->baseUrl().'/themes/v1/js/knockout-2.3.0.js');
	}
}