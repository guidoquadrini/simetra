<?php

class default_forms_inicio extends Zend_Form
{

	public function init()
    {
		$this->addElement(
        $this->createElement('text','username')
		->setRequired(true)
		->setLabel('Usuario')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-medium',
			'maxlength' => 15,
			'placeholder' => 'Usuario'
		))
			->setErrorMessages(array('Usuario Inválido'))
		//->addValidator('emailAddress')
		);

		$this->addElement(
        $this->createElement('password','password')
		->setLabel('Contraseña')
		->setRequired(true)
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-medium',
			'placeholder' => 'Contraseña',
			'maxlength' => 20
		))->addValidator('NotEmpty', true)
//			->setErrorMessages(array('Contrase&ntilde;a debe ser superior a 6 caracteres'))
//			->addValidator('stringLength', false, array(6, 20))
		);

		$hash = new Zend_Form_Element_Hash('___h', array('disableLoadDefaultDecorators' => true));
		$hash->setSalt('unique')
		->addDecorator('ViewHelper');
		$this->addElement($hash);
	}
}
