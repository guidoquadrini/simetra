<?php
/**
 *
 * @author Guido Nicolás Quadrini
 */
interface VistasGenericasInterface {
    public function indexAction();
    public function formularioAction();
    public function eliminarAction();
    public function imprimirAction();
    
}
