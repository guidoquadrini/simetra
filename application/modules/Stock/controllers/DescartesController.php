<?php

class Stock_DescartesController extends BootPoint {

   public function indexAction() {
       
        /**
         * ----------------------------------------------------------
         * INDEX - Listado principal de los descartes
         * ----------------------------------------------------------
         */
       
        //$nro = $this->getRequest()->getParam('nro');
        //$hem_id = $this->getRequest()->getParam('hem_id');
        $fecha = $this->getRequest()->getParam('fecha');
        $estado = $this->getRequest()->getParam('estado');
        $efector_id = $this->getRequest()->getParam('efector_id');
        
        if( $efector_id == ''){
             $efector_id = $this->efector_activo['id_efector'];                   
             $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 1; /* Por defecto busca los pendientes */

        $perPage = 18;
        
        $modelo = new Stock_models_DescartesStock();
        
        //llamada con efes_ids en vez de efector_id
        $resultados = $modelo->getDescartesByEfe($efes_ids, $fecha, $nro, $estado, $this->page, $perPage )->toArray();

        $this->view->busqueda = 'Estado: '.$estado . '<br/>' . 'Cód.Donación: '. $nro . '<br/>';

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }
      
        $this->view->hem = $hem_id;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->nro = $nro;
        $this->view->efector_nombre = $efector_nombre;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;              
 
    }
    
    
    // -- descarta del stock producido al hemocomoponente seleccionado
    public function descartarstockAction(){
       // UPDATE del estado en stock.estzdo = 2  (ver en tabla hemos producidos q onda). 
    }
    
    public function confirmarAction() {
        
    }

    public function informeAction() {
        
    }

    public function validarAction() {
        
    }

    public function historialAction() {
        
    }

 
   

    /*   
     * ---------------------------------------------------------------
     * PDF - genera pdf de los descartes del index
     * ---------------------------------------------------------------
     */
     public function verpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $fecha = $this->getRequest()->getParam('fecha');
        $estado = $this->getRequest()->getParam('estado');
       
       
        if( $efector_id == ''){
             $efector_id = $this->efector_activo['id_efector'];                   
             $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }        

        if ($estado == 0) {
            $estado_titulo = "Todos";
        } elseif ($estado == 1) {
            $estado_titulo = "Vencidos";
        } elseif ($estado == 2) {
            $estado_titulo = "Descartados";
        } 
        
        
        $perPage = 999;
       
        $modelo = new Stock_models_SolicitudesStock();
        $resultados = $modelo->getSolicitudesTransferenciasByEfe($efector_id, $fecha, $this->page, $perPage, $estado)->toArray();

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8'; //'ISO-8859-1'
        $y = $page->getHeight();
        $xi = 0;
        $titulo = 'Descartes de Hemocomponetes - ' . $estado_titulo;
        $efector = $$efector_nombre;
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha Reporte: ' . date("d-m-Y");

        $page
                /**
                 * Logo
                 */
               
             ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y-50, 97, $y-8)
             //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
             ->setFont($font, 14)
             ->drawText($titulo, 100, $y-25, $encoding)
             ->setFont($font, 11)
             ->drawText($dateRPT, 100, $y-45, $encoding)
             ->drawText($efector, 350, $y-45, $encoding)             
             ->setFont($font, 11)
                
                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea columna1
               
                ->drawLine(133, $y - 55, 133, $y - 500) // Vertical fecha
                ->drawLine(225, $y - 55, 225, $y - 500) // Vertical estado
                ->drawLine(390, $y - 55, 390, $y - 500) // Vertical hemocomponente
                ->drawLine(430, $y - 55, 430, $y - 500) // Vertical ABO
                ->drawLine(450, $y - 55, 450, $y - 500) // Vertical RH
                ->drawLine(490, $y - 55, 490, $y - 500) // Vertical Irradiado
                ->drawLine(560, $y - 55, 560, $y - 500) // Vertical cantidad            
                ->drawLine(630, $y - 55, 630, $y - 500) // Vertical cantidad entregada
                
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical ultima lateral derecho
                
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)

                // titulos columnas                      
                ->drawText('Fecha', 30, $y - 70, $encoding)
                ->drawText('Estado', 140, $y - 70, $encoding)
                ->drawText('Hemocomponente', 250, $y - 70, $encoding)
                ->drawText('Grupo', 395, $y - 70, $encoding)
                ->drawText('RH', 432, $y - 70, $encoding)
                ->drawText('Irrad.', 457, $y - 70, $encoding)
                ->drawText('Cantidad', 500, $y - 70, $encoding)
		->drawText('Fecha Vto.', 570, $y-70, $encoding)
		->drawText('Cód. Donación', 640, $y-70, $encoding)
//			->drawText('Observaciones', 605, $y-70, $encoding)
        ;

        // Completado del reporte
        // Cambiar de página cuando excede ----
        //foreach ($this->resultados as $i => $v): 

        foreach ($resultados as $i => $v) {

            if( $v['estado'] == 1){
                $estado_solicitud = "Pendiente";
            } elseif ( $v['estado'] == 2){
                $estado_solicitud = "Pend. Recep.";
            } elseif ( $v['estado'] == 3){
                $estado_solicitud = "Cerrada";
            } 
            
            if( $v['irradiado'] == 0){
                $irradiado = "NO";
            } else {
                $irradiado = "SI";
            }             
            
            $fecha = $v['fecha'];
            $hemocomponente = $v['hem_descripcion'];      
            $grupo = $v['grupo'];      
            $factor = $v['factor'];      
            $cantidad_solicitada = $v['cantidad_solicitada'];
            $cantidad_entregada = $v['cantidad_entregada'];
            $fecha_vencimiento = $v['fecha_vencimiento'];
            $efector_responde = " ";
            $observaciones = sprintf("%10.31s", $v['observaciones']);

            
            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 11)
                        ->drawText($fecha, 30, $y - 90 - $h, $encoding)
                        ->drawText($estado_solicitud, 140, $y - 90 - $h, $encoding)
                        ->drawText($hemocomponente, 230, $y - 90 - $h, $encoding)
                        ->drawText($grupo, 400, $y - 90 - $h, $encoding)
                        ->drawText($factor, 435, $y - 90 - $h, $encoding)
                        ->drawText($irradiado, 458, $y - 90 - $h, $encoding)
                        ->drawText($cantidad_solicitada, 510, $y - 90 - $h, $encoding)
                        ->drawText($fecha_vencimiento, 568, $y - 90 - $h, $encoding) 
                	->drawText($observaciones, 637, $y-90-$h, $encoding) //hacer break
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 11)
                        ->drawText($fecha, 30, $y - 90 - $h, $encoding)
                        ->drawText($estado_solicitud, 140, $y - 90 - $h, $encoding)
                        ->drawText($hemocomponente, 230, $y - 90 - $h, $encoding)
                        ->drawText($grupo, 400, $y - 90 - $h, $encoding)
                        ->drawText($factor, 435, $y - 90 - $h, $encoding)       
                        ->drawText($irradiado, 458, $y - 90 - $h, $encoding)
                        ->drawText($cantidad_solicitada, 510, $y - 90 - $h, $encoding)
                        ->drawText($fecha_vencimiento, 568, $y - 90 - $h, $encoding) 
                	->drawText($observaciones, 637, $y-90-$h, $encoding) //hacer break
                ;
                $h2 = $h2 + 20;
            }
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }


    public function descarteAction() {
        
    }

    public function descartarAction() {
        
    }

}
