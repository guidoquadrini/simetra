<?php

class Stock_StockController extends BootPoint {

    public function indexAction() {

       /**
         * Buscar Solicitudes de Stock por Efector
         */
        $fecha = $this->getRequest()->getParam('fecha');
        //$nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $hem_id = $this->getRequest()->getParam('hem_id');
        $estado = $this->getRequest()->getParam('estado');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 1; /* Por defecto busca los pendientes */

        $perPage = 18;

//        /* Obtengo los efectores del usuario */
//        $modeloPry = new Acceso_models_Proyectos();
//        $nom = "Banco de sangre";
//        $pry = $modeloPry->getByNom($nom);
//
//        $pry_id = $pry->pry_id;
//        $modeloUsu = new Acceso_models_Usuarios();
//        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);
//
//        foreach ($efes_usu as $e)
//            $efes_ids[] = $e['id_efector'];
//        /* Fin efectores usuario */
        

        $modeloParam = new Efector_models_EfectoresParametros();
        $efector_id = $modeloParam->find($this->efector_activo['id_efector']);
        $efector_nombre = $this->efector_activo['nomest'];        

        $modelo = new Stock_models_Stock();
        //llamada con efes_ids en vez de efector_id
        $resultados = $modelo->getStockByEfe($fecha, $this->page, $perPage, $nro, $efes_ids, $estado, $hem_id);
        $this->view->busqueda = $fecha;

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $this->view->hem = $hem_id;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        //$this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;        
    }





    /* * *********************************************************
     *  *****     Ver listado de solicitudes de transferencia en PDF     ************
     * ******************************************************** */

    public function verpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        //$env_id = (int)$this->getRequest()->getParam('env_id');
        $fecha = $this->getRequest()->getParam('fecha');
        $nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');

        if ($this->getRequest()->getParam('estado') == "todos") {
            $estado = true;
        } else {
            $estado = false;
        }
        $perPage = 999;

//        /* Obtengo los efectores del usuario */
//        $modeloPry = new Acceso_models_Proyectos();
//        $nom = "Banco de sangre";
//        $pry = $modeloPry->getByNom($nom);
//
//        $pry_id = $pry->pry_id;
//        $modeloUsu = new Acceso_models_Usuarios();
//        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);
//
//        foreach ($efes_usu as $e)
//            $efes_ids[] = $e['id_efector'];
//        /* Fin efectores usuario */
        

        $modeloParam = new Efector_models_EfectoresParametros();
        $efector_id = $modeloParam->find($this->efector_activo['id_efector']);
        $efector_nombre = $this->efector_activo['nomest'];        


        $modelo = new Efector_models_Envios();
        //se pasa parámetro efes_ids en vez de efe_id
        $resultados = $modelo->getSolicitudesAndEfe($fecha, 1, $perPage, $nro, $efes_ids, $estado);
        
        // En el caso que haya datos ya cargados para esa donación, los obtenemos.
        $modeloInm = new Stock_models_SolicitudesStock();
//        $donacion = array();
//        foreach ($resultados as $i => $v)
//            $registros[$i] = $modeloInm->getDatosDonacion($v['don_id']);

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8'; //'ISO-8859-1'
        $y = $page->getHeight();
        $xi = 0;
        $titulo = 'Solicitudes de Transferencia - ' . $this->getRequest()->getParam('estado');
        $efector = $this->efector_activo['nomest'];
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha Reporte: ' . date("d-m-Y");

        $page
                /**
                 * Logo
                 */
               
             ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y-50, 97, $y-8)
             //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
             ->setFont($font, 14)
             ->drawText($titulo, 100, $y-25, $encoding)
             ->setFont($font, 11)
             ->drawText($dateRPT, 100, $y-45, $encoding)
             ->drawText($efector, 350, $y-45, $encoding)             
                
                
               // titulo reporte
//                ->setFont($font, 14)
//                ->drawText($titulo, 70, $y - 25, $encoding)

                // subtitulo reporte
                ->setFont($font, 11)
//                ->drawText($dateRPT, 70, $y - 45, $encoding)
//                ->drawText($efector, 230, $y - 45, $encoding)             
                
                
                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                //->drawRectangle(5, $y - 5, $page->getWidth() - 5, $y - 240, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                //->drawLine($page->getWidth() / 2, $y - 80, $page->getWidth() / 2, $y - 100)
                //         x1    y1    x2    y2
                //->drawLine(20, $y-27, 257, $y-27)
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea columna1
               
                ->drawLine(133, $y - 55, 133, $y - 500) // Vertical fecha final
                ->drawLine(225, $y - 55, 225, $y - 500) // Vertical cod. donacion final
                ->drawLine(350, $y - 55, 350, $y - 500) // Vertical perfil asociado final
                //->drawLine(600, $y - 55, 600, $y - 500)
                ->drawLine(585, $y - 55, 585, $y - 500) // Vertical observaciones final             
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical ultima lateral derecho
                
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)

                // titulos columnas                      
                ->drawText('Fecha', 30, $y - 70, $encoding)
                ->drawText('Estado', 140, $y - 70, $encoding)
                ->drawText('Efector Solicitante', 250, $y - 70, $encoding)
                ->drawText('Observaciones', 355, $y - 70, $encoding)
//			->drawText('Estado', 490, $y-70, $encoding)
//			->drawText('Observaciones', 605, $y-70, $encoding)
        ;

        // Completado del reporte
        // Cambiar de página cuando excede ----
        //foreach ($this->resultados as $i => $v): 

        foreach ($resultados as $i => $v) {

            $fecha_envio = $v['fecha'];
            $donacion_nro = $v['estado'];
            $obsTrunc = sprintf("%10.31s", $v['observaciones']);
            $perfilAsociado = " ";
            $notas = " ";
            

            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 11)
                        ->drawText($fecha_envio, 30, $y - 90 - $h, $encoding)
                        ->drawText($donacion_nro, 140, $y - 90 - $h, $encoding)
                        ->drawText($perfilAsociado, 230, $y - 90 - $h, $encoding)
                        ->drawText($obsTrunc, 355, $y - 90 - $h, $encoding)
                        ->drawText($notas, 230, $y - 70 - $h, $encoding) //hacer break
                //	->drawText($v['don_usu_extraccion'], 700, $y-90-$h, $encoding)$v['don_nro']
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 11)
                        ->drawText($fecha_envio, 30, $y - 90 - $h, $encoding)
                        ->drawText($donacion_nro, 140, $y - 90 - $h, $encoding)
                        ->drawText($perfilAsociado, 230, $y - 90 - $h, $encoding)
                        ->drawText($obsTrunc, 355, $y - 90 - $h, $encoding)
                        ->drawText($notas, 230, $y - 70 - $h, $encoding) //hacer break
                //	->drawText($v['don_usu_extraccion'], 700, $y-90-$h, $encoding)$v['don_nro']
                ;
                $h2 = $h2 + 20;
            }
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }



}
