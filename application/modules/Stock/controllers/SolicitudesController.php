<?php

class Stock_SolicitudesController extends BootPoint {

    
    /*
    * ---------------------------------------------------------------
    * INDEX Solicitudes de Transferencia de Hemocomponentes
    * ---------------------------------------------------------------
    */  
    public function indexAction() {

        $fecha = $this->getRequest()->getParam('fecha');
        //$nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        //$hem_id = $this->getRequest()->getParam('hem_id');
        $estado = $this->getRequest()->getParam('estado');
        
        //fecha=&efe_id=&estado=1

        if ($estado == 0){
            $estado = 1; /* Por defecto busca los pendientes */
        }
            
        $perPage = 18;
        
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }        
        
        
        $modelo = new Stock_models_SolicitudesStock();

        //llamada con efes_ids en vez de efector_id
        $resultados = $modelo->getSolicitudesTransferenciasByEfe($efes_ids, $fecha, $estado, $this->page, $perPage)->toArray();
        
        $this->view->busqueda = 'Estado: ' . $estado . '<br/>' . 'Fecha: ' . $fecha . '<br/>';

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }
        
        // ASOCIAR EFECTOR ID y RESPONDE EFECTOR ID
        $modeloEfector = new Acceso_models_Efectores();

        foreach ($resultados as $i => $v) {
            $efector_solicita[$v['efector_id']] = $modeloEfector->getNom($v['efector_id']);
                if( $v['responde_efector_id'] > 0){
                $efector_responde[$v['responde_efector_id']] = $modeloEfector->getNom($v['responde_efector_id']);
                }
            }
     
        $this->view->hem = $hem_id;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        //$this->view->nro = $nro;
        $this->view->efector_nombre = $efector_nombre;
        $this->view->efector = $efector_id;
        $this->view->efector_solicita = $efector_solicita;
        $this->view->efector_responde = $efector_responde;        
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;
        
    }
    
    
    
    /** Formulario de Respuesta a la solicitud de un Hemocomponente
     * 
     */
    public function respuestaAction() {

        $id = (int) $this->getRequest()->getParam('id');

        $modeloSolcitud = new Stock_models_SolicitudesStock();
        $solicitud = $modeloSolcitud->getById($id);

        $modeloEfector = new Acceso_models_Efectores();
        $efector_origen = $modeloEfector->fetchRow('id_establecimiento=' . $solicitud['efector_id']);
        $efector_destino = $modeloEfector->fetchRow('id_establecimiento=' . $this->efector_activo['id_efector']);

        $modeloHemocomponente = new Gestion_models_Hemocomponentes();
        $hemocomponente = $modeloHemocomponente->getById($solicitud['hemocomponente_id']);

        $this->view->solicitud = $solicitud;
        $this->view->efector_origen = $efector_origen;
        $this->view->efector_destino = $efector_destino;
        $this->view->hemocomponente = $hemocomponente;
        
    }    

    
        
//        $fecha = $solicitud['fecha'];
//        $estado = $solicitud['estado']; 
//        $efector_id = $solicitud['efector_id'];
//        $prioridad = $solicitud['prioridad'];
//        $cantidad_solicitada = $solicitud['cantidad_solicitada'];
//        $hemocomponente_id =  $solicitud['hemocomponente_id'];
//        $hemocomponente_grupo = $solicitud['grupo'];
//        $hemocomponente_factor = $solicitud['factor'];
//        $hemocomponente_irradiado = $solicitud['irradiado'];
//        $hemocomponente_fenotipo = $solicitud['fenotipo'];    
    
    /**
     * ---------------------------------------------------------------------
     * Responder a la Solicitud (Transferirle)
     * ---------------------------------------------------------------------
     */
    public function respondersolicitudAction() {
        $id = (int) $this->getRequest()->getParam('registro-id');
        $fecha = $this->getRequest()->getParam('registro-fecha');
        $prioridad = $this->getRequest()->getParam('registro-prioridad');
        $cantidad_solicitada = $this->getRequest()->getParam('registro-cantidad-solicitada');
        $hemocomponente_id = $this->getRequest()->getParam('registro-hemocomponente-id');
        $hemocomponente_grupo = $this->getRequest()->getParam('registro-hemocomponente-grupo');
        $hemocomponente_factor = $this->getRequest()->getParam('registro-hemocomponente-factor');
        $hemocomponente_irradiado = $this->getRequest()->getParam('registro-hemocomponente-irradiado');
        $hemocomponente_fenotipo = $this->getRequest()->getParam('registro-hemocomponente-fenotipo');
        $efector_id = $this->getRequest()->getParam('registro-solicita-efector-id');

        // respuesta (campos del form)
        $responde_fecha = date('Y-m-d hh:mm:ss');
        $responde_efector_id = $this->getRequest()->getParam('responde_efetor_id');
        $cantidad_entregada = $this->getRequest()->getParam('cantidad_entregada');
        $observaciones = trim($this->getRequest()->getParam('observaciones'));


        $modeloSolcitud = new Stock_models_SolicitudesStock();

        // SI LA CANTIDAD ENVIADA ES MENOR A LA SOLICITADA
//        if ($cantidad_entregada == $cantidad_solicitada) {

            echo "IGUAL";
            die();

            // UPDATE NORMAL
            $values = array('estado' => 3,
                'responde_fecha' => $responde_fecha,
                'responde_efector_id' => $responde_efector_id,
                'cantidad_entregada' => $cantidad_entregada,
                'observaciones' => $$observaciones,
                'responde_usuario_id' => $this->auth->getIdentity()->usu_id
            );

            echo '<pre>';
            print_r($values);
            echo '</pre>';
            die();

            $solicitudes = $modeloSolcitud->find($id)->current();

            $solicitudes->setFromArray($values)->save();

            $this->_helper->json(array('st' => 'ok'));
//        } elseif ($cantidad_entregada < $cantidad_solicitada) {
//            // 1) UPDATE NORMAL - CON LA CANT. ENTREGADA 
//            // 2) SE CREA NUEVA SOLICITUD X LA DIFERENCIA QUE EXISTE (que quedo pendiente)
//        } elseif ($cantidad_entregada > $cantidad_solicitada) {
//            // AVISA QUE NO SE PUEDE               
//        }

    }

    
    
    
   /*
    * ---------------------------------------------------------------------
    * GRABA la solicitud
    * ---------------------------------------------------------------------
    */ 
    public function grabarAction() {
        
        $DATA = $this->getRequest()->getParam('datos');
        
        $fecha = $DATA['header']['fecha'];
       
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }        
        
        
        // INSERT SOLICITUD (cada registro es una solicitud independiente)
        $modelo = new Stock_models_SolicitudesStock();
      
        foreach ($DATA['lineas'] as $linea) {
            
            if( $linea['hemocomponente_id'] <> '' ){
                
                if( $linea['irradiado'] == "NO"){
                    $irradiado = 0;
                } else {
                    $irradiado = 1; 
                }
                
                if( $linea['prioridad'] == "Baja"){
                    $prioridad = 1;
                } elseif ( $linea['prioridad'] == "Media") {
                    $prioridad = 2; 
                } elseif ( $linea['prioridad'] == "Alta") {
                    $prioridad = 3; 
                } elseif ( $linea['prioridad'] == "Urgente") {
                    $prioridad = 4; 
                }
                    
                $items = $modelo->createRow(array(
                    'fecha' => $fecha,
                    'estado' => 1,
                    'efector_id' => $efector_id,
                    'hemocomponente_id'=> $linea['hemocomponente_id'], 
                    'grupo' => $linea['grupo'],
                    'factor' => $linea['factor'],
                    'irradiado' => $irradiado,
                    'prioridad' => $prioridad,
                    'cantidad_solicitada' => $linea['cantidad'],
                    'observaciones' => $linea['notas'],
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'fecha_alta' => new Zend_Db_Expr('NOW()')
                    ))->save();               
            }
        }               
        
        $this->_helper->json(array('st' => 'ok'));
        
    }    
    
    
    
    
    
    /**
     * ---------------------------------------------------------------------
     * Recepcion del Envio (control de la transferencia)
     * ---------------------------------------------------------------------
     */
    public function recepcionarAction() {

        $id = (int) $this->getRequest()->getParam('id');

        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }              
        
        $modelo = new Stock_models_SolicitudesStock();

        $resultados = $modelo->getById($id);
        $this->view->resultados = $resultados;

        // UPDATE NORMAL
        $values = array('estado' => 3);

        $solicitudes = $modelo->find($id)->current();

        $solicitudes->setFromArray($values)->save();

        $this->_helper->json(array('st' => 'ok'));
    }

    
    /**
     * ---------------------------------------------------------------------
     * Alta de solicitud de Hemocomponentes
     * ---------------------------------------------------------------------
     */
    public function altaAction() {

        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }              
        
        $modelo = new Gestion_models_Hemocomponentes();
        $resultados = $modelo->getPairs();
        $this->view->resultados = $resultados;

        $this->renderScript('solicitudes/form.phtml');
    }
    
    

    public function cerrarAction() {
        
    }
    
    
    public function informeAction() {
        
    }

    public function pdfSolicitudAction() {
        
    }



    public function pdfEnvioAction() {
        
    }

    
    /*  
    /*  ---------------------------------------------------------------------
     *  PDF - Listado de solicitudes de transferencia de Hemocomponentes
     * ---------------------------------------------------------------------
     */

    public function verpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $fecha = $this->getRequest()->getParam('fecha');
        $estado = $this->getRequest()->getParam('estado');
        $efector_id = $this->getRequest()->getParam('efector_id');
        
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }              

        if ($estado == 0) {
            $estado_titulo = "Todos";
        } elseif ($estado == 1) {
            $estado_titulo = "Pendientes";
        } elseif ($estado == 2) {
            $estado_titulo = "Comprometidas por un envio";
        } elseif ($estado == 3) {
            $estado_titulo = "Pendientes de Recepción";
        } elseif ($estado == 4) {
            $estado_titulo = "Cerradas";
        } elseif ($estado == 5) {
            $estado_titulo = "Descartadas";
        }

        $perPage = 999;

        $modelo = new Stock_models_SolicitudesStock();

        $resultados = $modelo->getSolicitudesTransferenciasByEfe($efes_ids, $fecha, $estado, $this->page, $perPage)->toArray();

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8';
        $y = $page->getHeight();
        $xi = 0;
        $titulo = 'Solicitudes de Transferencia de Hemocomponetes - ' . $estado_titulo;
        $efector = $efector_nombre; 
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha Reporte: ' . date("d-m-Y");

        $page
                /**
                 * Logo
                 */
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
                ->setFont($font, 14)
                ->drawText($titulo, 100, $y - 25, $encoding)
                ->setFont($font, 11)
                ->drawText($dateRPT, 100, $y - 45, $encoding)
                ->drawText($efector, 350, $y - 45, $encoding)
                ->setFont($font, 11)

                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea columna1
                ->drawLine(133, $y - 55, 133, $y - 500) // Vertical fecha
                ->drawLine(225, $y - 55, 225, $y - 500) // Vertical estado
                ->drawLine(390, $y - 55, 390, $y - 500) // Vertical hemocomponente
                ->drawLine(423, $y - 55, 423, $y - 500) // Vertical ABO
                ->drawLine(455, $y - 55, 455, $y - 500) // Vertical RH
                ->drawLine(490, $y - 55, 490, $y - 500) // Vertical Irradiado
                ->drawLine(560, $y - 55, 560, $y - 500) // Vertical cantidad            
                ->drawLine(630, $y - 55, 630, $y - 500) // Vertical cantidad entregada
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical ultima lateral derecho
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)
                // titulos columnas                      
                ->drawText('Fecha', 30, $y - 70, $encoding)
                ->drawText('Estado', 140, $y - 70, $encoding)
                ->drawText('Hemocomponente', 250, $y - 70, $encoding)
                ->drawText('ABO', 394, $y - 70, $encoding)
                ->drawText('RH', 430, $y - 70, $encoding)
                ->drawText('Irrad.', 460, $y - 70, $encoding)
                ->drawText('Cantidad', 500, $y - 70, $encoding)
                ->drawText('Entregada', 570, $y - 70, $encoding)
                ->drawText('Observaciones', 640, $y - 70, $encoding)
//			->drawText('Observaciones', 605, $y-70, $encoding)
        ;

        foreach ($resultados as $i => $v) {

            if ($v['estado']  == 1) {
                $estado_solicitud = "Pendiente";
            } elseif ($v['estado']  == 2) {
                $estado_solicitud = "Comprometidas";
            } elseif ($v['estado']  == 3) {
                $estado_solicitud = "Pend. Recep.";
            } elseif ($v['estado']  == 4) {
                $estado_solicitud = "Cerradas";
            } elseif ($v['estado']  == 5) {
                $estado_solicitud = "Descartada";
            }          
            
            if ($v['irradiado'] == 0) {
                $irradiado = "NO";
            } else {
                $irradiado = "SI";
            }

            
            if ($v['factor'] == "+") {
                $factor = "POS";
            } else {
                $factor = "NEG";
            }            
            
            $fecha = $v['fecha'];
            $hemocomponente = $v['hem_descripcion'];
            $grupo = $v['grupo'];
            $cantidad_solicitada = $v['cantidad_solicitada'];
            $cantidad_entregada = $v['cantidad_entregada'];

            $efector_responde = " ";
            $observaciones = sprintf("%10.31s", $v['observaciones']);


            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 11)
                        ->drawText($fecha, 30, $y - 90 - $h, $encoding)
                        ->drawText($estado_solicitud, 140, $y - 90 - $h, $encoding)
                        ->drawText($hemocomponente, 230, $y - 90 - $h, $encoding)
                        ->drawText($grupo, 400, $y - 90 - $h, $encoding)
                        ->drawText($factor, 429, $y - 90 - $h, $encoding)
                        ->drawText($irradiado, 462, $y - 90 - $h, $encoding)
                        ->drawText($cantidad_solicitada, 510, $y - 90 - $h, $encoding)
                        ->drawText($cantidad_entregada, 580, $y - 90 - $h, $encoding)
                        ->drawText($observaciones, 635, $y - 90 - $h, $encoding) //hacer break
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 11)
                        ->drawText($fecha, 30, $y - 90 - $h, $encoding)
                        ->drawText($estado_solicitud, 140, $y - 90 - $h, $encoding)
                        ->drawText($hemocomponente, 230, $y - 90 - $h, $encoding)
                        ->drawText($grupo, 400, $y - 90 - $h, $encoding)
                        ->drawText($factor, 435, $y - 90 - $h, $encoding)
                        ->drawText($irradiado, 458, $y - 90 - $h, $encoding)
                        ->drawText($cantidad_solicitada, 510, $y - 90 - $h, $encoding)
                        ->drawText($cantidad_entregada, 580, $y - 90 - $h, $encoding)
                        ->drawText($observaciones, 615, $y - 90 - $h, $encoding) //hacer break
                ;
                $h2 = $h2 + 20;
            }
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }



}
