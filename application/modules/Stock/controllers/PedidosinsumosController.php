<?php

class Stock_PedidosinsumosController extends BootPoint {

/*
 1 = Generado
 2 = Enviado
 3 = Recep. Parcial
 4 = Cerrado
 5 = Cancelado
 6 = Pend. QA
 7 = Autorizar Pedido
 8 = Nuevo Envio
*/    
    
    
    
    /**
     * LAUNCH GESTION Pedidos Insumos
     *
     */
    public function indexAction() {

        $efector_id = $this->getRequest()->getParam('efector_id');
        
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }

        $perPage = 18;
        
        $modeloInsumosConsolidados = new Stock_models_Pedidosinsumosmovimientosconsolidados();
        
        // obtiene inusmos consolidados del efector
        $resultadoInsumosConsolidados = $modeloInsumosConsolidados->getInsumosEnStockConsolidados($efector_id);
        
        if(sizeof($resultadoInsumosConsolidados) > 0){
            
            $resultados_array = $resultadoInsumosConsolidados->toArray();

            foreach ($resultados_array as &$cantidad_stock){  
                
                $row = $modeloInsumosConsolidados->getInsumoExistenciaById($efector_id, $cantidad_stock['producto_id'])->current();

                $cantidad_stock['cantidad'] = $row['cantidad_existente'];   
                $cantidad_stock['nombre_producto'] = $row['nombre_producto']; 
                $cantidad_stock['ultimo_costo'] = $row['ultimo_costo'];    

                // TODO: verificar si hay que mostrar en la vista la ultima fecha de pedido (pedidos_insumos_detalle)
            }

            if (sizeof($resultadoInsumosConsolidados) > 0) {
                $total = count($resultados_array);
                $paginas = ceil($total / $perPage);
            }       
            
        } else {
                $paginas = 0;
        }
        
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;        
        $this->view->resultados = $resultados_array;        
        $this->view->paginas = $paginas;

    }

    
    
    /**
     * LISTAR Pedidos
     *
     */
    public function listarAction() {
        
        $estado = $this->getRequest()->getParam('estado');
        $fecha = $this->getRequest()->getParam('fecha');
        //$efector_id = $this->getRequest()->getParam('efector_id');
  
        
        if (is_null($estado))
            $estado = 1; /* Por defecto busca los pendientes */        
//       if (is_null($fecha))
//            $fecha = date('Y-m-d hh:mm:ss');

        $perPage = 18;
        
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // TODO: aqui ver si aplica para una vista con filtro de Efectores (sino queda siempre el activo)
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
            $efector_nombre = $datos_efector[0]['nomest'];             
        }     
         
        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $modelo_usuarios = new Acceso_models_Usuarios();

        $resultados = $modeloEncabezado->getPedidosinsumosByEfe($fecha, $this->page, $perPage, $estado, $efector_id);
        $resultados_array = $resultados->toArray();
        foreach ($resultados_array as &$encabezado_usuario){  
            $row = $modelo_usuarios->find($encabezado_usuario['usuario_id'])->current();
            $encabezado_usuario['usu_nombre'] = $row->usu_nombre;   
        }

  
        if (sizeof($resultados) > 0) {
            $total = count($resultados_array);
            $paginas = ceil($total / $perPage);
        }

        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;
        $this->view->resultados = $resultados_array;
        $this->view->paginas = $paginas;
    }

    /**
     * NUEVO Pedido
     *
     */
    public function pedidoAction() {
        
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;        
        
        $this->renderScript('pedidosinsumos/pedido.phtml');
    }

    
    
    
    /**
     * ALTA del Pedido
     *
     */
    public function altaAction() {

        $DATA = json_decode($_POST['datos']);

        $fecha = $DATA->header->fecha;
        $obervaciones = $DATA->header->observaciones;

        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];        
      
        // INSERT ENCABEZDO PEDIDO
        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $encabezado_id = $modeloEncabezado->createRow(array(
                    'fecha' => $fecha,
                    'estado' => 1,
                    'observaciones' => $obervaciones,
                    'efector_id' => $this->efector_activo['id_efector'],
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'fecha_alta' => new Zend_Db_Expr('NOW()')
                ))->save();
        
        // INSERTA EL DETALLE DEL PEDIDO
        $modeloDetalle = new Stock_models_Pedidosinsumosdetalle();
        foreach ($DATA as $linea) {
            if ($linea->producto_id <> '') {
                $items = $modeloDetalle->createRow(array(
                            'pedido_id' => $encabezado_id,
                            'producto_id' => $linea->producto_id,
                            'cantidad' => $linea->cantidad,
                            'notas' => $linea->notas
                        ))->save();
            }
        }
        
        // INSERTA EN PEDIDO LOG 
        // lleva una auditoria del pedido por cada instancia (datos con JSON)
        $modeloLog = new Stock_models_Pedidosinsumoslog();
        $log_id = $modeloLog->createRow(array(
                    'efector_id' => $this->efector_activo['id_efector'],
                    'fecha' => $fecha,
                    'pedido_id' => $encabezado_id,
                    'fecha' => new Zend_Db_Expr('NOW()'),
                    'accion' => 'Alta del Pedido',
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'datos' => $_POST['datos'],
                ))->save();

        $this->renderScript('pedidosinsumos/listar.phtml');
    }

    
    
      /**
     * ENIVAR Pedido al efector (Envio y Nuevo Envio)
     * 
     * Instancia de aprobacion y control por parte del CRH (administrativamente)
     */
    public function enviarAction() {
        $pedido_id = $this->getRequest()->getParam('id');

        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $resultadosEncabezado = $modeloEncabezado->getPedidosinsumosById($pedido_id)->toArray();
        
        $modeloDetalle = new Stock_models_Pedidosinsumosdetalle();
        $resultadosDetalle = $modeloDetalle->getPedidosinsumosdetalleById($pedido_id); //->toArray();

        $tipomovimientoajuste_id = 1;  // TODO: debe ser el correspondiente a INGRESO EN STOCK

        // obtenemos la cantidad recepcionada de cada producto en la tabla de movimientos de stock
        // para informarla en el formulario 
//        $modeloStockMovimientos = new Stock_models_Pedidosinsumosmovimientos();    
//        $resultadosCantidadProductos = $modeloStockMovimientos->getCantidadByPedido($pedido_id, $tipomovimientoajuste_id)->toArray();
        
        $this->view->resultadosEncabezado = $resultadosEncabezado;
        $this->view->resultadosDetalle = $resultadosDetalle;
        $this->view->pedido_id = $pedido_id;

        $this->renderScript('pedidosinsumos/enviar.phtml');
    }  
    
    
 
    
   /*
     * ACTUALIZAR ENVIO pedido (update del pedido con cantidad de insumo enviada)
     * 
     * El formulario envia por $_POST un array con los items del detalle del pedi
     * por cada uno enviado, tiene asociado N elementos
     * 
     * [item-id] => Array 
     * [item-producto-id] => Array 
     * [item-producto-descripcion] => Array 
     * [item-producto-cantidad] => Array
     * [item-producto-notas] => Array
     * [item-producto-cantidad-existente] => Array 
     * [recepciona_cantidad] => Array
     * [recepciona_notas] => Array
     * 
     * Variables de formulario (unicas)
     * [compronante-id] => 1  // pedido_id 
     * [observaciones] => TURCOMAN  // observaciones de la recepcion
     * 
    */   
    public function actualizarenvioAction() {
        $DATA =  $this->getRequest()->getPost(); 
        
        $pedido_id = $DATA['compronante-id'];
        $envia_observaciones = $DATA['envia-observaciones'];            
        $i = 0;   
        
        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $modeloDetalle    = new Stock_models_Pedidosinsumosdetalle();        
        
        $estadoPedido = $modeloEncabezado->getEstadoPedido($pedido_id);

        
        // control inicial si fue generado = 1
        if( $estadoPedido[0]['estado'] == 1){
            $estado  = 2; 
            $textLog = "Envio del Pedido";
        } else if ($estadoPedido[0]['estado'] == 2)  {
            $estado  = 8; 
            $textoLog = "Nuevo Envio";
        } else if ($estadoPedido[0]['estado'] == 3)  {
            $estado  = 8; 
            $textoLog = "Nuevo Envio";
        } else if ($estadoPedido[0]['estado'] == 8)  {
            $estado  = 8; 
            $textoLog = "Nuevo Envio";
        }

        // ID efector donde se ha generado el pedido
        // importante para la auditoria en el LOG
        $efector_id_pedido_generado = $estadoPedido[0]['efector_id'];

        $fecha_registracion = date('Y-m-d hh:mm:ss');

        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        
        // UPDATE ENCABEZADO DEL PEDIDO DE INSUMO
        // Cambio del estado actual del pedido al nuevo estado (Envio o Nuevo Envio)
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $values = array('estado' => $estado ); 
        $where = array( 'id = ?' => $pedido_id );
        $adapter->update('pedidos_insumos', $values, $where);                     
        
        // obtenemos el ID del efector que genero el pedido para luego actualizar LOG
        $resultadoEncabezado = $modeloEncabezado->getPedidosinsumosById($pedido_id);

        // cantidad de registros del detalle del envio para procesar
        $cantItemsDetalle = count($DATA['item-id']);
    
        // ACTUALIZA EN INSUMOS DETALLE
        for ($i = 0; $i < $cantItemsDetalle; $i++) {
            
            $registro_id = $DATA['item-id'][$i]; 
            $producto_id = $DATA['item-producto-id'][$i]; 
            $producto_nombre = $DATA['item-producto-descripcion'][$i]; 
            $producto_cantidad = $DATA['item-producto-cantidad'][$i]; 
            $producto_notas = $DATA[' item-producto-notas'][$i];
            $producto_cantidad_existente = $DATA['item-producto-cantidad-existente'][$i]; 
            $pedido_envia_cantidad = (float)$DATA['envia_cantidad'][$i];
            $pedido_envia_notas = $DATA['envia_notas'][$i];

            // verificamos que exista una cantidad para enviar (negativa o positiva)
            if( $pedido_envia_cantidad <> '' ){
               
                // obtiene la cantidad enviada previamente y la suma al envio actual
                //$resultadoDetalle = $modeloDetalle->getCantidadInsumoEnviadaById($registro_id)->toArray();  
                //$total_cantidad_enviada = (float)$resultadoDetalle[0]['cantidad_enviada'] + $pedido_envia_cantidad;
                
                $total_cantidad_enviada = $producto_cantidad_existente + $pedido_envia_cantidad;
                
                // actualiza la cantidad enviada
                $data = array('cantidad_enviada' => $total_cantidad_enviada );
                $where = array(  'id = ?' => $registro_id  );
                $adapter->update('pedidos_insumos_detalle', $data, $where);            

                // armado del array para guardar el JSON en la tabla LOG
                $strJSON .=  '"linea_' . $i . '":{"producto_id":"' . $producto_id .'","descripcion":"'. $producto_nombre .'","cantidad":"'. $pedido_envia_cantidad .'","notas":"'. $pedido_envia_notas .'"},' ;                       

            }
 
        }               
        
        $strJSON = '{' . $strJSON .   
                '"header":{"fecha":"' . $fecha_registracion . '","observaciones":"' . $envia_observaciones . '"}}';
             
       // FORMATEAR JSON PARA INSERTAR EN LOG         
       $itemsJSON = stripslashes(json_encode($strJSON, JSON_UNESCAPED_SLASHES));        
       $itemsJSON = substr($itemsJSON,1); // remueve primer comilla
       $itemsJSON = substr($itemsJSON,0,-1); // remueve ultima comilla
          
       // INSERTA EN PEDIDO LOG 
       // lleva una auditoria del pedido por cada instancia y para cada efector (datos con JSON)
       $modeloLog = new Stock_models_Pedidosinsumoslog();

       $log_id = $modeloLog->createRow(array(
                    'efector_id' => $efector_id_pedido_generado,
                    'fecha' => new Zend_Db_Expr('NOW()'), 
                    'pedido_id' => $pedido_id,
                    'accion' => $textoLog,
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'datos' => $itemsJSON,        
                ))->save();
      
       // ------------------------------------------------------------
       // Asigna nuevamente los valores para llamar a la vista LISTAR
       // ------------------------------------------------------------
        $perPage = 18;
        $fecha = null;
       
        $modelo_usuarios = new Acceso_models_Usuarios();

        $resultados = $modeloEncabezado->getPedidosinsumosByEfe($fecha, $this->page, $perPage, $estado, $efector_id);
        $resultados_array = $resultados->toArray();
        foreach ($resultados_array as &$encabezado_usuario){  
            $row = $modelo_usuarios->find($encabezado_usuario['usuario_id'])->current();
            $encabezado_usuario['usu_nombre'] = $row->usu_nombre;   
        }

        if (sizeof($resultados) > 0) {
            $total = count($resultados_array);
            $paginas = ceil($total / $perPage);
        }

        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;
        $this->view->resultados = $resultados_array;
        $this->view->paginas = $paginas;    
        
        $this->renderScript('pedidosinsumos/listar.phtml');
    }    
    
    
    
    
    
    /**
     * RECEPCION del Pedido
     *
     */
    public function recepcionAction() {

        $pedido_id = $this->getRequest()->getParam('id');

        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $resultadosEncabezado = $modeloEncabezado->getPedidosinsumosById($pedido_id)->toArray();
        
        $modeloDetalle = new Stock_models_Pedidosinsumosdetalle();
        $resultadosDetalle = $modeloDetalle->getPedidosinsumosdetalleById($pedido_id); //->toArray();

        $tipomovimientoajuste_id = 1;  // TODO: debe ser el correspondiente a INGRESO EN STOCK

        // obtenemos la cantidad recepcionada de cada producto en la tabla de movimientos de stock
        // para informarla en el formulario 
//        $modeloStockMovimientos = new Stock_models_Pedidosinsumosmovimientos();    
//        $resultadosCantidadProductos = $modeloStockMovimientos->getCantidadByPedido($pedido_id, $tipomovimientoajuste_id)->toArray();
        
        $this->view->resultadosEncabezado = $resultadosEncabezado;
        $this->view->resultadosDetalle = $resultadosDetalle;
        $this->view->pedido_id = $pedido_id;        
        
        $this->renderScript('pedidosinsumos/recepcion.phtml');
    }
 
    
    
    /*
     * ACTUALIZAR RECEPCION pedido (update de la recepcion)
     * 
     * El formulario envia por $_POST un array con los items del detalle del pedi
     * por cada uno enviado, tiene asociado N elementos
     * 
     * [item-id] => Array 
     * [item-producto-id] => Array 
     * [item-producto-descripcion] => Array 
     * [item-producto-cantidad] => Array
     * [item-producto-notas] => Array
     * [item-producto-cantidad-existente] => Array 
     * [recepciona_cantidad] => Array
     * [recepciona_notas] => Array
     * 
     * Variables de formulario (unicas)
     * [compronante-id] => 1  // pedido_id 
     * [observaciones] => TURCOMAN  // observaciones de la recepcion
     * 
    */   
    public function actualizarrecepcionAction() {
        $DATA =  $this->getRequest()->getPost(); 
        
        $estado  = 3; // recepcion parcial del pedido
        $tipomovimientoajuste_id = 1; // TODO: ver tabla el registro que sea INGRESO
        $fecha_registracion = date('Y-m-d hh:mm:ss');
        $pedido_id = $DATA['compronante-id'];
        $recepcionar_observaciones = $DATA['recepcion-observaciones'];    
        
        // cantidad de registros del detalle del pedido (respuesta) hay para procesar
        $cantItemsDetalle = count($DATA['item-id']);
        
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        
        /* ENCABEZADO DEL PEDIDO DE INSUMO
        * Actualizamos el estado a 3 = Recepecion Parcial
        * se guarda en el encabezado la ultima recepcion (fecha, usuario y observaciones del Pedido)     
        */
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $modeloEncabezado = new Stock_models_Pedidosinsumos();
         
        $values = array('estado' => $estado,
                      'ultima_recepcion_observaciones' => $recepcionar_observaciones,
                      'ultima_recepcion_usuario_id' => $this->auth->getIdentity()->usu_id,
                      'ultima_recepcion_fecha' => $fecha_registracion
                      );
                $where = array(
                    'id = ?' => $pedido_id
                );
        $adapter->update('pedidos_insumos', $values, $where);                     
     
        
        //TODO: hay que realizar una actualizacion del importe del producto ingresado en el form
        // de la recepcion en la tabla de productos -update- para que siempre mantenga el ultimo costo
        
        // INSERTA EN STOCK INSUMO MOVIMIENTO
        // se genera un movimiento por cada producto / insumo que se recepciona del pedido 
        $modeloStockMovimientos = new Stock_models_Pedidosinsumosmovimientos();
        $i = 0;
       
        for ($i = 0; $i < $cantItemsDetalle; $i++) {
            
            $registro_id = $DATA['item-id'][$i]; 
            $producto_id = $DATA['item-producto-id'][$i]; 
            $producto_nombre = $DATA['item-producto-descripcion'][$i]; 
            $producto_cantidad = $DATA['item-producto-cantidad'][$i]; 
            $producto_notas = $DATA['item-producto-notas'][$i];
            $producto_cantidad_existente = $DATA['item-producto-cantidad-existente'][$i]; 
            $pedido_recepciona_cantidad = $DATA['recepciona_cantidad'][$i];
            $pedido_recepciona_notas = $DATA['recepciona_notas'][$i];

            $items = $modeloStockMovimientos->createRow(array(
                    'fecha' => new Zend_Db_Expr('NOW()'),
                    'efector_id' => $this->efector_activo['id_efector'],
                    'pedido_id' => $pedido_id,           
                    'tipomovimientoajuste_id' => $tipomovimientoajuste_id,
                    'producto_id' => $producto_id,
                    'cantidad' => $pedido_recepciona_cantidad,
                    'notas' => $pedido_recepciona_notas,
                    ))->save();
                       
                    // armado del array para guardar el JSON en la tabla LOG
                   $strJSON .=  '"linea_' . $i . '":{"producto_id":"' . $producto_id .'","descripcion":"'. $producto_nombre .'","cantidad":"'. $pedido_recepciona_cantidad .'","notas":"'. $pedido_recepciona_notas .'"},' ;                       
            
        }               
        
        $strJSON = '{' . $strJSON .   
                '"header":{"fecha":"' . $fecha_registracion . '","observaciones":"' . $recepcionar_observaciones . '"}}';
             
       // FORMATEAR JSON PARA INSERTAR EN LOG         
       $itemsJSON = stripslashes(json_encode($strJSON, JSON_UNESCAPED_SLASHES));        
       $itemsJSON = substr($itemsJSON,1); // remueve primer comilla
       $itemsJSON = substr($itemsJSON,0,-1); // remueve ultima comilla


        // INSERTA EN PEDIDO LOG 
        // lleva una auditoria del pedido por cada instancia (datos con JSON)
        $modeloLog = new Stock_models_Pedidosinsumoslog();
        $log_id = $modeloLog->createRow(array(
                    'efector_id' => $this->efector_activo['id_efector'],
                    'fecha' => new Zend_Db_Expr('NOW()'),
                    'pedido_id' => $pedido_id,
                    'accion' => 'Recepcion del Pedido',
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'datos' => $itemsJSON, //$_POST['datos'],
                ))->save();
        
       // ------------------------------------------------------------
       // Asigna nuevamente los valores para llamar a la vista LISTAR
       // ------------------------------------------------------------
        $perPage = 18;
        $fecha = null;
       
        $modelo_usuarios = new Acceso_models_Usuarios();

        $resultados = $modeloEncabezado->getPedidosinsumosByEfe($fecha, $this->page, $perPage, $estado, $efector_id);
        $resultados_array = $resultados->toArray();
        foreach ($resultados_array as &$encabezado_usuario){  
            $row = $modelo_usuarios->find($encabezado_usuario['usuario_id'])->current();
            $encabezado_usuario['usu_nombre'] = $row->usu_nombre;   
        }

        if (sizeof($resultados) > 0) {
            $total = count($resultados_array);
            $paginas = ceil($total / $perPage);
        }

        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;
        $this->view->resultados = $resultados_array;
        $this->view->paginas = $paginas;    
        
        $this->renderScript('pedidosinsumos/listar.phtml');        
        
    }    

    
    
    
    /**
     * CERRAR PEDIDO (finalizar o cerrarlo)
     * 
     * cambia el estado a 4, solo aquellos que tienen Recepcion (estado 3)
     */
    public function cerrarAction() {

        $pedido_id = (int) $this->getRequest()->getParam('id');
        $estado = 4;
        $fecha_registracion = date('Y-m-d hh:mm:ss');
        $cerrar_observaciones = "Pedido Cerrado/Finalizado";
                
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];        

        /* ENCABEZADO */
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $modeloEncabezado = new Stock_models_Pedidosinsumos();

        $data = array('estado' => $estado );
        $where = array( 'id = ?' => $pedido_id );
        $adapter->update('pedidos_insumos', $data, $where);
        
        
        /* DETALLE */
        $modeloDetalle = new Stock_models_Pedidosinsumosdetalle();        
        // obtener detalle del pedido.
        $resultados = $modeloDetalle->getPedidosinsumosdetalleById($pedido_id);
        $c = 0;
                
        foreach ($resultados as $i) {
            $registro_id = $i['id']; 
            $producto_id = $i['producto_id']; 
            $producto_nombre = $i['nombre_producto']; 
            $producto_cantidad = $i['cantidad']; 
            $producto_notas = $i['notas'];

            // armado del array para guardar el JSON en la tabla LOG
            $strJSON .=  '"linea_' . $c . '":{"producto_id":"' . $producto_id .'","descripcion":"'. $producto_nombre .'","cantidad":"'. $producto_cantidad .'","notas":"'. $producto_notas .'"},' ;                       
            $c++;
        }               

        $strJSON = '{' . $strJSON .   
                '"header":{"fecha":"' . $fecha_registracion . '","observaciones":"' . $cerrar_observaciones . '"}}';
             
       // FORMATEAR JSON PARA INSERTAR EN LOG         
       $itemsJSON = stripslashes(json_encode($strJSON, JSON_UNESCAPED_SLASHES));        
       $itemsJSON = substr($itemsJSON,1); // remueve primer comilla
       $itemsJSON = substr($itemsJSON,0,-1); // remueve ultima comilla

        // INSERTA EN PEDIDO LOG 
        // lleva una auditoria del pedido por cada instancia (datos con JSON)
        $modeloLog = new Stock_models_Pedidosinsumoslog();
        $log_id = $modeloLog->createRow(array(
                    'efector_id' => $this->efector_activo['id_efector'],
                    'fecha' => $fecha_registracion,
                    'pedido_id' => $pedido_id,
                    'fecha' => new Zend_Db_Expr('NOW()'),
                    'accion' => 'Cierre del Pedido',
                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                    'datos' => $itemsJSON,
                ))->save();        
        

        $this->_helper->json(array('st' => 'ok'));
    }

    
    /**
     * Cancelar el pedido
     * 
     * cambia el estado a 5 de aquellos que solo estan "Generados"
     */
    public function cancelarAction() {

        $pedido_id = (int) $this->getRequest()->getParam('id');
        $estado = 5;

        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];        
        
        /* ENCABEZADO */
        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        
        $modeloEncabezado = new Stock_models_Pedidosinsumos();

        $data = array('estado' => $estado );
        $where = array( 'id = ?' => $pedido_id  );
        $adapter->update('pedidos_insumos', $data, $where);

        $this->_helper->json(array('st' => 'ok'));
    }




    /*     
     * * *********************************************************
     *  *****  Ver listado del pedido en PDF ****
     * ******************************************************** */

    public function verpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $fecha = $this->getRequest()->getParam('fecha');
        $estado = $this->getRequest()->getParam('estado');

        // TODO: revisar entre el valor por defecto y lo enviado por parametro
        //$efector_id = $this->efector_activo['id_efector'];                   
        //$efector_nombre = $this->efector_activo['nomest'];               

        $efector_id = $this->getRequest()->getParam('efector_id');
        
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }

        
        
        if ($this->getRequest()->getParam('estado') == 0) {
            $estado_filtro = "Todos";
        } else {
            if ($this->getRequest()->getParam('estado') == 1) {
                $estado_filtro = "Generado";
            } elseif ($this->getRequest()->getParam('estado') == 2) {
                $estado_filtro = "Enviado";
            } elseif ($this->getRequest()->getParam('estado') == 3) {
                $estado_filtro = "Recepciones Parciales";
            } elseif ($this->getRequest()->getParam('estado') == 4) {
                $estado_filtro = "Cerrados";
            } elseif ($this->getRequest()->getParam('estado') == 5) {
                $estado_filtro = "Cancelado";
            } elseif ($this->getRequest()->getParam('estado') == 6) {
                $estado_filtro = "Pend. QA";
            }
        }


        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $resultados = $modeloEncabezado->getPedidosinsumosByEfe($fecha, $this->page, $perPage, $estado, $efector_id)->toArray();

        $modelo_usuarios = new Acceso_models_Usuarios();
        
        foreach ($resultados as &$encabezado_usuario){  
            $row = $modelo_usuarios->find($encabezado_usuario['usuario_id'])->current();
            $encabezado_usuario['usu_nombre'] = $row->usu_nombre;   
        }        

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8';
        $y = $page->getHeight();
        $xi = 0;
        $titulo = 'Pedidos de Insumos y Productos - ' . $estado_filtro;
        $efector = $this->efector_activo['nomest'];
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha Reporte: ' . date("d-m-Y h:m:s") ." hs.";

        $page
                /**
                 * Logo
                 */
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
                ->setFont($font, 14)
                ->drawText($titulo, 100, $y - 25, $encoding)
                ->setFont($font, 11)
                ->drawText($dateRPT, 100, $y - 45, $encoding)
                ->drawText($efector, 350, $y - 45, $encoding)
                ->setFont($font, 11)

                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea columna1
                ->drawLine(133, $y - 55, 133, $y - 500) // Vertical fecha
                ->drawLine(225, $y - 55, 225, $y - 500) // Vertical estado
                ->drawLine(620, $y - 55, 620, $y - 500) // Vertical observaciones
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical ultima lateral derecho
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)
                // titulos columnas                      
                ->drawText('Fecha', 30, $y - 70, $encoding)
                ->drawText('Estado', 140, $y - 70, $encoding)
                ->drawText('Observaciones', 250, $y - 70, $encoding)
                ->drawText('Generado por', 640, $y - 70, $encoding)
        ;

        /**
         * Cuerpo del reporte
         * Cambiar de página cuando excede
         */        
        foreach ($resultados as $i => $v) {

            if ($v['estado'] == 1) {
                $estado_solicitud = "Generado";
            } elseif ($v['estado'] == 2) {
                $estado_solicitud = "Enviado";
            } elseif ($v['estado'] == 3) {
                $estado_solicitud = "Recep. Parcial";
            } elseif ($v['estado'] == 4) {
                $estado_solicitud = "Cerrado";
            } elseif ($v['estado'] == 5) {
                $estado_solicitud = "Cancelado";
            } elseif ($v['estado'] == 6) {
                $estado_solicitud = "Pend. QA";
            }            
            
            $fecha = $v['fecha'];
            $usuario = $v['usu_nombre'];
            $observaciones = sprintf("%10.31s", $v['observaciones']);


            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 11)
                        ->drawText($fecha, 30, $y - 90 - $h, $encoding)
                        ->drawText($estado_solicitud, 140, $y - 90 - $h, $encoding)
                        ->drawText($observaciones, 230, $y - 90 - $h, $encoding)
                        ->drawText($usuario, 637, $y - 90 - $h, $encoding) //hacer break
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 11)
                        ->drawText($fecha, 30, $y - 90 - $h, $encoding)
                        ->drawText($estado_solicitud, 140, $y - 90 - $h, $encoding)
                        ->drawText($observaciones, 230, $y - 90 - $h, $encoding)
                        ->drawText($usuario, 637, $y - 90 - $h, $encoding) //hacer break
                ;
                $h2 = $h2 + 20;
            }
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    
    

    
    /*     * *********************************************************
     *  *****  PDF del pedido generado
     * ******************************************************** */

    public function verpedidoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $pedido_id = $this->getRequest()->getParam('id');
        $fecha = $this->getRequest()->getParam('fecha');
        $efector_id = $this->getRequest()->getParam('efector_id');

        // TODO: revisar entre el valor por defecto y lo enviado por parametro
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];        

        $modeloEncabezado = new Stock_models_Pedidosinsumos();
        $resultadosEncabezado = $modeloEncabezado->getPedidosinsumosById($pedido_id)->toArray();

        $modeloDetalle = new Stock_models_Pedidosinsumosdetalle();
        $resultadosDetalle = $modeloDetalle->getPedidosinsumosdetalleById($pedido_id)->toArray();

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8';
        $y = $page->getHeight();
        $xi = 0;
        $titulo = '[X]  Pedido de Insumos';
        $efector = $this->efector_activo['nomest'];
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha: ' . date("d-m-Y");

        $page
                /**
                 * Logo
                 */
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
                ->setFont($font, 14)
                ->drawText($titulo, 350, $y - 25, $encoding)
                ->setFont($font, 11)
                ->drawText($dateRPT, 680, $y - 25, $encoding)
                ->drawText($efector, 350, $y - 45, $encoding)
                ->setFont($font, 11)

                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea cantidad (columna1)
                ->drawLine(133, $y - 55, 133, $y - 500) // Vertical producto
                ->drawLine(520, $y - 55, 520, $y - 500) // Vertical notas
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical ultima lateral derecho
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)
                // titulos columnas                      
                ->drawText('Cantidad', 30, $y - 70, $encoding)
                ->drawText('Insumos / Productos', 150, $y - 70, $encoding)
                ->drawText('Notas', 540, $y - 70, $encoding)
//			->drawText('Observaciones', 605, $y-70, $encoding)
        ;

        // Completado del reporte
        // Cambiar de página cuando excede ----
        foreach ($resultadosDetalle as $i => $v) {

            $cantidad = $v['cantidad'];
            $producto = $v['nombre_producto'];
            $notas = sprintf("%10.31s", $v['notas']);


            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 11)
                        ->drawText($cantidad, 50, $y - 90 - $h, $encoding)
                        ->drawText($producto, 150, $y - 90 - $h, $encoding)
                        ->drawText($notas, 530, $y - 90 - $h, $encoding) //hacer break
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 11)
                        ->drawText($cantidad, 50, $y - 90 - $h, $encoding)
                        ->drawText($producto, 150, $y - 90 - $h, $encoding)
                        ->drawText($notas, 530, $y - 90 - $h, $encoding) //hacer break
                ;
                $h2 = $h2 + 20;
            }
        }

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }    
}
