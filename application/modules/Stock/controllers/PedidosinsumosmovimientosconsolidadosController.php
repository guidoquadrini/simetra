<?php

class Stock_PedidosinsumosmovimientosconsolidadosController extends BootPoint {

  public function indexAction() {

   }
    
   
    /*     
     * * *********************************************************
     *  ***** Ver listado del stock consolidado del efector en PDF ****
     * 
     * // EXISTENCIA DEL STOCK CONSOLIDADO DEL EFECTOR ACTIVO
     * ******************************************************** */

    public function verpdfstockexistenciaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];

        $perPage = 18;
        
        $modeloInsumosConsolidados = new Stock_models_Pedidosinsumosmovimientosconsolidados();
        
        // obtiene inusmos consolidados del efector
        $resultadoInsumosConsolidados = $modeloInsumosConsolidados->getInsumosEnStockConsolidados($efector_id);
            
        $resultados_array = $resultadoInsumosConsolidados->toArray();

        foreach ($resultados_array as &$cantidad_stock){  
            $row = $modeloInsumosConsolidados->getInsumoExistenciaById($efector_id, $cantidad_stock['producto_id'])->current();
            $cantidad_stock['cantidad'] = $row['cantidad_existente'];   
            $cantidad_stock['nombre_producto'] = $row['nombre_producto']; 
            $cantidad_stock['ultimo_costo'] = $row['ultimo_costo'];    
        }

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8';
        $y = $page->getHeight();
        $xi = 0;
        $titulo = 'Stock Consolidado';
        $efector = $efector_nombre;
        $h = 0;
        $h2 = 0;
        $dateRPT = 'Fecha Reporte: ' . date("d-m-Y h:m:s") ." hs.";

        $page
                /**
                 * Logo
                 */
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
                ->setFont($font, 14)
                ->drawText($titulo, 100, $y - 25, $encoding)
                ->setFont($font, 11)
                ->drawText($dateRPT, 100, $y - 45, $encoding)
                ->drawText($efector, 350, $y - 45, $encoding)
                ->setFont($font, 11)

                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(16, $y - 55, 810, $y - 55)
                ->drawLine(16, $y - 75, 810, $y - 75)
                ->drawLine(16, $y - 55, 16, $y - 500) // linea columna1
                ->drawLine(225, $y - 55, 225, $y - 500) // Vertical insumo
                ->drawLine(350, $y - 55, 350, $y - 500) // Vertical cantidad
                ->drawLine(810, $y - 55, 810, $y - 500) // Vertical ultima lateral derecho
                ->drawLine(16, $y - 500, 810, $y - 500) // Horizontal cierre columnas (footer)
                // titulos columnas                      
                ->drawText('Insumo', 30, $y - 70, $encoding)
                ->drawText('Stock Actual', 240, $y - 70, $encoding)
                ->drawText('Ultimo Costo', 360, $y - 70, $encoding)
                //->drawText('Generado por', 640, $y - 70, $encoding)
        ;

        /**
         * Cuerpo del reporte
         * Cambiar de página cuando excede
         */        
        foreach ($resultados_array as $i => $v) {
           
            $producto = $v['nombre_producto'];
            $cantidad = $v['cantidad'];
            $costo    = $v['ultimo_costo']; 

            if ($h < ($y + 40)) {
                $page
                        ->setFont($font, 11)
                        ->drawText($producto, 30, $y - 90 - $h, $encoding)
                        ->drawText($cantidad, 230, $y - 90 - $h, $encoding)
                        ->drawText($costo, 360, $y - 90 - $h, $encoding)
                ;
                $h = $h + 20;
            } else {
                $page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
                $page2
                        ->setFont($font, 11)
                        ->drawText($producto, 30, $y - 90 - $h, $encoding)
                        ->drawText($cantidad, 230, $y - 90 - $h, $encoding)
                        ->drawText($costo, 360, $y - 90 - $h, $encoding)
                ;
                $h2 = $h2 + 20;
            }
        }          
       
        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

      
   
    
}
