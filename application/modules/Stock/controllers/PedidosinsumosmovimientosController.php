<?php

class Stock_PedidosinsumosmovimientosController extends BootPoint {

  public function indexAction() {
 

 }
    
 
 
 
    /**
     * CONSOLIDAR - Formulario
     *
     */
    public function consolidarAction() {
        
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;
        
        $this->renderScript('pedidosinsumosmovimientos/consolidar-stock.phtml');
    }     
   
 
    /**
     * PROCESO DE CONSOLIDACION DE STOCK
     *
     */
    public function procesoconsolidarAction() {

        $efector_id = $this->efector_activo['id_efector'];
        $efector_nombre = $this->efector_activo['nomest'];
        
        $modeloStockMovimientos = new Stock_models_Pedidosinsumosmovimientos();    
        $modeloStockConsolidado = new Stock_models_Pedidosinsumosmovimientosconsolidados();
        
        // obtiene la cantidad (existencia) de cada insumo (positivos y negativos) con un SUM
        // para asi saber la existencia -SIN CONSOLIDAR- de los movimientos (Pedidos, ajustes, etc)
        //            [0] => Array
        //                (
        //                    [producto_id] => 1
        //                    [existencia_actual] => 37.00
        //                )
        $resultadoCantidadInsumosSinConsolidar = $modeloStockMovimientos->getCantidadSinConsolidar($efector_id); 

        // si existen insumos sin consolidar (movimientos) procesa, sino retorna nok        
        if (sizeof($resultadoCantidadInsumosSinConsolidar) > 0) {
            
            // LOOP para los registros sin consolidar por producto (movimientos)
            foreach ($resultadoCantidadInsumosSinConsolidar as $i) {

                $producto_id = $i['producto_id'];
                $cantidad_existente = $i['existencia_actual'];
                
                // INSERT en tabla de consolidacion con la cantidad del producto
                $consolidado_id = $modeloStockConsolidado->createRow(array(
                            'fecha' => new Zend_Db_Expr('NOW()'),
                            'efector_id' => $this->efector_activo['id_efector'],
                            'producto_id' => $producto_id,
                            'cantidad' => $cantidad_existente,
                            'ulitmo_id' => 0
                        ))->save();           

                // Retorna la fecha/hora del registro de consolidado del producto 
                $resultadosConsolidado = $modeloStockConsolidado->getFechaConsolidacion($consolidado_id);
                foreach ($resultadosConsolidado as $j => $f) {
                    $fecha_consolidado = $f['fecha'];
                }

                $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();        

                // retorna todos registros sin consolidar para un efector y un producto              
                $resultadosMovimientosSinConsolidar = $modeloStockMovimientos->getInsumoSinConsolidar($efector_id, $producto_id);  
                
                // para cada registro de un producto "sin consolidar" se actualiza la fecha de consolidacion
                foreach ($resultadosMovimientosSinConsolidar as $c) {
                        $id_movimiento = $c['id'];
                        $data = array('fecha_consolidado' => $fecha_consolidado );
                        $where = array(
                            'id = ?' => $id_movimiento
                        );
                       
                        $adapter->update('stock_insumos_movimientos', $data, $where);
                }
               
            }            

             $this->_helper->json(array('st' => 'ok'));
            
        } else {
             $this->_helper->json(array('st' => 'nok'));
        }
 

    } 
 
    
    
    
    /**
    * AJUSTE Pedidos
    *
    */
    public function ajusteAction() {
               
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];
        
        $perPage = 18;
        
        
        // consolidar 
        //$resultadoConsolidado = $this->procesoconsolidarAction();
        
        // TODO: ver la respuesta JSON de la funcion de consolidar como procesarla para 
        // que siga su curso o no
        // RESULTADO DEVUELTO
        //{"st":"ok"}   // {"st":"nok"}
        

        // ESTE QUERY NO SIRVE X QUE TRAE TODOS LOS PRODUCTOS ID y solo se necesita el ultimo consolidado
        //    SELECT producto_id, cantidad
        //      FROM stock_insumos_movimientos_consolidados
        //     WHERE producto_id IN 
        //           (SELECT producto_id 
        //              FROM productos) 
        //AND efector_id = 785
       
       
        $modeloInsumos = new Stock_models_Productos();
        $resultados = $modeloInsumos->getInsumos();
        
        $modeloInsumosConsolidados = new Stock_models_Pedidosinsumosmovimientosconsolidados();
        $resultados_array = $resultados->toArray();

        // por cada producto/insumo obtiene la ultima existencia consolidada de los movimientos
        // para asignarle la cantidad existente de stock

        for ($i = 0 ; $i<count($resultados_array)-1;$i++){  
            $producto_id = $resultados_array[$i]['id'];       
            $row = $modeloInsumosConsolidados->getInsumoExistenciaById($efector_id, $producto_id)->current();         
            // al venir valors NULL de los productos/insumos, asigna el valor del objeto al array
            $resultados_array[$i]['cantidad_existente'] = $row->cantidad_existente;                    
        } 

        if (sizeof($resultadoInsumosConsolidados) > 0) {
            $total = count($resultados_array);
            $paginas = ceil($total / $perPage);
        }            
        
        
        $modeloTiposAjusteStock = new Stock_models_Tiposmovimientosstock();
        $resultadosTipos = $modeloTiposAjusteStock->getPairs();
        
        $this->view->efector = $efector_id;
        $this->view->efector_nombre = $efector_nombre;        
        $this->view->resultados = $resultados_array;        
        $this->view->resultadosTipos = $resultadosTipos;        
        $this->view->paginas = $paginas;
        
        $this->renderScript('pedidosinsumosmovimientos/ajuste-manual.phtml');
    }  
        
    
    
    /**
     * PROCESO DE AJUSTE de Insumos
     *
     */
    public function procesoajusteAction() {

        $DATA = $this->getRequest()->getParams();

        $fecha_registracion = date('Y-m-d hh:mm:ss');
        $ajuste_observaciones = "Ajuste Manual de Stock";
         
        $efector_id = $this->efector_activo['id_efector'];                   
        $efector_nombre = $this->efector_activo['nomest'];    

//Array
//(
//    [module] => stock
//    [controller] => pedidosinsumosmovimientos
//    [action] => procesoajuste
//    [producto_id] => 2
//    [nombre_producto] => Guantes de Latex por 100 Und.
//    [cantidad_existente] => 4.00
//    [cantidad_ajuste] => 33
//    [tipo] => 2
//    [notas] => AAA
//)          
        
        // TODO: ajuste manual del movimiento INCOMPLETO        
        if( $DATA['cantidad_ajuste'] <> ''){
            
            // INSERTA EN STOCK INSUMO MOVIMIENTO
            // se genera un movimiento para el producto / insumo que se ajusta en el movimiento 
            $modeloStockMovimientos = new Stock_models_Pedidosinsumosmovimientos();

                $producto_id = $DATA['producto_id']; 
                $producto_nombre = $DATA['nombre_producto']; 
                $producto_cantidad_existente = $DATA['cantidad_existente'];             
                $producto_cantidad = $DATA['cantidad_ajuste']; 
                $producto_notas = $DATA['notas'];
                $tipomovimientoajuste_id = $DATA['tipo'];
                
                //$cantidad_total = $DATA['cantidad_existente'] + $DATA['cantidad_ajuste'];
               
                $items = $modeloStockMovimientos->createRow(array(
                        'fecha' => new Zend_Db_Expr('NOW()'),
                        'efector_id' => $this->efector_activo['id_efector'],
                        'pedido_id' => 0,           
                        'tipomovimientoajuste_id' => $tipomovimientoajuste_id,
                        'producto_id' => $producto_id,
                        'cantidad' => $producto_cantidad,
                        'notas' => $producto_notas,
                        ))->save();

                        // armado del array para guardar el JSON en la tabla LOG
                       $strJSON .=  '"linea_' . 0 . '":{"producto_id":"' . $producto_id .'","descripcion":"'. $producto_nombre .'","cantidad":"'. $producto_cantidad .'","notas":"'. $producto_notas .'"},' ;                       

            $strJSON = '{' . $strJSON .   
                    '"header":{"fecha":"' . $fecha_registracion . '","observaciones":"' . $ajuste_observaciones . '"}}';

           // FORMATEAR JSON PARA INSERTAR EN LOG         
           $itemsJSON = stripslashes(json_encode($strJSON, JSON_UNESCAPED_SLASHES));        
           $itemsJSON = substr($itemsJSON,1); // remueve primer comilla
           $itemsJSON = substr($itemsJSON,0,-1); // remueve ultima comilla        


            // INSERTA EN PEDIDO LOG 
            // lleva una auditoria del pedido por cada instancia (datos con JSON)
            $modeloLog = new Stock_models_Pedidosinsumoslog();
            $log_id = $modeloLog->createRow(array(
                        'efector_id' => $this->efector_activo['id_efector'],
                        'pedido_id' => 0,
                        'fecha' =>  new Zend_Db_Expr('NOW()'),
                        'accion' => 'Ajuste Manual del Insumo',
                        'usuario_id' => $this->auth->getIdentity()->usu_id,
                        'datos' => $itemsJSON, //$_POST['datos'],
                    ))->save();            
            
            $this->_helper->json(array('st' => 'ok'));
            
        } else {
            
           $this->_helper->json(array('st' => 'nok')); 
        }
        

        

    }

        
    
}
