<?php

class Stock_TiposmovimientosstockController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
		$model = new Stock_models_Tiposmovimientosstock();
		$resultados = $model->fetchAll(null,'tipo_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
		$form = new Stock_forms_tiposmovimientosstock();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Stock_models_Tiposmovimientosstock(s);
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*
	*/
	public function modificarAction(){
		$id = $this->getRequest()->getParam('tipo_id');
		$form = new Stock_forms_tiposmovimientosstock();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Stock_models_Tiposmovimientosstock();
				$model->find($id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Stock_models_Tiposmovimientosstock();
			$form->populate($model->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

    public function eliminarAction(){
        $tipo_id = $this->getRequest()->getParam('tipo_id');

        $model = new Stock_models_Tiposmovimientosstock();
        $model->find($tipo_id)->current()->delete();

        $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
        $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
    }   
}

