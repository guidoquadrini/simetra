<?php

class Stock_InsumosController extends BootPoint {

  public function indexAction() {

//            $model = new Stock_models_SolicitudesStock();
//            $resultados = $model->fetchAll(null,'stcok')->toArray();
//            $this->view->resultados = $resultados;        
        
        
        /**
         * Buscar Solicitudes de Stock por Efector
         */
        $fecha = $this->getRequest()->getParam('fecha');
        //$nro = $this->getRequest()->getParam('nro');
        $efector_id = $this->getRequest()->getParam('efector_id');
        $hem_id = $this->getRequest()->getParam('hem_id');
        $estado = $this->getRequest()->getParam('estado');

        if ($this->getRequest()->getParam('estado') == "0")
            $estado = null;

        if (is_null($this->getRequest()->getParam('estado')))
            $estado = 1; /* Por defecto busca los pendientes */

        $perPage = 18;

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e)
            $efes_ids[] = $e['id_efector'];
        /* Fin efectores usuario */

        $modelo = new Stock_models_SolicitudesStock();
        //llamada con efes_ids en vez de efector_id
        $resultados = $modelo->getStockByEfe($fecha, $this->page, $perPage, $nro, $efes_ids, $estado, $hem_id);
        $this->view->busqueda = $fecha;

        if (sizeof($resultados) > 0) {
            $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $this->view->hem = $hem_id;
        $this->view->estado = $estado;
        $this->view->fecha = $fecha;
        //$this->view->nro = $nro;
        $this->view->efector = $efector_id;
        $this->view->resultados = $resultados;
        $this->view->paginas = $paginas;        
    }
    
    
    
   	/**
	* Transferencia
	*
	*/
	public function pedidoAction(){

                 $estado = 1; /* Por defecto busca los pendientes */

                 $perPage = 18;

                 /* Obtengo los efectores del usuario */
                 $modeloPry = new Acceso_models_Proyectos();
                 $nom = "Banco de sangre";
                 $pry = $modeloPry->getByNom($nom);

                 $pry_id = $pry->pry_id;
                 $modeloUsu = new Acceso_models_Usuarios();
                 $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

//                 foreach ($efes_usu as $e)
//                     $efes_ids[] = $e['id_efector'];
//                 /* Fin efectores usuario */
//
//                 $modelo = new Stock_models_productos();
//                 //llamada con efes_ids en vez de efector_id
//                 $resultados = $modelo->getPairs();
//                 $this->view->resultados = $resultados;

                 if (sizeof($resultados) > 0) {
                     $total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
                     $paginas = ceil($total / $perPage);
                 }

                 $this->renderScript('insumos/pedido.phtml');
	}    
 
    
    
    
    
    
    
    public function confirmarAction() {
        
    }

    public function eliminar_row() {
        
    }

    public function informeAction() {
        
    }

    public function validarAction() {
        
    }

    public function enviosAction() {
        
    }

    public function historialAction() {
        
    }

    public function irradiarAction() {
        
    }

    public function enviarAction() {
        
    }

    public function pdfEnvioAction() {
        
    }

    public function agregarAction() {
        
    }

    public function verpdfAction() {
        
    }


    public function descarteAction() {
        
    }


    public function descartarAction() {
        
    }

}
