<?php

class Stock_PedidosinsumosdetalleController extends BootPoint {

    public function indexAction() {
        
    }

    /*
     * Obtiene los insumos del detalle del pedido y retorna un JSON
     * 
     */

    public function getDetalleByIdAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $vRet = null;
        $pedido_id = $this->getRequest()->getParam('id');
        $modelo = new Stock_models_Pedidosinsumosdetalle();
        $resultados = $modelo->getPedidosinsumosdetalleById($pedido_id);
        if (!$resultados) {
            $vRet = array('st' => 'nok');
        } else {
            $vRet = array(
                'st' => 'ok',
                'data' => $resultados->toArray());
        }              
        $this->_helper->json($vRet);
    }
}
