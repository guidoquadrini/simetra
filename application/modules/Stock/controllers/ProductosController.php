<?php

class Stock_ProductosController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){
            // TODO: colocar un metodo en el MODELO PRODUCTO para q traiga asociado el nombre de la categoria
		$model = new Stock_models_Productos();
		$resultados = $model->fetchAll(null,'nombre_producto')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
		$form = new Stock_forms_Productos();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Stock_models_Productos();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*
	*/
	public function modificarAction(){
		$id = $this->getRequest()->getParam('id');
		$form = new Stock_forms_Productos();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Stock_models_Productos();
				$model->find($id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Stock_models_Productos();
			$form->populate($model->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

    public function eliminarAction(){
        $pro_id = $this->getRequest()->getParam('id');

        $model = new Sotck_models_Productos();
        $model->find($pro_id)->current()->delete();

        $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
        $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
    }   
    
    
    
    	/**
	* Nuevo (viene del formulario de pedidos)
	*
	*/
	public function nuevoAction(){
            
            $modelo = new Stock_models_Productos();
            
            $modelo->createRow(array(
                        'productocategoria_id' => $this->getRequest()->getParam('productocategoria_id'),
                        'codigo' => $this->getRequest()->getParam('codigo'),
                        'nombre_producto' => $this->getRequest()->getParam('nombre_producto'),
                        'fecha_alta' => new Zend_Db_Expr('NOW()'),
                        'usuario_id' => $this->auth->getIdentity()->usu_id,
                    ))->save();

            $this->_helper->json(array('st' => 'ok'));             

	}
    
    
}

