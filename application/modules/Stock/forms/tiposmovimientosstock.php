<?php

class Stock_forms_tiposmovimientosstock extends Zend_Form {

	public function init(){
           
		$this->addElement($this->createElement('text','tipo_descripcion')
		->setLabel('Nombre')
		->setAttribs(array(
			'required' => 'required',
			'maxlength' => 50
		))
		->setRequired(true));            

  		$this->addElement($this->createElement('text','tipo_signo')
		->setLabel('Signo (+ / -)')
		->setAttribs(array(
			'required' => 'required',
			'maxlength' => 1
		))
		->setRequired(true));         

	}
}
