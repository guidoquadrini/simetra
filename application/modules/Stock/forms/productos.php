<?php

class Stock_forms_productos extends Zend_Form {

    public function init() {

        $this->addElement($this->createElement('text', 'codigo')
                        ->setLabel('Código')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 10
                        ))
                        ->setRequired(true));


        $this->addElement($this->createElement('text', 'nombre_producto')
                        ->setLabel('Nombre')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 100
                        ))
                        ->setRequired(true));

        $this->addElement($this->createElement('text', 'ultimo_costo')
                        ->setLabel('Ultimo Costo')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 20
                        ))
                        ->setRequired(true));

        // relacional tabla productos_categorias
        $categorias = new Stock_models_Productoscategorias();
        $categorias_list = $categorias->getPairs();

        $this->addElement($this->createElement('select', 'productocategoria_id')
                        ->setLabel('Rubro del Insumo')
                        ->setAttrib('required', 'required')
                        ->addMultiOptions($categorias_list)
                        ->setRequired(true));
    }

}
