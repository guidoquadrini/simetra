<?php

class Stock_models_Pedidosinsumoslog extends Zend_Db_Table_Abstract {

    protected $_name = 'pedidos_insumos_log';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    public function indexAction() {

    }
    
    
    public function getByID($id) {
        return $this->fetchRow($this->select(true)->where('id=?', $id));
    }        

    public function getPedidosinsumoslogById($pedido_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'pedidos_insumos_detalle.id',
            'pedidos_insumos_detalle.pedido_id',
            'pedidos_insumos_detalle.fecha',                       
            'pedidos_insumos_detalle.accion',            
            'pedidos_insumos_log.usuario_id',  
            'pedidos_insumos_log.datos',   
        ));     
        
        if ($pedido_id != null)
            $query = $query->where("pedidos_insumos_log.pedido_id IN (?)", $pedido_id);

        $query = $query->order('pedidos_insumos_log.fecha');

        return $this->fetchAll($query);       
    }
    

    
    
    public function updateEstado($id, $estado) {
                $sql = "UPDATE pedidos_insumos_log SET estado='" . $estado . "' WHRE id ='" . $id . "'";
                $this->bd->update($sql);
    }
    
}
