<?php

class Stock_models_Pedidosinsumosdetalle extends Zend_Db_Table_Abstract {

    protected $_name = 'pedidos_insumos_detalle';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    public function indexAction() {

    }
    
    
    public function getByID($id) {
        return $this->fetchRow($this->select(true)->where('id=?', $id));
    }        

    
    
    /*
     * 
     */
    
    public function getInsumosDetalleById($pedido_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            'pedidos_insumos_detalle.id',        
            'pedidos_insumos_detalle.producto_id',        
            'pedidos_insumos_detalle.cantidad',
            'pedidos_insumos_detalle.importe',            
            'productos.nombre_producto as nombre_producto',
            'pedidos_insumos_detalle.notas', 
        ));     
        
        if ($pedido_id != null)
            $query = $query->where("pedidos_insumos_detalle.pedido_id IN (?)", $pedido_id);

        $query = $query->order('pedidos_insumos_detalle.producto_id');
        $query = $query->joinInner('productos', ' pedidos_insumos_detalle.producto_id=productos.id');

        $query = $query->order('pedidos_insumos_detalle.id');
        
        return $this->fetchAll($query);       
    }
        
    
    
    public function getPedidosinsumosdetalleById($pedido_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'pedidos_insumos_detalle.id as registro_detalle_id',        
            'pedidos_insumos_detalle.producto_id',        
            'pedidos_insumos_detalle.cantidad',
            'pedidos_insumos_detalle.cantidad_enviada',
            'pedidos_insumos_detalle.importe',            
            'productos.nombre_producto as nombre_producto',
            'pedidos_insumos_detalle.notas', 
        ));     
        
        if ($pedido_id != null)
            $query = $query->where("pedidos_insumos_detalle.pedido_id IN (?)", $pedido_id);

            $query = $query->joinInner('productos', ' pedidos_insumos_detalle.producto_id=productos.id');

            $query = $query->order('pedidos_insumos_detalle.id');
        
        return $this->fetchAll($query);       
    }
    

   /*
    * Retorna la cantidad enviada del insumo en el detalle del pedido
    */ 
   public function getCantidadInsumoEnviadaById($id) {  
         $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
           // new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),        
            'pedidos_insumos_detalle.cantidad_enviada',         
        ));     
        
        if ($id != null)
            $query = $query->where("pedidos_insumos_detalle.id IN (?)", $id);        

        return $this->fetchAll($query);     
       
   }
    
   
   
   
   
   
    
    
    public function getPedidosinsumosdetalleCantidadById($pedido_id, $producto_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'pedidos_insumos_detalle.cantidad',         
        ));     
        
        if ($pedido_id != null)
            $query = $query->where("pedidos_insumos_detalle.pedido_id IN (?)", $pedido_id);
        
        if ($producto_id != null)
            $query = $query->where("pedidos_insumos_detalle.producto_id IN (?)", $producto_id);        

        return $this->fetchAll($query);       
    }    
    
    
    public function updateEstado($id, $estado) {
                $sql = "UPDATE pedidos_insumos_detalle SET estado='" . $estado . "' WHRE id ='" . $id . "'";
                $this->bd->update($sql);
    }
    
}
