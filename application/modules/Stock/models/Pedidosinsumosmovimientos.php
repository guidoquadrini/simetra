<?php

class Stock_models_Pedidosinsumosmovimientos extends Zend_Db_Table_Abstract {

    protected $_name = 'stock_insumos_movimientos';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    public function indexAction() {

    }
    
    
    /*
     * Retorna la cantidad de cada producto sumarizado 
     * 
     * (existentes que no tienen fecha de consolidado)
     */    
    public function getCantidadSinConsolidar($efector_id) { 
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
             'producto_id', 
             'SUM(stock_insumos_movimientos.cantidad) as existencia_actual',         
        ));     

        if ($efector_id != null)
            $query = $query->where("stock_insumos_movimientos.efector_id IN (?)", $efector_id);
        
        
          $query = $query->where('stock_insumos_movimientos.fecha_consolidado IS NULL');        
        
          $query = $query->group( 'stock_insumos_movimientos.producto_id' );
        
        return $this->fetchAll($query);  
    }
    
    
   
    /*
     * Retorna aquellos registros que no tienen fecha de consolidado 
     * para el efector activo y el producto
     */
    public function getInsumoSinConsolidar($efector_id, $producto_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
             'id',         
             'fecha',         
             'efector_id',         
             'tipomovimientoajuste_id',         
             'pedido_id',         
             'producto_id',         
             'cantidad', 
             'notas',         
             'fecha_consolidado',         
        ));     
        
        if ($efector_id != null)
            $query = $query->where("stock_insumos_movimientos.efector_id IN (?)", $efector_id);
            
        if ($producto_id != null)
            $query = $query->where("stock_insumos_movimientos.producto_id IN (?)", $producto_id);        
        
        $query = $query->where('stock_insumos_movimientos.fecha_consolidado IS NULL');

        return $this->fetchAll($query);       
    }       
    
   

    /*
    * getCantidadByPedido
    * 
    * retorna la existencia de cada producto que tiene el pedido (solo lo ingresado)
    */
    public function getCantidadByPedido($pedido_id, $tipomovimientoajuste_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
             'pedido_id',       
             'producto_id', 
             'SUM(cantidad) as cantidad_ingresada',         
        ));     

        
        if ($pedido_id != null)
            $query = $query->where("stock_insumos_movimientos.pedido_id IN (?)", $pedido_id);       
        
        if ($tipomovimientoajuste_id != null)
            $query = $query->where("stock_insumos_movimientos.tipomovimientoajuste_id IN (?)", $tipomovimientoajuste_id); 

        //$query = $query->group( 'stock_insumos_movimientos.producto_id' );

        return $this->fetchAll($query);       
    }           
     

    
//    /*
//    *  Retorna la cantidad de un producto para un pedido
//    */
//    public function getCantidadById($pedido_id, $producto_id) {
//        $query = $this->select(true)
//                ->setIntegrityCheck(false)->reset('columns')
//                ->columns(array(
//            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
//             'SUM(stock_insumos_movimientos.cantidad) as existencia_actual',         
//        ));     
//        
//        if ($pedido_id != null)
//            $query = $query->where("stock_insumos_movimientos.pedido_id IN (?)", $pedido_id);
//        
//        if ($producto_id != null)
//            $query = $query->where("stock_insumos_movimientos.producto_id IN (?)", $producto_id);        
//
//        return $this->fetchAll($query);       
//    }          
      
    
    
}
