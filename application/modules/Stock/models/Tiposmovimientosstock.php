<?php

class Stock_models_Tiposmovimientosstock extends Zend_Db_Table_Abstract {

    protected $_name = 'tipos_movimentos_stock';
    protected $_primary = 'tipo_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('tipo_id', 'tipo_descripcion'))
                                //	->where('def=?',1)
                                ->order('tipo_id'));
    }

    
    
        
}
