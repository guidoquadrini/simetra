<?php

class Stock_models_Pedidosinsumosmovimientosconsolidados extends Zend_Db_Table_Abstract {

    protected $_name = 'stock_insumos_movimientos_consolidados';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    public function indexAction() {

    }
    
    
     /*
     *  retorna la fecha y hora de la registracion por el ID
     * 
     */
    public function getInsumosEnStockConsolidados($efector_id){
              $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
             'producto_id', 
        ));     
        
        if ($efector_id != null)
            $query = $query->where("stock_insumos_movimientos_consolidados.efector_id IN (?)", $efector_id);              
       
          $query = $query->group( 'stock_insumos_movimientos_consolidados.producto_id' );        
        
        return $this->fetchAll($query);    
    }
       
    
    
    
     /*
     * Retorna la cantidad EXISTENTE CONSOLIDADA del producto de un efector
     * Ultima condsolidacion 
     * obtener el ultimo registro procesado para el insumo
     */    
    public function getInsumoExistenciaById($efector_id, $producto_id) { 
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
             'stock_insumos_movimientos_consolidados.efector_id',        
             'stock_insumos_movimientos_consolidados.producto_id', 
             'productos.nombre_producto',     
             'productos.ultimo_costo', 
             'stock_insumos_movimientos_consolidados.cantidad as cantidad_existente',         
        ));     

        if ($efector_id != null)
            $query = $query->where("stock_insumos_movimientos_consolidados.efector_id IN (?)", $efector_id);
 
        if ($producto_id != null)
            $query = $query->where("stock_insumos_movimientos_consolidados.producto_id IN (?)", $producto_id);
                
        $query = $query->order('stock_insumos_movimientos_consolidados.producto_id');
        $query = $query->joinInner('productos', ' stock_insumos_movimientos_consolidados.producto_id=productos.id');        
        
        $query = $query->order('stock_insumos_movimientos_consolidados.fecha DESC')->limit(1);
           
        return $this->fetchAll($query);  
    }
    
    
    

    /*
     *  retorna la fecha y hora de la registracion por el ID
     * 
     */
    public function getFechaConsolidacion($id){
              $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
             'fecha', 
        ));     
        
        if ($id != null)
            $query = $query->where("stock_insumos_movimientos_consolidados.id IN (?)", $id);              
       
        return $this->fetchAll($query);    
    }
    

    
    
    public function getByID($id) {
        return $this->fetchRow($this->select(true)->where('id=?', $id));
    }        

    
}
