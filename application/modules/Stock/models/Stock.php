<?php

class Stock_models_Stock extends Zend_Db_Table_Abstract {

    protected $_name = 'stock';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

//    public function getStockList() {
//        $query = $this->select(true)
//            ->reset('columns')->columns(array('hemocomponente_id','COUNT(*) as cantidad'))
//            ->where('estado = ?', 2)
//            ->group('hemocomponente_id');
//
//        return $this->getDefaultAdapter()->fetchPairs($query);
//
//    }


    public function getStockByEfe($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false, $hem_id = null) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(stock.fecha,"%H:%i") as hora_movimiento',
            'DATE_FORMAT(stock.fecha,"%d/%m/%Y %H:%i") as fecha_movimiento',
            'TIMEDIFF(NOW(), stock.fecha) as tiempo_movimiento',
            'stock.cantidad',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
        ));


        if ($hem_id != null)
            $query = $query->where("stock.hemocomponente_id = ?", $hem_id);

        if ($fecha != null)
            $query = $query->where("stock.fecha LIKE '{$fecha}%'");

        if ($don_id != null)
            $query = $query->where("stock.donacion_id =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("stock.efector_id IN (?)", $efe_id);

        if ($todos != null)
            $query = $query->where('stock.estado =?', $todos);

        $query = $query->order('stock.fecha');
        $query = $query->order('stock.estado');
        $query = $query->order('stock.hemocomponente_id');
        $query = $query->joinInner('hem_hemocomponentes', ' stock.hemocomponente_id=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones',' stock.donacion_id=don_donaciones.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

        public function getSolicitudesListByEfe($efes_ids) {
            $query = $this->select(true)->setIntegrityCheck(false)
                ->reset('columns')->columns(array('hemocomponente_id','COUNT(*) as cantidad'))
                ->where('estado = ?', 2)
                ->group('hemocomponente_id');

            $query = $query->where("efe_id IN (?)",$efes_ids);

            $query = $query->joinInner('hem_hemocomponentes','stock.hemocomponente_id=hem_hemocomponentes.hem_id');
            $query = $query->joinInner('don_donaciones',' stock.donacion_id=don_donaciones.don_id');

            return $this->getDefaultAdapter()->fetchPairs($query);
        }
        
        
	public function getallhemocomponentesAction(){
		$model = new Gestion_models_Hemocomponentes();
		$resultados = $model->fetchAll(null,'hem_descripcion')->toArray();

		$this->view->resultados = $resultados;
	}        
        
        
//	// 27.08.2014 agregada condición 'validado = 1'
//    public function getStockListByIDs($don_ids) {
//        $query = $this->select(true)
//            ->reset('columns')->columns(array('hemocomponente_id','COUNT(*) as cantidad'))
//            ->where('estado = ?', 2)
//            ->where('donacion_id IN (?)', $don_ids)
//            ->group('hemocomponente_id');
//
//        return $this->getDefaultAdapter()->fetchPairs($query);
//
//    }
    
    

    
    

//    public function getByEnv($stock_id) {
//        $query = $this->select(true)
//            ->where('id = ?', $stock_id);
//
//        return $this->fetchAll($query)->toArray();
//
//    }
//    public function getByID($id) {
//        $query = $this->select(true)
//            ->reset('columns')->columns(array(
//	            'hemocomponente_id',
//	            'peso_produccion',
//	            'cantidad',
//	            'fecha',
//	            'donacion_id',
//	            'observaciones')
//            )
//            ->where('id = ?', $id);
//
//        return $this->fetchRow($query)->toArray();
//
//    }

}
