<?php

class Stock_models_Pedidosinsumos extends Zend_Db_Table_Abstract {

    protected $_name = 'pedidos_insumos';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    public function indexAction() {

    }
    
    public function getInsumosByEfe() {
        // AQUI INDEX 
    }
    
    
    
     public function getPedidosinsumosByEfe($fecha, $page = 1, $perPage = 10,  $estado, $efector_id ) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(fecha,"%H:%i") as hora',
            'DATE_FORMAT(fecha,"%d/%m/%Y %H:%i") as fecha',
            'TIMEDIFF(NOW(), fecha) as tiempo',
            'usuario_id',        
            'estado',
            'observaciones',     
            'ultima_recepcion_observaciones',        
            'ultima_recepcion_fecha',
            'ultima_recepcion_usuario_id',
        ));     
        
        if ($fecha != null)
            $query = $query->where("pedidos_insumos.fecha LIKE '{$fecha}%'");

        if ($efector_id )
            $query = $query->where("pedidos_insumos.efector_id IN (?)", $efector_id);

        if ($estado > 0 )
            $query = $query->where('pedidos_insumos.estado =?', $estado);


        $query = $query->order('pedidos_insumos.fecha DESC');
        //$query = $query->joinInner('efectores', ' pedidos_insumos.efector_id=efectores.id_efector');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);       
    }




     public function getPedidosinsumosById($id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            'DATE_FORMAT(fecha,"%H:%i") as hora',
            'DATE_FORMAT(fecha,"%d/%m/%Y %H:%i") as fecha',
            'TIMEDIFF(NOW(), fecha) as tiempo',
            'efector_id',
            'estado',
            'observaciones', 
            'DATE_FORMAT(fecha_alta,"%d/%m/%Y %H:%i") as fecha_alta',
            'usuario_id',       
            'ultima_recepcion_observaciones',        
            'ultima_recepcion_fecha',
            'ultima_recepcion_usuario_id',
        ));     

        if ($id != null)
            $query = $query->where("pedidos_insumos.id IN (?)", $id);

        //$query = $query->order('pedidos_insumos.fecha');
        return $this->fetchAll($query);       
    }


    
     public function getEstadoPedido($id) {
         $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            'pedidos_insumos.efector_id', 
            'pedidos_insumos.estado',         
        ));     
        
        if ($id != null)
            $query = $query->where("pedidos_insumos.id IN (?)", $id);        

        return $this->fetchAll($query);   
    }           
    
    
}




