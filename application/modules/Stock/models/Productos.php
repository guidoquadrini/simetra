<?php

class Stock_models_Productos extends Zend_Db_Table_Abstract {

    protected $_name = 'productos';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'nombre_producto', 'productocategoria_id', 'ultimo_costo'))
                                //	->where('def=?',1)
                                ->order('nombre_producto'));
    }

    
    
     public function getInsumoById($id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'id',
            'nombre_producto',
            'productocategoria_id',
            'estado',
            'ultimo_costo',
        ));     

        if ($id != null)
            $query = $query->where("productos.id IN (?)", $id);

        //$query = $query->order('pedidos_insumos.fecha');
        return $this->fetchAll($query);       
    }    
    
    
    
     public function getInsumos() {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'id',
            'nombre_producto',
            'productocategoria_id',
            'estado',
            'ultimo_costo',
        ));     

        return $this->fetchAll($query);       
    }       
}
