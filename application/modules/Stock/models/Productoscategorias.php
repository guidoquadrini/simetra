<?php

class Stock_models_Productoscategorias extends Zend_Db_Table_Abstract {

    protected $_name = 'productos_categorias';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'nombre_categoria'))
                                //	->where('def=?',1)
                                ->order('nombre_categoria'));
    }

    
    
        
}
