<?php

class Stock_models_DescartesStock extends Zend_Db_Table_Abstract {

    protected $_name = 'stock';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    
    // $efes_ids, $fecha, $this->page, $perPage, $estado

      public function getDescartesByEfe($efe_id = null, $fecha, $nro, $estado, $page = 1, $perPage = 10 ) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(stock.fecha,"%H:%i") as hora',
            'DATE_FORMAT(stock.fecha,"%d/%m/%Y %H:%i") as fecha',
            'TIMEDIFF(NOW(), stock.fecha) as tiempo',
            'DATE_FORMAT(stock.fecha_vencimiento,"%d/%m/%Y") as fecha_vencimiento',
            'DATE_FORMAT(stock.fecha_descarte,"%d/%m/%Y") as fecha_descarte',
            'stock.estado',
            'stock.cantidad',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
            'don_donaciones.don_nro as nro',
            'stock.grupo',        
            'stock.factor',        
            'stock.irradiado',     
            'stock.observaciones', 
        ));     
        
//        if ($hem_id != null)
//            $query = $query->where("stock.hemocomponente_id = ?", $hem_id);

//        if ($fecha != null)
//            $query = $query->where("stock.fecha LIKE '{$fecha}%'");
//
//        
//        if ($nro != null)
//            $query = $query->where("stock.nro LIKE '{$nro}%'");       
//        
        if ($efe_id != null)
            $query = $query->where("stock.efector_id IN (?)", $efe_id);

        if ($estado != null)
            $query = $query->where('stock.estado =?', $estado);

        $query = $query->order('stock.fecha');
        //$query = $query->order('stock.estado');
        //$query = $query->order('stock.hemocomponente_id');
        $query = $query->joinInner('hem_hemocomponentes', ' stock.hemocomponente_id=hem_hemocomponentes.hem_id');
        $query = $query->joinInner('don_donaciones',' stock.donacion_id=don_donaciones.don_id');        
        $query = $query->limitPage($page, $perPage);
        
        
//        echo '<pre>';
//        print_r($query);
//        echo '</pre>';die();  

        return $this->fetchAll($query);
       
        //return $this->getAdapter()->fetchAll($query);
        
    }

        public function getDescartesListByEfe($efes_ids) {
            $query = $this->select(true)->setIntegrityCheck(false)
                ->reset('columns')->columns(array('hemocomponente_id','COUNT(*) as cantidad'))
               // ->where('estado = ?', 2)
                ->group('hemocomponente_id');

            $query = $query->where("efe_id IN (?)",$efes_ids);

            $query = $query->joinInner('hem_hemocomponentes',' stock.hemocomponente_id=hem_hemocomponentes.hem_id');
            $query = $query->joinInner('don_donaciones',' stock.donacion_id=don_donaciones.don_id');        

            
            // TODO: FALTA JOIN INNER CON LA TABLA DE USUARIOS
                
            return $this->getDefaultAdapter()->fetchPairs($query);
        }
        
        
	public function getallhemocomponentes(){
		$model = new Gestion_models_Hemocomponentes();
		//$resultados = $model->fetchAll('hem_id','hem_descripcion')->toArray();
                //$this->view->resultados = $resultados;
                
                $resultados = $model->getPairs();
                
                return $this->fetchAll($resultados);
		
	}        
        
  
        
        
        public function getHemocomponentes() {
//            $query = $this->select(true)
//            ->reset('columns')->columns(array('hem_id', 'hem_descripcion'))
//            ->where('hem_estado = ?', 1);
//            
//            return $this->fetchAll($query)->toArray();
        }

}




