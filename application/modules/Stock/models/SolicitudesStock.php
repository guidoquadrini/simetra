<?php

class Stock_models_SolicitudesStock extends Zend_Db_Table_Abstract {

    protected $_name = 'stock_solicitudes';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

     // getSolicitudesTransferenciasByEfe(NULL, 1, 18, NULL, Array, 1, NULL)

      public function getSolicitudesTransferenciasByEfe($efe_id = null, $fecha, $estado, $page = 1, $perPage = 10) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(stock_solicitudes.fecha,"%H:%i") as hora',
            'DATE_FORMAT(stock_solicitudes.fecha,"%d/%m/%Y %H:%i") as fecha',
            'TIMEDIFF(NOW(), stock_solicitudes.fecha) as tiempo',
            'DATE_FORMAT(stock_solicitudes.fecha_vencimiento,"%d/%m/%Y") as fecha_vencimiento',
            'stock_solicitudes.estado',
            'stock_solicitudes.cantidad_solicitada',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
            'stock_solicitudes.grupo',        
            'stock_solicitudes.factor',        
            'stock_solicitudes.irradiado',     
            'stock_solicitudes.grupo_inverso',
            'stock_solicitudes.observaciones', 
            'stock_solicitudes.efector_id', 
            'DATE_FORMAT(stock_solicitudes.responde_fecha,"%d/%m/%Y") as responde_fecha',
            'stock_solicitudes.responde_efector_id', 
            'stock_solicitudes.responde_usuario_id', 
            'stock_solicitudes.cantidad_entregada',      
        ));     
        
//        if ($hem_id != null)
//            $query = $query->where("stock_solicitudes.hemocomponente_id = ?", $hem_id);

        if ($fecha != null)
            $query = $query->where("stock_solicitudes.fecha LIKE '{$fecha}%'");

        if ($efe_id != null)
            $query = $query->where("stock_solicitudes.efector_id IN (?)", $efe_id);

        if ($estado > 0)
            $query = $query->where('stock_solicitudes.estado =?', $estado);

        $query = $query->order('stock_solicitudes.fecha DESC');
        $query = $query->joinInner('hem_hemocomponentes', ' stock_solicitudes.hemocomponente_id=hem_hemocomponentes.hem_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);       
    }

        public function getSolicitudesListByEfe($efes_ids) {
            $query = $this->select(true)->setIntegrityCheck(false)
                ->reset('columns')->columns(array('hemocomponente_id','COUNT(*) as cantidad'))
               // ->where('estado = ?', 2)
                ->group('hemocomponente_id');

            $query = $query->where("efe_id IN (?)",$efes_ids);
            $query = $query->joinInner('hem_hemocomponentes',' stock_solicitudes.hemocomponente_id=hem_hemocomponentes.hem_id');
            
            // TODO: FALTA JOIN INNER CON LA TABLA DE USUARIOS
            return $this->getDefaultAdapter()->fetchPairs($query);
        }
        
        

        public function getById($id) {
            $query = $this->select()
                    ->from($this)
                    ->where('id=?', $id);

            return $this->fetchRow($query);
        }
        
        
	public function getallhemocomponentes(){
		$model = new Gestion_models_Hemocomponentes();
		//$resultados = $model->fetchAll('hem_id','hem_descripcion')->toArray();
                //$this->view->resultados = $resultados;
                
                $resultados = $model->getPairs();
                
                return $this->fetchAll($resultados);
		
	}        
        
  
        
        public function getSolicitudById($id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(stock_solicitudes.fecha,"%H:%i") as hora',
            'DATE_FORMAT(stock_solicitudes.fecha,"%d/%m/%Y %H:%i") as fecha',
            'TIMEDIFF(NOW(), stock_solicitudes.fecha) as tiempo',
            'DATE_FORMAT(stock_solicitudes.fecha_vencimiento,"%d/%m/%Y") as fecha_vencimiento',
            'stock_solicitudes.estado',
            'stock_solicitudes.cantidad_solicitada',
            'stock_solicitudes.cantidad_entregada',
            'hem_hemocomponentes.hem_descripcion as hem_descripcion',
            'stock_solicitudes.grupo',        
            'stock_solicitudes.factor',        
            'stock_solicitudes.irradiado',     
            'stock_solicitudes.grupo_inverso',
            'stock_solicitudes.observaciones', 
            'stock_solicitudes.efector_id', 
            'DATE_FORMAT(stock_solicitudes.responde_fecha,"%d/%m/%Y") as responde_fecha',
            'stock_solicitudes.responde_efector_id', 
            'stock_solicitudes.responde_usuario_id', 
            'stock_solicitudes.cantidad_entregada',      
        ));     
        

        $query = $query->joinInner('hem_hemocomponentes', ' stock_solicitudes.hemocomponente_id=hem_hemocomponentes.hem_id');

        return $this->fetchAll($query);       

        }        


}




