<?php

return [
    [
        'label' => 'Persona',
        'url' => '#',
        'class' => 'dropdown',
        'module' => 'efector',
        'controller' => 'personas',
        'action' => 'buscar-persona',
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle',
        //        'tabindex' => '-1',
        ],
        'pages' => [
            [
                'label' => 'Buscar Persona',
                'module' => 'efector',
                'controller' => 'personas',
                'action' => 'buscar-persona',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Historial de Procesos Iniciados',
                'module' => 'efector',
                'controller' => 'solicitudes',
                'action' => 'index',
                'customHtmlAttribs' => [],
            ],
        ],
    ],
    [
        'label' => 'Entrevista',
        'module' => 'efector',
        'controller' => 'entrevistas',
        'customHtmlAttribs' => [],
    ],
    [
        'label' => 'Examen',
        'module' => 'efector',
        'controller' => 'examenes',
        'customHtmlAttribs' => [
        ],
    ],
    [
        'label' => 'Extracción',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Donaciones de Sangre',
                'module' => 'efector',
                'controller' => 'extraccion',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Aferesis',
                'module' => 'efector',
                'controller' => 'aferesis',
                'customHtmlAttribs' => [],
            ],
        ],
    ],
    [
        'label' => 'Envíos',
        'module' => 'efector',
        'controller' => 'envios',
        'customHtmlAttribs' => [],
    ],
    [
        'label' => 'Recepción',
        'module' => 'banco',
        'controller' => 'recepcion',
        'customHtmlAttribs' => [],
    ],
    [
        'label' => 'Inmuno',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Inmunologia Donaciones',
                'module' => 'banco',
                'controller' => 'inmunologia',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Transfusional',
                'module' => 'paciente',
                'controller' => 'inmunotransfusional',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Inmunología',
                'module' => 'banco',
                'controller' => 'informes',
                'action' => 'inmunologia',
                'customHtmlAttribs' => [
                    'tabindex' => '-1',
                ],
            ],
        ],
    ],
    [
        'label' => 'I.T.T.',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            "data-toggle" => "dropdown",
            "class" => 'dropwon-toggle'
        ],
        'pages' => [
            [
                'label' => 'Serologia Donaciones',
                'module' => 'banco',
                'controller' => 'serologia',

            ],
            [
                'label' => 'Hist. Determinaciones Cargadas',
                'module' => 'banco',
                'controller' => 'serologia',
                'action' => 'visualizador',
                'customHtmlAttribs' => [],
            ],
        ]
        ],
    [
        'label' => 'Produccion',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Planificación de la Producción',
                'module' => 'banco',
                'controller' => 'preproduccion',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Producción de Hemocoponentes',
                'module' => 'banco',
                'controller' => 'produccion',
                'customHtmlAttribs' => [],
            ],
        ],
    ],
    [
        'label' => 'Stock',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Gestión de Stock',
                'module' => 'stock',
                'controller' => 'stock',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Gestión de Solicitudes',
                'module' => 'stock',
                'controller' => 'solicitudes',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Gestión de Descartes',
                'module' => 'stock',
                'controller' => 'descartes',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Gestión de Insumos',
                'module' => 'stock',
                'controller' => 'pedidosinsumos',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => '',
                'uri' => '#',
                'class' => 'divider',
                'style' => 'border-buttom:1px solid black;'
            ],
            [
                'label' => 'Stock',
                'module' => 'banco',
                'controller' => 'produccion',
                'action' => 'stock',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Envíos',
                'module' => 'banco',
                'controller' => 'produccion',
                'action' => 'envios',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Historial envíos',
                'module' => 'banco',
                'controller' => 'produccion',
                'action' => 'historial',
                'customHtmlAttribs' => [],
            ],
        ],
    ],
    [
        'label' => 'Paciente',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Gestión de O.T.',
                'module' => 'paciente',
                'controller' => 'ordentransfusional',
                'customHtmlAttribs' => [],
            ],
        ],
    ],
    [
        'label' => 'Validaciones',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Validar Hemocomponentes producidos',
                'module' => 'banco',
                'controller' => 'produccion',
                'action' => 'informe',
                'customHtmlAttribs' => [
                    'tabindex' => '-1',
                ],
            ],
            
            [
                'label' => 'Validar Serología',
                'module' => 'banco',
                'controller' => 'serologia',
                'action' => 'informe',
                'customHtmlAttribs' => [
#                    'tabindex' => '-1',
                ],
            ],
            [
                'label' => 'Descartar Hemocomponentes',
                'module' => 'banco',
                'controller' => 'produccion',
                'action' => 'descarte'
            ],
        ],
    ],
    [
        'label' => 'Informes',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => 'Donaciones',
                'module' => 'banco',
                'controller' => 'informes',
                'action' => 'pacientes',
                'customHtmlAttribs' => [
                    'tabindex' => '-1',
                ],
            ],
            
        ],
    ],
    [
        'label' => '',
        'uri' => '#',
        'class' => 'dropdown',
        'visible' => 1,
        'customHtmlAttribs' => [
            'data-toggle' => 'dropdown',
            'data-icon' => 'icon-wrench',
            'class' => 'dropdown-toggle'
        ],
        'pages' => [
            [
                'label' => '',
                'uri' => '#',
                'class' => 'divider',
            ],
            [
                'label' => 'Gestion de Cuestionario',
                'uri' => '#',
                'visible' => 1,
                'customHtmlAttribs' => [
                    'class' => 'alert alert-warning'
                ]
            ],
            [
                'label' => 'Plantilla Cuestionario',
                'module' => 'gestion',
                'controller' => 'plantillascuestionario',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => 'Categorías de Preguntas',
                'module' => 'gestion',
                'controller' => 'cuestionariopreguntascategorias',
                'customHtmlAttribs' => [],
            ],
            [
                'label' => '',
                'uri' => '#',
                'class' => 'divider',
            ],
            [
                'label' => 'Mantenimiento de Códigos Centralizados',
                'module' => 'gestion',
                'controller' => 'centralizado'
            ],
            [
                'label' => '',
                'uri' => '#',
                'class' => 'divider',
            ],
            [
                'label' => 'Clasificación de Tipos de Aferesis',
                'module' => 'gestion',
                'controller' => 'aferesis',
            ],
            [
                'label' => 'Clasificación de Determinaciones',
                'module' => 'gestion',
                'controller' => 'determinaciones',
            ],
            [
                'label' => 'Clasificación de Donaciones por Practica',
                'module' => 'gestion',
                'controller' => 'donaciones',
            ],
            [
                'label' => 'Clasificación de Donaciones por Receptor',
                'module' => 'gestion',
                'controller' => 'tipodonacion',
            ],
            [
                'label' => 'Clasificacion de Hemocomponentes',
                'module' => 'gestion',
                'controller' => 'hemocomponentes',
            ],
            [
                'label' => 'Clasificación de Motivos Diferidos',
                'module' => 'gestion',
                'controller' => 'motivosdiferidos',
            ],
            [
                'label' => 'Clasificación de Motivos Descartes',
                'module' => 'gestion',
                'controller' => 'motivosdescartes',
            ],
            [
                'label' => 'Clasificación de Motivos Rechazos',
                'module' => 'gestion',
                'controller' => 'motivosrechazos',
            ],
            [
                'label' => 'Clasificación de Ocupaciones',
                'module' => 'gestion',
                'controller' => 'ocupaciones',
            ],
            [
                'label' => 'Clasificación de Marcas de Bolsas',
                'module' => 'gestion',
                'controller' => 'bolsasmarcas',
            ],
            [
                'label' => 'Clasificación de Tipos de Bolsas',
                'module' => 'gestion',
                'controller' => 'bolsastipos',
            ],
            [
                'label' => 'Clasificación de Solución Conservadora de las Bolsas',
                'module' => 'gestion',
                'controller' => 'bolsas',
            ],
            [
                'label' => 'Definición de Perfiles Producción',
                'module' => 'gestion',
                'controller' => 'perfilesproduccion',
            ],
            [
                'label' => '',
                'uri' => '#',
                'class' => 'divider',
            ],
            [
                'label' => 'Gestion de Stocks del Sistema',
                'uri' => '#',
                'visible' => 1,
                'customHtmlAttribs' => [
                    'class' => 'alert alert-warning'
                ]
                ],
            [
                'label' => 'Tipos de Movimientos (Stock)',
                'module' => 'stock',
                'controller' => 'tiposmovimientosstock',
            ],
            [
                'label' => 'Categorías de Productos (Stock)',
                'module' => 'stock',
                'controller' => 'productoscategorias',
            ],
            [
                'label' => 'Productos / Insumos (Stock)',
                'module' => 'stock',
                'controller' => 'productos',
            ],
            [
                'label' => '',
                'uri' => '#',
                'class' => 'divider',
            ],
            [
                'label' => 'Soporte',
                'module' => 'soporte',
                'controller' => 'soporte',
            ],
            [
                'label' => 'Usuarios',
                'module' => 'acceso',
                'controller' => 'usuarios',
            ],
            [
                'label' => 'Newsletter',
                'module' => 'gestion',
                'controller' => 'newsletters',
            ],
        ],
    ],
];
