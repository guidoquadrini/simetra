<?php

class Promocion_models_Colectas extends Zend_Db_Table_Abstract {

    protected $_name = 'colectas';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    /**
     * 
     * Estados posibles para la colecta
     * 
     * 0:Pendiente Publicacion
     * 1:Publicada
     * 2:Terminada
     * 3:Suspendida
     * 4:Eliminada
     * 
     */
    
    public function obtenerColectasActivas() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->where('estado != ?',4)
                                ->order('fecha_colecta'));
    }

    
    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id', 'codigo_colecta'))
                                ->where('estado != ?',4)
                                ->order('id'));
    }

    
    
        
}
