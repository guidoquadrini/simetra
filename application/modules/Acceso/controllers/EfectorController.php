<?php

class Acceso_EfectorController extends BootPoint {

    public function indexAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $nod_id = (int) $this->getRequest()->getParam('nod_id');

        $mNodos = new Acceso_models_Nodos();
        $nodos = $mNodos->getNodosRegion();
        if ($nod_id > 0) {
            $modelo = new Acceso_models_Efectores();
            $efectores = $modelo->getEfectoresNodo($nod_id);

            $this->view->efectores = $efectores;
            $this->view->nod_id = $nod_id;
        }
        unset($nodos[0]);
        $this->view->nodos = $nodos;
        $this->view->usu_id = $usu_id;
    }

    public function asociarAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $efe_id = (int) $this->getRequest()->getParam('efe_id');

        $modelo = new Acceso_models_UsuariosEfectores();
        $select = $modelo->select(true)
                ->where('usu_id=?', $usu_id)
                ->where('pry_id=?', $this->pryId)
                ->where('id_efector=?', $efe_id);
        $usuEfector = $modelo->getDefaultAdapter()->fetchRow($select);
        if (!$usuEfector) {
            $modelo->createRow(array(
                'usu_id' => $usu_id,
                'pry_id' => $this->pryId,
                'id_efector' => $efe_id,
                'usu_efe_activo' => 1
            ))->save();
        } else {
            if ($usuEfector['usu_efe_activo'] != 1) {
                $modelo->find($usuEfector['usu_efe_id'])->current()->setFromArray(
                        array('usu_efe_activo' => 1)
                )->save();
            }
        }
        $this->_helper->redirector('manage', 'usuarios', 'acceso', array('usu_id' => $usu_id));
    }

    public function removerAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $efe_id = (int) $this->getRequest()->getParam('efe_id');

        $modelo = new Acceso_models_UsuariosEfectores();
        $modelo->update(array('usu_efe_activo' => 0, 'efe_defecto' => 0), array('usu_id=?' => $usu_id, 'pry_id=?' => $this->pryId, 'id_efector=?' => $efe_id));

        $this->_helper->redirector('manage', 'usuarios', 'acceso', array('usu_id' => $usu_id));
    }

    public function defectoAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $efe_id = (int) $this->getRequest()->getParam('efe_id');

        $modelo = new Acceso_models_UsuariosEfectores();
        $modelo->update(array('efe_defecto' => 0), array('usu_id=?' => $usu_id, 'pry_id=?' => $this->pryId)); //otros efectores

        $modelo->update(array('efe_defecto' => 1), array('id_efector=?' => $efe_id, 'usu_id=?' => $usu_id, 'pry_id=?' => $this->pryId)); //efector

        $this->_helper->redirector('manage', 'usuarios', 'acceso', array('usu_id' => $usu_id));
    }

    public function defectoAjaxAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        //Obtengo el ID de usuario del request.
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $efe_id = (int) $this->getRequest()->getParam('efe_id');

        $modelo = new Acceso_models_UsuariosEfectores();
        $modelo->update(array('efe_defecto' => 0), array('usu_id=?' => $usu_id, 'pry_id=?' => $this->pryId)); //otros efectores

        $modelo->update(array('efe_defecto' => 1), array('id_efector=?' => $efe_id, 'usu_id=?' => $usu_id, 'pry_id=?' => $this->pryId)); //efector
        #$this->cambiar-efector-activo($usu_id);
    }

}
