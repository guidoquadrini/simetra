<?php

class Acceso_UsuariosController extends BootPoint {

    public function preDispatch() {
        $this->view->tab1 = 'active';
    }

    public function indexAction() {
        $q = $this->getRequest()->getParam('q');
        if ($q != '') {
            $perPage = 15;
            $model = new Acceso_models_Usuarios();
            $usuarios = $model->getUsuarios($q, $this->page, $perPage);
            if (sizeof($usuarios) > 0) {
                $total = $model->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
                $paginas = ceil($total / $perPage);
                $this->view->total = $total;
                $this->view->paginas = $paginas;
            }
            $this->view->resultados = $usuarios;
        }
        $this->view->q = $q;
        $this->view->smenu11 = 'active';
        $this->view->menu4 = 'active';
    }

    public function verAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $model = new Acceso_models_Usuarios();
        $usuario = $model->fetchRow('usu_id = ' . $usu_id);
        $roles = $model->getRoles($usu_id, $this->pryId);
        $this->view->roles = $roles;
        $this->view->resultado = $usuario;
        $this->view->usu_id = $usu_id;
    }

    public function addAction() {
        $form = new Acceso_forms_usuarios();
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            if ($form->isValid($post)) {
                $values = $form->getValues();
                $model = new Acceso_models_Usuarios();
                $existe = $model->existe($values['usu_usuario']);
                if (!$existe) {
                    $values['usu_clave'] = md5($values['usu_clave']);
                    $values['usu_activo'] = 1;
                    unset($values['usu_rclave']);
                    $model->insert($values);
                    $usu_id = $model->getDefaultAdapter()->lastInsertId();
                    $this->_helper->redirector('proyecto', $this->_request->controller, $this->_request->module, array('usu_id' => $usu_id));
                }
                $form->usu_usuario->setErrorMessages(array('Usuario ya existente'));
                $form->usu_usuario->markAsError();
            }
        }

        $this->view->form = $form;
    }

    public function editAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $form = new Acceso_forms_usuarios();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $values = $this->getRequest()->getPost();
            $model = new Acceso_models_Usuarios();
            $model->fetchRow('usu_id = ' . $usu_id)->setFromArray($values)->save();
            $this->_helper->redirector('ver', $this->_request->controller, $this->_request->module, array('usu_id' => $usu_id));
        } else {
            $model = new Acceso_models_Usuarios();
            $usuario = $model->fetchRow('usu_id = ' . $usu_id)->toArray();
            $form->populate($usuario);
            
        }

        $this->view->form = $form;
        $this->view->usu_id = $usu_id;
    }

    /**
     * Llamado al form de cambio de clave
     */
    public function cambiarpasswordAction() {

        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        $model = new Acceso_models_Usuarios();
        $usuario = $model->fetchRow('usu_id=' . $usu_id);
        if ($usuario)
            $usuario = $usuario->toArray();
        $this->view->resultados = $usuario;

        $this->renderScript('usuarios/cambiarpassword.phtml');
    }

    /**
     * Cambio de contraseña
     */
    public function procesocambiarpasswordAction() {

        $usuario_id = (int) $this->getRequest()->getParam('usu_id');
        $usuario_clave = md5($this->getRequest()->getParam('clave'));

        $modelo = new Acceso_models_Usuarios();
        
        $values = array('usu_clave' => $usuario_clave);
        
        $usuario = $modelo->fetchRow('usu_id = ' . $usuario_id);
        $usuario->setFromArray($values)->save();
    
        $this->_helper->json(array(
            'st' => 'ok',
            'error' => "Clave cambiada con Exito !!!;"
        ));
    }

    public function manageAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');

        $model = new Acceso_models_Usuarios();
        $usuario = $model->fetchRow('usu_id = ' . $usu_id)->toArray();


        $roles = $model->getRoles($usu_id, $this->pryId);
        if (!empty($roles)) {
            foreach ($roles as $i => $v) {
                $perfiles[$v['per_id']]['roles'][] = $v;
            }
        }

        $pertenece = $model->getPertenece($usu_id, $this->pryId);
        if ($pertenece) {
            $this->view->pertenece = true;
        }

        $efectores = $model->getEfectores($usu_id, $this->pryId);
        $this->view->perfiles = $perfiles;
        $this->view->resultado = $usuario;
        $this->view->roles = $roles;
        $this->view->efectores = $efectores;
    }

    public function assocAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');

        $model = new Acceso_models_Usuarios();
        $usuario = $model->fetchRow('usu_id = ' . $usu_id)->toArray();
        $roles = $model->getRoles($usu_id, $this->pryId);
        if (!empty($roles)) {
            foreach ($roles as $i => $v) {
                $perfiles[$v['per_id']]['roles'][] = $v;
            }
        }

        $collect = array();
        foreach ($roles as $i => $v)
            $collect[] = $v['rol_id'];
        $todos = $model->getRolesRestantes($this->pryId, $collect);
        $this->view->perfiles = $perfiles;
        $this->view->resultado = $usuario;
        $this->view->roles = $roles;
        $this->view->todos = $todos;
    }

    public function agregarAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');

        if ($this->getRequest()->isPost()) {
            $roles = $this->getRequest()->getParam('rol');

            if (!empty($roles)) {
                $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
                foreach ($roles as $i) {
                    $sql = "INSERT INTO {$this->settings->dbusers}.usuarios_roles (usu_id,rol_id,usr_activo) VALUES ({$usu_id},{$i},1) ON DUPLICATE KEY UPDATE usr_activo=1";
                    $adapter->query($sql);
                }
            }
        }
        $this->_helper->redirector('assoc', $this->_request->controller, $this->_request->module, array('usu_id' => $usu_id));
    }

    public function quitarAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');
        if ($this->getRequest()->isPost()) {
            $roles = $this->getRequest()->getParam('rol');

            if (!empty($roles)) {
                $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
                $adapter->update($this->settings->dbusers . '.usuarios_roles', array('usr_activo' => 0), "usu_id={$usu_id} AND rol_id IN (" . implode(',', $roles) . ")");
            }
        }
        $this->_helper->redirector('assoc', $this->_request->controller, $this->_request->module, array('usu_id' => $usu_id));
    }

    public function proyectoAction() {
        $usu_id = (int) $this->getRequest()->getParam('usu_id');

        $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
        $adapter->insert($this->settings->dbusers . '.usuarios_proyectos', array(
            'usu_id' => $usu_id,
            'pry_id' => $this->pryId,
            'pry_activo' => 1,
            'pry_actualizacion' => new Zend_Db_Expr('NOW()')
        ));

        $this->_helper->redirector('manage', $this->_request->controller, $this->_request->module, array('usu_id' => $usu_id));
    }

    public function controlPermisosAction(){
        $usuario_id = $this->getRequest()->getParam('usuario_id');
        $modulo = $this->getRequest()->getParam('modulo');
        $contradora = $this->getRequest()->getParam('controladora');
        $metodo = $this->getRequest()->getParam('metodo');
        $vRet = false;        
        if ($this->controlPermisoModuloControllerMetodo($usuario_id, $modulo, $controller, $metodo)) {
                $vRet = array(
                    'st' => 'OK',
                    'mensaje' => "El usuario tiene permiso para realizar esta operacion."
                );
            } else {
                $vRet = array(
                    'st' => 'NOK',
                    'mensaje' => "El usuario no posee permisos suficientes."
                );
            }
        $this->_helper->json($vRet);
    }
    
    /**
     *  Comprobar que usuario y contraseña sean correctos y pertenezcan al proyecto actual
     */
    public function controlCredencialesUsuarioAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $vRet = false;
        $username = $this->getRequest()->getParam('usuario');
        $password = $this->getRequest()->getParam('password');
        //Validamos que no vengan parametro vacios.
        if (!empty($username) || !empty($password)) {
            $modeloUsuarios = new Acceso_models_Usuarios();
            $usuario = $modeloUsuarios->validarCredenciales($username, $password);
            //Controlamos si el usuario existe y sus credenciales son validas.
            if (!empty($usuario)) {
                $pertenece = $modeloUsuarios->getPertenece($usuario['usu_id'], $this->pryId);
                //Controlamos que el usuario pertenezca al proyecto
                if ($pertenece) {
                    $vRet = true;
                }
            }
            if ($vRet) {
                $this->_helper->json(array(
                    'st' => 'OK',
                    'mensaje' => "Usuario Identificado con éxito."
                ));
            } else {
                $this->_helper->json(array(
                    'st' => 'NOK',
                    'mensaje' => "Las credenciales del usuario no son adecuadas."
                ));
            }
        }
    }
    
    /**
     * Controla que el usuario exista, pertenezca al proyecto y que el modulo exista.
     * @param type $usuario_id
     * @param type $modulo
     * @return type boolean
     */
    private function _controlPermisoModulo($usuario_id, $modulo) {
        $vRet = false;
        // Validar que exista el usuario
        $modeloUsuarios = new Acceso_models_Usuarios();
        $usuario = $modeloUsuarios->fetchNew('usu_id' . $usuario_id)->current();
        //Valido que el usuario exista
        if (!empty($usuario)) {
            $pertenece = $modeloUsuarios->getPertenece($usuario['usu_id'], $this->pryId);
            //Controlamos que el usuario pertenezca al proyecto
            if ($pertenece) {
                $modulosDir = $this->getInvokeArg('bootstrap')->getResource('front')->getControllerDirectory();
                $arrModulos = array_keys($modulosDir);
                //asort($arrModulos);
                $arrModulos = array_diff($arrModulos, array('default', 'rest'));
                //Busca el modulo en el conjunto de modulos filtrado.
                $vRet = in_array(strtoupper($modulo), array_map('strtoupper', $arrModulos));
            }
        }
        return $vRet;
    }

    /**
     * Controla que el usuario exista, pertenezca al proyecto, permisos sobre el modulo y controladora.
     * @param type $usuario_id
     * @param type $modulo
     * @param type $controller
     * @return boolean
     */
    private function _controlPermisoModuloController($usuario_id, $modulo, $controller) {
        
        
        $vRet = false;
        if (!empty($usuario)) {
            $moduloPermitido = controlPermisoModulo($usuario_id, $modulo);
            if ($moduloPermitido) {
                $modelRecursos = new Acceso_models_Recursos();
                //Verificar que exista la controladora en el modulo.
                //Verificar que tenga permisos en la controladora.
                $ControllerPermitido = '';
                if ($ControllerPermitido) {
                    $vRet = true;
                }
            }
        }
        return $vRet;
    }

    private function _controlPermisoModuloControllerMetodo($usuario_id, $modulo, $controller, $metodo) {
        $vRet = false;
        // Validar que exista el usuario
        $usuario = "";

        if (!empty($usuario)) {
            $controllerPermitido = controlPermisoModuloController($usuario_id, $modulo, $controller);

            if ($controllerPermitido) {
                $metodoPermitido = "";
                // verificar si existe y tiene permisos el metodo
                if ($metodoPermitido) {
                    $vRet = true;
                }
            }
        }
        return $vRet;
    }

}
