<?php

class Acceso_RecursosController extends BootPoint {

    public $arrModules = array();
    public $arrControllers = array();
    public $arrActions = array();
    public $doc = array();
    public $filename = '../cache/recursos.php';
    private $arrIgnore = array('.', '..', '.svn', 'ErrorController.php');

    public function preDispatch() {
        $this->view->tab4 = 'active';
        $this->settings = $this->getInvokeArg('bootstrap')->getResource('settings');
    }

    public function indexAction() {
        //escribir archivo en algun lado
        if (file_exists($this->filename)) {
            $config = new Zend_Config(require $this->filename, true);

            $this->view->recursos = $config->modules;
        }
    }

    /**
     * Funciones
     *
     */
    public function assocAction() {
        $rol_id = (int) $this->getRequest()->getParam('rol_id');

        $model = new Acceso_models_Roles();
        $rol = $model->find($rol_id)->current()->toArray();
        $recursos = $model->getRecursos($rol_id);

        $new = array();
        foreach ($recursos as $i => $v) {
            if ($v['controller'] == '')
                $new[$v['module']] = array();
            elseif ($v['action'] == '') {
                $new[$v['module']][$v['controller']] = array();
            } else {
                $new[$v['module']][$v['controller']][] = $v['action'];
            }
        }

        $this->view->rol = $rol;
        $this->view->resultados = $new;
        $this->indexAction();
    }

    public function agregarAction() {
        $rol_id = (int) $this->getRequest()->getParam('rol_id');

        if ($this->getRequest()->isPost()) {
            $modulos = $this->getRequest()->getParam('modulo');
            $controladores = $this->getRequest()->getParam('controlador');
            $acciones = $this->getRequest()->getParam('accion');

            $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();

            if (!empty($modulos)) {
                foreach ($modulos as $i => $v) {
                    //#limpieza hereditaria
                    unset($controladores[$v]);
                    unset($accion[$v]);
                    /*
                      update hereditario */
                    $adapter->update($this->settings->dbusers . '.roles_recursos', array('rec_activo' => 0), "rol_id={$rol_id} AND module='{$v}' AND (controller!='' OR action!='')");

                    $sql = "INSERT INTO {$this->settings->dbusers}.roles_recursos (pry_id,rol_id,module,controller,action,rec_activo) "
                            . "VALUES ({$this->pryId},{$rol_id},'{$v}','','',1) ON DUPLICATE KEY UPDATE rec_activo=1";
                    $adapter->query($sql);
                    //insert if exist update
                }
            }

            if (!empty($controladores)) {
                foreach ($controladores as $i => $v) {
                    unset($accion[$i][$v]); //limpieza
                    /* si existen acciones o modulo desactiva */
                    foreach ($v as $ii => $c) {
                        $adapter->update($this->settings->dbusers . '.roles_recursos', array('rec_activo' => 0), "rol_id={$rol_id} AND module='{$i}' AND ((controller='{$c}' AND action!='') OR (controller='' AND action=''))");

                        //insert if exist update
                        $sql = "INSERT INTO {$this->settings->dbusers}.roles_recursos (pry_id,rol_id,module,controller,action,rec_activo) "
                                . "VALUES ({$this->pryId},{$rol_id},'{$i}','{$c}','',1) ON DUPLICATE KEY UPDATE rec_activo=1";
                        $adapter->query($sql);
                    }
                }
            }

            if (!empty($acciones)) {
                foreach ($acciones as $m => $c) {
                    foreach ($c as $i => $v) {
                        $adapter->update($this->settings->dbusers . '.roles_recursos', array('rec_activo' => 0), "rol_id={$rol_id} AND module='{$m}' AND (controller='' OR controller='{$i}') AND action=''");

                        foreach ($v as $b => $a) {
                            $sql = "INSERT INTO {$this->settings->dbusers}.roles_recursos (pry_id,rol_id,module,controller,action,rec_activo) "
                                    . "VALUES ({$this->pryId},{$rol_id},'{$m}','{$i}','{$a}',1) ON DUPLICATE KEY UPDATE rec_activo=1";
                            $adapter->query($sql);
                            //insert if exist update
                        }
                    }
                }
            }
        }
        $this->_helper->redirector('assoc', $this->_request->controller, $this->_request->module, array('rol_id' => $rol_id));
    }

    public function removerAction() {
        $rol_id = (int) $this->getRequest()->getParam('rol_id');

        if ($this->getRequest()->isPost()) {
            $modulos = $this->getRequest()->getParam('modulo');
            $controladores = $this->getRequest()->getParam('controlador');
            $acciones = $this->getRequest()->getParam('accion');

            $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
            $model = new Acceso_models_Recursos();

            if (!empty($modulos)) {
                unset($controladores[$v]);
                unset($accion[$v]);
                $model->update(array('rec_activo' => 0), array(
                    'rol_id = ?' => $rol_id,
                    'module IN (?)' => $modulos
                ));
            }

            if (!empty($controladores)) {
                foreach ($controladores as $i => $v) {
                    unset($accion[$i][$v]); //limpieza
                    $model->update(array('rec_activo' => 0), array(
                        'rol_id = ?' => $rol_id,
                        'module = ?' => $i,
                        'controller IN (?)' => $v
                    ));
                }
            }
            if (!empty($acciones)) {
                foreach ($acciones as $m => $c) {
                    foreach ($c as $i => $v) {
                        $model->update(array('rec_activo' => 0), array(
                            'rol_id = ?' => $rol_id,
                            'module = ?' => $m,
                            'controller = ?' => $i,
                            'action IN (?)' => array_values($v)
                        ));
                    }
                }
            }
        }

        $this->_helper->redirector('assoc', $this->_request->controller, $this->_request->module, array('rol_id' => $rol_id));
    }

    /**
     *  END funciones
     *
     */

    /**
     * Carga de Modulos
     *
     */
    public function buildModulesAction() {
        $modulesDir = $this->getInvokeArg('bootstrap')->getResource('front')->getControllerDirectory();
        $arrModules = array_keys($modulesDir);
        asort($arrModules);
        $arrModules = array_diff($arrModules, array('default', 'rest', 'central'));

        $config = new Zend_Config(array(), true);
        $config->modules = array();
        foreach ($arrModules as $i)
            $config->modules->{$i} = array();

        unlink($this->filename);
        $writer = new Zend_Config_Writer_Array();
        $writer->write($this->filename, $config);

        $this->_helper->json(array('st' => 1, 'modulos' => $arrModules));
    }

    /**
     * Build Controllers
     *
     */
    public function buildControllerAction() {
        $module = $this->getRequest()->getParam('modulo');

        $arrControllers = array();

        if (!empty($module)) {
            $datControllerFolder = opendir(APPLICATION_PATH . '/modules/' . ucfirst($module) . '/controllers');
            while (($dstFile = readdir($datControllerFolder) ) !== false) {
                if (!in_array($dstFile, $this->arrIgnore)) {
                    if (preg_match('/Controller/', $dstFile)) {
                        $arrControllers[] = strtolower(substr($dstFile, 0, -14));
                    }
                }
            }
            asort($arrControllers);
            closedir($datControllerFolder);
        }

        $config = new Zend_Config(require $this->filename, true);
        foreach ($arrControllers as $i)
            $config->modules->{$module}->{$i} = array();

        unlink($this->filename);
        $writer = new Zend_Config_Writer_Array();
        $writer->write($this->filename, $config);

        $this->_helper->json(array('st' => 1, 'controladores' => array_values($arrControllers)));
    }

    /**
     * Build Actions
     *
     */
    public function buildActionsAction() {
        $module = $this->getRequest()->getParam('modulo');
        $controllers = $this->getRequest()->getParam('controllers');

        $arrActions = array();

        if (!empty($controllers)) {
            
            foreach ($controllers as $strController) {
                $strClassName = ucfirst($module) . '_' . ucfirst($strController . 'Controller');
                if (!@class_exists($strClassName)) {
                    Zend_Loader::loadFile(APPLICATION_PATH . '/modules/' . ucfirst($module) . '/controllers/' . ucfirst($strController) . 'Controller.php');
                }

                $objReflection = new Zend_Reflection_Class($strClassName);
                $arrMethods = $objReflection->getMethods();
                foreach ($arrMethods as $objMethods) {
                    if (preg_match('/Action/', $objMethods->name)) {
                        $name = substr($this->_camelCaseToHyphens($objMethods->name), 0, -6);
                        //$this->doc[$module][$strController][$name] = $objMethods->getDocComment();
                        $arrActions[$strController][] = $name;
                    }
                }
                asort($arrActions[$strController]);
            }
        }

        $config = new Zend_Config(require $this->filename, true);
        foreach ($controllers as $i)
            $config->modules->{$module}->{$i} = $arrActions[$i];

        unlink($this->filename);
        $writer = new Zend_Config_Writer_Array();
        $writer->write($this->filename, $config);

        $this->_helper->json(array('st' => 1));
    }

    private function _camelCaseToHyphens($string) {
        if ($string == 'currentPermissionsAction') {
            $found = true;
        }
        $length = strlen($string);
        $convertedString = '';
        for ($i = 0; $i < $length; $i++) {
            if (ord($string[$i]) > ord('A') && ord($string[$i]) < ord('Z')) {
                $convertedString .= '-' . strtolower($string[$i]);
            } else {
                $convertedString .= $string[$i];
            }
        }
        return strtolower($convertedString);
    }

}
