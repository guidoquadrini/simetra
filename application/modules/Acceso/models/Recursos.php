<?php

class Acceso_models_Recursos extends Zend_Db_Table_Abstract {

    protected $_name = 'roles_recursos';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;
        parent::__construct();
    }

}
