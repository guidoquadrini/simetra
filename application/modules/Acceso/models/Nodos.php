<?php

class Acceso_models_Nodos extends Zend_Db_Table_Abstract {

    protected $_name = 'nodos';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;

        parent::__construct();
    }

    public function getNodosRegion() {
        return $this->fetchAll(
                        $this->select(true)->reset('columns')
                                ->columns(array('id_nodo', 'nom_nodo'))
                                ->order('nom_nodo'))->toArray();
    }

}
