<?php

class Acceso_models_UsuariosEfectores extends Zend_Db_Table_Abstract {

    protected $_name = 'usuarios_efectores';
    protected $_primary = 'usu_efe_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;
        parent::__construct();
    }

}
