<?php

class Acceso_models_Usuarios extends Zend_Db_Table_Abstract {

    protected $_name = 'usuarios';
    protected $_primary = 'usu_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();        
        $this->_schema = $registry->config->db_front;
        $this->schema_hmi2 = $registry->config->db_hmi2;
        parent::__construct();
    }

    /**
     * Obtener Perfiles Asociados a un Usuario
     *
     * @param mixed $usu_id
     * @param mixed $pry_id
     */
    public function getRoles($usu_id, $pry_id) {
        return $this->getDefaultAdapter()->fetchAssoc(
                        $this->getDefaultAdapter()->select()
                                ->from('roles as r', 'r.*', $this->_schema)
                                ->joinLeft('usuarios_roles as ur', 'ur.rol_id=r.rol_id', null, $this->_schema)
                                ->where('ur.usu_id=?', $usu_id)
                                ->where('pry_id=?', $pry_id)
                                ->where('usr_activo=1')
                                ->where('rol_activo=1')
                                ->order('rol_nombre'));
    }

    public function getRolesRestantes($pry_id, $not_in) {
        $select = $this->getDefaultAdapter()->select()
                ->from('roles as r', '*', $this->_schema)
                ->where('pry_id=?', $pry_id)
                ->order('rol_nombre');

        if (!empty($not_in))
            $select->where('rol_id NOT IN (?)', $not_in);

        return $this->getDefaultAdapter()->fetchAll($select);
    }

    /**
     * Obtener recursos asociados a un perfil
     *
     * @param mixed $rol_ids
     */
    public function getRecursosRoles($rol_ids) {
        if (empty($rol_ids))
            return array();

        return $this->getDefaultAdapter()->fetchAll(
                        $this->getDefaultAdapter()->select()
                                ->from('roles_recursos', array('module', 'controller', 'action'), $this->_schema)
                                ->where("rol_id IN (" . implode(',', $rol_ids) . ")")
                                ->where('rec_activo=?', 1)
                                ->group(array('module', 'controller', 'action'))
                                ->order(array('module', 'controller', 'action')));
    }

    /**
     * Obtener Usuarios por querystring
     *
     * @param mixed $q
     * @param mixed $page
     * @param mixed $perPage
     */
    public function getUsuarios($q, $page, $perPage) {
        return $this->getDefaultAdapter()->fetchAll($this->select()
                                //->reset('columns')->columns('SQL_CALC_FOUND_ROWS *')
                                ->where("usu_nombre LIKE '%{$q}%'")
                                ->orWhere("usu_usuario LIKE '%{$q}%'")
                                //->limit($perPage,$perPage*($page-1))
                                ->order('usu_usuario'));
    }

    /**
     * Usuario pertenece a un proyecto
     *
     * @param mixed $usu_id
     * @param mixed $pry_id
     */
    public function getPertenece($usu_id, $pry_id) {
        return $this->getDefaultAdapter()->fetchOne( 
                        $this->getDefaultAdapter()->select()
                                ->from('usuarios_proyectos', '*', $this->_schema)
                                ->where('usu_id = ?', $usu_id)
                                ->where('pry_id = ?', $pry_id));
    }

    public function getEfectores($usu_id, $pry_id) {
        $select = $this->getDefaultAdapter()->select()
                ->from('usuarios_efectores as ue', array('id_efector', 'efe_defecto'), $this->_schema)
                ->joinInner('efectores as e', 'ue.id_efector=e.id_efector', array('id_efector as id_establecimiento', 'nom_efector as nomest'), $this->schema_hmi2)
                ->where('usu_id=?', $usu_id)
                ->where('pry_id=?', $pry_id)
                ->where('usu_efe_activo=1')
                ->order('nomest');

        return $this->getDefaultAdapter()->fetchAssoc($select);
    }

    public function existe($username) {
        return $this->getDefaultAdapter()->fetchOne(
                        $this->select(true)->reset('columns')->columns('usu_id')->where('usu_usuario=?', $username));
    }

    
    public function validarCredenciales($username, $password) {
        return $this->getDefaultAdapter()->fetchRow(
                        $this->select()
                        ->where('usu_usuario=?', $username)
                        ->where('usu_clave=?', md5($password))
                );
    }   
    
}
