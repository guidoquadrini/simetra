<?php

class Acceso_models_Roles extends Zend_Db_Table_Abstract {

    protected $_name = 'roles';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;
        parent::__construct();
    }

    /**
     * Obtener Roles de Proyecto
     *
     * @param mixed $pry_id
     */
    public function getRoles($pry_id) {
        return $this->getDefaultAdapter()->fetchAll($this->select(true)->where('pry_id = ?', $pry_id)->order('rol_nombre'));
    }

    /**
     * Obtener Recursos de Rol
     *
     * @param mixed $rol_id
     * @return array
     */
    public function getRecursos($rol_id) {
        $result = $this->getDefaultAdapter()->fetchAll(
                        $this->getDefaultAdapter()->select()
                                ->from('roles_recursos', '*', $this->_schema)
                                ->where('rol_id=?', $rol_id)
                                ->where('rec_activo=1')
                                ->order(array('module', 'controller', 'action')));                
        return $result;
    }

}
