<?php

class Acceso_models_Proyectos extends Zend_Db_Table_Abstract {

    protected $_name = 'proyectos';
    protected $_primary = 'pry_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;
        parent::__construct();
    }

    public function getByNom($nom) {
        return $this->fetchAll($this->select(true)
                                ->where("pry_nombre LIKE '%$nom%'"))->current();
    }

}
