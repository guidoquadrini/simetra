<?php

class Acceso_models_Efectores extends Zend_Db_Table_Abstract {
    
    protected $_name = '_establecimiento';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_mod_sims;
        $this->_name = $registry->config->prefijo_tabla . $this->_name;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('id_establecimiento', 'nomest'))
                                ->order('nomest'));
    }

    public function getEfectoresNodo($nod_id) {
        return $this->fetchAll(
                        $this->select(true)->reset('columns')
                                ->columns(array('id_establecimiento', 'nomest'))
                                ->where('nodo=?', $nod_id)
                                ->order('nomest'))->toArray();
    }

    public function getNom($id) {
        return $this->getDefaultAdapter()->fetchAll($this->select(true)
                                ->reset('columns')->columns(array('nomest'))
                                ->where('id_establecimiento=?', $id));
    }

}
