<?php

class Acceso_forms_usuarios extends Zend_Form
{

	public function init()
    {
		$this->addElement(
        $this->createElement('text','usu_usuario')
		->setRequired(true)
		->setLabel('Usuario')
		->setAttribs(array(
			'required' => 'required',
			'maxlength' => 15,
			'class' => 'input-medium'
		))
		->addFilter('stringToLower'));

		$this->addElement(
        $this->createElement('text','usu_nombre')
		->setRequired(true)
		->setLabel('Nombres')
		->setAttribs(array(
			'required' => 'required',
			'maxlength' => 60,
			'class' => 'input-medium'
		)));

		$this->addElement(
        $this->createElement('text','usu_apellido')
		->setRequired(true)
		->setLabel('Apellido')
		->setAttribs(array(
			'required' => 'required',
			'maxlength' => 80,
			'class' => 'input-medium'
		)));

		$this->addElement(
        $this->createElement('password','usu_clave')
		->setRequired(true)
		->setLabel('Contraseña')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-medium'
		)));

		$this->addElement(
        $this->createElement('password','usu_rclave')
		->setRequired(true)
		->setLabel('Repetir Contraseña')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-medium',
			'pattern' => '((?=.*[a-z])(?=.*[A-Z])(?=.+[0-9|@#$%])(?!.*\s).{8,20})'
		))
		->addValidator('Identical',false,array('token'=>'usu_clave')));

		$this->addElement(
        $email = $this->createElement('text','usu_email')
		->setLabel('Correo Electronico')
		->addValidator('emailAddress')
		->setAttrib('class','input-large')
		->addFilter('stringToLower'));
	}
}
