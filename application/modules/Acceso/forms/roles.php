<?php

class Acceso_forms_roles extends Zend_Form
{

	public function init()
    {
		$this->addElement($this->createElement('text','rol_nombre')
		->setRequired(true)
		->setLabel('Nombre Rol')
		->setAttribs(array(
			'required' => 'required',
			'class' => 'input-medium',
			'maxlength' => 40
		)));
	}
}
