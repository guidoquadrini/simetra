<?php

/**
* Pensar en separar este modulo de WebServices y crear un modulo REST
* uri http://www.url.org/panel/rest/controller/action
* example http://www.url.org/panel/rest/usuario/
*/
class Rest_SistemaController extends Zend_Controller_Action {

	/**
	* Prepara la disposicion del WS
	*
	*/
	public function init(){
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}

	/**
	* Información Basica de Sistema
	*
	*/
	public function indexAction(){
		$server = new Zend_Rest_Server();
		$server->setClass('Rest_models_Sistema');
		$server->handle();
	}
}