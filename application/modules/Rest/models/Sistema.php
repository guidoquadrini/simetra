<?php

class Rest_models_Sistema {

    protected $adapter = null;
    private $schema_hmi2;

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;
        $this->schema_hmi2 = $registry->config->db_hmi2;
        $this->adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
    }

    /**
     * Listado de Efectores
     */
    public function efectores() {
        $select = $this->adapter->select()
                ->from('efectores', new Zend_Db_Expr('id_efector,nom_efector,id_nodo'), $this->schema_hmi2)
                ->order('nom_efector');

        $efectores = $this->adapter->fetchAssoc($select);
        foreach ($efectores as $i => $v) {
            $efectores[$i]['nom_efector'] = utf8_encode($v['nom_efector']);
        }
        return array('data' => $efectores);
    }

    /**
     * Listado de Nodos
     *
     */
    public function nodos() {
        $select = $this->adapter->select()
                ->from('nodos', '*', 'dsalud_hmi2')
                ->order('nom_nodo');
        return array('data' => $this->adapter->fetchAssoc($select));
    }

    /**
     * Listado de Efectores HTTP -que poseen panel-
     *
     */
    public function efectoreshttp() {
        $select = $this->adapter->select()
                ->from('efectores AS e', new Zend_Db_Expr('e.id_efector,nom_efector,id_nodo'), 'dsalud_hmi2')
                ->join('efectores_ext as h', 'e.id_efector=h.id_efector', 'http_url', $this->_schema)
                ->order('nom_efector');

        $efectores = $this->adapter->fetchAssoc($select);
        foreach ($efectores as $i => $v) {
            $efectores[$i]['nom_efector'] = utf8_encode($v['nom_efector']);
        }

        return array('data' => $efectores);
    }

    /**
     * Listado de Tipos de Camas
     *
     */
    public function clasificacioncamas() {
        $select = $this->adapter->select()
                ->from('clasificaciones_camas', array('id_clasificacion_cama', 'clasificacion_cama', 'abreviatura'), $this->schema_hmi2)
                ->order('clasificacion_cama');

        return array('data' => $this->adapter->fetchAssoc($select));
    }

}
