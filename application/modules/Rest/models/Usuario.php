<?php

class Rest_models_Usuario {

    protected $adapter = null;

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_front;
        $this->adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
    }

    /**
     * Login de Usuario
     *
     * @param string $user
     * @param string md5 $pass
     * @return bool
     */
    public function auth($user, $pass) { //$codename
        $select = $this->adapter->select()
                ->from('usuarios', '*', $this->_schema)
                ->where('usu_usuario=?', $user)
                ->where('usu_clave=MD5(?)', $pass)
                ->limit(1);

        $fetch = $this->adapter->fetchRow($select);
        return (is_array($fetch)) ? array('usuario' => $fetch) : 0;

        /**
         * Proyecto -> Usuario
         */
        $select = $this->adapter->select()
                ->from('usuarios_proyectos AS up', '', $this->_schema)
                ->joinInner('proyectos AS p', 'p.proyecto_id=up.proyecto_id', 'p.pro_activo', $this->_schema)
                ->where('usu_id=?', $front['usu_id'])
                ->where('codename=?', $codename)
                ->limit(1);
        $fetch = $this->adapter->fetchRow($select);
    }

    /**
     * Actualiza Datos de Usuario
     *
     * @param int $usu_id
     * @param string $widgets
     */
    public function update($usu_id, $widgets) {
        $this->adapter->update($this->_schema . '.usuarios', array('usu_widgets' => $widgets), "usu_id={$usu_id}");
    }

}
