<?php

class Soporte_CentralizadosController extends BootPoint {

	/**
	* Listar
	*
	*/
	public function indexAction(){

                $model = new Stock_models_Centralizados();
		$resultados = $model->fetchAll(null,'nroLote')->toArray();

		$this->view->resultados = $resultados;
	}

	/**
	* Alta
	*
	*/
	public function agregarAction(){
            
		$form = new Stock_models_Centralizados();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Stock_models_Productos();
				$model->createRow($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Alta';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

	/**
	* Modificar
	*
	*/
	public function modificarAction(){
            
		$id = $this->getRequest()->getParam('don_id');
                
		$form = new Stock_models_Centralizados();

		if($this->getRequest()->isPost()){
			$post = $this->getRequest()->getPost();
			if($form->isValid($post)){
				$model = new Stock_models_Productos();
				$model->find($don_id)->current()
					->setFromArray($form->getValues())->save();

				$this->getInvokeArg('bootstrap')->getResource('cache')->clean();
				$this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
			}
		}
		else{
			$model = new Stock_models_Productos();
			$form->populate($model->find($id)->current()->toArray());
		}

		$this->view->form = $form;
		$this->view->form_desc = 'Modificar';

		$this->renderScript("{$this->_request->controller}/form.phtml");
	}

    public function eliminarAction(){
        $pro_id = $this->getRequest()->getParam('don_id');

        $model = new Sotck_models_Productos();
        $model->find($don_id)->current()->delete();

        $this->getInvokeArg('bootstrap')->getResource('cache')->clean();
        $this->_helper->redirector('index', $this->_request->controller, $this->_request->module);
    }   
    
    
    
    	/**
	* Nuevo (viene del formulario de pedidos)
	*
	*/
	public function nuevoAction(){
            
            $modelo = new Stock_models_Centralizados();
            
            $modelo->createRow(array(
                        'nroLote' => $this->getRequest()->getParam('nroLote'),
                        'efe_id' => $this->getRequest()->getParam('efe_id'),
                        'sol_id' => $this->getRequest()->getParam('sol_id'),
                        'per_dni' => $this->getRequest()->getParam('per_dni'),     
                        'don_nro' => $this->getRequest()->getParam('don_nro'),     
                        'don_fh_inicio' => $this->getRequest()->getParam('don_fh_inicio'),     
                        'usu_inicio' => $this->getRequest()->getParam('usu_inicio'),  
                        'don_fh_extraccion' => $this->getRequest()->getParam('don_fh_extraccion'),     
                        'don_estado' => $this->getRequest()->getParam('don_estado'),                  
                        'lote_estado' => $this->getRequest()->getParam('lote_estado'),     
                    ))->save();

            $this->_helper->json(array('st' => 'ok'));             

	}
    
    
}

