<?php

class Stock_forms_centralizados extends Zend_Form {

    public function init() {

        $this->addElement($this->createElement('text', 'nroLote')
                        ->setLabel('Nro. de Lote')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 10
                        ))
                        ->setRequired(true));

        $this->addElement($this->createElement('text', 'efe_id')
                        ->setLabel('ID Efector')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 10
                        ))
                        ->setRequired(true));

        $this->addElement($this->createElement('text', 'sol_id')
                        ->setLabel('ID Solicitud')
                        ->setAttribs(array(
                            'maxlength' => 10
                        ))
                        ->setRequired(true));

       $this->addElement($this->createElement('text', 'per_dni')
                        ->setLabel('DNI Persona')
                        ->setAttribs(array(
                            'maxlength' => 20
                        ))
                        ->setRequired(true));    
        
       $this->addElement($this->createElement('text', 'don_nro')
                        ->setLabel('Nro. Donacion')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 32
                        ))
                        ->setRequired(true));           
        
       
        $this->addElement($this->createElement('text', 'don_fh_inicio')
                        ->setLabel('Fecha Donacion Inicio (AAAA-MM-DD)')
                        ->setAttribs(array(
                            'maxlength' => 10
                        ))
                        ->setRequired(true));          
       
   
        
       $this->addElement($this->createElement('text', 'usu_inicio')
                        ->setLabel('ID Usuario Inicio')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 10
                        ))
                        ->setRequired(true));            
        
        
       
        $this->addElement($this->createElement('text', 'don_fh_extraccion')
                        ->setLabel('Fecha Extraccion (AAAA-MM-DD)')
                        ->setAttribs(array(
                            'maxlength' => 10
                        ))
                        ->setRequired(true));           
       
       
       $this->addElement($this->createElement('text', 'don_estado')
                        ->setLabel('Donacion Estado')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 1
                        ))
                        ->setRequired(true));            
       
       
       
       
       $this->addElement($this->createElement('text', 'lote_estado')
                        ->setLabel('Donacion Estado')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 11
                        ))
                        ->setRequired(true));            
        

    }

}
