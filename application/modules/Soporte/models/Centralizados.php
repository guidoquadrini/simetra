<?php

class Stock_models_Centralizados extends Zend_Db_Table_Abstract {

    protected $_name = 'centralizados';
    protected $_primary = 'don_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getPairs() {
        return $this->getDefaultAdapter()->fetchPairs($this->select(true)
                                ->reset('columns')->columns(array('don_id', 
                                    'nroLote', 
                                    'efe_id', 
                                    'sol_id', 
                                    'per_dni', 
                                    'don_nro', 
                                    'don_fh_inicio', 
                                    'usu_inicio', 
                                    'don_fh_extraccion', 
                                    'don_estado',                                   
                                    'lote_estado'
                                    ))
                                //	->where('def=?',1)
                                ->order('don_fh_inicio'));
    }

    
    
    
     public function getById($don_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                'don_id', 
                'nroLote', 
                'efe_id', 
                'sol_id', 
                'per_dni', 
                'don_nro', 
                'don_fh_inicio', 
                'usu_inicio', 
                'don_fh_extraccion', 
                'don_estado',                                   
                'lote_estado',
        ));     

        if ($id != null)
            $query = $query->where("centralizados.don_id IN (?)", $don_id);

        return $this->fetchAll($query);       
    }    
    
    
    
     public function getCentralizados() {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                'don_id', 
                'nroLote', 
                'efe_id', 
                'sol_id', 
                'per_dni', 
                'don_nro', 
                'don_fh_inicio', 
                'usu_inicio', 
                'don_fh_extraccion', 
                'don_estado',                                   
                'lote_estado',
        ));     

        return $this->fetchAll($query);       
    }       
}
