<?php

/**
 * EXAMENES
 */
class Efector_ExamenesController extends BootPoint {

    /**
     * Lista de Examenes para Iniciar
     *
     */
    public function indexAction() {

        $nro = $this->getRequest()->getParam('nro');

        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');

        if ($fechaDesde == '') {
            $fechaDesde = (new DateTime())->setTime('0', '0', '0')->format('d-m-Y H:i:s');
        } else {
            $fechaDesde = (new DateTime($fechaDesde))->setTime('0', '0', '0')->format('d-m-Y H:i:s');
        }

        if ($fechaHasta == '') {
            $fechaHasta = (new DateTime())->setTime('23', '59', '59')->format('d-m-Y H:i:s');
        } else {
            $fechaHasta = (new DateTime($fechaHasta))->setTime('23', '59', '59')->format('d-m-Y H:i:s');
        }

        $perPage = 18;

        /* INICIO - EFECTORES DEL USUARIO */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);

        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);
        $efes_ids = array();
        foreach ($efes_usu as $e) {
            $efes_ids[] = $e['id_efector'];
        }
        /* FIN - EFECTORES DEL USUARIO */

        $modeloDonacionacion = new Banco_models_Donaciones();
        $examenes = $modeloDonacionacion->getByEstadoAndEfe($efes_ids, 104, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro);

        $this->view->busqueda = '';
        if (!empty($nro))
            $this->view->busqueda .= '<br/> Cód. de Donación: ' . $nro . ' <br/>';

        if (!empty($fechaDesde))
            $this->view->busqueda .= 'Fecha Desde: ' . $fechaDesde . '<br/>';


        if (!empty($fechaHasta))
            $this->view->busqueda .= 'Fecha Hasta: ' . $fechaHasta . '<br/>';

        if (!empty($examenes)) {
            //$total = $modeloSolicitudes->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $total = $modeloDonacionacion->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $modeloBolsas = new Efector_models_Bolsas();
        $bolsas = $modeloBolsas->getActivas()->toArray();

        $this->view->cod_colecta = $this->efector_activo_cod . $this->efector_activo['id_efector'];
        $this->view->bolsas = $bolsas;
        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->nro = $nro;
        $this->view->resultados = $examenes;
        $this->view->paginas = $paginas;
    }

    /** Formulario de Examen Medico
     * 
     */
    public function formularioExamenAction() {

        $donacion_id = $this->getRequest()->getParam('don_id');

        $modeloDonacionacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacionacion->fetchRow('don_id=' . $donacion_id);

        $persona_id = $donacion['per_id'];

        $modeloPersona = new Efector_models_Personas();
        $persona = $modeloPersona->fetchRow('per_id=' . $persona_id);

        $codigo_autogenerado = $modeloPersona->getCodigoAutogenerado($persona_id);

        $modeloTipoDonacion = new Gestion_models_Tipodonacion();
        $tipo_donacion = $modeloTipoDonacion->getTipoDonacionFiltrado();

        $modeloColectas = new Promocion_models_Colectas();
        $colectas = $modeloColectas->fetchAll('id=1');

        $this->view->perfiles_produccion = (new Gestion_models_PerfilesProduccion())->getPairs();
        $this->view->cod_colecta_automatico = $this->efector_activo_cod . $this->efector_activo['id_efector'];
        $this->view->colectas = $colectas;
        $this->view->codigo_autogenerado = $codigo_autogenerado;
        $this->view->donacion = $donacion;
        $this->view->persona = $persona;
        $this->view->tipodonacion = $tipo_donacion;
    }

    /** Cambiar estado de la Donacion
     * 2 
     * 3 (Descarte)
     * 6 
     * Cambiar estado de la solicitud
     * 2 - Diferida Temporalmente + Motivo
     *
     */
    public function cambiarEstadoAction() {

        /**
         * Obtener todos los parametros del Request.
         */
        $perfiles_para_produccion = $this->getRequest()->getParam('perfil_de_produccion');
        $numero_de_gestas = $this->getRequest()->getParam('numero_de_gestas');
        $codigo_colecta = $this->getRequest()->getParam('cod_colecta');
        $donacion_id = (int) $this->getRequest()->getParam('don_id');
        $hemoglobina = $this->getRequest()->getParam('hb');
        $hematocritos = $this->getRequest()->getParam('hematocritos');
        $peso = (int) $this->getRequest()->getParam('peso');
        $tension_arterial = $this->getRequest()->getParam('ta');
        $pulso = $this->getRequest()->getParam('pulso');
        $temperatura_corporal = $this->getRequest()->getParam('temperatura_corporal');
        $observacion_examen = $this->getRequest()->getParam('observacion_examen');
        $observacion_diferimiento = $this->getRequest()->getParam('observacion_diferimiento');
        $estado_proceso = (int) $this->getRequest()->getParam('st');
        $motivos_diferimiento = $this->getRequest()->getParam('mot_id');
        
       
        $usuario = $this->auth->getIdentity()->usu_id;
        $fecha_de_inicio = $fecha_de_inicio = new Zend_Db_Expr('NOW()');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->fetchRow('don_id=' . $donacion_id);
        $sol_id = $donacion->sol_id;

        // Donacion Extendida
        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();
        $donacionDetalle = $modeloDetallesDonacion->fetchRow('don_id=' . $donacion_id);

        $modeloSolicitudes = new Efector_models_Solicitudes();

        $solicitud = $modeloSolicitudes->fetchRow('sol_id = ' . $sol_id);

        if (!$solicitud) {
            $this->_helper->json(array('st' => 'NOK', 'mensaje' => 'No existe solicitud alguna vinculada a la donacion. Consulte con Asistencia tecnica para evaluar este Incidente.'));
        }

        if ($perfiles_para_produccion != null) {
            $aPerfiles = array();
            foreach ($perfiles_para_produccion as $perfil) {
                $aPerfiles['perfiles_de_produccion'] = implode(',', $perfiles_para_produccion);
                if ($perfil == 4) {//Multipara
                    $aPerfiles['obeservaciones'] = array(
                        'numero_de_gestas' => $numero_de_gestas
                    );
                }
            }
        }

        //Variable de Retorno y registros para actualizacion.
        $vRet = array();
        $rowSolicitud = array();

        /** ACTUALIZACION DEL REGISTRO EN TABLA CENTRALIZADO * */
        $modeloCentralizado = new Gestion_models_Centralizado();
        $centralizado = $modeloCentralizado->getByNro($donacion->don_nro);

        /**
         * Estados posibles en Tabla Centralizados | don_estado
         * 
         * 1:Aceptada para Examen Medico
         * 2:Aceptada para Extraccion
         * 3:Diferir Donante
         * 
         * */
        $estado_donacion = ($estado_proceso == 2 || $estado_proceso == 3) ? 3 : 2;

        //Actualizar estado del codigo centralizado.
        $centralizado->setFromArray(array(
            'don_estado' => $estado_donacion,
            'estadoLote' => 4,
            'sol_id' => $sol_id,
            'don_fh_inicio' => $fecha_de_inicio
        ))->save();   // actualiza el registro en el modelo centralizado        

        $rowDonacion = array(
            'ppr_id' => ($aPerfiles != null) ? json_encode($aPerfiles) : $aPerfiles,
            'usu_inicio' => $usuario,
            'don_fh_examen' => $fecha_de_inicio,
            'usu_examen' => $usuario, // TODO: Usuario que confirma accion (Hemovigilancia)
            'observaciones' => $donacion->observaciones . (($donacion->observaciones == '') ? '' : " || ") . 'Examen Medico: ' . $observacion_examen
        );

        $rowDonacionDetalle = array(
            'cod_colecta' => $codigo_colecta,
            'hb' => $hemoglobina,
            'hematocritos' => $hematocritos,
            'peso' => $peso,
            'ta' => $tension_arterial,
            'pulso' => $pulso,
            'temperatura_corporal' => $temperatura_corporal
        );

       
        $fecha_hora_fin = new Zend_Db_Expr('NOW()');
        
        switch ($estado_proceso) {
            case 2: //Diferir Temporalmente (3 en Donacion, 2 en Solicitud)
                
                $rowDonacion['don_estado'] = ($estado_proceso == 2) ? 3 : 4; //3: Diferir Donante - 4: Rechazar Donante
                //Aca se actualiza el estado de la solicitud con $st.
                $rowSolicitud = array(
                    'sol_fh_fin' => $fecha_hora_fin,
                    'usu_cierre' => $usuario,
                    'sol_diferido' => null, // Campo deprecado. $dias,
                    'sol_estado' => $estado_proceso,
                    'observacion' => $donacion->observaciones . (($donacion->observaciones == '') ? '' : ' || ') . 'Donación Diferida. Más información en las obsercaciones de la donación.'
                );

                $rowDonacion = array(
                    'don_estado' => 3,
                    'observaciones' => $rowDonacion['observaciones'] . (($rowDonacion['observaciones'] == '') ? '' : " || ") . 'Examen Diferido Temporal: ' . $observacion_diferimiento
                );

                
                /* Grabar en sol_diferimientos los N motivos */
                $modeloSolicitudesiitudDiferimientos = new Efector_models_SolicitudesDiferimientos();
                foreach ($motivos_diferimiento as $motivo_id) {
                    $modeloSolicitudesiitudDiferimientos->createRow(array(
                        'sol_id' => $solicitud->sol_id,
                        'mot_id' => $motivo_id
                    ))->save();
                }

                $vRet = array('st' => 'OK', 'mensaje' => 'El diferimiento Temporal fue exitoso.');

                break;

            case 3: //Diferir Permanentemente
                //$fecha_hora_fin = new Zend_Db_Expr('NOW()');
                $rowDonacion['don_estado'] = ($estado_proceso == 2) ? 3 : 4; //3: Diferir Donante - 4: Rechazar Donante
                //Aca se actualiza el estado de la solicitud con $st.
                $rowSolicitud = array(
                    'sol_fh_fin' => $fecha_hora_fin,
                    'usu_cierre' => $usuario,
                    'sol_diferido' => null, // Campo deprecado. $dias,
                    'sol_estado' => $estado_proceso,
                    'observacion' => $donacion->observaciones . (($donacion->observaciones == '') ? '' : ' || ') . 'Donación Diferida. Más información en las obsercaciones de la donación.'
                );

                $rowDonacion = array(
                    'don_estado' => 4,
                    'observaciones' => $rowDonacion['observaciones'] . (($rowDonacion['observaciones'] == '') ? '' : " || ") . 'Examen Diferido Permanente: ' . $observacion_diferimiento
                );

                /* Grabar en sol_diferimientos los N motivos */
                $modeloSolicitudesiitudDiferimientos = new Efector_models_SolicitudesDiferimientos();
                foreach ($motivos_diferimiento as $motivo_id) {
                    $modeloSolicitudesiitudDiferimientos->createRow(array(
                        'sol_id' => $solicitud->sol_id,
                        'mot_id' => $motivo_id
                    ))->save();
                }

                $vRet = array('st' => 'OK', 'mensaje' => 'El diferimiento Permanente fue exitoso.');

                break;
            case 4: //Pasa a Extraccion

                $rowDonacion['don_estado'] = 2;

                $rowSolicitud = array(
                    'sol_estado' => $estado_proceso
                );

                $vRet = array('st' => 'OK', 'mensaje' => 'Se ha grabado el Exámen Médico !!!');

                break;
        }

        // Actualiza el estado de la DONACION
        $donacion->setFromArray($rowDonacion)->save();

        // Actualiza el estado de la DONACION EXTENDIDA (detalle)
        $donacionDetalle->setFromArray($rowDonacionDetalle)->save();

        // Actualiza el estado de la solicitud.
        $solicitud->setFromArray($rowSolicitud)->save();

        $this->_helper->json($vRet);
    }

    public function codigosAction() {


        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cod = $this->getRequest()->getParam('cod');

        $pdf = new Zend_Pdf();

        $x = 290;
        $y = 70;
        $page = new Zend_Pdf_Page($x, $y);
        //$font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/arial.ttf');
        $page->setFont($font, 8);

        $pdf->pages[] = $page;
        $top = 25;
        $leftA = 6;
        $leftB = 155;

        $modeloCon = new Banco_models_Contingencia();
        $rowset = $modeloCon->fetchAll();

        foreach ($rowset as $r) {
            if ($r->don_nro == $cod)
                $row = $r;
        }
        /* Sacar efector de base de datos, no el activo */
        $efector = $this->efector_activo['nomest'];
        $efecTrunc = sprintf("%10.25s", $efector);

        /* Codigo de barra A */
        $page->drawText($efecTrunc, $leftA + 5, 50);

        $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', array(
                    'font' => 'fonts/arial.ttf',
                    'text' => $row->don_nro,
                    'fontSize' => 12,
                    'barThickWidth' => 3,
                    'barThinWidth' => 1,
                    'stretchText' => true
                        ), array(
                    'topOffset' => $top,
                    'leftOffset' => $leftA
                        )
                )->setResource($pdf)->draw();

        /* Codigo de barra B - Igual a A excepto en el leftOffset */
        $page->drawText($efecTrunc, $leftB + 5, 50);
        $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', array(
                    'font' => 'fonts/arial.ttf',
                    'text' => $row->don_nro,
                    'fontSize' => 12,
                    'barThickWidth' => 3,
                    'barThinWidth' => 1,
                    'stretchText' => true
                        ), array(
                    'topOffset' => $top,
                    'leftOffset' => $leftB
                        )
                )->setResource($pdf)->draw();

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

}
