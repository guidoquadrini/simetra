<?php

/** Estados de la solicitud
 * 0 - Cerrada sin problemas
 * 1 - Aceptada
 * 2 - Diferida + Motivo
 * 3 - Rechazada + Motivo + texto?
 * 4 - Pendiente
 * 5 - Pasa a Donacion
 */
class Efector_SolicitudesController extends BootPoint { //implements VistasGenericasInterface{

    /*     * FORMULARIOS (VISTAS)* */
    /** Seccion para buscar Solicitudes por fecha * */

    /** Calendario para elegir dias / dni * */
    public function indexAction() {
        $fecha = $this->getRequest()->getParam('fecha');
        $dni = (int) $this->getRequest()->getParam('dni');

        $perPage = 18;

        /* Obtengo los efectores del usuario */

        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";
        $pry = $modeloPry->getByNom($nom);
        $pry_id = $pry->pry_id;
        $modeloUsu = new Acceso_models_Usuarios();
        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);

        foreach ($efes_usu as $e) {
            $efes_ids[] = $e['id_efector'];
        }
        /* Fin efectores usuario */

        $mSolicitudes = new Efector_models_Solicitudes();
        if ($dni > 0) {
            $mPersonas = new Efector_models_Personas();
            $persona = $mPersonas->getByDni($dni)->toArray();
            $solicitudes = $mSolicitudes->getByDniPersonaAndEfe($efes_ids, $persona['per_id'], $this->page, $perPage);

            $this->view->busqueda = 'DNI: ' . $dni;
        } else {
            if (is_null($fecha)) {
                $fecha = date('Y-m-d');
            }

            $solicitudes = $mSolicitudes->getByFechaAndEfe($efes_ids, $fecha, $this->page, $perPage);

            $this->view->busqueda = $fecha;
        }

        if (sizeof($solicitudes) > 0) {
            $mMotivosDiferimientos = new Efector_models_Diferimientos();
            //Buscar los motivos de dif que esten dentro de las solicitudes
            $SolicitudesMotivosDiferimiento = $mMotivosDiferimientos->getMotivosPorSolicitudes(
                    array_column($solicitudes->toArray(), 'sol_id')
            );
            $total = $mSolicitudes->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }
        $mMotivos = new Gestion_models_Motivos();
        $motivos = $mMotivos->fetchAll()->toArray();
        $modeloEfec = new Acceso_models_Efectores();
        $efectores = $modeloEfec->fetchAll()->toArray();

        $this->view->motivos = $motivos;
        $this->view->efectores = $efectores;
        $this->view->fecha = $fecha;
        $this->view->dni = $dni;
        $this->view->resultados = $solicitudes;
        $this->view->resultados_motivosDiferimiento = $SolicitudesMotivosDiferimiento;
        $this->view->paginas = $paginas;
    }

    public function eliminarAction() {
        $sol_id = (int) $this->getRequest()->getParam('sol_id');
        $this->view->sol_id = $sol_id;
        //TODO: Validar que venga un id de solicitud por parametro.
        $mSolicitudes = new Efector_models_Solicitudes();
        $solicitud = $mSolicitudes->fetchRow('sol_id =' . $sol_id);
        //TODO: Validar que encontro una solicitud
        //TODO: Validar que la solicitud tenga una persona asosciada.
        $mPersonas = new Efector_models_Personas();
        $persona = $mPersonas->fetchRow('per_id = ' . $solicitud['per_id']);
        //TODO: Validar que la persona existe (Trajo Datos)
        $persona = $persona->toArray();
        $codigo_autogenerado = $mPersonas->getCodigoAutogenerado($persona['per_id']);
        $persona['codigo_autogenerado'] = $codigo_autogenerado;

        $mDonacion = new Banco_models_Donaciones();
        $donacion = $mDonacion->fetchRow('sol_id = ' . $sol_id);

        $this->view->donacion = $donacion;
        $this->view->solicitud = $solicitud;
        $this->view->persona = $persona;
    }

    public function crearCargaRapidaAction() {
        $params = $this->getRequest()->getParams();
        $mSolicitudes = new Efector_models_Solicitudes();
        $mMotivos = new Gestion_models_Motivos();
        $mUsuarios = new Acceso_models_Usuarios();
        $mPersonas = new Efector_models_Personas();
        $mDonacion = new Banco_models_Donaciones();
        $mCentralizado = new Gestion_models_Centralizado();
        $mDonacionDetalles = new Efector_models_DetallesDonaciones();
        $mEfectoresParametros = new Efector_models_EfectoresParametros();
        $mPersonaSicap = new Efector_models_PersonasSicap();
        $mEnvio = new Efector_models_Envios();
        $mEfectores_Acceso = new Acceso_models_Efectores();
        $Catalogo_efectores = $mEfectores_Acceso->getPairs();
        $oPersona = $mPersonas->getByDni2($params['dni'])->toArray();




        //Inicio Controles
        //Control de solicitudes pendientes
        $solicitud = $mSolicitudes->getUltimaDni($oPersona[per_id], 1);
        if ($solicitud) {
            $response = array(
                'st' => 'solicitudes_pendientes',
                'mensaje' => 'El donante tiene una solicitud pendiente'
            );
            $this->_helper->json($response);
        }
        //Control de serologia

        if ($oPersona['ult_serologia']) {
            $response = array(
                'st' => 'serologia',
                'mensaje' => 'Donante no apto',
                'ser' => (int) $oPersona['ult_serologia']
            );
            $this->_helper->json($response);
        }

        //Control donante diferido
        $solicitudesDiferidas = $mSolicitudes->getByDniSolDiferida($oPersona['per_id']);
        if (count($solicitudesDiferidas) != 0) {
            /* Motivo por el cual esta diferido */
            $usuarios = array();
            foreach ($solicitudesDiferidas as $i => $s) {
                $response[] = [
                    'motivo' => $s['mot_id'],
                    'tiempo' => $s['sol_diferido'],
                    'efector' => $s['efe_id'],
                    'usuario_id' => $s['usu_cierre'],
                    'usuario' => $mUsuarios->find($s['usu_cierre'])->current()->toArray(),
                    'motivo_descripcion' => $mMotivos->getDesc($s['mot_id'])
                ];
            }
            $detalle = '<ol id="response_motivo">';
            foreach ($response as $b => $c) {
                $detalle .= '<li>Motivo: (' . $c['motivo'] . ')' . $c['motivo_descripcion'] . '<br></li>';
                $detalle .= '<span id="response_dias">Cantidad de dias: ' . $c['tiempo'] . '</span><br>';
                $detalle .= '<span id="response_usuario">Usuario registro: ' . $c['usuario']['nombre'] . ', ' . $c['usuario']['nombre'] . '</span><br>';
                $detalle .= '<span id="response_usuario">Realizado en el efector: ' . $this->view->cache_efectores[$c['efector']] . '</span></li>';
            }
            $detalle .= '</ol>';

            $this->_helper->json(array('st' => 'paciente_diferido',
                'mensaje' => 'El donante posee solicitud/es diferidas activas',
                'detalle' => $detalle
            ));
        }

        //Fin Controles
        //        
        //Crear Solicitud

        $row_solicitud = array(
            'per_id' => $oPersona['per_id'],
            'sol_cod' => $this->efector_activo['id_efector'] . date("YmdHis"),
            'efe_id' => $this->efector_activo['id_efector'],
            'usu_creacion' => $this->auth->getIdentity()->usu_id,
            'tdo_id' => isset($params['dtp_id']) ? $params['dtp_id'] : null,
            'ocu_id' => isset($params['ocu_id']) ? $params['ocu_id'] : 1,
            'afe_id' => isset($params['afe_id']) ? $params['afe_id'] : null,
            'sol_estado' => 5, // 5 - Pasa a Donacion
            'cph' => $params['chk_cph'],
            'plaqueta' => $params['chk_plaquetas']
        );

        $pk_solicitud = $mSolicitudes->createRow($row_solicitud)->save();
        $oSolicitud = $mSolicitudes->find($pk_solicitud)->current();

        $mPersonaSicap->update(array('CodOcupacion' => $params['ocu_id']), "NroDocumento = '{$params['dni']}'");
        //Fin Solicitud
        //Crear Donancion   
        //Busco en modelo de codigo centralizado el cod, si existe.
        $oCentralizado = $mCentralizado->getByNro($params['nro_donacion']);
        //  don_estado = 1 :Aun no pasada a don_donaciones
        //  don_estado = 2 :Ya pasada a don_donaciones

        $oCentralizado->setFromArray(array(
            'don_estado' => 2,
            'estadoLote' => 4,
            'sol_id' => $oSolicitud->sol_id,
            'don_fh_inicio' => $params['fecha_hora_extraccion'],
            'don_fh_extraccion' => $params['fecha_hora_extraccion']
        ))->save();

        $estado = 4;

        $row_Donacion = array(
            'efe_id' => $oCentralizado->efe_id,
            'sol_id' => $oSolicitud->sol_id,
            'per_id' => $oSolicitud->per_id,
            'don_nro' => $oCentralizado->don_nro,
            //'ppr_id' => , 
            'don_fh_inicio' => $params['fecha_hora_extraccion'],
            'usu_inicio' => $this->auth->getIdentity()->usu_id,
            'don_fh_aceptacion' => new Zend_Db_Expr('NOW()'),
            'usu_aceptacion' => $this->auth->getIdentity()->usu_id,
            'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
            'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
            'don_fh_recepcion' => new Zend_Db_Expr('NOW()'),
            'don_usu_recepcion' => $this->auth->getIdentity()->usu_id,
            //despacho_id	int(11) NULL	 
            //mot_id	int(11) NULL	 
            //observaciones	varchar(255) NULL	             
            'don_estado' => $estado,
            'don_tiempo_extraccion' => $params['tiempo_extraccion'],
            'don_peso' => (int) $params['peso_donante'],
            'pro_estado' => $params['estado_donacion'],
            'recepcion_bolsa' => $params['recepcion_bolsa'],
            'recepcion_muestra' => $params['recepcion_muestra'],
            'don_temp' => $params['temperatura'],
            'cod_muni' => '' //Este codigo no va mas.            
        );

        $pk_Donacion = $mDonacion->createRow($row_Donacion)->save();
        $oDonacion = $mDonacion->find($pk_Donacion)->current();

        $row_donacion_detalles = array(
            'don_id' => (int) $oDonacion->don_id,
            'cod_colecta' => $params['cod_colecta'], //Revisar como se hace en extraccion/examen
            'tipo' => $params['tipo_donacion'],
            'peso' => (int) $params['peso_bolsa'],
            'hb' => (int) $params['hb'],
            'ta' => (int) $params['ta'],
            'pulso' => (int) $params['pulso'],
            'tipo_bolsa' => $params['tipo_bolsa'],
            'marca_bolsa' => $params['marca_bolsa'],
            'hematocritos' => $params['hematocritos'],
            'temperatura_corporal' => $params['temperatura_corporal'],
            'nroLote_bolsa' => $params['nroLote_bolsa'],
            'nroTubuladura' => $params['nroTubuladura'],
            'reaccion' => $params['tipo_reaccion']
        );

        $pk_donacionDetalles = $mDonacionDetalles->createRow($row_donacion_detalles)->save();
        $oDonacionDetalle = $mDonacionDetalles->find($pk_donacionDetalles)->current();

        //Descartar Donacion //Reproduccion de descarte estilo extraccion.
        //Revisar si esta es la mejor manera.        


        if (($params['estado_donacion'] == 2) || ($params['estado_donacion'] == 3)) {
            $oSolicitud->setFromArray(array(
                'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
                'usu_cierre' => $this->auth->getIdentity()->usu_id,
                'sol_estado' => 0
            ))->save();
            $oDonacion->setFromArray(array(
                'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
                'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
                'don_estado' => 3,
                'mot_id' => $params['motivo_descarte'],
                'observaciones' => $params['observaciones']
            ))->save();
        }

        //Crear Envio de la donacion.///////////////////////////////////////////
        $row_envio = array(
            'efe_id' => $params['efe_id'],
            'efe_nombre_origen' => $Catalogo_efectores[$params['efe_id']],
            'env_nro' => $params['efe_id'] . date("YmdHis"),
            'env_fh' => new Zend_Db_Expr('NOW()'),
            'env_usuario' => $this->auth->getIdentity()->usu_id,
            'env_estado' => 1,
            'observaciones' => $params['obs_envio']
        );

        $pk_envio = $mEnvio->createRow($row_envio)->save();

        $oDonacion->setFromArray(array('despacho_id' => $pk_envio))->save();

        //Recibir la donacion.//////////////////////////////////////////////////
        if ($params['recepcion_bolsa'] == 1) {
            $params['recepcion_bolsa'] = ($params['recepcion_bolsa'] == null) ? 0 : $params['recepcion_bolsa'];
            $params['recepcion_muestra'] = ($params['recepcion_muestra'] == null) ? 0 : $params['recepcion_muestra'];
            $params['temperatura'] = (!$params['temperatura']) ? 0 : $params['temperatura'];

            $row_recepcion = array('don_estado' => 10,
            );
            $oDonacion->setFromArray($row_recepcion)->save();

            /* Si fue aceptada, insertamos en pre-produccion *
             * *********************************************** */
            if ($params['estado_donacion'] == 1) {

                $don_id = $oDonacion->don_id;
                $ppr_id = $oDonacion->ppr_id;

                $modeloPre = new Banco_models_DetallesPreProduccion();
                $modeloHemoPer = new Banco_models_HemocomponentesPerfiles();
                $perfiles = explode(",", $ppr_id);
                if ($ppr_id != null && $ppr_id != "") {
                    $hemo = $modeloHemoPer->listHemoByPerfiles(explode(",", $ppr_id));
                    $hemos = array();
                    foreach ($hemo as $i => $v)
                        $hemos[] = $v["idhemocomponente"];

                    /* Listo hemocomponentes quitando los no permitidos por el perfil */
                    $modeloHem = new Gestion_models_Hemocomponentes();
                    $hem = $modeloHem->getPairsPerfil($hemos);

                    foreach ($hem as $i) {
                        $pre_id = $modeloPre->createRow(array(
                                    'don_id' => $don_id,
                                    'hem_id' => $i['hem_id'],
                                    'estado' => 1,
                                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                                    'timestamp' => new Zend_Db_Expr('NOW()'),
                                    'tipo_prod' => 1
                                ))->save();
                    }
                } else {
                    $modeloHem = new Gestion_models_Hemocomponentes();
                    $hemo = $modeloHem->getPairs();
                    foreach ($hemo as $i => $v) {
                        $pre_id = $modeloPre->createRow(array(
                                    'don_id' => $don_id,
                                    'hem_id' => $i,
                                    'estado' => 1,
                                    'usuario_id' => $this->auth->getIdentity()->usu_id,
                                    'timestamp' => new Zend_Db_Expr('NOW()'),
                                    'tipo_prod' => 1
                                ))->save();
                    }
                }
            }
        }

        $oUsuario = $mUsuarios->find($oDonacion->usu_aceptacion)->current();
        $donante_nombreCompleto = $oPersona['per_nombres'] . " " . $oPersona['per_apellido'];
        $usuario_nombreCompleto = $oUsuario->usu_nombre . " " . $oUsuario->usu_apellido;
        $idEnvioCodEnvio = "(" . $pk_envio . ") - " . $params['efe_id'] . date('YmdHis');
        $response = array(
            'st' => 'carga_rapida_exitosa',
            'mensaje' => "<ul><li>Solicitud: $oDonacion->sol_id</li>" .
            "<li>Nro. Donacion: $oDonacion->don_nro </li>" .
            "<li>Codigo de Envio Donacion:$idEnvioCodEnvio</li>" .
            "<li>Donante:$donante_nombreCompleto</li>" .
            "<li>Usuario:$usuario_nombreCompleto</li></ul>"
        );

        //Respuesta Final///////////////////////////////////////////////////////
        $this->_helper->json($response);
    }

    /*     * PROCESOS Y OPERACIONES* */

    public function eliminarsolicitudAction() {
        $sol_id = (int) $this->getRequest()->getParam('solicitudes_id');
        $don_id = (int) $this->getRequest()->getParam('donaciones_id');
        $per_id = (int) $this->getRequest()->getParam('personas_id'); //No se utiliza.

        $mSolicitudes = new Efector_models_Solicitudes();
        $mSolicitudes->fetchRow('sol_id = ' . $sol_id)->setFromArray(array('sol_estado' => 0))->save();
        $mDonaciones = new Banco_models_Donaciones();
        $donacion = $mDonaciones->fetchRow('don_id = ' . $don_id);
        $donacion->setFromArray(array('don_estado' => 102))->save();

        $mCentralizado = new Gestion_models_Centralizado();

        $centralizado = $mCentralizado->fetchRow('don_nro = "' . $donacion->don_nro . '"');

        $centralizado->setFromArray(array('don_estado' => 102))->save();

        $this->_helper->json(array('estado' => 'OK'));
    }

    public function editarAction() {
        //TODO: Mejorar el control de errores.
        $sol_id = (int) $this->getRequest()->getParam('sol_id');
        $mSolicitudes = new Efector_models_Solicitudes();
        $mSolicitudes->find($sol_id)->current()->setFromArray(array(
            'sol_estado' => 1
        ))->save();
        $mDonacion = new Banco_models_Donaciones();
        $donacion = $mDonacion->getBySolID($sol_id);

        if ($donacion) {
            $donacion->setFromArray(array('don_estado' => 3))->save();
            $this->_helper->json(array('st' => 'ok'));
        }
        $this->_helper->json(array('st' => 'nok'));
    }

    private function getCenterText($width, $text, $font, $fontSize) {

        return ($width / 2) - ($this->getTextWidth($text, $font, $fontSize) / 2);
    }

    private function getTextWidth($text, $font, $fontSize) {
        $drawingText = iconv('ISO-8859-1', 'ISO-8859-1', $text);

        $characters = array();
        for ($i = 0; $i < strlen($drawingText); $i++) {
            $characters[] = ord($drawingText[$i]);
        }
        $widths = $font->widthsForGlyphs($font->glyphNumbersForCharacters($characters));
        return (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
    }

    public function cargaRapidaAction() {
        //Tomar parametros para identificar al Donante.
        $documento = (int) $this->getRequest()->getParam('documento');
        $tipo_documento = $this->getRequest()->getParam('tipo');

        $dni_donante = $documento; //this->getRequest()->getParam('dni');
        //Cargar los efectores.
        $modeloEfe = new Acceso_models_Efectores();
        $this->view->efectores = $modeloEfe->getPairs();
        //Cargar los tipos de bolsa
        $mBolsas = new Efector_models_Bolsas();
        $this->view->tipo_bolsas = $mBolsas->getActivas();
        //Cargar las marcas de las bolsas
        $this->view->marca_bolsas = ['Baxter', 'Rivero'];
        //Tipos de donacion
        $this->view->tipos_donacion = ['Voluntaria', 'Reposición', 'Autologa'];
        //Cargar los tipos de aferesis
        $mAferesis = new Gestion_models_Aferesis();
        $this->view->tipos_eferesis = $mAferesis->getPairs();
        //Pasar el paciente consultado.
        $mUsuarios = new Efector_models_Personas();
        $oUsuario = $mUsuarios->getByDni($dni_donante);
        $this->view->persona = ['documento' => $documento, 'tipo' => $tipo_documento,
            'nombreCompleto' => $mUsuarios->getNombreCompleto($dni_donante)
        ];
    }

    /** Json * */
    public function altaAction() {
        //Si se postea,  tomar los parametros
        //Buscar en la base de donantes si existe el donante.
        //En el caso que exista poner los botones de accion y datos en pantalla.
        //En el caso que no exista mostrar boton de alta.
    }

    /** Crear Solicitud - JSON ? * */
    public function crearAction() {
        $documento = (int) $this->getRequest()->getParam('documento');
        $tipo_documento = $this->getTipoDocumentoPorNombre($this->getRequest()->getParam('tipo'));

        $bol_cph = (int) $this->getRequest()->getParam('chk_cph');
        $bol_plaquetas = (int) $this->getRequest()->getParam('chk_plaquetas');
        //$dni = (int) $this->getRequest()->getParam('dni');
        $post = $this->getRequest()->getPost();
        $mSolicitudes = new Efector_models_Solicitudes();
        $mMotivos = new Gestion_models_Motivos();

        $mUsuariosAcceso = new Acceso_models_Usuarios();

        // Modelo personas, para guardar per_id en vez de DNI como id
        $mPersonas = new Efector_models_Personas();
        $persona = $mPersonas->getByDni2($documento, $tipo_documento)->toArray();

        $solDiferidas = $mSolicitudes->getByDniSolDiferida($persona[per_id]);

        $solicitud = $mSolicitudes->getUltimaDni($persona[per_id], 1);

        if ($solicitud) {
            $this->_helper->json(array('st' => 'nok', 'mensaje' => 'Usted tiene una solicitud pendiente',
                'motivo' => '',
                'dias' => '',
                'efector' => '',
                'usuario' => ''
            ));
        }

        if ($persona['ult_serologia']) {
            $this->_helper->json(array('st' => 'serologia', 'mensaje' => 'Donante no apto',
                'motivo' => '',
                'dias' => '',
                'efector' => '',
                'ser' => (int) $persona['ult_serologia'],
                'usuario' => ''
            ));
        }

        if (count($solDiferidas) != 0) {
            /* Motivo por el cual est� diferido */
            $motivos = array();
            $tiempos = array();
            $efectores = array();
            $usuarios = array();
            foreach ($solDiferidas as $i => $s) {
                $motivos[] = $s['mot_id'];
                $tiempos[] = $s['sol_diferido'];
                $efectores[] = $s['efe_id'];
                $usuarios[] = $s['usu_cierre'];
            }
            $desc = $mMotivos->getDesc($motivos[0]);
            $usuario = $mUsuariosAcceso->find($usuarios[0])->current()->toArray();

            foreach ($desc as $i => $d)
                $descripcion[] = $i;

            $this->_helper->json(array('st' => 'nok',
                'mensaje' => 'Usted tiene una solicitud diferida activa',
                'motivo' => 'Motivo: ' . $descripcion[0],
                'dias' => 'D&iacute;as diferidos: ' . $tiempos[0],
                'efector' => 'Efector: ' . $this->view->cache_efectores[$efectores[0]],
                'usuario' => 'Usuario: ' . $usuario['usu_nombre']
            ));
        }

        /* Si es af�resis, dejo el id de af�resis 
         *  Si no es af�resis, elimino el id de 
         *  tipo de af�resis, lo mismo hago si
         *  viene en vac�o
         */
        if ($post['dtp_id'] == 1) {
            /* Sigue igual */
        } else {
            unset($post['afe_id']);
        }

        if ($post['afe_id'] == '')
            unset($post['afe_id']);

        //Se agreg� ID de af�resis
        // el numero de solicitud se crea con el id_efector + date("YmdHis")
        $row = $mSolicitudes->createRow(array(
            'per_id' => $persona['per_id'],
            'sol_cod' => $this->efector_activo['id_efector'] . date("YmdHis"),
            'efe_id' => $this->efector_activo['id_efector'],
            'usu_creacion' => $this->auth->getIdentity()->usu_id,
            'tdo_id' => $post['dtp_id'],
            'ocu_id' => $post['ocu_id'],
            'afe_id' => $post['afe_id'],
            'sol_estado' => 1,
            'cph' => $bol_cph,
            'plaqueta' => $bol_plaquetas
        ));

        $row->save();
        $mPersonasSicap = new Efector_models_PersonasSicap();
        $mPersonasSicap->update(array('CodOcupacion' => $post['ocu_id']), "NroDocumento = '{$documento}'");
        $this->_helper->json(array('st' => 'ok', 'id' => $row->sol_id));
    }

    // Accion para crear solicitud a pesar de serología positiva.
    public function crearSinControlAction() {
        //$documento = (int) $this->getRequest()->getParam('documento');
        $tipo_documento = $this->getTipoDocumentoPorNombre($this->getRequest()->getParam('tipo'));

        $post = 'documento=' . $this->getRequest()->getParam('documento');
        $exploded = array();
        parse_str($post, $exploded);
        $bol_cph = (int) $exploded['chk_cph'];
        $documento = (int) $exploded['documento'];
        $dtp_id = (int) $exploded['dtp_id'];
        $ocu_id = (int) $exploded['ocu_id'];
        $bol_plaquetas = (int) $exploded['chk_plaquetas'];
        //$post = $this->getRequest()->getPost();

        $mSolicitudes = new Efector_models_Solicitudes();
        $mMotivos = new Gestion_models_Motivos();
        $modeloEfe = new Acceso_models_Efectores();
        $mUsuariosAcceso = new Acceso_models_Usuarios();

        // Modelo personas, para guardar per_id en vez de DNI como id
        $mPersonas = new Efector_models_Personas();
        $persona = $mPersonas->getByDni2($documento, $tipo_documento)->toArray();

        $efectoresAll = $modeloEfe->getPairs();

        $row = $mSolicitudes->createRow(array(
            'per_id' => $persona['per_id'],
            'sol_cod' => $this->efector_activo['id_efector'] . date("YmdHis"),
            'efe_id' => $this->efector_activo['id_efector'],
            'usu_creacion' => $this->auth->getIdentity()->usu_id,
            'tdo_id' => $dtp_id,
            'ocu_id' => $ocu_id,
            'sol_estado' => 1,
            'cph' => $bol_cph,
            'plaqueta' => $bol_plaquetas
        ));
        $row->save();


        $mPersonasSicap = new Efector_models_PersonasSicap();
        $mPersonasSicap->update(array('CodOcupacion' => $post['ocu_id']), "NroDocumento = '{$dni}'");

        $this->_helper->redirector('alta', $this->_request->controller, $this->_request->module, array());
    }

    public function formNuevaDonacionAction() {
        //TODO: Aqui se deberia acceder directamente con el ID de Persona.

        $tipo_documento = $this->getRequest()->getParam('tipo_documento');
        $documento = (int) $this->getRequest()->getParam('nro_documento');

        $modeloPersona = new Efector_models_Personas();
        $persona = $modeloPersona->getByTipoYNum($documento, $tipo_documento)->toArray();

        $persona['codigo_autogenerado'] = $modeloPersona->getCodigoAutogenerado($persona['per_id']); //

        $this->view->persona = $persona;
    }

    //Crea una nueva donacion. Primero crea una solicitud y luego la donacion.
    public function grabarNuevaDonacionAction() {
        $vRet = [
            'estado' => 'OK'
        ];

        //Asigno parametros a variables.
        $donacion_codigo = $this->getRequest()->getParam('donacion_codigo');
        $clasificacion_donante = $this->getRequest()->getParam('clasificacion_donante');
        $tipo_donacion = $this->getRequest()->getParam('tipo_donacion');
        $tipo_extraccion = $this->getRequest()->getParam('tipo_extraccion');
        $aferesis_id = $this->getRequest()->getParam('aferesis_id');
        $registrar_para_cph = ($this->getRequest()->getParam('chk_cph', false) !== false) ? true : false;
        $registrar_para_plaquetas = ($this->getRequest()->getParam('chk_plaquetas', false) !== false) ? true : false;
        $ocupacion_id = $this->getRequest()->getParam('ocupacion_id');
        $persona_id = $this->getRequest()->getParam('persona_id');
        $tipo_documento = $this->getRequest()->getParam('documento_tipo');
        $numero_documento = $this->getRequest()->getParam('documento_numero');

        if (($persona_id == null || $numero_documento == null || $tipo_documento == null)) {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0006',
                'mensaje' => 'El proceso de "grabarNuevaDonacion" no pudo ejecutarse adecuadamente. Debido a un'
                . 'Error en los parametros de entrada.'
            ];
            $this->_helper->json($vRet);
            exit;
        }
        /**
          //$doancion_codigo //TODO: Validar que el codigo de donacion no haya sido utilizado.
          //         .on('submit', '.trata-sol', function () {
          //                    //var Respuesta = validaTubuladura();
          //                    if (1) {
          //                        $(this).find('button[type=submit]').button('loading');
          //                        $.post('<!?= $this->baseUrl(); ?>/efector/examenes/cambiar-estado', $(this).serialize(), function (d) {
          //                            if (d.st === 'ok') {
          //                                window.location.href = '<!?= $this->baseUrl(); ?>/efector/examenes';
          //                            } else if (d.st === 'notok') {
          //                                alert("El código de contingencia o código de barras de la donación ya ha sido ingresado");
          //                                $(':submit').button('reset');
          //                            } else if (d.st === 'nok_generacion_codigo') {
          //                                alert("Se debe ingresar un código de barras de la donación válido antes de continuar");
          //                                $(':submit').button('reset');
          //                            } else {
          //                                alert("El código de contingencia o código de barras de la donación ingresado no existe");
          //                                $(':submit').button('reset');
          //                            }
          //                        }, 'json');
          //                    } else {
          //                        alert('Revisar el Código de tubuladura, este ya fue utilizado.');
          //                    }
          //                });* */
        $mPersonas = new Efector_models_Personas();
        $persona = $mPersonas->getByDocuemento($tipo_documento, $numero_documento);
        $persona->setFromArray(array(
            'clasificaciondonante_id' => $clasificacion_donante,
            'codOcupacion' => $ocupacion_id,
        ))->save();
        $mSolicitudes = new Efector_models_Solicitudes();
        $mMotivos = new Gestion_models_Motivos();
        $mUsuariosAcceso = new Acceso_models_Usuarios();

        /* Metodo de contruccion del la Solicitud ID
         * Solicitud ID = EFECTOR ID  + FECHA Y HORA ACTUAL
         */

        //Crear nuvo registro de SOLICITUD
        $nueva_solicitud = array(
            //            sol_id //Autogenerado con la creacion
            //            sol_fh_fin //Actualiza con el cierre.
            //            usu_cierre //Actualizada con el cierre
            //            sol_diferido //Revision
            //            observacion //Revision
            'per_id' => $persona_id,
            'sol_cod' => $this->efector_activo['id_efector'] . date("YmdHis"),
            'efe_id' => $this->efector_activo['id_efector'],
            'sol_fh_ini' => new Zend_Db_Expr('NOW()'), //REVISAR SI VA AHORA
            'usu_creacion' => $this->auth->getIdentity()->usu_id,
            'tdo_id' => $tipo_donacion,
            'ocu_id' => $ocupacion_id,
            'afe_id' => (empty($aferecis_id)) ? NULL : $aferesis_id,
            'sol_estado' => 1,
            'cph' => $registrar_para_cph,
            'plaqueta' => $registrar_para_plaquetas,
            'sol_estado' => 1, //REVISAR EL ESTADO CORRECTO
        );

        //Grabo y Obtengo el ID de la Solicitud recien creada.
        $solicitud_id = $mSolicitudes->createRow($nueva_solicitud)->save();

        // Creo el registro para la Donacion
        if (!empty($solicitud_id)) {

            //Actualizo Ocupacion de la Persona
            $mPersonas = new Efector_models_Personas();
            $mPersonas->update(array('codOcupacion' => $ocupacion_id), "per_id = '{$persona_id}'");

            try {
                $modelDonaciones = new Banco_models_Donaciones();
                $nueva_donacion = array(
                    //                   don_id //Autogenerado con la creacion
                    'efe_id' => $this->efector_activo['id_efector'],
                    'sol_id' => $solicitud_id,
                    'per_id' => $persona_id,
                    'don_nro' => $donacion_codigo,
                    'don_fh_inicio' => new Zend_Db_Expr('NOW()'),
                    'usu_inicio' => $this->auth->getIdentity()->usu_id,
                    //                   ppr_id
                    //                   don_fh_extraccion
                    //                   don_usu_extraccion
                    //                   don_fh_examen
                    //                   usu_examen
                    //                   despacho_id
                    //                   mot_id
                    //                   observaciones
                    'don_estado' => 1, // Alta y Listo para Entrevista
                        //                   don_tiempo_extraccion
                        //                   don_peso
                        //                   pro_estado
                        //                   recepcion_bolsa
                        //                   recepcion_muestra
                        //                   don_fh_recepcion
                        //                   don_usu_recepcion
                        //                   don_temp
                        //                   cod_muni //Obsoleto.
                );
                $donacion_id = $modelDonaciones->createRow($nueva_donacion)->save();

                $modelDetallesDonaciones = new Efector_models_DetallesDonaciones();
                $nuevo_detallesDonacion = array(
                    //'ext_id' => ,//Autogenerado con la creacion
                    'don_id' => $donacion_id,
                    'cod_colecta' => $this->efector_activo_cod . $this->efector_activo['id_efector'],
                    'tipo_extraccion' => $tipo_extraccion,
                );
                $modelDetallesDonaciones->createRow($nuevo_detallesDonacion)->save();

                //Obtengo el ID de la Solicitud recien creada.

                if (empty($donacion_id)) {
                    throw new Exception('Error en la donacion. Parametro vacio post creacion del registro.');
                }
            } catch (Exception $exc) {
                $mSolicitudes->delete('sol_id = ' . $solicitud_id);
                $vRet['estado'] = 'NOK';
                $vRet['error'][] = [
                    'codigo' => 'E0008',
                    'mensaje' => 'Error al intentar crear la donacion, se deshace la solicitud. ROLLBACK. | ' . $exc->getTraceAsString()
                ];
                $this->_helper->json($vRet);
            }
        } else {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0007',
                'mensaje' => 'Error en la solicitud. Parametro vacio post creacion del registro.'
            ];
            $this->_helper->json($vRet);
        }


        $this->_helper->json($vRet);
    }

    /** Impresiones * */
    public function constanciaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = (int) $this->getRequest()->getParam('sol_id');

        $mSolicitud = new Efector_models_Solicitudes();
        $solicitud = $mSolicitud->getBySol($id)->current();

        $mDonacion = new Banco_models_Donaciones();
        $donacion = $mDonacion->getBySolID($solicitud['sol_id']);

        $mPersonas = new Efector_models_Personas();
        //previamente getbydni
        $persona = $mPersonas->getByID($solicitud->per_id)->current()->toArray();

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8';
        $y = $page->getHeight();

        $titulo = 'Constancia de Donación de Sangre';
        $pdf->properties['Title'] = $titulo . " - " . $donacion['don_nro'];
        $subtitulo = 'Programa Provincial de Hemoteriapia';
        $efector = $this->efector_activo['nomest'];

        $page
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y - 50, 97, $y - 8)
                ->setFont($font, 14)
                ->drawText($titulo, $this->getCenterText($page->getWidth(), $titulo, $font, 14), $y - 20, $encoding)
                ->setFont($font, 10)
                ->drawText($subtitulo, $this->getCenterText($page->getWidth(), $subtitulo, $font, 10), $y - 32, $encoding)
                ->setFont($font, 10)
                ->drawText($efector, $this->getCenterText($page->getWidth(), $efector, $font, 10), $y - 44)
                ->drawText("Se deja constancia que el/la Sr/Sra {$persona['per_apellido']}, {$persona['per_nombres']} con DNI {$persona['per_nrodoc']} se ha presentado", 50, $y - 80)
                ->drawText("en {$efector} en el dia de la fecha.", 50, $y - 95, $encoding)
                ->drawText("Se extiende la presente acorde a las normativas vigentes (Ley Nacional de Sangre N° 22.990, las Normas", 50, $y - 120, $encoding)
                ->drawText("Técnicas y Administrativas y sus modificaciones) a su pedido y para ser presentado ante quien corresponda.", 50, $y - 135, $encoding)
                ->drawText(utf8_decode('CÓDIGO DE DONACIÓN: ') . $donacion->don_nro, 50, $y - 210)
                ->drawText('FECHA: ' . date('d / m / Y'), 50, $y - 195)
                ->drawText('FIRMA Y SELLO RESPONSABLE', 350, $y - 250);

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    /* Impresion de comprobante de solicitud. */

    public function imprimirSolicitudAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $sol_id = (int) $this->getRequest()->getParam('sol_id');
        $mSolicitudes = new Efector_models_Solicitudes();
        $solicitud = $mSolicitudes->find($sol_id)->current();

        $mPersonas = new Efector_models_Personas();
        $personaBanco = $mPersonas->getByID($solicitud->per_id);

        $mDonacion = new Banco_models_Donaciones();
        $mDonacionExt = new Efector_models_DetallesDonaciones();

        if (!is_null($mDonacion->getBySolID($solicitud->sol_id))) {
            $don = $mDonacion->getBySolID($solicitud->sol_id);
            $don_ext = $mDonacionExt->getByDonID($don->don_id)->toArray();
        }
        
        $modeloLoc = new Efector_models_Localidades();

        $localidad = $personaBanco->current()['localidad_otra'];
        if ($localidad == null){
            $localidad = $modeloLoc->getNombre($personaBanco->current()['codLocalidad'])->nomloc;
        }
        
        $pdf = new Zend_Pdf();
        //$pdf->setJavascript('print(true);');
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $fontb = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/arial.ttf');

        $encoding = 'UTF-8'; //ISO-8859-1
        $y = $page->getHeight();
        $persona = $personaBanco->current()->toArray();

        $edad = utf8_decode(html_entity_decode($this->view->niceage($persona['per_fnac'])));
        $bis = $persona['bis'];
        $domicilio = implode(' ', array($persona['calle'], $persona['numero'], $persona['piso'], $persona['depto'], $bis));
        $xCol2 = ($page->getWidth() / 2) + 8;

        $reaccion_si = null;
        $reaccion_no = null;


        if (is_null($don_ext['reaccion'])) {
            $reaccion_no = 'X';
        } else {
            $reaccion_si = 'X';
        }

        $modeloTipoBolsas = new Gestion_models_Bolsastipos();
        $tipo_bolsa = $modeloTipoBolsas->fetchRow("id = " . $don_ext['tipo_bolsa']);

        $modeloMarcaBolsas = new Gestion_models_Bolsasmarcas();
        $marca_bolsa = $modeloMarcaBolsas->fetchRow("id = " . $don_ext['marca_bolsa']);

        $modeloTipoExtraccion = new Gestion_models_TdoTiposDonaciones();
        $tipo_extraccion = $modeloTipoExtraccion->fetchRow("tdo_id = " . $don_ext['tipo_extraccion']);
        
        $documento = $this->view->cache_tipo_doc[$persona['per_tipodoc']] . " " . $persona['per_nrodoc'];        
                
        $page
                /**
                 * Logo
                 */
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 8, $y - 50, 50, $y - 8)
                /**
                 * Dibujo Lineal
                 */
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawRectangle(5, $y - 5, $page->getWidth() - 5, $y - 240, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawLine($page->getWidth() / 2, $y - 80, $page->getWidth() / 2, $y - 240)
                ->drawLine(5, $y - 80, $page->getWidth() - 5, $y - 80)
                ->drawLine(5, $y - 100, $page->getWidth() - 5, $y - 100)
                ->drawLine(5, $y - 120, $page->getWidth() - 5, $y - 120)
                ->drawLine(5, $y - 140, $page->getWidth() - 5, $y - 140)
                ->drawLine(5, $y - 160, $page->getWidth() - 5, $y - 160)
                ->drawLine(5, $y - 180, $page->getWidth() - 5, $y - 180)
                ->drawLine(5, $y - 200, $page->getWidth() - 5, $y - 200)
                ->drawLine(5, $y - 220, $page->getWidth() / 2, $y - 220)

                /**
                 * Filas Columna Derecha
                 */
                ->setFont($font, 8)
                ->drawText("Fecha: " . date('d / m / y'), $xCol2, $y - 94, $encoding)
                ->drawText("Código de Colecta:", $xCol2, $y - 114, $encoding)
                ->drawText($don_ext['cod_colecta'], $xCol2 + 80, $y - 114, $encoding)
                ->drawText("Tipo de Donación: ", $xCol2 + 160, $y - 114, $encoding)
                ->drawText($tipo_extraccion->tdo_descripcion, $xCol2 + 227, $y - 114, $encoding)
                ->drawText("Peso:", $xCol2, $y - 134)
                ->drawText($don_ext['peso'], $xCol2 + 30, $y - 134)
                ->drawText("Hb:", $xCol2 + 70, $y - 134)
                ->drawText($don_ext['hb'], $xCol2 + 95, $y - 134)
                ->drawText("TA:", $xCol2 + 140, $y - 134)
                ->drawText($don_ext['ta'], $xCol2 + 165, $y - 134)
                ->drawText("Pulso:", $xCol2 + 210, $y - 134)
                ->drawText($don_ext['pulso'], $xCol2 + 240, $y - 134)
                ->drawText("Tipo de Bolsa:", $xCol2, $y - 154, $encoding)
                ->drawText($tipo_bolsa->bolsa_tipo, $xCol2 + 60, $y - 154, $encoding)
                ->drawText("Marca Bolsa: ", $xCol2 + 160, $y - 154)
                ->drawText($marca_bolsa->marca_bolsa, $xCol2 + 210, $y - 154)
                ->drawText("Reacciones Post-Extracción:", $xCol2, $y - 174, $encoding)
                ->drawText("Si", $xCol2 + 175, $y - 174)
                ->drawText($reaccion_si, $xCol2 + 194, $y - 174)
                ->drawRectangle($xCol2 + 190, $y - 164, $xCol2 + 204, $y - 176, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawText("No", $xCol2 + 235, $y - 174)
                ->drawText($reaccion_no, $xCol2 + 254, $y - 174)
                ->drawRectangle($xCol2 + 250, $y - 164, $xCol2 + 264, $y - 176, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawText("Tipo de reaccion: ", $xCol2, $y - 194, $encoding)
                ->drawText($don_ext['reaccion'], $xCol2 + 30, $y - 194, $encoding)
                ->drawText("Firma del Técnico:", $xCol2, $y - 214, $encoding)
                /**
                 * Informacion Personal
                 */
                ->setFont($font, 8)
                ->drawText("GOBIERNO DE LA PROVINCIA DE SANTA FE", 62, $y - 20, $encoding)
                ->setFont($fontb, 8)
                ->drawText("PROGRAMA PROVINCIAL DE HEMOTERAPIA", 12, $y - 76, $encoding)
                ->setFont($font, 8)
                ->drawText("Nombres: {$persona['per_nombres']}", 12, $y - 94, $encoding)
                ->drawText("Apellido: {$persona['per_apellido']}", 12, $y - 114, $encoding)
                ->drawText("Fecha Nacimiento: " . date("d-m-Y", strtotime($persona['FechaNacimiento'])) . "  " . $edad, 12, $y - 134, $encoding)
                ->drawText("Documento: {$documento}", 180, $y - 134)
                ->drawText("Domicilio: {$domicilio}", 12, $y - 154, $encoding)
                ->drawText("Localidad: {$localidad}", 12, $y - 174, $encoding)
                ->drawText("Nacionalidad: {$persona['codPaisNacimiento']}", 180, $y - 174)
                ->drawText("Ocupación: {$this->view->cache_ocupaciones[$solicitud['ocu_id']]}", 12, $y - 194, $encoding)
                ->drawText("Teléfono: {$persona['telefono']}", 180, $y - 214, $encoding)
                ->drawText("Celular: {$persona['celular']}", 180, $y - 234, $encoding)
                ->drawText("Email: {$persona['email']}", 12, $y - 214, $encoding)
                ->drawText("Sexo: ", 12, $y - 234);

        if ($persona['per_sexo'] == 'M') {
            $page->drawText("Hombre", 40, $y - 234);
            //->drawRectangle(72,$y-224,86,$y-236);
        } elseif ($persona['per_sexo'] == 'F') {
            //tilde y gestas
            $page
                    ->drawText("Mujer", 40, $y - 234)
                    //->drawRectangle(120,$y-224,134,$y-236)
                    ->drawText("Gestas: ____", 180, $y - 234);
        }

        $pdf->pages[] = $page;

        $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', array(
                    'font' => APPLICATION_PATH . '/arial.ttf',
                    'text' => $solicitud['sol_cod'],
                    'fontSize' => 14,
                    'barThickWidth' => 4,
                    'barThinWidth' => 2,
                    //'withQuietZones' => false,
                    'stretchText' => true
                        ), array(
                    'topOffset' => 40,
                    'HorizontalPosition' => 'right',
                    'leftOffset' => 330
                        )
                )->setResource($pdf)->draw();

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    //Impresion de listado de procesos iniciados
    public function imprimirpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //INICIO - PEGADO DE HISTORIAL DE DONACIONES
        //$dias_ultima_donacion = $this->settings->dias_entre_donaciones->valor;

        $nro_documento = $this->getRequest()->getParam('nro_documento');
        $id_tipo_documento = $this->getRequest()->getParam('tipo_documento');

        if (!empty($nro_documento) && !empty($id_tipo_documento)) {

            $tipo_documento = $this->view->cache_tipo_doc[$id_tipo_documento];
            $personaSimetra = $this->_BuscarPersonaEnSimetra($id_tipo_documento, $nro_documento);

            if ($personaSimetra['estado']) {

                $modeloSol = new Efector_models_Solicitudes();
                $modeloPer = new Efector_models_Personas();
                $modeloPerSicap = new Efector_models_PersonasSicap();
                $modeloDon = new Banco_models_Donaciones();
                $modeloDonExt = new Efector_models_DetallesDonaciones();
                $modeloPerfiles = new Gestion_models_PerfilesProduccion();
                $modeloEfe = new Acceso_models_Efectores();
                $modeloLoc = new Efector_models_Localidades();
                $modeloSerologia = new Banco_models_Serologia();
                $modeloInmunologia = new Banco_models_Inmunologia();

                $ultimaDonacion = $modeloDon->getUltimaByDni($personaSimetra['data']['per_id']);

                if ($ultimaDonacion != null) {
                    $diasDesde = $ultimaDonacion['dias_desde'];
                    if ($diasDesde > $dias_ultima_donacion) {
                        $niceDesde = ', ' . $this->view->nicetime($ultimaDonacion['don_fh_inicio']);
                    }
                    $totalDonaciones = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
                    $ultimaDonacion = $ultimaDonacion->toArray();
                }

                $efectoresAll = $modeloEfe->getPairs();
                $donaciones = $modeloDon->getDonacionesHistorialParaHistorial($personaSimetra['data']['per_id']);

                if ($donaciones != null) {
                    $donaciones = $donaciones->toArray();
                    foreach ($donaciones as &$donacion) {
                        $donacion['efector_nombre'] = $modeloEfe->getNom($donacion['efe_id']);
                        $donacion['desc_perfil'] = $modeloPerfiles->getDesc($donacion['ppr_id']);
                        $serologia = $modeloSerologia->getDatosDonacion($donacion['don_id']);
                        $resultadoSerologico = "ERROR";

                        if ($serologia['sifilis'] === 'Reactivo' || $serologia['brucelosis'] === 'Reactivo' ||
                                $serologia['chagas'] === 'Reactivo' || $serologia['hepatitisB'] === 'Reactivo' ||
                                $serologia['hepatitisC'] === 'Reactivo' || $serologia['hiv'] === 'Reactivo' ||
                                $serologia['htlv'] === 'Reactivo'
                        ) {
                            $resultadoSerologico = "<b class='badge badge-important'>REACTIVO</b>";
                            ;
                        } else if ($serologia['sifilis'] === 'No reactivo' && $serologia['brucelosis'] === 'No reactivo' &&
                                $serologia['chagas'] === 'No reactivo' && $serologia['hepatitisB'] === 'No reactivo' &&
                                $serologia['hepatitisC'] === 'No reactivo' && $serologia['hiv'] === 'No reactivo' &&
                                $serologia['htlv'] === 'No reactivo'
                        ) {
                            $resultadoSerologico = "No reactivo";
                        } else {
                            $resultadoSerologico = "<b class='badge'>Sin determinar</b>";
                        }
                        $donacion['serologia'] = $resultadoSerologico;
                        $inmunologia = $modeloInmunologia->getDatosDonacion($donacion['don_id']);

                        $donacion['inmunologia'] = "Grupo: " . $inmunologia['grupo']
                                . ", Factor: " . $inmunologia['factor']
                                . ", Rastreo: " . $inmunologia['rastreo']
                                . ", DU: " . $inmunologia['du']
                                . ", CDE: " . $inmunologia['cde']
                                . ", Fenotipo: " . $inmunologia['fenotipo'];

                        $donacion['extendida'] = $modeloDonExt->getByDonID($donacion['don_id']);
                    }

                    $solicitudes = $modeloSol->getByDni($personaSimetra['data']['per_id'], array(0, 1, 2, 3, 4, 5))->toArray(); //comprobar
                    $data = array(
                        'persona' => $personaSimetra,
                        'donacionesPrevias' => [
                            'total' => count($donaciones),
                            'donaciones' => $donaciones
                        ],
                        'diasDesde' => $diasDesde,
                        'desde' => $niceDesde,
                        'crearSol' => $puedeCrear,
                        'solicitudes' => sizeof($solicitudes),
                        'dias_sugeridos_entre_donaciones' => $dias_ultima_donacion,
                    );
                } else {
                    $data = array(
                        'persona' => $personaSimetra,
                        'dias_sugeridos_entre_donaciones' => $dias_ultima_donacion,
                    );
                }
                $this->view->data = $data;
            }
        }
        //FIN - PEGADO DE HISTORIAL DE DONACIONES

        $perPage = 18;

        if (!$fecha) {
            $fecha = date('Y-m-d');
        }


        //$solicitudes = $mSolicitudes->getByFechaAndEfe($efes_ids, $fecha, $this->page, $perPage);

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $fontb = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/arial.ttf');

        $encoding = 'UTF-8'; //ISO-8859-1
        $y = $page->getHeight();
        $x = $page->getWidth();
//        echo "<pre>";
//        print_r($x);
//        echo "</pre>";
//        die;
        $titulo = 'Historial de Donaciones';
//        echo "<pre>";
//        print_r(round(($x/2)- strlen($titulo)/2),0);
//        echo "</pre>";
//        die;

        if (1) {//Ver Grilla
            for ($i = 0; $i < $y; $i += 15) {
                $page
                        ->setFont($fontb, 6)
                        ->drawText('Y' . $i, 15, $i, $encoding)
                        ->setLineWidth(0.2)
                        ->setLineColor(Zend_Pdf_Color_Html::color('#dddddd'))
                        ->drawLine(0, $i, $x, $i);
            }
            for ($i = 0; $i < $x; $i += 15) {
                $page
                        ->setFont($fontb, 6)
                        ->drawText('X' . $i, $i, $y - 24, $encoding)
                        ->setLineWidth(0.2)
                        ->setLineColor(Zend_Pdf_Color_Html::color('#DDDDDD'))
                        ->drawLine($i, 0, $i, $y)
                        ->setLineWidth(1)
                        ->setLineColor(Zend_Pdf_Color_Html::color('#000000'))
                        ->drawRectangle(45, 810, 570, 30, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
            }
            $layout = true;
            if ($layout == true) {
                $page
                        ->drawRectangle(45, 810, 135, 720, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                        ->drawRectangle(135, 810, 465, 720, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                        ->drawRectangle(465, 810, 570, 780, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                        ->drawRectangle(465, 780, 570, 750, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                        ->drawRectangle(465, 750, 570, 720, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                        ->setLineColor(Zend_Pdf_Color_Html::color('#999999'))
                        ->setLineDashingPattern(array(3, 2, 3, 4), 1.6)
                        ->drawRectangle(60, 705, 555, 45, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ;
            }
        }

//COMPLETAR CABECARA
        $page
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 46, 746, 134, 785)
                ->setFont($fontb, 14)
                ->drawText("Historico de Procesos de Doanción iniciados", 165, 765, $encoding)
                ->setFont($fontb, 10)
                ->drawText("Fecha:", 470, 790, $encoding)
                ->drawText(date("d/m/Y"), 510, 790, $encoding)
                ->setFont($fontb, 12)
                ->drawText("Información Personal", 60, 690, $encoding)
                ->setFont($fontb, 11)
                ->drawText("Nombre y Apellido:", 60, 660, $encoding)
                ->drawText("Documento:", 60, 645, $encoding)
                ->drawText("Fecha Ultima Donacion:", 60, 630, $encoding)
                ->setFont($fontb, 12)
                ->drawText("Historico de Donaciones", 60, 600, $encoding)

                //Carga de datos
                ->setFont($fontb, 11)
                ->drawText('Guido Nicolas Quadrini', 210, 660, $encoding)
                ->drawText("DNI 20639965", 210, 645, $encoding)
                ->drawText("24/10/2018", 210, 630, $encoding)

                //Dibuja Tabla
                ->setLineColor(Zend_Pdf_Color_Html::color('#000000'))
                ->setLineDashingPattern(Zend_Pdf_Page::LINE_DASHING_SOLID, 1.6)
                ->drawRectangle(60, 590, 165, 570, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawRectangle(165, 590, 315, 570, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawRectangle(315, 590, 450, 570, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawRectangle(450, 590, 555, 570, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawText("Fecha Donación", 65, 575, $encoding)
                ->drawText("Efector", 220, 575, $encoding)
                ->drawText("Código de Donación", 330, 575, $encoding)
                ->drawText("Estado", 480, 575, $encoding)
                ->setLineWidth(0.1)
                ->setLineColor(Zend_Pdf_Color_Html::color('#777777'))
                ->drawLine(20, $y - 110, $page->getWidth() - 25, $y - 110)
        ;
        if ($data != null) {
            foreach ($data['donacionesPrevias']['donaciones'] as $donacion) {
                $page
                        ->setFont($font, 11)
//                    ->drawText(date("d/m/Y - H:m ", strtotime($sol['sol_fh_ini'])), 30, $y - 80, $encoding)
//                    ->drawText($sol['sol_cod'], 170, $y - 80, $encoding)
//                    ->drawText($sol['sol_id'], 300, $y - 80, $encoding)
//                    ->drawText($sol['per_apellido'] . " , " . $sol['per_nombres'], 410, $y - 80, $encoding)
//                    ->drawText($this->view->estados_solicitudes[$sol['sol_estado']], 690, $y - 80, $encoding)
                ;
                $y = $y - 25;
            }
        } else {
            $mensaje = 'No registra donaciones en su historial.';
            $mensaje_pos_x = ( round(($x / 2) - (strlen($mensaje) * 3), 0)); //Centrar Texto
            $page
                    //Dibuja la fila
                    ->setLineColor(Zend_Pdf_Color_Html::color('#000000'))
                    ->setLineDashingPattern(Zend_Pdf_Page::LINE_DASHING_SOLID, 1.6)
                    ->drawRectangle(60, 570, 555, 555, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                    ->setFont($font, 11)
                    ->drawText($mensaje, 75, 558, $encoding)
            ;
        }
        $page->drawLayoutBox(null, $x, $y);

        $pdf->pages[] = $page;

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    public function formularioAction() {/* No implementado */
    }

    public function imprimirAction() {/* No implementado */
    }

    private function getFila($nro_fila) {
        return ($nro_fila * 15) + 15;
    }

}
