<?php

class Efector_AferesisController extends BootPoint
{

	/**
	* Ver solicitudes Aptas para Extracci�n
	*
	*/
	public function indexAction()
    {
        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');
        $nro = $this->getRequest()->getParam('nro');
        $estado = (int)$this->getRequest()->getParam('estado');
		$perPage = 18;
		
		/* **********************************************
		 * Por defecto, busca las pendientes
		 * si busca enviado, tambi�n se agrega recibido
		 * **********************************************/
		if ($estado === 0) 
			$estado = 4;
            
        if ($fechaDesde == ''  || $fechaHasta == '') {
            $fechaHasta = date('Y-m-d');
            $fechaDesde = date('Y-m-d',strtotime("-1 days"));
        }   

		/* Obtengo los efectores del usuario */
		$modeloPry = new Acceso_models_Proyectos();
		$nom = "Banco de sangre";
		$pry = $modeloPry->getByNom($nom);
		
		$pry_id = $pry->pry_id;
		$modeloUsu = new Acceso_models_Usuarios();
		$efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id,$pry_id);

		foreach ($efes_usu as $e)
			$efes_ids[] = $e['id_efector'];
		/* Fin efectores usuario */	

		$modeloDon = new Banco_models_Donaciones();
       
        /* Listo s�lo las extracciones con tipo de donaci�n 1,af�resis  */
        if ($estado == "-1") 
            $extracciones = $modeloDon->getExtracciones($efes_ids, null, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, 1);
        else 
            $extracciones = $modeloDon->getExtracciones($efes_ids, $estado, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, 1);

		if(!empty($extracciones)){
			$total = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
			$paginas = ceil($total/$perPage);
		}
		
		foreach ($extracciones as $e)
			$donaciones[] = $e->don_id;

		$modeloDetallesDon = new Efector_models_DetallesDonaciones();
		if (!empty($donaciones))
			$datos_extra = $modeloDetallesDon->getByDonIDs($donaciones);
       
        $this->view->busqueda = '';
        if (!empty($nro))
            $this->view->busqueda .= '<br/> N&uacute;mero de Donaci&oacute;n: '.$nro .' <br/>';

        if (!empty($fecha))
            $this->view->busqueda .= 'Fecha: '. $fecha . '<br/>';

        $modeloBol = new Efector_models_Bolsas();
        $bolsas = $modeloBol->getActivas()->toArray();

        $this->view->bolsas = $bolsas;
		$this->view->estado = $estado;
        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->nro = $nro;
		$this->view->resultados = $extracciones;
		$this->view->paginas = $paginas;
		$this->view->anteriores = $finalizadas;
		$this->view->extra = $datos_extra;
	}


	public function estadoAction()
    {
        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');
        $nro = $this->getRequest()->getParam('nro');
        $estado = (int)$this->getRequest()->getParam('estado');
		$perPage = 18;
		
		/* **********************************************
		 * Por defecto, busca las pendientes
		 * si busca enviado, tambi�n se agrega recibido
		 * **********************************************/
		if ($estado === 0) 
			$estado = 4;
            
        if ($fechaDesde == ''  || $fechaHasta == '') {
            $fechaHasta = date('Y-m-d');
            $fechaDesde = date('Y-m-d',strtotime("-1 days"));
        }   

		/* Obtengo los efectores del usuario */
		$modeloPry = new Acceso_models_Proyectos();
		$nom = "Banco de sangre";
		$pry = $modeloPry->getByNom($nom);
		
		$pry_id = $pry->pry_id;
		$modeloUsu = new Acceso_models_Usuarios();
		$efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id,$pry_id);

		foreach ($efes_usu as $e)
			$efes_ids[] = $e['id_efector'];
		/* Fin efectores usuario */	

		$modeloDon = new Banco_models_Donaciones();
        $modeloInmuno = new Banco_models_Inmunologia();
        $modeloSer = new Banco_models_Serologia(); 

        /* Listo s�lo las extracciones con tipo de donaci�n 1,af�resis  */
        if ($estado == "-1") 
            $extracciones = $modeloDon->getExtracciones($efes_ids, null, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, 1);
        else 
            $extracciones = $modeloDon->getExtracciones($efes_ids, $estado, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, 1);

		if(!empty($extracciones)){
			$total = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
			$paginas = ceil($total/$perPage);
		}
		
		foreach ($extracciones as $e) {
			$donaciones[] = $e->don_id;
            $serologia[$e->don_id] = $modeloSer->getDatosDonacion($e->don_id);
            $inmuno[$e->don_id] = $modeloInmuno->getDatosDonacion($e->don_id);
        }      

		$modeloDetallesDon = new Efector_models_DetallesDonaciones();
		if (!empty($donaciones))
			$datos_extra = $modeloDetallesDon->getByDonIDs($donaciones);
       
        $this->view->busqueda = '';
        if (!empty($nro))
            $this->view->busqueda .= '<br/> N&uacute;mero de Donaci&oacute;n: '.$nro .' <br/>';

        if (!empty($fecha))
            $this->view->busqueda .= 'Fecha: '. $fecha . '<br/>';

        $modeloBol = new Efector_models_Bolsas();
        $bolsas = $modeloBol->getActivas()->toArray();

        $this->view->bolsas = $bolsas;
		$this->view->estado = $estado;
        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->nro = $nro;
		$this->view->resultados = $extracciones;
		$this->view->paginas = $paginas;
		$this->view->anteriores = $finalizadas;
		$this->view->extra = $datos_extra;
	}

	public function verAction()
    {
		$ext_id = (int)$this->getRequest()->getParam('ext_id');

		$modeloDon = new Banco_models_Donaciones();
		$extraccion = $modeloDon->getRegistro($ext_id)->toArray();

		$modeloPer = new Efector_models_Personas();

		$persona = $modeloPer->getByID($extraccion['per_id']);

		$usu_id = $extraccion['usu_inicio'];
		$model = new Acceso_models_Usuarios();
                $usuario = $model->find($usu_id)->current()->toArray();

		$this->view->persona = $persona;
		$this->view->dni = $extraccion['per_id'];
		$this->view->extraccion = $extraccion;
		$this->view->usuario= $usuario['usu_usuario'];

		if($extraccion['don_estado'] == 4)
			return $this->renderScript('aferesis/form.phtml');

	}

	/**
	* Inicia el proceso de donaci�n
	* Genera n�mero donaci�n y se imprime un ticket/donaci�n. (estado intermedio, Cierra la solicitud, abre la donaci�n)
	*
	* 1 - Finaliza OK
	* 3 - Anulada
	* 4 - Extracci�n
	*
	* PRO_ESTADO
	* 0 - Formulario de extraccion segura ANULA!
	* 1 - Pasar a Producci�n
	* 2 - Rechazar Produci�n
	* 3 - Aceptar para Procesar
	* 4 - No aceptar para proceso
	*/
	public function finalizarAction()
    {
		if(!$this->getRequest()->isPost())
			$this->_helper->json(array('st' => 'nok'));

		$post = $this->getRequest()->getPost();
		$don_id = (int)$this->getRequest()->getParam('don_id');

		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($don_id)->current();
		if(!$donacion)
			$this->_helper->json(array('st' => 'nok'));

		$modeloSol = new Efector_models_Solicitudes();
		$modeloSol->find($donacion->sol_id)->current()->setFromArray(array(
			'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
			'usu_cierre' => $this->auth->getIdentity()->usu_id
		))->save();

		$donacion->setFromArray(array(
			'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
			'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
			'don_estado' => 1,
			'observaciones' => $post['observaciones'],
			'don_tiempo_extraccion' => $post['tiempo'],
		))->save();

		$this->_helper->redirector('ver',$this->_request->controller,$this->_request->module,array('ext_id' => $don_id));
	}


	public function agregarMuniAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$cod_muni = $this->getRequest()->getParam('muni');
		$don_id = $this->getRequest()->getParam('cod');
		
		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($don_id)->current();
		
		$values = array(
			'cod_muni' => $cod_muni,
		);
		
		$donacion->setFromArray($values)->save();
		
		$this->_helper->json(array('st' => 'ok')); 
	}

	/**
	* Descartes
	*
	*/
	public function descartarAction()
    {
		if(!$this->getRequest()->isPost())
			$this->_helper->json(array('st' => 'nok'));

		$post = $this->getRequest()->getPost();
		$don_id = (int)$this->getRequest()->getParam('don_id');

		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($don_id)->current();
		if(!$donacion)
			$this->_helper->json(array('st' => 'nok'));

		$modeloSol = new Efector_models_Solicitudes();
		$modeloSol->find($donacion->sol_id)->current()->setFromArray(array(
			'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
			'usu_cierre' => $this->auth->getIdentity()->usu_id,
			'sol_estado' => 0
		))->save();

		$donacion->setFromArray(array(
			'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
			'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
			'don_estado' => 3,
			'mot_id' => $post['motivo'],
			'observaciones' => $post['observaciones']
		))->save();

		$this->_helper->redirector('ver',$this->_request->controller,$this->_request->module,array('ext_id' => $don_id));
	}

	public function formFesAction()
    {
		$don_id = (int)$this->getRequest()->getParam('don_id');
		$st = (int)$this->getRequest()->getParam('st');

		$sol_estado = ($st == 0) ? 5 : 0;
		$pro_estado = ($st == 0) ? 0 : 1;

		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($don_id)->current();

		//$this->auth->getIdentity()->usu_id,

		$modeloSol = new Efector_models_Solicitudes();
		$modeloSol->find($donacion->sol_id)->current()->setFromArray(array(
			'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
			'usu_cierre' => $this->auth->getIdentity()->usu_id,
			'sol_estado' => $sol_estado
		))->save();

		$donacion->setFromArray(array(
			'pro_estado' =>  $pro_estado
		))->save();

		$this->_helper->redirector('ver',$this->_request->controller,$this->_request->module,array('ext_id' => $don_id));
	}

	public function agregarDatosAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
				
		$cod = (int)$this->getRequest()->getParam('cod');
		$codCol = (int)$this->getRequest()->getParam('codcolecta');
		$tipo = $this->getRequest()->getParam('tipo');
		$peso = (int)$this->getRequest()->getParam('peso');
		$hb = $this->getRequest()->getParam('hb');
		$ta = $this->getRequest()->getParam('ta');
		$pulso = (int)$this->getRequest()->getParam('pulso');
		$t_bolsa = $this->getRequest()->getParam('tipo_bolsa');
		$marca_bolsa = $this->getRequest()->getParam('marca');
		$reaccion = $this->getRequest()->getParam('reaccion');
		$t_reaccion = $this->getRequest()->getParam('tiporeaccion');
		
		$modeloDetallesDon = new Efector_models_DetallesDonaciones();
		
		$modeloDetallesDon->find($cod)->current()->setFromArray(array(
			'cod_colecta' => $codCol,
			'tipo' => $tipo,
			'peso' => $peso,
			'hb' => $hb,
			'ta' => $ta,
			'pulso' => $pulso,
			'tipo_bolsa' => $t_bolsa,
			'marca_bolsa' => $marca_bolsa,
			'reaccion' => $t_reaccion,
		))->save();		
		
		$this->_helper->json(array('st' => 'ok'));
		
	}

	/**
	* Autoexclusion mas contancia
	*
	*/
	public function fesconstanciaAction()
    {

		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$id = (int)$this->getRequest()->getParam('id');

		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($id)->current();

		$modeloPer = new Efector_models_Personas();

		$persona = $modeloPer->getByID($donacion->per_id);

		$pdf = new Zend_Pdf();

		$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

		$encoding = 'UTF-8';

		$titulo = 'Constancia de Donaci�n de Sangre';
		$efector = $this->efector_activo['nomest'];

		$y = $page->getHeight();
		$page
			->setFont($font,10)
			->drawText($this->efector_activo['nomest'], 25, $y-20)
			->setFont($font,14)
			->drawText('Formulario de Autoexclusi�n', 25, $y-50,$encoding)
			->setFont($font,10)
			->drawText('Estimado Donante:', 28, $y-76)
			->drawText('Si Ud. ya don� sangre y se sinti� obligado a hacerlo, si no contest� el cuestionario', 120, $y-90,$encoding)
			->drawText('sinceramente por temor o por verg�enza, si piensa que su sangre puede no ser segura, est� a tiempo ', 28, $y-104)
			->drawText('para evitar mayor riesgo al paciente que lo reciba.', 28, $y-118)
			->drawText('Marque con una X una de las siguientes opciones. Su respuesta es confidencial.',120,$y-132)
			->drawText('USAR MI SANGRE',50,$y-180)
			->drawRectangle(30,$y-172,40,$y-182,Zend_Pdf_Page::SHAPE_DRAW_STROKE )
			->drawText('NO USAR MI SANGRE',50,$y-200)
			->drawRectangle(30,$y-192,40,$y-202,Zend_Pdf_Page::SHAPE_DRAW_STROKE )
			//->drawText('FIRMA: ___________________________',300,$y-220)
			//->drawText("DONANTE: {$persona['per_apellido']}, {$persona['per_nombres']}",300,$y-240)
			->drawText('FECHA: '.date('d / m / Y'),300,$y-240)
			
			//->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 8, $y-350, 50, $y-300)
			->setFont($font,14)
			->drawText($titulo, $this->getCenterText($page->getWidth(),$titulo,$font,14), $y-400,$encoding)
			->setFont($font,10)
			->drawText($efector,$this->getCenterText($page->getWidth(),$efector,$font,10) , $y-420)

			->drawText('Se deja constancia que', 18, $y-470)
			->drawText("El Sr/Sra {$persona['per_apellido']}, {$persona['per_nombres']} con DNI {$persona['per_nrodoc']}", 28, $y-490,$encoding)
			->drawText("se ha presentado en esta instituci�n {$efector}", 28, $y-510,$encoding)
			->drawText('FIRMA Y SELLO RESPONSABLE',350,$y-570)
			->drawText("DONACION Nro: {$donacion->don_nro}",18,$y-600)
			->drawText('FECHA: '.date('d / m / Y'),18,$y-620)

			;

		$pdf->pages[] = $page;
                
                
                $barcode = Zend_Barcode::factory(
                    'code128', 'pdf', 
                        array(
                            'font' => 'fonts/arial.ttf',                
                            'text' => $donacion->don_nro,
                            'fontSize' => 10,
                            'barThickWidth' => 4,
                            'barThinWidth' => 2,
                            //'withQuietZones' => false,
                            'stretchText' => true
                            ), 
                        array(
                            'topOffset' => 5,
                            //HorizontalPosition' => 'right'
                            'leftOffset' => 330
			)
		)->setResource($pdf)->draw();

		header('Content-Type: application/pdf');
		echo $pdf->render();
		
	}
	/**
	* Codigo de Barras
	*
	*/
	public function ticketsAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$pdf = new Zend_Pdf();
		//$pdf->setJavascript('print(true);');

		//tama�o de la p�gina del ticket
		//$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
		$x = 290; //ancho
		$y = 70;  //alto
		$page = new Zend_Pdf_Page($x, $y);
		$font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH.'/arial.ttf');
		$page->setFont($font,8); //antes = 5
		//$page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 20);
		$pdf->pages[] = $page;

		$id = (int)$this->getRequest()->getParam('id');


		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($id)->current();

		$top = 25;//20
		$leftA = 6;
		$leftB = 155;
		
		//imprimir en la cabecera del c�digo
		$efector = $this->efector_activo['nomest'];
		//Truncado de $efector
		$efecTrunc = sprintf("%10.25s",$efector);
		//C�digo de barra A
		$page->drawText($efecTrunc, $leftA+5, 50);
                
                
                $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', 
                        array(
                            'font' => 'fonts/arial.ttf',                
                            'text' => $donacion->don_nro,
                            'fontSize' => 12,
                            'barThickWidth' => 3, 
                            'barThinWidth' => 1,  
                            'stretchText' => true
                            ), 
                        array(
                            'topOffset' => $top,
                            'leftOffset' => $leftA
			)
		)->setResource($pdf)->draw();
		
		//C�digo de barra B - Igual a A excepto en el leftOffset
		$page->drawText($efecTrunc, $leftB+5, 50);
                
                $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', 
                        array(
                            'font' => 'fonts/arial.ttf',    
				'text' => $donacion->don_nro,
				'fontSize' => 12,
				'barThickWidth' => 3, 
				'barThinWidth' => 1,  
				'stretchText' => true
                            ), 
                        array(
				'topOffset' => $top,
        	        	'leftOffset' => $leftB 
                            )
		)->setResource($pdf)->draw();
			


		header('Content-Type: application/pdf');
		echo $pdf->render();
	}

	public function constanciaAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$id = (int)$this->getRequest()->getParam('id');

		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($id)->current();

		$modeloPer = new Efector_models_Personas();
	
		$persona = $modeloPer->getByID($donacion->per_id);

		$pdf = new Zend_Pdf();
		$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

		$encoding = 'UTF-8';
		$y = $page->getHeight();

		$titulo = 'Constancia de Donaci�n de Sangre';
		$efector = $this->efector_activo['nomest'];

		$page
			->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 8, $y-50, 50, $y-8)
			->setFont($font,14)
			->drawText($titulo, $this->getCenterText($page->getWidth(),$titulo,$font,14), $y-20,$encoding)
			->setFont($font,10)
			->drawText($efector,$this->getCenterText($page->getWidth(),$efector,$font,10) , $y-40)

			->drawText('Se deja constancia que', 18, $y-76)
			->drawText("El Sr/Sra {$persona['per_apellido']}, {$persona['per_nombres']} con DNI {$persona['per_nrodoc']}", 28, $y-96,$encoding)
			->drawText("se ha presentado en esta instituci�n {$efector}", 28, $y-110,$encoding)
			->drawText('FIRMA Y SELLO RESPONSABLE',350,$y-200)
			->drawText("DONACION Nro: {$donacion->don_nro}",18,$y-160)
			->drawText('FECHA: '.date('d / m / Y'),18,$y-180)
			;

		$pdf->pages[] = $page;

		header('Content-Type: application/pdf');
		echo $pdf->render();
	}


	public function fesAction()
    {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$id = (int)$this->getRequest()->getParam('id');

		$modeloDon = new Banco_models_Donaciones();
		$donacion = $modeloDon->find($id)->current();

		$modeloPer = new Efector_models_Personas();

		$persona = $modeloPer->getByID($donacion->per_id);

		$pdf = new Zend_Pdf();
		$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

		$encoding = 'UTF-8';

		$y = $page->getHeight();
		$page
			->setFont($font,10)
			->drawText($this->efector_activo['nomest'], 25, $y-20)
			->setFont($font,14)
			->drawText('Formulario de Autoexclusi�n', 25, $y-50,$encoding)
			->setFont($font,10)
			->drawText('Estimado Donante:', 28, $y-76)
			->drawText('Si Ud. ya don� sangre y se sinti� obligado a hacerlo, si no contest� el cuestionario', 120, $y-90,$encoding)
			->drawText('sinceramente por temor o por verg�enza, si piensa que su sangre puede no ser segura, est� a tiempo ', 28, $y-104)
			->drawText('para evitar mayor riesgo al paciente que lo reciba.', 28, $y-118)
			->drawText('Marque con una X una de las siguientes opciones. Su respuesta es confidencial.',120,$y-132)
			->drawText('USAR MI SANGRE',50,$y-180)
			->drawRectangle(30,$y-172,40,$y-182,Zend_Pdf_Page::SHAPE_DRAW_STROKE )
			->drawText('NO USAR MI SANGRE',50,$y-200)
			->drawRectangle(30,$y-192,40,$y-202,Zend_Pdf_Page::SHAPE_DRAW_STROKE )
			->drawText('FECHA: '.date('d / m / Y'),300,$y-240)
			;

		$pdf->pages[] = $page;

                $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', 
                        array(
                            'font' => 'fonts/arial.ttf',    
                            'text' => $donacion->don_nro,
                            'fontSize' => 10,
                            'barThickWidth' => 4,
                            'barThinWidth' => 2,
                            'stretchText' => true
                            ), 
                        array(
                            'topOffset' => 5,
                            'leftOffset' => 330
        		)
		)->setResource($pdf)->draw();

		header('Content-Type: application/pdf');
		echo $pdf->render();
	}

	private function getCenterText($width,$text,$font,$fontSize)
    {
		return ($width/2)-($this->getTextWidth($text,$font,$fontSize)/2);
	}

	private function getTextWidth($text, $font, $fontSize)
    {
        $drawingText = iconv('UTF-8', 'UTF-8', $text);

        $characters = array ();
        for($i=0; $i<strlen($drawingText);$i++) {
            $characters[] = ord($drawingText[$i]);
        }
        $widths = $font->widthsForGlyphs($font->glyphNumbersForCharacters($characters));
        return (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
    }
}
