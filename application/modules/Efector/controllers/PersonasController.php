<?php

class Efector_PersonasController extends BootPoint {

    // //TODO: Agregar en el confir.ini parametro WS_SICAP bool
    // private $ws_sicap = false;
    // private $registry;
    // protected $form;

    public function indexAction(){}


    public function preDispatch() {
        $this->registry = Zend_Registry::getInstance();

        $cache = $this->getInvokeArg('bootstrap')->getResource('cache');
        if (!$localidades = $cache->load('localidades')) {
            $modelo = new Efector_models_Sicap();
            $estadosC = $modelo->getEstadosCivil();
            $provincias = $modelo->getProvincias();

            $cache->save($estadosC, 'estadosc');
            $cache->save($provincias, 'provincias');
        }

        $this->view->cache_estadosc = $cache->load('estadosc');
        $this->view->cache_provincias = $cache->load('provincias');
    }

    /** Alta de Persona **/
    public function altaAction() {
        $documento = (int) $this->getRequest()->getParam('nro_documento');
        $id_tipo_documento = $this->getRequest()->getParam('tipo_documento');
        $tipo_documento = $this->getTipoDocumentoPorNombre($this->getRequest()->getParam('tipo_documento'));

        $this->form = new Efector_forms_personasFusion();
        $form = &$this->form;

        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();
            $validacion_extendida = $this->isValidExtendida();

            if ($validacion_extendida) {

                $values = $form->getValues();
                //Ajuste de Formato de Fecha
                $date = new Zend_Date(null, null, 'es_AR');
                $values['FechaNacimiento'] = $date->set($values['FechaNacimiento'])->get('y-MM-dd');

                if (empty($values['Calle'])) {
                    $calle = 'OTRA CALLE';
                } else {
                    $calle = $values['Calle'];
                }

                //Si es otra provincia, agrega localidad 99999
                if ($values['CodPais'] !== 'ARG') {
                    $CodLocalidad = '99999';
                    $CodProvincia = '99';
                    $CodLocalidad_Otra = $values['CodLocalidad_Otra'];
                    if ($values['Calle'] !== 'OTRA CALLE' && !empty($values['Calle'])) {
                        $Calle_Otra = '';
                    } else {
                        $Calle_Otra = $values['Calle_Otra'];
                    }
                } else {


                    $CodProvincia = $values['CodProvincia'];
                    if ($values['CodProvincia'] !== '82') {
                        $CodLocalidad = '99999';
                        $CodLocalidad_Otra = $values['CodLocalidad_Otra'];
                        //$CodProvincia = $values['CodProvincia'];
                    } else {
                        if ((empty($values['CodLocalidad'])) ||
                                ($values['CodLocalidad'] === '99999')) {
                            $CodLocalidad = '99999';
                            $CodLocalidad_Otra = $values['CodLocalidad_Otra'];
                            //$CodProvincia = $values['CodProvincia'];
                        }
                    }

                    if ((!empty($values['CodLocalidad'])) &&
                            ($values['CodLocalidad'] !== '99999')) {
                        $CodLocalidad = $values['CodLocalidad'];
                        $CodLocalidad_Otra = '';
                    }
                    if ($values['Calle'] !== 'OTRA CALLE' && !empty($values['Calle'])) {
                        $Calle_Otra = '';
                    } else {
                        $Calle_Otra = $values['Calle_Otra'];
                    }
                }

                if ($values['CodPaisNacimiento'] !== 'ARG') {
                    $CodLocalidadNacimiento = '99999';
                    $CodProvinciaNacimiento = '99';
                    $CodLocalidadNacimiento_Otra = $values['CodLocalidadNacimiento_Otra'];
                } else {
                    $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                    if ($values['CodProvinciaNacimiento'] !== '82') {
                        $CodLocalidadNacimiento = '99999';
                        $CodLocalidadNacimiento_Otra = $values['CodLocalidadNacimiento_Otra'];
                        $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                    } else {
                        if ((empty($values['CodLocalidadNacimiento'])) ||
                                ($values['CodLocalidadNacimiento'] === '99999')) {
                            $CodLocalidadNacimiento = '99999';
                            $CodLocalidadNacimiento_Otra = $values['CodLocalidadNacimiento_Otra'];
                            $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                        } else {
                            $CodLocalidadNacimiento = $values['CodLocalidadNacimiento'];
                            $CodLocalidadNacimiento_Otra = '';
                            $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                        }
                    }
                    if ((!empty($values['CodLocalidadNacimiento'])) &&
                            ($values['CodLocalidadNacimiento'] !== '99999')) {
                        $CodLocalidadNacimiento = $values['CodLocalidadNacimiento'];
                        $CodLocalidadNacimiento_Otra = '';
                    }
                }

                $fuente = strtoupper($this->registry->config->codename) . " v" . $this->registry->config->pry_version;

                $aPersonaBanco = [
                    'per_tipodoc' => $values['CodTipoDocumento'],
                    'per_nrodoc' => $values['Documento'],
                    'per_nombres' => $values['Nombres'],
                    'per_apellido' => $values['Apellido'],
                    'per_fnac' => $values['FechaNacimiento'],
                    'per_sexo' => $values['CodSexo'],
                    'clasificaciondonante_id' => NULL, //REVISAR ESTO.int(11)
                    'codOcupacion' => NULL,
                    'codEstCivil' => $values['EstadoCivil'],
                    'codNivelEducacion' => NULL,
                    'codPais' => $values['CodPais'],
                    'pais_otro' => $values['CodPais_Otro'],
                    'codProvincia' => $CodProvincia,
                    'codLocalidad' => $CodLocalidad,
                    'localidad_otra' => $CodLocalidad_Otra,
                    'codPostal' => NULL,
                    'municipio' => '1',
                    'distrito' => '1',
                    'calle' => $calle,
                    'calle_otra' => $Calle_Otra,
                    'numero' => $values['Numero'],
                    'piso' => $values['Piso'],
                    'depto' => $values['Depto'],
                    'manzana' => '1',
                    'entreCalleA' => '1',
                    'entreCalleB' => '1',
                    'bis' => $values['Bis'] == 1 ? 'S' : 'N',
                    'gps_Longitud' => '0',
                    'gps_Latitud' => '0',
                    'telefono' => $values['Telefono'],
                    'celular' => $values['Celular'],
                    'email' => $values['Email'],
                    'codPaisNacimiento' => $values['CodPaisNacimiento'],
                    'pais_nacimiento_otro' => $values['CodPaisNacimiento_Otro'],
                    'codProvinciaNacimiento' => $CodProvinciaNacimiento,
                    'codLocalidadNacimiento' => $CodLocalidadNacimiento,
                    'localidad_nacimiento_otra' => $CodLocalidadNacimiento_Otra,
                    'ult_serologia' => NULL,
                    'observacion' => $values['Observacion'],
                    'fuente' => $fuente,
                    'fechaAlta' => new Zend_Db_Expr('NOW()'),
                    'usuarioIngreso' => $this->auth->getIdentity()->usu_id . ':' . $this->auth->getIdentity()->usu_usuario,
                    'fechaModificacion' => '0000-00-00 00:00:00',
                    'usuarioModificacion' => NULL,
                    'fechaBaja' => '0000-00-00 00:00:00',
                    'usuarioBaja' => null,
                    'motivoBaja' => null,
                    'sicap_id' => NULL,
                ];

                $oPersona = new Efector_models_Personas();
                $oPersona->createRow($aPersonaBanco, 'defaultDb')->save();
                $this->_helper->redirector('buscar-persona', $this->_request->controller, $this->_request->module, array(
                    'nro_documento' => $values['Documento'],
                    'tipo_documento' => $values['CodTipoDocumento']));
            }
        } else {
            //Validacion NO existosa
            $form->populate($form->getValues());
        }


        //Asignacion de Valores a los "Combos de seleccion"
        $form->CodPais->addMultiOptions($this->view->cache_paises);
        $form->CodPaisNacimiento->addMultiOptions($this->view->cache_paises);
        $form->CodLocalidadNacimiento->addMultiOptions($this->view->cache_localidades_santafe);
        $form->CodProvincia->addMultiOptions($this->view->cache_provincias);
        $form->CodLocalidad->addMultiOptions($this->view->cache_localidades_santafe);
        $form->EstadoCivil->addMultiOptions($this->view->cache_estadosc);
        $form->CodTipoDocumento->addMultiOptions($this->view->cache_tipo_doc);
        $form->setDefault("CodTipoDocumento", $tipo_documento);
        $form->CodProvinciaNacimiento->addMultiOptions($this->view->cache_provincias);
        $form->setDefault("Documento", $documento);

        //Inicializacion de datos para el ALTA de Donante
        $this->view->form = $form;
        $this->view->form_title = '<h3 style="font-weight:300;">Formulario de <b>alta</b> de datos</h3>';
        $this->view->tpo_accion = 'ALTA';
        $this->renderScript('personas/form.phtml');
    }

    public function sicapToDniAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $sicapid = (int) $this->getRequest()->getParam('sicap');

        $modeloSicap = new Efector_models_PersonasSicap();
        $persona = $modeloSicap->find($sicapid)->current();

        $modeloPer = new Efector_models_Personas();
        $modeloPer->createRow(array(
            'per_nrodoc' => $persona->NroDocumento,
            'per_tipodoc' => $persona->CodTipDocumento,
            'per_nombres' => $persona->Nombre,
            'per_apellido' => $persona->Apellido,
            'per_fnac' => $persona->FechaNacimiento,
            'per_sexo' => $persona->CodSexo,
            'sicap_id' => $persona->NumeroPaciente
                ), 'defaultDb')->save();

        $this->_helper->redirector('ver', $this->_request->controller, $this->_request->module, array('documento' => $persona->NroDocumento, 'tipo' => $persona->CodTipDocumento));
    }

    public function detalleAction() {
        $documento = (int) $this->getRequest()->getParam('nro_documento');
        $id_tipo_documento = $this->getRequest()->getParam('tipo_documento');
        $tipo_documento = $this->getTipoDocumentoPorNombre($this->getRequest()->getParam('tipo_documento'));

        $this->form = new Efector_forms_personasFusion();
        $form = &$this->form;

        $modeloPersonas = new Efector_models_Personas();
        $aPersonaBanco = $modeloPersonas->getByTipoYNum($documento, $tipo_documento)->toArray();
        $NumeroPaciente = $aPersonaBanco['sicap_id'];

        $modeloPerSicap = new Efector_models_PersonasSicap();
        $aPersonaSicap = []; //$modeloPerSicap->find($NumeroPaciente)->current()->toArray();
        $date = new Zend_Date(null, null, 'es_AR');
        $aPersonaBanco['FechaNacimiento'] = $date->set($aPersonaBanco['per_fnac'])->get('dd/MM/y');
        $aPersonaFusion = array_merge($aPersonaBanco, $aPersonaSicap);


        $aPersonaFusion['Nombres'] = $aPersonaFusion['per_nombres'];
        $aPersonaFusion['Apellido'] = $aPersonaFusion['per_apellido'];
        $aPersonaFusion['CodTipoDocumento'] = $aPersonaFusion['per_tipodoc'];
        $aPersonaFusion['Documento'] = $aPersonaFusion['per_nrodoc'];
        $aPersonaFusion['EstadoCivil'] = $aPersonaFusion['codEstCivil'];
        $aPersonaFusion['CodPaisNacimiento'] = $aPersonaFusion['codPaisNacimiento'];
        $aPersonaFusion['CodPaisNacimiento_Otro'] = $aPersonaFusion['pais_nacimiento_otro'];
        $aPersonaFusion['CodProvinciaNacimiento'] = $aPersonaFusion['codProvinciaNacimiento'];
        $aPersonaFusion['CodLocalidadNacimiento'] = $aPersonaFusion['codLocalidadNacimiento'];
        $aPersonaFusion['CodLocalidadNacimiento_Otra'] = $aPersonaFusion['localidad_nacimiento_otra'];
        $aPersonaFusion['CodPais'] = $aPersonaFusion['codPais'];
        $aPersonaFusion['CodSexo'] = $aPersonaFusion['per_sexo'];
        $aPersonaFusion['CodPais_Otro'] = $aPersonaFusion['pais_otro'];
        $aPersonaFusion['CodProvincia'] = $aPersonaFusion['codProvincia'];
        $aPersonaFusion['CodLocalidad'] = $aPersonaFusion['codLocalidad'];
        $aPersonaFusion['CodLocalidad_Otra'] = $aPersonaFusion['localidad_otra'];
        $aPersonaFusion['CodPostal'] = $aPersonaFusion['codPostal'];
        $aPersonaFusion['Calle'] = $aPersonaFusion['calle'];
        $aPersonaFusion['Calle_Otra'] = $aPersonaFusion['calle_otra'];
        $aPersonaFusion['Numero'] = $aPersonaFusion['numero'];
        $aPersonaFusion['Piso'] = $aPersonaFusion['piso'];
        $aPersonaFusion['Depto'] = $aPersonaFusion['depto'];
        $aPersonaFusion['Bis'] = ($aPersonaFusion['bis'] === 'N') ? 0 : 1;
        $aPersonaFusion['Telefono'] = $aPersonaFusion['telefono'];
        $aPersonaFusion['Celular'] = $aPersonaFusion['celular'];
        $aPersonaFusion['Email'] = $aPersonaFusion['email'];
        $aPersonaFusion['Observacion'] = $aPersonaFusion['observacion'];


        $form->populate($aPersonaFusion);


        //Asignacion de Valores a los "Combos de seleccion"
        $form->CodPais->addMultiOptions($this->view->cache_paises);
        $form->CodPaisNacimiento->addMultiOptions($this->view->cache_paises);
        $form->CodLocalidadNacimiento->addMultiOptions($this->view->cache_localidades_santafe);
        $form->CodProvincia->addMultiOptions($this->view->cache_provincias);
        $form->CodLocalidad->addMultiOptions($this->view->cache_localidades_santafe);
        $form->EstadoCivil->addMultiOptions($this->view->cache_estadosc);
        $form->CodTipoDocumento->addMultiOptions($this->view->cache_tipo_doc);
        $form->setDefault("CodTipoDocumento", $tipo_documento);
        $form->CodProvinciaNacimiento->addMultiOptions($this->view->cache_provincias);
        $form->setDefault("Documento", $documento);

        $this->view->form = $form;
        $this->view->form_title = '<h3 style="font-weight:300;">Información detallada de la Persona</h3>';
        $this->view->tpo_accion = 'XXX';


        $modeloPer = new Efector_models_Personas();
        $modeloPerSicap = new Efector_models_PersonasSicap();
        $modeloDon = new Banco_models_Donaciones();

        $personaBanco = $modeloPer->getByTipoYNum($documento, $tipo_documento)->toArray();
        $modeloSol = new Efector_models_Solicitudes();
        $solicitudes = $modeloSol->getByDni($documento);
        $donaciones = $modeloDon->getUltimaByDni2($documento);
        $this->view->tipo_docuemnto = $tipo_documento;
        $this->view->solicitudes = $solicitudes;
        $this->view->ultimoa_donaciones = $donaciones;
        $this->view->date = new Zend_Date(null, null, 'es_AR');
    }

    public function modificacionAction() {
        $documento = (int) $this->getRequest()->getParam('nro_documento');
        $id_tipo_documento = $this->getRequest()->getParam('tipo_documento');
        $tipo_documento = $this->getTipoDocumentoPorNombre($this->getRequest()->getParam('tipo_documento'));

        $this->form = new Efector_forms_personasFusion();
        $form = &$this->form;

        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();
            $validacion_extendida = $this->isValidExtendida();

            if ($validacion_extendida) {
                $values = $form->getValues();

                if (empty($values['Calle'])) {
                    $calle = 'OTRA CALLE';
                } else {
                    $calle = $values['Calle'];
                }
                $fuente = strtoupper($this->registry->config->codename) . " v" . $this->registry->config->pry_version;

                //Ajuste de Formato de Fecha
                $date = new Zend_Date(null, null, 'es_AR');
                $values['FechaNacimiento'] = $date->set($values['FechaNacimiento'])->get('y-MM-dd');
                $UsuarioModificacion = $this->auth->getIdentity()->usu_id . ':' . $this->auth->getIdentity()->usu_usuario;
                $values['UsuarioModificacion'] = $UsuarioModificacion;
                $values['FechaModificacion'] = new Zend_Db_Expr('NOW()');


                //Si es otra provincia, agrega localidad 99999
                if ($values['CodPais'] !== 'ARG') {
                    $CodLocalidad = '99999';
                    $CodProvincia = '99';
                    $CodLocalidad_Otra = $values['CodLocalidad_Otra'];
                    if ($values['Calle'] !== 'OTRA CALLE' && !empty($values['Calle'])) {
                        $Calle_Otra = '';
                    } else {
                        $Calle_Otra = $values['Calle_Otra'];
                    }
                } else {
                    $CodProvincia = $values['CodProvincia'];
                    if ($values['CodProvincia'] !== '82') {
                        $CodLocalidad = '99999';
                        $CodLocalidad_Otra = $values['CodLocalidad_Otra'];
                        //$CodProvincia = $values['CodProvincia'];
                    } else {
                        if ((empty($values['CodLocalidad'])) ||
                                ($values['CodLocalidad'] === '99999')) {
                            $CodLocalidad = '99999';
                            $CodLocalidad_Otra = $values['CodLocalidad_Otra'];
                        }
                    }
                    if ((!empty($values['CodLocalidad'])) &&
                            ($values['CodLocalidad'] !== '99999')) {
                        $CodLocalidad = $values['CodLocalidad'];
                        $CodLocalidad_Otra = '';
                    }
                    if ($values['Calle'] !== 'OTRA CALLE' && !empty($values['Calle'])) {
                        $Calle_Otra = '';
                    } else {
                        $Calle_Otra = $values['Calle_Otra'];
                    }
                }

                if ($values['CodPaisNacimiento'] !== 'ARG') {
                    $CodLocalidadNacimiento = '99999';
                    $CodProvinciaNacimiento = '99';
                    $CodLocalidadNacimiento_Otra = $values['CodLocalidadNacimiento_Otra'];
                } else {
                    $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                    if ($values['CodProvinciaNacimiento'] !== '82') {
                        $CodLocalidadNacimiento = '99999';
                        $CodLocalidadNacimiento_Otra = $values['CodLocalidadNacimiento_Otra'];
                        $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                    } else {
                        if ((empty($values['CodLocalidadNacimiento'])) ||
                                ($values['CodLocalidadNacimiento'] === '99999')) {
                            $CodLocalidadNacimiento = '99999';
                            $CodLocalidadNacimiento_Otra = $values['CodLocalidadNacimiento_Otra'];
                            $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                        } else {
                            $CodLocalidadNacimiento = $values['CodLocalidadNacimiento'];
                            $CodLocalidadNacimiento_Otra = '';
                            $CodProvinciaNacimiento = $values['CodProvinciaNacimiento'];
                        }
                    }
                    if ((!empty($values['CodLocalidadNacimiento'])) &&
                            ($values['CodLocalidadNacimiento'] !== '99999')) {
                        $CodLocalidadNacimiento = $values['CodLocalidadNacimiento'];
                        $CodLocalidadNacimiento_Otra = '';
                    }
                }

                $aPersonaBanco = [
                    'per_tipodoc' => $values['CodTipoDocumento'],
                    'per_nrodoc' => $values['Documento'],
                    'per_nombres' => $values['Nombres'],
                    'per_apellido' => $values['Apellido'],
                    'per_fnac' => $values['FechaNacimiento'],
                    'per_sexo' => $values['CodSexo'],
                    'clasificaciondonante_id' => NULL, //REVISAR ESTO.int(11)
                    'codOcupacion' => NULL,
                    'codEstCivil' => $values['EstadoCivil'],
                    'codNivelEducacion' => NULL,
                    'codPais' => $values['CodPais'],
                    'pais_otro' => $values['CodPais_Otro'],
                    'codProvincia' => $CodProvincia,
                    'codLocalidad' => $CodLocalidad,
                    'localidad_otra' => $CodLocalidad_Otra,
                    'codPostal' => NULL,
                    'municipio' => '1',
                    'distrito' => '1',
                    'calle' => $calle,
                    'calle_otra' => $Calle_Otra,
                    'numero' => $values['Numero'],
                    'piso' => $values['Piso'],
                    'depto' => $values['Depto'],
                    'manzana' => '1',
                    'entreCalleA' => '1',
                    'entreCalleB' => '1',
                    'bis' => $values['Bis'] == 1 ? 'S' : 'N',
                    'gps_Longitud' => '0',
                    'gps_Latitud' => '0',
                    'telefono' => $values['Telefono'],
                    'celular' => $values['Celular'],
                    'email' => $values['Email'],
                    'codPaisNacimiento' => $values['CodPaisNacimiento'],
                    'pais_nacimiento_otro' => $values['CodPaisNacimiento_Otro'],
                    'codProvinciaNacimiento' => $CodProvinciaNacimiento,
                    'codLocalidadNacimiento' => $CodLocalidadNacimiento,
                    'localidad_nacimiento_otra' => $CodLocalidadNacimiento_Otra,
                    'ult_serologia' => NULL,
                    'observacion' => $values['Observacion'],
                    'fuente' => $fuente,
                    //'fechaAlta' => new Zend_Db_Expr('NOW()'),
                    //'usuarioIngreso' => $this->auth->getIdentity()->usu_id . ':' . $this->auth->getIdentity()->usu_usuario,
                    'fechaModificacion' => new Zend_Db_Expr('NOW()'),
                    'usuarioModificacion' => $this->auth->getIdentity()->usu_id . ':' . $this->auth->getIdentity()->usu_usuario,
                    'fechaBaja' => '0000-00-00 00:00:00',
                    'usuarioBaja' => null,
                    'motivoBaja' => null
                ];



                $mPersonaBanco = new Efector_models_Personas();
                $where[] = "per_id = " . $values['per_id'];
                $mPersonaBanco->update($aPersonaBanco, $where);


                $this->_helper->redirector('buscar-persona', $this->_request->controller, $this->_request->module, array(
                    'tipo_documento' => $tipo_documento,
                    'nro_documento' => $documento
                ));
            }
        } else {

            $modeloPersonas = new Efector_models_Personas();
            $aPersonaBanco = $modeloPersonas->getByTipoYNum($documento, $tipo_documento)->toArray();
            $NumeroPaciente = $aPersonaBanco['sicap_id'];

            $modeloPerSicap = new Efector_models_PersonasSicap();
            $aPersonaSicap = []; //$modeloPerSicap->find($NumeroPaciente)->current()->toArray();
            $date = new Zend_Date(null, null, 'es_AR');
            $aPersonaBanco['FechaNacimiento'] = $date->set($aPersonaBanco['per_fnac'])->get('dd/MM/y');
            $aPersonaFusion = array_merge($aPersonaBanco, $aPersonaSicap);


            $aPersonaFusion['Nombres'] = $aPersonaFusion['per_nombres'];
            $aPersonaFusion['Apellido'] = $aPersonaFusion['per_apellido'];
            $aPersonaFusion['CodTipoDocumento'] = $aPersonaFusion['per_tipodoc'];
            $aPersonaFusion['Documento'] = $aPersonaFusion['per_nrodoc'];
            $aPersonaFusion['EstadoCivil'] = $aPersonaFusion['codEstCivil'];
            $aPersonaFusion['CodPaisNacimiento'] = $aPersonaFusion['codPaisNacimiento'];
            $aPersonaFusion['CodPaisNacimiento_Otro'] = $aPersonaFusion['pais_nacimiento_otro'];
            $aPersonaFusion['CodProvinciaNacimiento'] = $aPersonaFusion['codProvinciaNacimiento'];
            $aPersonaFusion['CodLocalidadNacimiento'] = $aPersonaFusion['codLocalidadNacimiento'];
            $aPersonaFusion['CodLocalidadNacimiento_Otra'] = $aPersonaFusion['localidad_nacimiento_otra'];
            $aPersonaFusion['CodPais'] = $aPersonaFusion['codPais'];
            $aPersonaFusion['CodSexo'] = $aPersonaFusion['per_sexo'];
            $aPersonaFusion['CodPais_Otro'] = $aPersonaFusion['pais_otro'];
            $aPersonaFusion['CodProvincia'] = $aPersonaFusion['codProvincia'];
            $aPersonaFusion['CodLocalidad'] = $aPersonaFusion['codLocalidad'];
            $aPersonaFusion['CodLocalidad_Otra'] = $aPersonaFusion['localidad_otra'];
            $aPersonaFusion['CodPostal'] = $aPersonaFusion['codPostal'];
            $aPersonaFusion['Calle'] = $aPersonaFusion['calle'];
            $aPersonaFusion['Calle_Otra'] = $aPersonaFusion['calle_otra'];
            $aPersonaFusion['Numero'] = $aPersonaFusion['numero'];
            $aPersonaFusion['Piso'] = $aPersonaFusion['piso'];
            $aPersonaFusion['Depto'] = $aPersonaFusion['depto'];
            $aPersonaFusion['Bis'] = ($aPersonaFusion['bis'] === 'N') ? 0 : 1;
            $aPersonaFusion['Telefono'] = $aPersonaFusion['telefono'];
            $aPersonaFusion['Celular'] = $aPersonaFusion['celular'];
            $aPersonaFusion['Email'] = $aPersonaFusion['email'];
            $aPersonaFusion['Observacion'] = $aPersonaFusion['observacion'];


            $form->populate($aPersonaFusion);
        }

        //Asignacion de Valores a los "Combos de seleccion"
        $form->CodPais->addMultiOptions($this->view->cache_paises);
        $form->CodPaisNacimiento->addMultiOptions($this->view->cache_paises);
        $form->CodLocalidadNacimiento->addMultiOptions($this->view->cache_localidades_santafe);
        $form->CodProvincia->addMultiOptions($this->view->cache_provincias);
        $form->CodLocalidad->addMultiOptions($this->view->cache_localidades_santafe);
        $form->EstadoCivil->addMultiOptions($this->view->cache_estadosc);
        $form->CodTipoDocumento->addMultiOptions($this->view->cache_tipo_doc);
        $form->setDefault("CodTipoDocumento", $tipo_documento);
        $form->CodProvinciaNacimiento->addMultiOptions($this->view->cache_provincias);
        $form->setDefault("Documento", $documento);

        //Inicializacion de datos para el ALTA de Donante
        $this->view->form = $form;
        $this->view->form_title = '<h3 style="font-weight:300;">Formulario de <b>actualizaci&oacute;n</b> de datos</h3>';
        $this->view->tpo_accion = 'MODIFICACION';

        $this->renderScript('personas/form.phtml');
    }

   //  /** Json & WS * */
   //  public function getAction() {

   //      $documento = (int) $this->getRequest()->getParam('documento');
   //      $tipo_documento = $this->getTipoDocumentoPorNombre($this->getRequest()->getParam('tipo'));

   //      $sicap = (int) $this->getRequest()->getParam('sicap');

   //      $modeloSol = new Efector_models_Solicitudes();
   //      $modeloPer = new Efector_models_Personas();
   //      $modeloPerSicap = new Efector_models_PersonasSicap();
   //      $modeloDon = new Banco_models_Donaciones();
   //      $modeloPerfiles = new Gestion_models_PerfilesProduccion();
   //      $modeloEfe = new Acceso_models_Efectores();
   //      $modeloLoc = new Efector_models_Localidades();

   //      /* *************************************************************
   //       * Primero busco en tabla per_persona, si esta, procedo con   *
   //       * esos datos, si no, voy a sicap.pacientes					  *
   //       * ************************************************************ */
   //      $resultado = $modeloPer->getByTipoYNum($documento, $tipo_documento);

   //      if (count($resultado) > 0) {
   //          $per_sicap = $modeloPerSicap->find($resultado->sicap_id)->current();
   //          $ver = $per_sicap->toArray();
   //          $_persona = $per_sicap->toArray();
   //          echo "<pre>";
   //          print_r($_persona);
   //          echo "</pre>";
   //          die;
   //          $modeloLoc = new Efector_models_Localidades();
   //          $localidad = $modeloLoc->getNombre($_persona['ClaveLocalidad']);


   //          $_persona['Nombre'] = utf8_encode($_persona['Nombre']);
   //          $_persona['Apellido'] = utf8_encode($_persona['Apellido']);
   //          $_persona['fullname'] = utf8_encode($_persona['Apellido'] . ', ' . $_persona['Nombre']);
   //          $_persona['edad'] = $this->calcularEdad($_persona['FechaNacimiento']) . " a&ntilde;os";
   //          $_persona['estado_civil'] = utf8_encode($this->view->cache_estadosc[$_persona['CodEstCiv']]);
   //          $_persona['Provincia'] = utf8_encode($this->view->cache_provincias[$_persona['CodProvincia']]);

   //          $personaBanco = $resultado->toArray();

   //          if ((empty($_persona['Calle'])) || ($_persona['Calle'] === null)) {
   //              $_persona['Calle'] = $personaBanco['calle_otra'];
   //          }

   //          $_persona['localidad'] = utf8_encode($localidad['nomloc']);
   //          $_persona['email'] = utf8_encode($_persona['email']);
   //          $_persona['Celular'] = utf8_encode($_persona['celular']);

   //          $ultimaDonacion = $modeloDon->getUltimaByDni($resultado->per_id);

   //          if ($ultimaDonacion) {
   //              $diasDesde = $ultimaDonacion['dias_desde'];
   //              if ($diasDesde > 31) {
   //                  $niceDesde = ', ' . $this->view->nicetime($ultimaDonacion['don_fh_inicio']);
   //              }
   //              $totalDonaciones = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
   //              $ultimaDonacion = $ultimaDonacion->toArray();
			// 	//$puedeCrear = false;
   //          }

   //          $efectoresAll = $modeloEfe->getPairs();
   //          $donaciones = $modeloDon->getByDni($resultado->per_id)->toArray();

   //          $fechas = array();
   //          $usuarios = array();
   //          $efectores = array();
   //          $descPerfil = array();
   //          foreach ($donaciones as $i => $v) {
   //              $fechas[] = $v['don_fh_inicio'];
   //              $usuarios[] = $v['don_usu_extraccion'];
   //              $nomEfe = $modeloEfe->getNom($v['efe_id']);
   //              foreach ($nomEfe as $j => $u) {
   //                  $efectores[] = utf8_encode($u['nomest']);
   //              }
   //              $descPerfil = $modeloPerfiles->getDesc($v['ppr_id']);
   //              foreach ($descPerfil as $j => $u)
   //                  $perfiles[] = $j;
   //          }

   //          $puedeCrear = true;
   //          $solicitudes = $modeloSol->getByDni($resultado->per_id, array(0, 1, 2, 3, 4, 5))->toArray(); //comprobar
   //          $data = array(
   //              'persona' => $_persona,
   //              'donacionesPrevias' => (int) $totalDonaciones,
   //              'diasDesde' => $diasDesde,
   //              'desde' => $niceDesde,
   //              'crearSol' => $puedeCrear,
   //              'solicitudes' => sizeof($solicitudes),
   //              'fechas' => $fechas,
   //              'usuarios' => $usuarios,
   //              'efectores' => $efectores,
   //              'perfiles' => $perfiles
   //          );

			// //TODO: Implementacion para windows.
			// //$data['persona']['edad'] = '32 ';

   //          $this->_helper->json(array(
   //              'st' => 1,
   //              'data' => $data
   //          ));
   //      } else {

   //          /*** ***************************************************** *
   //           * Acá, buscar en SICAP,                                   *
   //           * si hay un solo resultado: insertar en tabla per_persona *
   //           * si hay mas de uno: mostrar varios y llamar get          *
   //           * También chequeo si vengo de una llamada con sicap id de * 
   //           * múltiples personas con mismo dni                        *
   //           * ******************************************************* */
   //          $persona = $modeloPerSicap->getByDniSimple($documento, $tipo_documento);
   //          $personas = $modeloPerSicap->getByDniMultiple($documento, $tipo_documento);

   //          /* Si hay varios resultados */
   //          if (sizeof($personas) > 1) {
   //              $_personas = $personas->toArray();
			// /*                foreach ($_personas as $i => $p) {
			//                       $localidad = $modeloLoc->getNombre($p['ClaveLocalidad']);
			//                       if ($localidad)
			//                           $_personas[$i]['nomloc'] = $localidad['nomloc'];
			//                       else
			//                           $_personas[$i]['nomloc'] = "Desconocida";
			// */                }
   //              $this->_helper->json(array('st' => 'varios', 'personas' => $_personas));
   //          }

   //          /* Si hay sólo un resultado */
   //          if (!$persona) {
   //              $this->_helper->json(array(
   //                  'st' => 0,
   //                  'tipo' => $tipo_documento,
   //                  'documento' => $documento,
   //              ));
   //          } else {
   //              $_persona = $persona->toArray();
   //              $_persona['Nombre'] = utf8_encode($_persona['Nombre']);
   //              $_persona['Apellido'] = utf8_encode($_persona['Apellido']);
   //              $_persona['fullname'] = utf8_encode($_persona['Apellido'] . ', ' . $_persona['Nombre']);
   //              $_persona['edad'] = html_entity_decode(utf8_encode($this->view->niceage($_persona['FechaNacimiento'])));
   //              $_persona['estado_civil'] = utf8_encode($this->view->cache_estadosc[$_persona['CodEstCiv']]);
   //              $_persona['Provincia'] = utf8_encode($this->view->cache_provincias[$_persona['CodProvincia']]);
   //              $_persona['localidad'] = utf8_encode($_persona['ClaveLocalidad']);
   //              $_persona['email'] = utf8_encode($_persona['email']);

   //              /* Alta en tabla per_personas */
   //              $per_persona = $modeloPer->createRow(array(
   //                          'per_nrodoc' => $documento,
   //                          'per_tipodoc' => $tipo_documento,
   //                          'per_nombres' => $_persona['Nombre'],
   //                          'per_apellido' => $_persona['Apellido'],
   //                          'per_fnac' => $persona->FechaNacimiento,
   //                          'per_sexo' => $persona->CodSexo,
   //                          'sicap_id' => $persona->NumeroPaciente
   //                      ))->save();
   //              $puedeCrear = true;
   //              $this->_helper->json(array(
   //                  'st' => 1,
   //                  'data' => array(
   //                      'persona' => $_persona,
   //                      'crearSol' => $puedeCrear
   //                  )
   //              ));
   //          }
   //      }
   //  }

    public function getLocalidadesAction() {
		#if($this->getRequest()->isXmlHttpRequest())
        $idProvincia = $this->getRequest()->getParam('id');
        $modelo = new Efector_models_Sicap();
        $modelo->getDefaultAdapter()->query('SET NAMES utf8');
        $resultados = $modelo->getLocalidades($idProvincia);

        $this->_helper->json(array('res' => $resultados));
    }

    public function getPaisesAction() {
		#if($this->getRequest()->isXmlHttpRequest())        
        $modelo = new Efector_models_Paises();
        $modelo->getDefaultAdapter()->query('SET NAME utf8');
        $resultados = $modelo->getAll();
        $this->_helper->json(array('res' => $resultados));
    }

    public function getCallesAction() {
		#if($this->getRequest()->isXmlHttpRequest())
        $idLocalidad = $this->getRequest()->getParam('id');
        $modelo = new Efector_models_Calle();
        $modelo->getDefaultAdapter()->query('SET NAMES utf8');
        $resultados = $modelo->getCalles($idLocalidad);
        $this->_helper->json(array('res' => $resultados));
    }

    public function calcularEdad($fechaNacimiento) {
        $date = new DateTime($fechaNacimiento);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    /** Busca el historial de donaciones con sus datos relacionados **/
    public function historicoDonacionesAction() {

        //TODO: Agregar dias entre donaciones en confir o tabla.
        $dias_ultima_donacion = 30;

        $nro_documento = $this->getRequest()->getParam('nro_documento');
        $id_tipo_documento = $this->getRequest()->getParam('tipo_documento');

        if (!empty($nro_documento) && !empty($id_tipo_documento)) {

            $tipo_documento = $this->view->cache_tipo_doc[$id_tipo_documento];
            $personaSimetra = $this->buscarPersonaEnSimetra($id_tipo_documento, $nro_documento);

            if ($personaSimetra['estado']) {

                $modeloSol = new Efector_models_Solicitudes();
                $modeloPer = new Efector_models_Personas();
                $modeloPerSicap = new Efector_models_PersonasSicap();
                $modeloDon = new Banco_models_Donaciones();
                $modeloDonExt = new Efector_models_DetallesDonaciones();
                $modeloPerfiles = new Gestion_models_PerfilesProduccion();
                $modeloEfe = new Acceso_models_Efectores();
                $modeloLoc = new Efector_models_Localidades();
                $modeloSerologia = new Banco_models_Serologia();
                $modeloInmunologia = new Banco_models_Inmunologia();
                
                $modeloEnfermedades = new Banco_models_Enfermedades();
                $cat_enfermedades = $modeloEnfermedades->fetchAll();

                $ultimaDonacion = $modeloDon->getUltimaByDni($personaSimetra['data']['per_id']);

                if ($ultimaDonacion != null) {
                    $diasDesde = $ultimaDonacion['dias_desde'];
                    if ($diasDesde > $dias_ultima_donacion) {
                        $niceDesde = ', ' . $this->view->nicetime($ultimaDonacion['don_fh_inicio']);
                    }
                    $totalDonaciones = $modeloDon->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
                    $ultimaDonacion = $ultimaDonacion->toArray();
                }

                $efectoresAll = $modeloEfe->getPairs();

                $donaciones = $modeloDon->getDonacionesHistorialParaHistorialNotCurrent($personaSimetra['data']['per_id']);
                $retorno = array();
                if ($donaciones != null) {
                    foreach ($donaciones as $donacion) {
                        $donacion = $donacion->toArray();
                        $retorno_donacion = $donacion;
                        $retorno_donacion['efector_nombre'] = $modeloEfe->getNom($donacion['efe_id']);
                        if (isset($donacion['ppr_id'])) {
                            $retorno_donacion['desc_perfil'] = $modeloPerfiles->getDesc($donacion['ppr_id']);
                        }

                        $serologia = $modeloSerologia->getDatosDonacion($donacion['don_id']);
                        if ($serologia != null) {
                            $serologia = $serologia->toArray();


                            $modeloSeroEnf = new Banco_models_Serologiasenfermedades();

                            $enfermedadesDeterminaciones = $modeloSeroEnf->fetchAll('serologias_id = ' . $serologia['id']);

                            $serologia['determinaciones'] = $enfermedadesDeterminaciones;

                            $retorno_donacion['serologia'] = $serologia;
                        }
                        $inmunologia = $modeloInmunologia->getDatosDonacion($donacion['don_id']);

                        $retorno_donacion['inmunologia'] = "Grupo: " . $inmunologia['grupo']
                                . ", Factor: " . $inmunologia['factor']
                                . ", Rastreo: " . $inmunologia['rastreo']
                                . ", DU: " . $inmunologia['du']
                                . ", CDE: " . $inmunologia['cde']
                                . ", Fenotipo: " . $inmunologia['fenotipo'];

                        $retorno_donacion['extendida'] = $modeloDonExt->getByDonID($donacion['don_id']);
                        $retorno[] = $retorno_donacion;
                    }

                    $solicitudes = $modeloSol->getByDni($personaSimetra['data']['per_id'], array(0, 1, 2, 3, 4, 5))->toArray(); //comprobar
                    $data = array(
                        'persona' => $personaSimetra,
                        'donacionesPrevias' => [
                            'total' => count($donaciones),
                            'donaciones' => $retorno,
                        ],
                        'diasDesde' => $diasDesde,
                        'desde' => $niceDesde,
                        'crearSol' => $puedeCrear,
                        'solicitudes' => sizeof($solicitudes),
                        'dias_sugeridos_entre_donaciones' => $dias_ultima_donacion,
                    );
                } else {
                    $data = array(
                        'persona' => $personaSimetra,
                        'dias_sugeridos_entre_donaciones' => $dias_ultima_donacion,
                    );
                }

                $this->view->data = $data;
                $this->view->enfermedades = $cat_enfermedades->toArray();
            } else {
                $this->view->sin_datos = true;
            }
        }
    }

    /** Buscar Personas en el Sistema SIMETRA y SICAP
     * 
     * Estados de Retorno:
     * 
     * Variable A: Buscar Persona en SIMETRA - B: Estado Vinculacion SIMETRA - SICAP - C: Buscar Persona en SICAP
     * 
     * A B C   Estados	
     * 0 0 0   0:Crear todo
     * 0 0 1   1:Ver cuantas encontro, elegir una y crear automaticamente en SIMETRA con vinculacion
     * 0 1 0   2:IRRACIONAL
     * 0 1 1   3:IRRACIONAL
     * 1 0 0   4:Crear Persona en SICAP y vincularla.
     * 1 0 1   5:Ver cuantas encontro y elegir una y crear automaticamente en vinculacion con SIMETRA
     * 1 1 0   6:IRRACIONAL
     * 1 1 1   7:Mostrar datos
     * 
     * 
     */

    public function buscarPersonaAction() {
        $ws_sicap = false;
        $minData = ($this->getRequest()->getParam('nro_documento') != NULL) && ($this->getRequest()->getParam('tipo_documento') != NULL);
        
        if (($this->getRequest()->isPost() == true) || ($minData == true)) {
            $post = $this->getRequest()->getPost();

            $nro_documento = $this->getRequest()->getParam('nro_documento');
            $id_tipo_documento = $this->getRequest()->getParam('tipo_documento');
            $tipo_documento = $this->view->cache_tipo_doc[$id_tipo_documento];

            $personaSimetra = $this->buscarPersonaEnSimetra($id_tipo_documento, $nro_documento);

            $vinculacion = $this->EstadoVinculacion($personaSimetra);

            if ($this->ws_sicap) {

                if (($personaSimetra['estado'] === false) && ($vinculacion === false)) {
                    $personaSicap = $this->buscarPersonaEnSicap(null, $id_tipo_documento, $nro_documento);
                    if ($personaSicap['estado'] === true) {//0 0 1
                        $estado = [
                            'valor' => 1,
                            'Descripcion' => "Estado 1:Ver cuantas encontro, elegir una y crear automaticamente en SIMETRA con vinculacion"
                        ];
                        //Ver cuantas encontro, si es ua sola crear automaticamente la persons en Simetra con su vinculo a SICAP
                        //Si encontro varias, el usuario debe elegir y luego se crea la persona vincualada al sicap_id de la 
                        //persona seleccionada.
                    } else {//0 0 0
                        $estado = [
                            'valor' => 0,
                            'Descripcion' => "Estado 0:Crear todo"
                        ];
                    }
                }

                if (($personaSimetra['estado'] === true) && ($vinculacion === false)) {
                    $personaSicap = $this->buscarPersonaEnSicap(null, $id_tipo_documento, $nro_documento);
                    if ($personaSicap['estado'] === true) {//1 0 1
                        $estado = [
                            'valor' => 5,
                            'Descripcion' => "Estado 5:Ver cuantas encontro y elegir una y crear automaticamente en vinculacion con SIMETRA"
                        ];
                        //Ver cuantas encontro, si es ua sola crear automaticamente el vinculo con SIMETRA.
                        //Si encontro varias, el usuario debe elegir y luego se crea el vinculo en SIMETRA.
                    } else { //1 0 0
                        $estado = [
                            'valor' => 4,
                            'Descripcion' => "Estado 4:Crear Persona en SICAP y vincularla."
                        ];
                    }
                }

                if (($personaSimetra['estado'] === true) && ($vinculacion === true)) {
                    //TODO: buscar directamente por SICAP_ID no por tipo y nro de documento
                    $personaSicap = $this->buscarPersonaEnSicap($personaSimetra['data']['sicap_id'], null, null);
                    if ($personaSicap['estado'] === true) {//1 1 1
                        $estado = [
                            'valor' => 7,
                            'Descripcion' => "Estado 7:Mostrar datos"
                        ];
                    }
                }
            } else {

                if ($personaSimetra['estado'] === true) {

                    $estado = [
                        'valor' => 7,
                        'Descripcion' => "Estado 7:Mostrar datos"
                    ];
                } else {

                    $estado = [
                        'valor' => 0,
                        'Descripcion' => "Estado 0:Crear todo"
                    ];
                }
            }

            $this->view->result = [
                'buscado' => [
                    'tipo_documento' => $id_tipo_documento,
                    'nro_documento' => $nro_documento
                ],
                'estado' => $estado,
                'simetra' => $personaSimetra,
                'vinculacion' => $vinculacion,
                'sicap' => $personaSicap
            ]; //        $this->renderScript('personas/alta.phtml');

        }
    }

    /** Buscar Persona en SIMETRA x Tipo y Nro. de Documento
     * 
     * @param type $id_tipo_documento
     * @param type $nro_documento
     * @return type Array(bool,obj)
     */
    function buscarPersonaEnSimetra($id_tipo_documento, $nro_documento) {

        $mPersonas = new Efector_models_Personas();
        $persona = $mPersonas->getByTipoYNum($nro_documento, $id_tipo_documento);
        if ($persona !== null) {
            $vRet = [
                'estado' => true,
                'data' => $persona,
                'codigo_autogenerado' => $mPersonas->getCodigoAutogenerado($persona['per_id'])
            ];
        } else {
            $vRet = [
                'estado' => false,
                'data' => null
            ];
        }
        return $vRet;
    }

    /** Devuelve el estado de vinculacion entre las personas de ambos sistemas. Y corrige vinculos rotos.
     * 
     * @param type $PersonaSimetra
     * @return boolean
     */
    private function EstadoVinculacion($PersonaSimetra) {

        if (!$this->ws_sicap) {
            return false;
        }

        $vRet = false;

        if ($PersonaSimetra['estado'] === true) {
            if ($PersonaSimetra['data']['sicap_id'] == null) {
                $vRet = false;
            } else {
                $mPersonaSICAP = new Efector_models_PersonasSicap();
                $personaSicap = $mPersonaSICAP->find($PersonaSimetra['data']['sicap_id'])->current();

                if (!empty($personaSicap)) {
                    $vRet = true;
                } else {
                    $PersonaSimetra['sicap_id'] = null;
                    $PersonaSimetra->save();
                    $vRet = false;
                }
            }
        } else {
            $vRet = false;
        }
        return $vRet;
    }

    /** Busca personas en Sicap para el tipo y numero de documento ingresado.
     * 
     * @param type $sicap_id Si quiero buscar por este parametro los otros van en null y viceversa.
     * @param type $id_tipo_documento
     * @param type $nro_documento
     * @return array(bool, cant, obj)
     */
    private function buscarPersonaEnSicap($sicap_id, $id_tipo_documento, $nro_documento) {

        if (!$this->ws_sicap) {
            return ['estado' => false, 'dato' => null];
        }

        $vret = false;
        $mPersonaSicap = new Efector_models_PersonasSicap();

        if ($sicap_id === null) {
            $personaSicap = $mPersonaSicap->getByDniMultiple($nro_documento, $id_tipo_documento);
        } else {
            $personaSicap = $mPersonaSicap->find($sicap_id);
        }
        if (($personaSicap->count() > 0)) {
            $vret = [
                'estado' => true,
                'dato' => $personaSicap
            ];
        } else {
            $vret = [
                'estado' => false,
                'dato' => null
            ];
        }

        return $vret;
    }

    private function isValidExtendida() {
        $post = $this->getRequest()->getPost();
        $form = &$this->form;
        $vRet = true;
        $vRet = ($vRet && $form->isValid($post));

		/**Validar que si el Pais De Nacimiento no es Argentina, el campo otra 
		localidad de naciemiento tenga algun valor.**/
        if ($post['CodPaisNacimiento'] !== 'ARG') {
            if ($post['CodLocalidadNacimiento_Otra'] === '') {
                $vRet = false;
                $form->getElement('CodLocalidadNacimiento_Otra')
                        ->addError("El campo localidad de nacimiento no puede "
                                . "estar vacio.");
            }
        }

		/**Validar que si el Pais de Residencia no es ARgentina, el camo otra 
		Localidad dene estar vacio.**/
        if ($post['CodPais'] !== 'ARG') {
            if ($post['CodLocalidad_Otra'] === '') {
                $vRet = false;
                $form->getElement('CodLocalidad_Otra')
                        ->addError("El campo localidad no puede estar vacio.");
            }
            if ($post['Calle_Otra'] === '') {
                $vRet = false;
                $form->getElement('Calle_Otra')
                        ->addError("El campo calle no puede estar vacio.");
            }
        }

		/*Validar que si la provincia de nacimiento es Distinta de Santa Fe, el 
		campo otra Localidad Nacimiento no puede ser nulo.*/
        if ($post['CodProvinciaNacimiento'] !== '82') {
            if ($post['CodLocalidadNacimiento_Otra'] === "") {
                $vRet = false;
                $form->getElement('CodLocalidadNacimiento_Otra')
                        ->addError("El campo localidad de nacimiento no puede"
                                . " estar vacio.");
            }
        }

		//Validar que si la provincia de  es Distinta de Santa Fe, el campo otra
		// Localidad  no puede ser nulo.
        if ($post['CodProvincia'] !== '82') {
            if ($post['CodLocalidad_Otra'] === "") {
                $vRet = false;
                $form->getElement('CodLocalidad_Otra')
                        ->addError("El campo localidad no puede "
                                . "estar vacio.");
            }
            if ($post['Calle_Otra'] === '') {
                $vRet = false;
                $form->getElement('Calle_Otra')
                        ->addError("El campo calle no puede estar vacio.");
            }
        }

		//Validar que si la Localidad es Otra, el campo otra
		// Localidad  no puede ser nulo.
        if ($post['CodLocalidad'] === '99999') {
            if ($POST['CodLocalidad_Otra'] === "") {
                $vRet = false;
                $form->getElement('CodLocalidad_Otra')
                        ->addError("El campo localidad no puede "
                                . "estar vacio.");
            }
            if ($post['Calle_Otra'] === '') {
                $vRet = false;
                $form->getElement('Calle_Otra')
                        ->addError("El campo calle no puede estar vacio.");
            }
        }

		//Validar que si la Localidad Naciemiento es Otra, el campo otra
		// Localidad Nacimiento no puede ser nulo.
        if ($post['CodLocalidadNacimiento'] === '99999') {
            if ($POST['CodLocalidadNacimiento_Otra'] === "") {
                $vRet = false;
                $form->getElement('CodLocalidadNacimiento_Otra')
                        ->addError("El campo localidad no puede "
                                . "estar vacio.");
            }
        }

		//Validar que si la Calle es Otra, el campo otra
		// Calle  no puede ser nulo.
        if ($post['Calle'] === 'OTRA CALLE') {
            if ($POST['Calle_Otra'] === "") {
                $vRet = false;
                $form->getElement('Calle_Otra')
                        ->addError("El campo Calle no puede estar vacio.");
            }
        }
        return $vRet;
    }

    public function getPermisoDonacionPorPeridAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $vRet = [
            'estado' => 'OK',
        ];

        $persona_id = $this->getRequest()->getParam('persona_id');
        if (empty($persona_id)) {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0000',
                'mensaje' => 'No se puede ejecutar el control de la Persona. Proceso Detenido, reporte este incidente.'
            ];
        }

        //TODO: Agregar en el config.
        $minDiasEntreDonaciones = 3;

        $mPersonas = new Efector_models_Personas();

        $aPersona = $mPersonas->fetchRow('per_id =' . $persona_id);

        if (empty($aPersona)) {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0001',
                'mensaje' => 'Persona no encontrada.'
            ];
        }

        $mSolicitudes = new Efector_models_Solicitudes();
        $solicitudes_diferidas = $mSolicitudes->getSolicitudesDiferidasByPerId($persona_id);

        $solicitudes_diferidas = $mSolicitudes->getSolicitudesDiferidasByPerId($persona_id);

        if ($solicitudes_diferidas->count() > 0) {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0002',
                'mensaje' => 'El paciente se encuentra diferido. Revise su historial para más información.',
                'data' => $solicitudes_diferidas
            ];
        }

        $solicitudes_pendientes = $mSolicitudes->getSolicituresPorPerIdEstado($persona_id, 1);

        if ($solicitudes_pendientes->count() > 0) {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0003',
                'mensaje' => 'Usted tiene ' . $solicitudes_pendientes->count() . ' solicitud pendiente. Revise su historial para más información.'
            ];
        }

        $mDonaciones = new Banco_models_Donaciones();

        $ultimaDonacion = $mDonaciones->getUltimaByDni($persona_id);

        if (!empty($ultimaDonacion) && ($ultimaDonacion['dias_desde'] < $minDiasEntreDonaciones)) {
            $vRet['estado'] = 'NOK';
            $vRet['error'][] = [
                'codigo' => 'E0004',
                'mensaje' => 'Usted no puede donar debido que no paso un minimo de ' . $minDiasEntreDonaciones . ' días desde su utima donacion.'
            ];
        }

        $donaciones = $mDonaciones->getByDni($persona_id)->toArray();

        //Consultar Si tiene id de la ultima serologia realizada.
        if ($aPersona['ult_serologia']) {//Este campo solo tiene algun valor si la persona dono almenos una vez.
            //Buscar los datos de esta serologia y verificar el estado del registro.
            $mSerologias = new Banco_models_Serologia();
            $oSerologia = $mSerologias->fetchRow('id = ' . $aPersona['ult_serologia']);
            //Estados Serologia: 
            /*
             * 1:Algun analisis todavia no fue determinado
             * 2:Algun analisis dio Reactivo
             * 3:Todo los analisis dieron No Reactivo
             */
            if ($oSerologia->estado != 3) {
                $vRet['estado'] = 'NOK';
                $vRet['error'][] = [
                    'codigo' => 'E0005',
                    'mensaje' => 'Usted no puede donar debido riesgos en Serologia. Para mas informacion revise el historial de donaciones.'
                ];
            }
        }


        $this->_helper->json($vRet);
    }

}
