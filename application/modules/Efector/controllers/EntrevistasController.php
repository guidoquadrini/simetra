<?php

//use application\modules\Efector\views\helpers\myPDF;

/**
 *
 */
class Efector_EntrevistasController extends BootPoint {

    /**
     * INDEX de Entrevistas
     * Listado de entrevistas pendientes y trabajadas durante el dia.
     */
    public function indexAction() {
        
        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');
        
        if ($fechaDesde == '') {
            $fechaDesde = (new DateTime())->setTime('0', '0', '0')->format('d-m-Y H:i:s');
        } else {
            $fechaDesde = (new DateTime($fechaDesde))->setTime('0', '0', '0')->format('d-m-Y H:i:s');
        }
        
        if ($fechaHasta == '') {
            $fechaHasta = (new DateTime())->setTime('23', '59', '59')->format('d-m-Y H:i:s');
        } else {
            $fechaHasta = (new DateTime($fechaHasta))->setTime('23', '59', '59')->format('d-m-Y H:i:s');
        }
               
        $nro = $this->getRequest()->getParam('nro');
 
        $perPage = 18;
   
        $efector_id = $this->getRequest()->getParam('efector_id');
        
        if( $efector_id == ''){
            $efector_id = $this->efector_activo['id_efector'];                   
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);    
            $efector_nombre = $datos_efector[0]['nomest'];            
        }            
        
        $modeloDonacion = new Banco_models_Donaciones();
        $entrevistas = $modeloDonacion->getByEstadoAndEfe($this->efector_activo['id_efector'], '*', $this->page, $perPage, $fechaDesde, $fechaHasta, $nro);        
        
        $this->view->busqueda = '';
        if (!empty($nro))
            $this->view->busqueda .= '<br/> Cód. de Donación: ' . $nro . ' <br/>';

        if (!empty($fechaDesde))
            $this->view->busqueda .= 'Fecha Desde: ' . $fechaDesde . '<br/>';


        if (!empty($fechaHasta))
            $this->view->busqueda .= 'Fecha Hasta: ' . $fechaHasta . '<br/>';
        
        if (!empty($entrevistas)) {
            $total = $modeloDonacion->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }
        
        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->nro = $nro;
        $this->view->resultados = $entrevistas;
        $this->view->paginas = $paginas;
    }

    
    
    /**
     * Alta de Entrevista
     *
     */
    public function altaAction() {
        
    }

    /**
     * Formulario de ENTREVISTAS
     *
     */
    public function formularioEntrevistaAction() {

        $don_id = $this->getRequest()->getParam('don_id');
       
        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->getByID($don_id);

        $modeloPersona = new Efector_models_Personas();
        $persona = $modeloPersona->getByID($donacion['per_id'])->toArray();
        //$codigo_autogenerado = $modeloPersona->getCodigoAutogenerado($donacion['per_id']);
        
        $modeloCategoriaPreguntas = new Gestion_models_Cuestionariopreguntascategorias();

        // obtenemos la plantilla activa de la encuesta
        $modeloPlantilla = new Gestion_models_Plantillascuestionario();
        $plantilla = $modeloPlantilla->getPlantillaActiva();

        if ($plantilla != null) {

            // obtenemos las categorias que tienen las preguntas para la plantilla activa
            $modeloPreguntas = new Gestion_models_Plantillascuestionariodetalle();
            $categorias_activas = $modeloPreguntas->getCategoriasActivasPreguntas($plantilla['id']);

            $resultado = null;

            // arma $resultado con el ID y Nombre de la categoria agrupadora de las pregntas de la encuesta
            foreach ($categorias_activas as $categoria) {
                $resultado[] = $modeloCategoriaPreguntas->getByID($categoria['cuestionario_pregunta_categoria_id']);
            }
            
            foreach ($resultado as &$categoria) {
                $categoria['preguntas'] = $modeloPreguntas
                        ->getPreguntasPorPlantillaPorCategoria($plantilla['id'], $categoria['id']);
            }

            $this->view->preguntas_por_categoria = $resultado;
            $this->view->persona = $persona;
            //$this->view->codigo_autogenerado = $codigo_autogenerado;
            $this->view->donacion = $donacion;
        } else {

            echo "No existe una planilla activa para la encuesta / cuestionario";
            die();
        }
    }

    /**
     * Grabar Entrevista
     *
     */
    public function grabarAction() {

        $DATA = json_decode($this->getRequest()->getParam('datos'));
        
        // obtenemos la plantilla activa de la encuesta
        $modeloPlantilla = new Gestion_models_Plantillascuestionario();
        $plantilla = $modeloPlantilla->getPlantillaActiva();

        if ($plantilla != null) {
            $plantilla_id = $plantilla['id'];
        }

        $fecha = str_replace('T', ' ', $DATA->header->fecha);  //"fecha":"2018-04-18T10:49:11"
        
        $persona_id = $DATA->header->persona_id;
        $donacion_id = $DATA->header->donacion_id;
        $don_nro = $DATA->header->don_nro;
        $obervaciones = $DATA->header->observaciones;

        $efector_id = $this->efector_activo['id_efector'];
        $efector_nombre = $this->efector_activo['nomest'];

        // INSERTA DATOS ENECABEZADO ENTREVISTA
        $modeloEncabezado = new Efector_models_Cuestionariosrespuestas();
        $encabezado_id = $modeloEncabezado->createRow(array(
                    'fecha_inicio' => $fecha,
                    'fecha_fin' => new Zend_Db_Expr('NOW()'),
                    'efector_id' => $this->efector_activo['id_efector'],
                    'posta_id' => 0,
                    'estado' => 1,
                    'plantilla_cuestionario_id' => $plantilla_id,
                    'donacion_id' => $donacion_id,
                    'don_nro' => $don_nro,
                    'observaciones' => $obervaciones,
                    'usuario_responsable_id' => $this->auth->getIdentity()->usu_id
                ))->save();


        // INSERTA PREGUNTAS DETALLE ENTREVISTA
        $modeloDetalle = new Efector_models_Cuestionariosrespuestasdetalle();

        foreach ($DATA as $linea) {
            if ($linea->pregunta_id <> '') {

                $items = $modeloDetalle->createRow(array(
                            'cuestionario_respuesta_id' => $encabezado_id,
                            'pregunta_id' => $linea->pregunta_id,
                            'respuesta_id' => $linea->valor,
                            'observacion' => $linea->observacion
                        ))->save();
            }
        }

        
        // DONACION: estado = 104 pendiente examen medico
        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->fetchRow('don_id=' . $donacion_id);
        $donacion->setFromArray(array(
                        'don_estado' => 104
                  ))->save();
        
        $sol_id = $donacion['sol_id'];
        
        // SOLICITUD: estado = 5 pasa a donacion
        $modeloSolicitud = new Efector_models_Solicitudes();
        $solicitud = $modeloSolicitud->fetchRow('sol_id=' . $sol_id);
        
        $solicitud->setFromArray(array(
                        'sol_estado' => 5
                  ))->save();
       
        $this->_helper->json(array('st' => 'OK'));
    }

    
    
    public function verpdfAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $don_id = $this->getRequest()->getParam('don_id');

        if ($efector_id == '') {
            $efector_id = $this->efector_activo['id_efector'];
            $efector_nombre = $this->efector_activo['nomest'];
        } else {
            // Asigna valores cuando viene por parametro (nombre del efector)
            $modeloEfector = new Acceso_models_Efectores();
            $datos_efector = $modeloEfector->getNom($efector_id);
            $efector_nombre = $datos_efector[0]['nomest'];
        }

        
        //$pdf->properties['Title']= $titulo . " - " . $donacion['don_nro'];
        
        $modeloDonaciones = new Banco_models_Donaciones();
        $donacion = $modeloDonaciones->fetchRow('don_id = ' . $don_id);

        $modeloPersona = new Efector_models_Personas();
        $persona = $modeloPersona->fetchRow('per_id = '.$donacion['per_id'])->toArray();
        
        $persona['codigo_autogenerado'] = $modeloPersona->getCodigoAutogenerado($persona['per_id']);
        
        $persona['donacion_numero'] = $donacion['don_nro'];
        $persona['donacion_id'] = $don_id;
        
        $modeloSolicitudes = new Efector_models_Solicitudes();
        $solicitud = $modeloSolicitudes->fetchRow('sol_id = ' . $donacion['sol_id']);
        
        $persona['sol_id'] = $solicitud['sol_id'];
        
        $modeloCategoriaPreguntas = new Gestion_models_Cuestionariopreguntascategorias();

        // obtenemos la plantilla activa de la encuesta
        $modeloPlantilla = new Gestion_models_Plantillascuestionario();
        $plantilla = $modeloPlantilla->getPlantillaActiva();

        if ($plantilla != null) {
           // obtenemos las categorias que tienen las preguntas para la plantilla activa
           $modeloPreguntas = new Gestion_models_Plantillascuestionariodetalle();
           $categorias_activas = $modeloPreguntas->getCategoriasActivasPreguntas($plantilla['id']);
            //$fecha = str_replace('T', ' ', $DATA->header->fecha);
           // obtenemos el ID de la encuesta para asocicar el cuestionario de respuestas
           $cuestionario = new Efector_models_Cuestionariosrespuestas();
           $cuestionario_id = $cuestionario->getCabeceraCuestionarioByDonacion($persona['donacion_numero']);
           $cuestionario_id->toArray();

           // obtener las respuestas de la encuentas
           $modeloRespuestasPreguntas = new Efector_models_Cuestionariosrespuestasdetalle();
            
           $resultado = null;
           
           foreach ($categorias_activas as $categoria) {
                $resultado[] = $modeloCategoriaPreguntas
                                ->fetchRow('id = ' . $categoria['cuestionario_pregunta_categoria_id'])->toArray();
           }
           foreach ($resultado as &$categoria) {
                $categoria['preguntas'] = $modeloPreguntas
                        ->getPreguntasPorPlantillaPorCategoria($plantilla['id'], $categoria['id'])
                        ->toArray();
           }
        } else {
           echo "no existe una planilla definida para la encuesta / cuestionario";
           die();
        }

        // ========================================
        // GENERACION DEL PDF 
        // ========================================
        // DATOS DE LA PESONA (encabezado)
        $PDF_persona_don_nro = $persona['donacion_numero'];
        $PDF_persona_autogen = $persona['codigo_autogenerado'];
        $PDF_persona_nombre = $persona['per_apellido'] . ", " . $persona['per_nombres'];
        $PDF_persona_tipodoc = $this->view->cache_tipo_doc[$persona['per_tipodoc']];
        $PDF_persona_documento = "(" . $PDF_persona_tipodoc . ": " . $persona['per_nrodoc'] . ")";
        
        $fecha_impresion = date('Y-m-d hh:mm:ss');     
        
        define('FPDF_FONTPATH', 'tcpdf/fonts/');

        require 'tcpdf/tcpdf.php';
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetAuthor('SIMETRA');
        $pdf->SetTitle('ENCUESTA DEL DONANTE');
        $pdf->SetSubject('ENCUESTA DEL DONANTE');
        $pdf->SetKeywords('ENCUESTA DEL DONANTE');
        $pdf->SetSubject('datos de cabercera');
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Encuesta del Donante' , 'Fecha Impresión: ' . $fecha_impresion);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // set font
        $pdf->SetFont('helvetica', 'B', 10);
        
        // add a page
        $pdf->AddPage();
        
        $tblPersona = '<table border="0px" style="width: 740px;" cellspacing="0">
             <tr>
              <td style="width: 70px; text-align:left">Donante:</td>
              <td style="width: 270px; text-align:left">'. $PDF_persona_nombre .'</td> 
              <td style="width: 90px; text-align:left">Tipo y Nro:</td>
              <td style="width: 120px; text-align:left">'. $PDF_persona_documento .'</td>
             </tr>
             <tr><td colspan="4"></td></tr>
             <tr>
              <td style="width: 70px; text-align:left">Cód.Don.</td>
              <td style="width: 270px; text-align:left">'. $PDF_persona_don_nro .'</td> 
              <td style="width: 90px; text-align:left"></td>
              <td style="width: 100px; text-align:left"></td>
             </tr>             
            </table>';        
        
        
        // CATEGORIAS DE PREGUNTAS (agrupadores)
        $tblCategorias = "";
            
        foreach ($resultado as $categoria) {

            $PDF_categoria_id     = $categoria['id'];
            $PDF_categoria_nombre = $categoria['nombre'];    
            
            $tblCategorias .= '
                <table border="1px" style="width: 740px;" cellspacing="0">
                <tr>
                 <td style="width: 340px; text-align:left">'. $PDF_categoria_nombre .'</td>
                 <td style="width: 100px; text-align:left">Respuesta</td>
                 <td style="width: 200px; text-align:left">Observaciones</td>
                </tr>
                </table>';
            

           // obtenemos las respuestas con la pregunta asociada
           $respuestas_cuestionario = $modeloRespuestasPreguntas->getRespuestasPreguntasByCategoria($cuestionario_id['id'], $PDF_categoria_id);            
           
            // preguntas y respuestas
            $tblPreguntas = '<table style="width: 740px;" cellspacing="0">';     
            
            
            foreach ($respuestas_cuestionario as $pregunta) {
 
                    $pregunta_nombre = $pregunta['orden'] . " " . $pregunta['pregunta'];
                    $observaciones = $pregunta['observacion'];
                    if ($pregunta['respuesta_id'] == 0) { $valor = "NO"; } else { $valor = "SI"; }
                   
                    $tblPreguntas .= '<tr>
                          <td style="width: 340px; text-align:left">'. $pregunta_nombre .'</td>
                          <td style="width: 100px; text-align:left">'. $valor . '</td>
                          <td style="width: 200px; text-align:left">'. $observaciones .'</td>
                         </tr>';                    
             }           
            
             $tblPreguntas .= '</table>';
             
             $tblCategorias .= $tblPreguntas;
             
        }

        $pdf->writeHTML($tblPersona . $tblCategorias, true, false, false, false, '');  
                    
        $pdf->Output('PDF_ENCUESTA_DONANTE.pdf', 'I');
        
    }

    
 
}
