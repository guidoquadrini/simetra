<?php

class Efector_ExtraccionController extends BootPoint {

    /**
     * Ver solicitudes Aptas para Extraccion
     *
     */
    public function indexAction() {
        
        $fechaDesde = $this->getRequest()->getParam('from');
        $fechaHasta = $this->getRequest()->getParam('to');
        $don_nro = $this->getRequest()->getParam('nro');
        $persona_id = $this->getRequest()->getParam('persona_id');
        //$estado = (int) $this->getRequest()->getParam('estado');
        $perPage = 18;

        //Estado de donacion que se requieren para la vista.
        $estado = 2;
        //Tipo de extraccion que se requiere para la vista.
        $tipo_extraccion = 2;

        if ($fechaDesde == '') {
            $fechaDesde = (new DateTime())->setTime('0', '0', '0')->format('d-m-Y H:i:s');
        } else {
            $fechaDesde = (new DateTime($fechaDesde))->setTime('0', '0', '0')->format('d-m-Y H:i:s');
        }
        
        if ($fechaHasta == '') {
            $fechaHasta = (new DateTime())->setTime('23', '59', '59')->format('d-m-Y H:i:s');
        } else {
            $fechaHasta = (new DateTime($fechaHasta))->setTime('23', '59', '59')->format('d-m-Y H:i:s');
        }

        /* Obtengo los efectores del usuario */
        $modeloPry = new Acceso_models_Proyectos();
        $nom = "Banco de sangre";

        $efector_activo = $this->efector_activo['id_efector'];

        /* Fin efectores usuario */

        $modeloDonacion = new Banco_models_Donaciones();

        /* Listo solo las extracciones con tipo de donacion 2, donaciones manuales, no aferesis  */
        if ($estado == "-1")
            $extracciones = $modeloDonacion->getExtracciones($efector_activo, null, $this->page, $perPage, $fechaDesde, $fechaHasta, $don_nro, $persona_id, $tipo_extraccion);
        else
            $extracciones = $modeloDonacion->getExtracciones($efector_activo, $estado, $this->page, $perPage, $fechaDesde, $fechaHasta, $don_nro, $persona_id, $tipo_extraccion);

        if (!empty($extracciones)) {
            $total = $modeloDonacion->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        foreach ($extracciones as $e)
            $donaciones[] = $e->don_id;

        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();
        if (!empty($donaciones))
            $datos_extra = $modeloDetallesDonacion->getByDonIDs($donaciones);

        $this->view->busqueda = '';
        if (!empty($don_nro))
            $this->view->busqueda .= '<br/> Cód. de donación: ' . $don_nro . ' <br/>';

        if (!empty($fechaDesde))
            $this->view->busqueda .= 'Fecha Desde: ' . $fechaDesde . '<br/>';


        if (!empty($fechaHasta))
            $this->view->busqueda .= 'Fecha Hasta: ' . $fechaHasta . '<br/>';



        if (!empty($extracciones)) {
            $total = $modeloDonacion->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
            $paginas = ceil($total / $perPage);
        }

        $modeloBolsas = new Efector_models_Bolsas();
        $bolsas = $modeloBolsas->getActivas()->toArray();

        $this->view->cod_colecta = $this->efector_activo_cod . $this->efector_activo['id_efector'];
        $this->view->bolsas = $bolsas;
        $this->view->estado = $estado;
        $this->view->fechaDesde = $fechaDesde;
        $this->view->fechaHasta = $fechaHasta;
        $this->view->nro = $don_nro;
        $this->view->persona_id = $persona_id;
        $this->view->resultados = $extracciones;
        $this->view->paginas = $paginas;
    }

//    public function indexAction() {
//        
//        $fechaDesde = $this->getRequest()->getParam('from');
//        $fechaHasta = $this->getRequest()->getParam('to');
//        $nro = $this->getRequest()->getParam('nro');
//        //$estado = (int) $this->getRequest()->getParam('estado');
//        $perPage = 18;
//
//        $estado_donacion = 2;
//
//        if ($fechaDesde == '' || $fechaHasta == '') {
//            $fechaHasta = date('Y-m-d');
//            $fechaDesde = date('Y-m-d', strtotime("-1 days"));
//        }
//
//        /* Obtengo los efectores del usuario */
//        $modeloPry = new Acceso_models_Proyectos();
//        $nom = "Banco de sangre";
//        $pry = $modeloPry->getByNom($nom);
//
//        $pry_id = $pry->pry_id;
//        $modeloUsu = new Acceso_models_Usuarios();
//        $efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id, $pry_id);
//
//        foreach ($efes_usu as $e) {
//            $efes_ids[] = $e['id_efector'];
//        }
//        /* Fin efectores usuario */
//
//        $modeloDonacion = new Banco_models_Donaciones();
//
//        /* Listo solo las extracciones con tipo de donacion 2, donaciones manuales, no aferesis  */
//        if ($estado_donacion == "-1")
//            $extracciones = $modeloDonacion->getExtracciones($efes_ids, null, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, 2);
//        else
//            $extracciones = $modeloDonacion->getExtracciones($efes_ids, $estado_donacion, $this->page, $perPage, $fechaDesde, $fechaHasta, $nro, $doc, 2);
//
//        if (!empty($extracciones)) {
//            $total = $modeloDonacion->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
//            $paginas = ceil($total / $perPage);
//        }
//        
//        foreach ($extracciones as $e)
//            $donaciones[] = $e->don_id;
//
//        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();
//        if (!empty($donaciones))
//            $datos_extra = $modeloDetallesDonacion->getByDonIDs($donaciones);
//
//        $this->view->busqueda = '';
//        if (!empty($nro))
//            $this->view->busqueda .= '<br/> Cód. de donación: ' . $nro . ' <br/>';
//
//        if (!empty($fecha))
//            $this->view->busqueda .= 'Fecha: ' . $fecha . '<br/>';
//
//        $modeloBol = new Efector_models_Bolsas();
//        $bolsas = $modeloBol->getActivas()->toArray();
//
//        $this->view->bolsas = $bolsas;
//        $this->view->estado = $estado_donacion;
//        $this->view->fechaDesde = $fechaDesde;
//        $this->view->fechaHasta = $fechaHasta;
//        $this->view->nro = $nro;
//        $this->view->resultados = $extracciones;
//        $this->view->paginas = $paginas;
//        $this->view->anteriores = $finalizadas;
//        $this->view->extra = $datos_extra;
//    }

    /**
     * Formulario de Extraccion
     *
     */
    public function formularioExtraccionAction() {

        $don_id = $this->getRequest()->getParam('id');

//        $efector_id = $this->efector_activo['id_efector'];
//        $efector_nombre = $this->efector_activo['nomest'];
//        $this->view->efector = $efector_id;
//        $this->view->efector_nombre = $efector_nombre;

        $modeloDonacionacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacionacion->fetchRow('don_id=' . $don_id);

        $modeloPersonasona = new Efector_models_Personas();
        $persona = $modeloPersonasona->fetchRow("per_id = " . $donacion['per_id'])->toArray();

        $persona['codigo_autogenerado'] = $modeloPersonasona->getCodigoAutogenerado($persona['per_id']);
        $persona['donacion_numero'] = $modeloDonacionacion->getNroDonacionBySolicitud($donacion['sol_id']);


        $modeloBolsaTipo = new Gestion_models_Bolsastipos();
        $bolsa_tipo_list = $modeloBolsaTipo->getTiposBolsaPorTipoExtraccion(2);

        $modeloBolsaMarca = new Gestion_models_Bolsasmarcas();
        $bolsa_marca_list = $modeloBolsaMarca->getPairs();

        $modeloBolsa = new Gestion_models_Bolsas();
        $bolsa_list = $modeloBolsa->getPairs();

        $this->view->donacion = $donacion;
        $this->view->persona = $persona;
        $this->view->bolsa_tipo_list = $bolsa_tipo_list;
        $this->view->bolsa_marca_list = $bolsa_marca_list;
        $this->view->bolsa_list = $bolsa_list;
    }

    public function verAction() {
        $ext_id = (int) $this->getRequest()->getParam('ext_id');

        $modeloDonacion = new Banco_models_Donaciones();
        $extraccion = $modeloDonacion->getRegistro($ext_id)->toArray();

        $modeloPersona = new Efector_models_Personas();

        $persona = $modeloPersona->getByID($extraccion['per_id']);

        $usu_id = $extraccion['usu_inicio'];
        $model = new Acceso_models_Usuarios();
        $usuario = $model->find($usu_id)->current()->toArray();

        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();
        if (!empty($extraccion['don_id']))
            $datos_extra = $modeloDetallesDonacion->getByDonIDs($extraccion['don_id']);

        $modeloBol = new Efector_models_Bolsas();
        $bolsas = $modeloBol->getActivas()->toArray();
        $this->view->bolsas = $bolsas;
        $this->view->persona = $persona;
        $this->view->dni = $extraccion['per_id'];
        $this->view->extraccion = $extraccion;
        $this->view->usuario = $usuario['usu_usuario'];
        $this->view->extra = $datos_extra;

        if ($extraccion['don_estado'] == 4)
            return $this->renderScript('extraccion/form.phtml');
    }

    /**
     * Inicia el proceso de donacion
     * Genera n�mero donaci�n y se imprime un ticket/donaci�n. (estado intermedio, Cierra la solicitud, abre la donaci�n)
     *
     * 1 - Finaliza OK
     * 3 - Anulada
     * 4 - Extraccion
     *
     * PRO_ESTADO
     * 0 - Formulario de extraccion segura ANULA!
     * 1 - Pasar a Produccion
     * 2 - Rechazar Producion
     * 3 - Aceptar para Procesar
     * 4 - No aceptar para proceso
     */
    public function finalizarAction() {

        if (!$this->getRequest()->isPost())
            $this->_helper->json(array('st' => 'NOK'));

        $post = $this->getRequest()->getPost();
        $don_id = (int) $this->getRequest()->getParam('don_id');
        $cod = $this->getRequest()->getParam('cod');
        //Datos Extra
        $codCol = (int) $this->getRequest()->getParam('codcolecta');
        $tipo = $this->getRequest()->getParam('tipo');
        $peso = (int) $this->getRequest()->getParam('peso');
        $hb = $this->getRequest()->getParam('hb');
        $ta = $this->getRequest()->getParam('ta');
        $pulso = (int) $this->getRequest()->getParam('pulso');
        $nroTubuladura = $this->getRequest()->getParam('nroTubuladura');
        $t_bolsa = $this->getRequest()->getParam('tipo_bolsa');
        $marca_bolsa = $this->getRequest()->getParam('marca');
        $nroLote_bolsa = $this->getRequest()->getParam('nroLote_bolsa');
        $reaccion = $this->getRequest()->getParam('reaccion');
        $t_reaccion = $this->getRequest()->getParam('tiporeaccion');
        $hematocritos = $this->getRequest()->getParam('hematocritos');
        $temperatura_corporal = $this->getRequest()->getParam('temperatura_corporal');


        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();

        $modeloDetallesDonacion->find($cod)->current()->setFromArray(array(
            'cod_colecta' => $codCol,
            'tipo' => $tipo,
            'peso' => $peso,
            'hb' => $hb,
            'hematocritos' => $hematocritos,
            'temperatura_corporal' => $temperatura_corporal,
            'ta' => $ta,
            'pulso' => $pulso,
            'tipo_bolsa' => $t_bolsa,
            'marca_bolsa' => $marca_bolsa,
            'reaccion' => $t_reaccion,
            'nroLote_bolsa' => $nroLote_bolsa,
            'nroTubuladura' => $nroTubuladura
        ))->save();


        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->find($don_id)->current();
        if (!$donacion)
            $this->_helper->json(array('st' => 'nok'));

        $modeloSolicitud = new Efector_models_Solicitudes();
        $modeloSolicitud->find($donacion->sol_id)->current()->setFromArray(array(
            'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
            'usu_cierre' => $this->auth->getIdentity()->usu_id
        ))->save();

        $donacion->setFromArray(array(
            'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
            'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
            'don_estado' => 1,
            'observaciones' => $post['observaciones'],
            'don_tiempo_extraccion' => $post['tiempo'],
        ))->save();

        $this->_helper->redirector('ver', $this->_request->controller, $this->_request->module, array('ext_id' => $don_id));
    }

    public function extraccionMultipleAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $donaciones = $this->getRequest()->getParam('extracciones');
        $tiempo = (int) $this->getRequest()->getParam('tiempo');

        $modeloDonacion = new Banco_models_Donaciones();

        $modeloSolicitud = new Efector_models_Solicitudes();
        $observaciones = "No se observaron complicaciones";
        foreach ($donaciones as $don_id) {
            $donacion = $modeloDonacion->find($don_id)->current();
            if (!$donacion)
                $this->_helper->json(array('st' => 'NOK'));

            $modeloSolicitud->find($donacion->sol_id)->current()->setFromArray(array(
                'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
                'usu_cierre' => $this->auth->getIdentity()->usu_id
            ))->save();

            $donacion->setFromArray(array(
                'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
                'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
                'don_estado' => 1,
                'observaciones' => $observaciones,
                'don_tiempo_extraccion' => $tiempo
            ))->save();
        }
        $this->_helper->json(array('st' => 'OK'));
    }

    public function agregarMuniAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $cod_muni = $this->getRequest()->getParam('muni');
        $don_id = $this->getRequest()->getParam('cod');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->find($don_id)->current();

        $values = array(
            'cod_muni' => $cod_muni,
        );

        $donacion->setFromArray($values)->save();

        $this->_helper->json(array('st' => 'OK'));
    }

    /**
     * Descartes
     *
     */
    public function descartarAction() {
        if (!$this->getRequest()->isPost())
            $this->_helper->json(array('st' => 'NOK'));

        $post = $this->getRequest()->getPost();
        $don_id = (int) $this->getRequest()->getParam('don_id');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->find($don_id)->current();
        if (!$donacion)
            $this->_helper->json(array('st' => 'NOK'));

        $modeloSolicitud = new Efector_models_Solicitudes();
        $modeloSolicitud->find($donacion->sol_id)->current()->setFromArray(array(
            'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
            'usu_cierre' => $this->auth->getIdentity()->usu_id,
            'sol_estado' => 0
        ))->save();

        $donacion->setFromArray(array(
            'don_fh_extraccion' => new Zend_Db_Expr('NOW()'),
            'don_usu_extraccion' => $this->auth->getIdentity()->usu_id,
            'don_estado' => 3,
            'mot_id' => $post['motivo'],
            'observaciones' => $post['observaciones']
        ))->save();

        $this->_helper->redirector('ver', $this->_request->controller, $this->_request->module, array('ext_id' => $don_id));
    }

    public function formFesAction() {
        $don_id = (int) $this->getRequest()->getParam('don_id');
        $st = (int) $this->getRequest()->getParam('st');

        $sol_estado = ($st == 0) ? 5 : 0;
        $pro_estado = ($st == 0) ? 0 : 1;

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->find($don_id)->current();

        //$this->auth->getIdentity()->usu_id,

        $modeloSolicitud = new Efector_models_Solicitudes();
        $modeloSolicitud->find($donacion->sol_id)->current()->setFromArray(array(
            'sol_fh_fin' => new Zend_Db_Expr('NOW()'),
            'usu_cierre' => $this->auth->getIdentity()->usu_id,
            'sol_estado' => $sol_estado
        ))->save();

        $donacion->setFromArray(array(
            'pro_estado' => $pro_estado
        ))->save();

        $this->_helper->redirector('ver', $this->_request->controller, $this->_request->module, array('ext_id' => $don_id));
    }

//
//    public function cambiarEstadoAction() {
//
//        if (!$this->getRequest()->isPost()) {
//            $this->_helper->json(array('st' => 'NOK', 'mensaje' => 'Acceso no válido al método'));
//        }        
//        
//        $this->_helper->layout()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
//
//        $cod = (int) $this->getRequest()->getParam('cod');
//        $codCol = (int) $this->getRequest()->getParam('codcolecta');
//        //$tipo = $this->getRequest()->getParam('tipo');
//        //$peso = (int) $this->getRequest()->getParam('peso');
//        //$hb = $this->getRequest()->getParam('hb');
//        //$ta = $this->getRequest()->getParam('ta');
//        //$pulso = (int) $this->getRequest()->getParam('pulso');
//        //$reaccion = $this->getRequest()->getParam('reaccion');
//        //$temperatura_corporal = $this->getRequest()->getParam('temperatura_corporal');
//        //$hematocritos = $this->getRequest()->getParam('hematocritos');
//        $tipo_bolsa = $this->getRequest()->getParam('bolsatipo-lis');
//        $marca_bolsa = $this->getRequest()->getParam('bolsamarca-list');
//        $nroTubuladura = $this->getRequest()->getParam('nroTubuladura');
//        $nroLote_bolsa = $this->getRequest()->getParam('nroLoteBolsa');
//        $t_reaccion = $this->getRequest()->getParam('tiporeaccion');
//
//        $hora_inicio = $this->getRequest()->getParam('fecha_hora_inicio');
//        $hora_finaliza = $this->getRequest()->getParam('fecha_hora_finalizacion');
//
//        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();
//        $donacionDetalle = $modeloDetallesDonacion->fetchRow('don_id=' . $donacion_id);
//        
//        // Actualiza el estado de la donacion extendida (detalle)
//        $donacionDetalle->setFromArray(array(
//                'tipo_bolsa' => $tipo_bolsa,
//                'marca_bolsa' => $marca_bolsa,
//                'reaccion' => $t_reaccion,
//                'nroLote_bolsa' => $nroLote_bolsa,
//                'brazo_intervenido' => $brazo_intervenido,
//                'nroTubuladura' => $nroTubuladura     
//                ))->save();           
//        
//        
//        // Actualizac Solicitud (solo para diferimientos)
//
//        $modeloSolicitud = new Efector_models_Solicitudes();
//        $solicitud = $modeloSolicitud->fetchRow('sol_id=' . $sol_id);        
//        if( $estado == 2 ) {
////            $solicitud->setFromArray(array(
////                            'sol_estado' => 0 // TODO: Cerrada sin problemas ???
////                      ))->save();        
//         }  else  {
//             // 2: Diferida + Motivo
//             // 3: Rechazada + Motivo           
////            $solicitud->setFromArray(array(
////                            'sol_estado' => 5
////                      ))->save();           
//        } 
//        
//        
////        $modeloDonacion->find($donacion_id)->current()->setFromArray(array(
////            'don_estado' => $estado_donacion,
////            'observaciones' => $observaciones
////        ))->save();       
//        
////        // TODO: faltan los campos Hora Inicio / Final en la tabla don_donaciones_ext
////        $modeloDetallesDonacion->find($donacion_id)->current()->setFromArray(array(
////            'tipo' => $tipo,
////            'tipo_bolsa' => $tipo_bolsa,
////            'marca_bolsa' => $marca_bolsa,
////            'reaccion' => $t_reaccion,
////            'nroLote_bolsa' => $nroLote_bolsa,
////            'brazo_intervenido' => $brazo_intervenido,
////            'nroTubuladura' => $nroTubuladura,
////            'reaccion' => $nombre_reaccion
////        ))->save();
//// 
//        
//        $this->_helper->json(array('st' => 'OK'));
//    }

    /**
     * Autoexclusion mas contancia
     *
     */
    public function fesconstanciaAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $id = (int) $this->getRequest()->getParam('id');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->find($id)->current();

        $modeloPersona = new Efector_models_Personas();

        $persona = $modeloPersona->getByID($donacion->per_id);

        $pdf = new Zend_Pdf();

        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8'; //'ISO-8859-1'

        $titulo = 'Constancia de Donación de Sangre';
        $efector = $this->efector_activo['nomest'];

        $y = $page->getHeight();
        $page
                ->setFont($font, 10)
                ->drawText($this->efector_activo['nomest'], 25, $y - 20)
                ->setFont($font, 14)
                ->drawText('Formulario de Autoexclusión', 25, $y - 50, $encoding)
                ->setFont($font, 10)
                ->drawText('Estimado Donante:', 28, $y - 76)
                ->drawText('Si Ud. ya donó sangre y se sintió obligado a hacerlo, si no contestó el cuestionario', 120, $y - 90, $encoding)
                ->drawText('sinceramente por temor o por verguenza, si piensa que su sangre puede no ser segura, está a tiempo ', 28, $y - 104)
                ->drawText('para evitar mayor riesgo al paciente que lo reciba.', 28, $y - 118)
                ->drawText('Marque con una X una de las siguientes opciones. Su respuesta es confidencial.', 120, $y - 132)
                ->drawText('USAR MI SANGRE', 50, $y - 180)
                ->drawRectangle(30, $y - 172, 40, $y - 182, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                ->drawText('NO USAR MI SANGRE', 50, $y - 200)
                ->drawRectangle(30, $y - 192, 40, $y - 202, Zend_Pdf_Page::SHAPE_DRAW_STROKE)
                //->drawText('FIRMA: ___________________________',300,$y-220)
                //->drawText("DONANTE: {$persona['per_apellido']}, {$persona['per_nombres']}",300,$y-240)
                ->drawText('FECHA: ' . date('d / m / Y'), 300, $y - 240)

                //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 8, $y-350, 50, $y-300)
                ->setFont($font, 14)
                ->drawText($titulo, $this->getCenterText($page->getWidth(), $titulo, $font, 14), $y - 400, $encoding)
                ->setFont($font, 10)
                ->drawText($efector, $this->getCenterText($page->getWidth(), $efector, $font, 10), $y - 420)
                ->drawText('Se deja constancia que', 18, $y - 470)
                ->drawText("El Sr/Sra {$persona['per_apellido']}, {$persona['per_nombres']} con DNI {$persona['per_nrodoc']}", 28, $y - 490, $encoding)
                ->drawText("se ha presentado en esta institución {$efector}", 28, $y - 510, $encoding)
                ->drawText('FIRMA Y SELLO RESPONSABLE', 350, $y - 570)
                ->drawText("DONACION Nro: {$donacion->don_nro}", 18, $y - 600)
                ->drawText('FECHA: ' . date('d / m / Y'), 18, $y - 620)

        ;

        $pdf->pages[] = $page;

        $barcode = Zend_Barcode::factory(
                        'code39', 'pdf', array(
                    'font' => APPLICATION_PATH . '/arial.ttf',
                    'text' => $donacion->don_nro,
                    'fontSize' => 10,
                    'barThickWidth' => 4,
                    'barThinWidth' => 2,
                    //'withQuietZones' => false,
                    'stretchText' => true
                        ), array(
                    'topOffset' => 5,
                    //HorizontalPosition' => 'right'
                    'leftOffset' => 330
                        )
                )->setResource($pdf)->draw();

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    /**
     * Codigo de Barras
     *
     */
    public function ticketsAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $pdf = new Zend_Pdf();

        /* Tama�o de la p�gina del ticket */
        $x = 290; //ancho
        $y = 70;  //alto
        $page = new Zend_Pdf_Page($x, $y);
        $font = Zend_Pdf_Font::fontWithPath(APPLICATION_PATH . '/arial.ttf');
        $page->setFont($font, 8); //antes = 5
        $pdf->pages[] = $page;

        $id = (int) $this->getRequest()->getParam('id');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->find($id)->current();

        $top = 25; //20
        $leftA = 6;
        $leftB = 155;

        //imprimir en la cabecera del c�digo
        $efector = $this->efector_activo['nomest'];
        //Truncado de $efector
        $efecTrunc = sprintf("%10.25s", $efector);

        /*         * ******************************* 
         *  Reemplazar code39 por code128  *
         *  fontSize se puede aumentar     *
         *  barThickWidth => 4             *
         *  barThinWidth  => 2             *
         *  ajustar un poco leftOffset     *
         * ******************************* */

        //C�digo de barra A
        $page->drawText($efecTrunc, $leftA + 5, 50);
        $barcode = Zend_Barcode::factory(
                        'code39', 'pdf', array(
                    'font' => APPLICATION_PATH . '/arial.ttf',
                    'text' => $donacion->don_nro,
                    'fontSize' => 12,
                    'barThickWidth' => 3,
                    'barThinWidth' => 1,
                    'stretchText' => true
                        ), array(
                    'topOffset' => $top,
                    'leftOffset' => $leftA
                        )
                )->setResource($pdf)->draw();

        //C�digo de barra B - Igual a A excepto en el leftOffset
        $page->drawText($efecTrunc, $leftB + 5, 50);
        $barcode = Zend_Barcode::factory(
                        'code39', 'pdf', array(
                    'font' => APPLICATION_PATH . '/arial.ttf',
                    'text' => $donacion->don_nro,
                    'fontSize' => 12,
                    'barThickWidth' => 3,
                    'barThinWidth' => 1,
                    'stretchText' => true
                        ), array(
                    'topOffset' => $top,
                    'leftOffset' => $leftB
                        )
                )->setResource($pdf)->draw();



        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    public function constanciaAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $donacion_id = (int) $this->getRequest()->getParam('id');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->fetchRow('don_id=' . $donacion_id);
        $sol_id = $donacion['sol_id'];
        $persona_id = $donacion['per_id'];

        $modeloPersona = new Efector_models_Personas();
        $persona = $modeloPersona->fetchRow('per_id=' . $persona_id);

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $encoding = 'UTF-8'; //ISO-8859-1
        $y = $page->getHeight();

        $titulo = 'Constancia de Donación de Sangre';
        $subtitulo = 'Programa Provincial de Hemoteriapia';

        $efector = $this->efector_activo['nomest'];

        $page
                ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 35, $y - 70, 127, $y - 8)
                ->setFont($font, 14)
                ->drawText($titulo, $this->getCenterText($page->getWidth(), $titulo, $font, 14), $y - 30, $encoding)
                ->setFont($font, 10)
                ->drawText($efector, $this->getCenterText($page->getWidth(), $efector, $font, 10), $y - 50)
                ->drawText("Se deja constancia que el/la Sr/Sra {$persona['per_apellido']}, {$persona['per_nombres']} con DNI {$persona['per_nrodoc']} se ha presentado", 50, $y - 90)
                //->drawText('Se deja constancia que', 18, $y-76)
                //->drawText("El Sr/Sra {$persona['per_apellido']}, {$persona['per_nombres']} con DNI {$persona['per_nrodoc']}", 28, $y-96,$encoding)
                //->drawText("se ha presentado en esta instituci�n {$efector}", 28, $y-110,$encoding)
                ->drawText("en {$efector} en el día de la fecha.", 50, $y - 105, $encoding)
                ->drawText("Se extiende la presente acorde a las normativas vigentes (Ley Nacional de Sangre N° 22.990, las Normas", 50, $y - 130, $encoding)
                ->drawText("Técnicas y Administrativas y sus modificaciones) a su pedido y para ser presentado ante quien corresponda.", 50, $y - 145, $encoding)
                ->drawText(utf8_decode('CÓDIGO DE DONACIÓN: ') . $donacion->don_nro, 50, $y - 210)
                ->drawText('FECHA: ' . date('d / m / Y'), 50, $y - 195)
                ->drawText('FIRMA Y SELLO RESPONSABLE', 350, $y - 250);
        ;

        $pdf->pages[] = $page;
        $pdf->properties['Title'] = $titulo . " - " . $donacion['don_nro'];

        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    public function formularioAutoexclusionAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $donacion_id = (int) $this->getRequest()->getParam('id');

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->fetchRow('don_id=' . $donacion_id);
        $sol_id = $donacion['sol_id'];
        $persona_id = $donacion['per_id'];

        $modeloPersona = new Efector_models_Personas();
        $persona = $modeloPersona->fetchRow('per_id=' . $persona_id);

        $pdf = new Zend_Pdf();
        $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

        $titulo = "Formulario de Autoexclusión ";

        $encoding = 'UTF-8';

        $y = $page->getHeight();
        $page
                ->setFont($font, 10)
                ->drawText($this->efector_activo['nomest'], 25, $y - 20, $enccoding)
                ->setFont($font, 14)
                ->drawText('Formulario de Autoexclusión', 25, $y - 50, $encoding)
                ->setFont($font, 10)
                ->drawText('Estimado Donante:', 28, $y - 76)
                ->drawText('Ud. ya donó sangre. Si se sintió obligado a hacerlo por alguna circunstancia, si no pudo contestar', 120, $y - 90, $encoding)
                ->drawText(' sinceramente las preguntas, si piensa que su sangre puede no ser segura, todavía está a tiempo de evitar un riesgo', 28, $y - 105, $encoding)
                ->drawText(' al paciente que la reciba. Para ellomarque con una X.', 28, $y - 120)
                ->drawText('Respóndemos con la verdad; su RESPUESTA ES ABSOLUTAMENTE CONFIDENCIAL.', 120, $y - 135, $encoding)
                ->drawText('Independientemente de su respuesta, a su sangre se le realizarán todos los estudio correspondiente.', 50, $y - 150)
                ->drawText('No utilicen mi sangre para ser transfundida, TENGO DUDAS.', 50, $y - 200)
                ->drawRectangle(330, $y - 182, 360, $y - 210, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
                

        $pdf->pages[] = $page;

        $barcode = Zend_Barcode::factory(
                        'code128', 'pdf', array(
                    'font' => 'fonts/arial.ttf',
                    'text' => $donacion->don_nro,
                    'fontSize' => 10,
                    'barThickWidth' => 4,
                    'barThinWidth' => 2,
                    'stretchText' => true
                        ), array(
                    'topOffset' => 5,
                    'leftOffset' => 330
                        )
                )->setResource($pdf)->draw();

        $pdf->properties['Title'] = $titulo . " - " . $donacion['don_nro'];
        header('Content-Type: application/pdf');
        echo $pdf->render();
    }

    private function getCenterText($width, $text, $font, $fontSize) {
        return ($width / 2) - ($this->getTextWidth($text, $font, $fontSize) / 2);
    }

    private function getTextWidth($text, $font, $fontSize) {
        $drawingText = iconv('UTF-8', 'UTF-8', $text); //ISO-8859-1

        $characters = array();
        for ($i = 0; $i < strlen($drawingText); $i++) {
            $characters[] = ord($drawingText[$i]);
        }
        $widths = $font->widthsForGlyphs($font->glyphNumbersForCharacters($characters));
        return (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
    }

    //NOTA DE VERSION: La tubuladura a partir de la version 3.7 solo se evalua en extraccion.
    //Esto se incorpora para la presentacion. debe revisarse ya que 
    //no deberia ser nulo el nro de tubuladura.
//        if ($this->getRequest()->getParam('nroTubuladura') != '') {
//            $nroTubuladura = $this->getRequest()->getParam('nroTubuladura');
//        } else {
//            //$nroTubuladura = 'Sin Tubuladura'; //Se implementa esto para poder seguir.
//            $this->_helper->json(array('st' => 'nok', 'mensaje' => 'No se ha registrado el Nro. de Tubuladura.'));
//        }

    public function codTubuladuraFueUtilizadoAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $nroTubuladura = $this->getRequest()->getParam('nroTubuladura');
        $model = new Efector_models_DetallesDonaciones();
        $count = $model->codTubuladuraFueUtilizado($nroTubuladura);
        if ($count > 0) {
            $this->_helper->json(array('st' => 'OK'));
        } else {
            $this->_helper->json(array('st' => 'NOK'));
        }
    }

    /** Cambiar estado de la Donacion
     * 2 
     * 3 (Descarte)
     * 6 
     * Cambiar estado de la solicitud
     * 2 - Diferida Temporalmente + Motivo
     * 3 - Rechazada + Motivo + texto?
     * 4 - Extraccion
     *
     */
    public function cambiarEstadoAction() {

        
        /**
         * Obtener todos los parametros del Request.
         */
        $fecha_hora_inicio = date($this->getRequest()->getParam('fecha_hora_inicio'));
        $fecha_hora_finalizacion = date($this->getRequest()->getParam('fecha_hora_finalizacion'));
        $bolsa_tipo = $this->getRequest()->getParam('bolsa_tipo');
        $marca_bolsa = $this->getRequest()->getParam('marca_bolsa');
        $nroTubuladura = $this->getRequest()->getParam('nroTubuladura');
        $nroLoteBolsa = $this->getRequest()->getParam('nroLoteBolsa');
        $solucion_conservadora = $this->getRequest()->getParam('solucion_conservadora');
        $brazo_intervenido = $this->getRequest()->getParam('brazo_intervenido');
        $reaccion = $this->getRequest()->getParam('tiporeaccion');
        $observaciones_extraccion = $this->getRequest()->getParam('observaciones_extraccion');
        $observaciones_descarte = $this->getRequest()->getParam('observaciones_descarte');
        $donacion_id = (int) $this->getRequest()->getParam('don_id');
        $estado_proceso = (int) $this->getRequest()->getParam('st');
        $tiempo_extrccion = $this->getRequest()->getParam('tiempo_extraccion');
        $fecha_vencimiento_bolsa = $this->getRequest()->getParam('fecha_vencimiento_bolsa');

        $usuario = $this->auth->getIdentity()->usu_id;
        
        
        
        $fecha_hora_inicio_formateada = ( date(substr($fecha_hora_inicio,0,10)) . " " . substr($fecha_hora_inicio,13,22));
        $fecha_hora_finalizacion_formateada = ( date(substr($fecha_hora_finalizacion,0,10)) . " " . substr($fecha_hora_finalizacion,13,22));
        

        $modeloDonacion = new Banco_models_Donaciones();
        $donacion = $modeloDonacion->fetchRow('don_id=' . $donacion_id);
        $sol_id = $donacion->sol_id;

        // Donacion Extendida
        $modeloDetallesDonacion = new Efector_models_DetallesDonaciones();
        $donacionDetalle = $modeloDetallesDonacion->fetchRow('don_id=' . $donacion_id);

        $modeloSolicitudes = new Efector_models_Solicitudes();

        $solicitud = $modeloSolicitudes->fetchRow('sol_id = ' . $sol_id);

        if (!$solicitud) {
            $this->_helper->json(array('st' => 'NOK', 'mensaje' => 'No existe solicitud alguna vinculada a la donacion. Consulte con Asistencia tecnica para evaluar este Incidente.'));
        }

        //Variable de Retorno y registros para actualizacion.
        $vRet = array();
        $rowSolicitud = array();

        /** ACTUALIZACION DEL REGISTRO EN TABLA CENTRALIZADO * */
        $modeloCentralizado = new Gestion_models_Centralizado();
        $centralizado = $modeloCentralizado->getByNro($donacion->don_nro);

        /**
         * Estados posibles en Tabla Centralizados | don_estado
         * 
         * 1:Aceptada para Examen Medico
         * 2:Aceptada para Extraccion
         * 3:Diferir Donante
         * 
         * */
        $estado_donacion = ($estado_proceso == 5) ? 105 : 4; //Si Sol Estado pasa a 5, don_estado pasa a 105 sino rechazada con 4.
        //Actualizar estado del codigo centralizado.
        $centralizado->setFromArray(array(
            'don_estado' => $estado_donacion,
            'don_fh_extraccion' => $fecha_hora_inicio_formateada
        ))->save();   // actualiza el registro en el modelo centralizado        

        $rowDonacion = array(
            'don_tiempo_extraccion' => $tiempo_extrccion,
            'don_fh_extraccion' => $fecha_hora_inicio_formateada,
            'don_usu_extraccion' => $usuario,
            'observaciones' => $donacion->observaciones . (($donacion->observaciones == '') ? '' : " || ") . 'Obs. Extraccion: ' . $observaciones_extraccion
        );

        $rowDonacionDetalle = array(
            'tipo_bolsa' => $bolsa_tipo,
            'marca_bolsa' => $marca_bolsa,
            'nroLote_bolsa' => $nroLoteBolsa,
            'nroTubuladura' => $nroTubuladura,
            'reaccion' => $reaccion,
            'brazo_intervenido' => $brazo_intervenido,
            'fecha_vencimiento_bolsa' => $fecha_vencimiento_bolsa,
            'bolsas_id' => $solucion_conservadora
        );

        switch ($estado_proceso) {
            case 3: //Rechazar Donacion.
                $fecha_hora_fin = new Zend_Db_Expr('NOW()');
                $rowDonacion['don_estado'] = ($estado_proceso == 2) ? 3 : 4; //3: Diferir Donante - 4: Rechazar Donante
                //Aca se actualiza el estado de la solicitud con $st.
                $rowSolicitud = array(
                    'sol_fh_fin' => $fecha_hora_fin,
                    'usu_cierre' => $usuario,
                    'sol_diferido' => null, // Campo deprecado. $dias,
                    'sol_estado' => $estado_proceso,
                    'observacion' => $donacion->observaciones . (($donacion->observaciones == '') ? '' : ' || ') . 'Donación Descartada. Más información en las obsercaciones de la donación.'
                );

                $rowDonacion = array(
                    'observaciones' => $rowDonacion['observaciones'] . (($rowDonacion['observaciones'] == '') ? '' : " || ") . 'Donacion Descartada: ' . $observaciones_descarte
                );

                /* Grabar en sol_diferimientos los N motivos */
                $modeloSolicitudesiitudDiferimientos = new Efector_models_SolicitudesDiferimientos();
                foreach ($motivos_diferimiento as $motivo_id) {
                    $modeloSolicitudesiitudDiferimientos->createRow(array(
                        'sol_id' => $solicitud->sol_id,
                        'mot_id' => $motivo_id
                    ))->save();
                }

                $vRet = array('st' => 'OK', 'mensaje' => 'El diferimiento Temporal fue exitoso.');

                break;
            case 5: //Pasa a Extraccion

                $rowDonacion['don_estado'] = 105;

                $rowSolicitud = array(
                    'sol_estado' => $estado_proceso
                );

                $vRet = array('st' => 'OK', 'mensaje' => 'Se ha grabado el Exámen Médico !!!');

                break;
        }
        // Actualiza el estado de la DONACION
        $donacion->setFromArray($rowDonacion)->save();

        // Actualiza el estado de la DONACION EXTENDIDA (detalle)
        $donacionDetalle->setFromArray($rowDonacionDetalle)->save();

        // Actualiza el estado de la solicitud.
        $solicitud->setFromArray($rowSolicitud)->save();

        $this->_helper->json($vRet);
    }

}
