<?php

class Efector_EnviosController extends BootPoint {

	public function indexAction(){
        
		$fecha = $this->getRequest()->getParam('fecha');
		$nro = $this->getRequest()->getParam('nro');

		$perPage = 18;

		/* Obtengo los efectores del usuario */
		$modeloPry = new Acceso_models_Proyectos();
		$nom = "Banco de sangre";
		$pry = $modeloPry->getByNom($nom);
		
		$pry_id = $pry->pry_id;
		$modeloUsu = new Acceso_models_Usuarios();
		$efes_usu = $modeloUsu->getEfectores($this->auth->getIdentity()->usu_id,$pry_id);

		foreach ($efes_usu as $e)
			$efes_ids[] = $e['id_efector'];
		/* Fin efectores usuario */

		$modelo = new Efector_models_Envios();

		$resultados = $modelo->getByFechaAndEfe($efes_ids, $fecha, $this->page, $perPage, $nro)->toArray();

		$this->view->busqueda = 'C&oacute;digo: '.$nro . '<br/>' . 'Fecha: '. $fecha . '<br/>';

		if(sizeof($resultados) > 0){
			$total = $modelo->getDefaultAdapter()->fetchOne('SELECT FOUND_ROWS()');
			$paginas = ceil($total/$perPage);
		}

		$this->view->fecha = $feha;
		$this->view->nro = $nro;
		$this->view->resultados = $resultados;
		$this->view->paginas = $paginas;
	}

	/**
	* Preparar Envio
	*
	*/

	#####################

	public function altaAction(){
		$modelo = new Banco_models_Donaciones();
		$donaciones = $modelo->getSinDespachoByEfe($this->efector_activo['id_efector']);
		
		$this->view->donaciones = $donaciones;
	}

	/**
	*
	*/
	public function registrarAction(){
		$donaciones = $this->getRequest()->getParam('donacion');

		$modeloEnv = new Efector_models_Envios();
		
                $despacho_id = $modeloEnv->createRow(array(
			'efe_id' => $this->efector_activo['id_efector'],
			'efe_nombre_origen' => $this->efector_activo['nomest'],
			'env_nro' => $this->efector_activo['id_efector'].date("YmdHis"),
			'env_fh' => new Zend_Db_Expr('NOW()'),
			'env_usuario' => $this->auth->getIdentity()->usu_id,
			'env_estado' => 1,
			'observaciones'=> $this->getRequest()->getParam('observaciones')
		))->save();

		$modeloDon = new Banco_models_Donaciones();
                $modeloSol = new Efector_models_Solicitudes();
                
		foreach($donaciones as $i => $v){
            $donacion = $modeloDon->find($v)->current();
            $sol_id = $donacion->sol_id;
            $modeloSol->find($sol_id)->current()->setFromArray(array('sol_estado' => 5))->save();
			$donacion->setFromArray(array('despacho_id' => $despacho_id, 
                                                      'don_estado' => 106, 
                                                      'pro_estado' => 0))->save();
		}
		$this->_helper->json(array('st' => 'ok' ,'url' => $this->view->url(array(
			'controller' => $this->_request->controller,
			'module' => $this->_request->module,
			'action' => 'ver',
			'env_id' => $despacho_id
		))));
	}

	public function verAction(){
		$env_id = (int)$this->getRequest()->getParam('env_id');
		$print = (int)$this->getRequest()->getParam('print');

		$modeloEnv = new Efector_models_Envios();
		$envio = $modeloEnv->getEnvio($env_id);

		$modeloDon = new Banco_models_Donaciones();
		$donaciones = $modeloDon->getDespacho($env_id);           
                
                $model = new Acceso_models_Usuarios();
                
                foreach ($donaciones as $i => $v) {
                    $usu_id = $v['don_usu_extraccion'];                
                    $usuario = $model->find($usu_id)->current();
                    if ($usuario != null){
                        $usuario = $usuario->toArray();
                    }
                    $donaciones[$i]['don_usu_extraccion'] = $usuario['usu_usuario'];
                }
                    
		$this->view->envio = $envio;
		$this->view->donaciones = $donaciones;
		$this->view->env_id = $env_id;

		if($print == 1){
			$this->view->layout()->disableLayout();
			$this->renderScript('envios/imprimir.phtml');
		}
	}

        /*****************************************************************
         * Visualiza donaciones del envío en ventana Modal
         *****************************************************************/     
	public function enviodetallemodalAction(){
                $this->_helper->layout()->disableLayout();
                $this->_helper->viewRenderer->setNoRender(true);
                
		$env_id = (int)$this->getRequest()->getParam('env_id');
		//$print = (int)$this->getRequest()->getParam('print');
                
		$modeloEnv = new Efector_models_Envios();
		$envio = $modeloEnv->getEnvio($env_id);

		$modeloDon = new Banco_models_Donaciones();
		$donaciones = $modeloDon->getDespacho($env_id);

                $model = new Acceso_models_Usuarios();
                
                foreach ($donaciones as $i => $v) {
                    $usu_id = $v['don_usu_extraccion'];
                    $usuario = $model->find($usu_id)->current();
                    if ($usuario != null){
                        $usuario = $usuario->toArray();
                    }
                    $donaciones[$i]['don_usu_extraccion'] = $usuario['usu_usuario'];
                }
		$this->view->envio = $envio;
		$this->view->donaciones = $donaciones;
		$this->view->env_id = $env_id;
            
                // Construir la tabla con el detalle a devolver      
                $tabla = '<table class="table table-striped table-bordered table-hover table-condense">'
                       . '<thead>'
                       . '<tr class="btn-info btn-small btn-inverse">'
		       . '<th width="130"><i class="icon-barcode icon-white"></i> Cód. Donación <i class="icon-chevron-up icon-white pull-right"></i></th>'
                       . '<th><i class="icon-calendar icon-white"></i> Fecha Muestra</th>'
                       . '<th><i class="icon-time icon-white"></i> Tiempo Extracción</th>'
		       . '<th>Operario</th>'
                       . '</tr>'
                       . '</thead>'
                       . '<tbody>´';
  
			foreach($donaciones as $i => $v) {
                            //$tiempo_calculado = $this->nicetime($v['don_fh_inicio']);
                            $tiempo_extraccion = ($v['don_tiempo_extraccion'] == 10) ? 'Menor o Igual a 10' : 'Mayor a 10';
                                 
			    $tabla .= '<tr>'
                                    . '<td width="150">' . $v['don_nro'] . '</td>'
                                    . '<td>' . $v['fh_muestra'] .'hs <span class="time"><i class="icon-time"></i> ' . $v['don_fh_inicio'] . '</span></td>'
                                    . '<td>' . $tiempo_extraccion . ' minutos</b></td>'          
                                    . '<td>' . $v['don_usu_extraccion'] . '</td>'
				    . '</tr>';
			 }
                         
		$tabla .= '</tbody>'
	 		. '</table>';        
        
                // devolucion al ajax del llamado        
                echo $tabla;
	}



         /*****************************************************************
         * Cantidad de donaciones del Envio
         *****************************************************************/     
	public function cantidaddonacionesenvioAction(){
                //$this->_helper->layout()->disableLayout();
                $this->_helper->viewRenderer->setNoRender(true);
                
		$env_id = (int)$this->getRequest()->getParam('env_id');
                
		$modeloEnv = new Efector_models_Envios();
		$envio = $modeloEnv->getEnvio($env_id);

		$modeloDon = new Banco_models_Donaciones();
		$donaciones = $modeloDon->getDespacho($env_id);

                $model = new Acceso_models_Usuarios();
                
                foreach ($donaciones as $i => $v) {
                    $usu_id = $v['don_usu_extraccion'];
                    $usuario = $model->find($usu_id)->current();
                    if ($usuario != null){
                        $usuario = $usuario->toArray();
                    }
                    $donaciones[$i]['don_usu_extraccion'] = $usuario['usu_usuario'];
                }
		$this->view->envio = $envio;
		$this->view->donaciones = $donaciones;
		$this->view->env_id = $env_id;
            
                // Construir la tabla con el detalle a devolver      
                $cantidad_donaciones = 0;
                
                foreach($donaciones as $i => $v) {
                    $cantidad_donaciones = $cantidad_donaciones + 1;
                 }

                // devolucion al ajax del llamado        
                echo $cantidad_donaciones;
	}       
        
        
    
        
        /* **********************************************************
         *  *****     Ver formulario de envío en PDF     ************
         * *********************************************************/
	public function verpdfAction(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$env_id = (int)$this->getRequest()->getParam('env_id');

                
                $efector_id = $this->efector_activo['id_efector'];                   
                $efector_nombre = $this->efector_activo['nomest'];                    
                
                
		$pdf = new Zend_Pdf();
		$page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

// Del controlador anterior
		$modeloEnv = new Efector_models_Envios();
		$envio = $modeloEnv->getEnvio($env_id);
		$modeloDon = new Banco_models_Donaciones();
		$donaciones = $modeloDon->getDespacho($env_id);
        
        $modelo_det_don = new Efector_models_DetallesDonaciones();
        
        
	$modeloPer = new Efector_models_Personas();
		
        $model = new Acceso_models_Usuarios();

        foreach ($donaciones as $i => $v) {
            $usu_id = $v['don_usu_extraccion'];
            $usuario = $model->find($usu_id)->current();
                    if ($usuario != null){
                        $usuario = $usuario->toArray();
                    }
            $donaciones[$i]['don_usu_extraccion'] = $usuario['usu_usuario'];

        }

		$estados_donaciones = array(
					1 => 'Finaliza',
					3 => 'Anulada',
					4 => 'Extracción',
					5 => 'pasar a prod',
					6 => 'rechazaform',
					6 => 'rechazar prod',
					7 => 'aceptar proceso',
					8 => 'Descarte definitivo',
					9 => 'Enviada',
					10 => 'Aceptar recepción',
					11 => 'Descarte parcial'	
		);
		
		$encoding = 'UTF-8'; //'ISO-8859-1'
		$y = $page->getHeight();
		$xi = 0;	
		$titulo = 'Comprobante de envío de donaciones';
		$efector = $this->efector_activo['nomest'];
		$h = 0;
		$h2 = 0;
		
               
		$page
                        
                        //->drawRectangle(101, 51, 199, 99)
			//         x1    y1    x2    y2
			//->drawLine(20, $y-27, 257, $y-27)  // horizontal linea subrayado titulo
			->drawLine(16, $y-55, 810, $y-55)
			->drawLine(16, $y-75, 810, $y-75)
                        
                        ->drawLine(16, $y-55, 16, $y-500)   // vertical inicio linea  Fecha
                        ->drawLine(133, $y-55, 133, $y-500) // vertical inicio linea  Codigos
                        ->drawLine(225, $y-55, 225, $y-500) // vertical inicio linea  Nro Tubuladura
 			->drawLine(350, $y-55, 350, $y-500) // vertical inicio linea  Tiempo de extracción
                        ->drawLine(485, $y-55, 485, $y-500) // vertical inicio linea  estado
			->drawLine(600, $y-55, 600, $y-500) // vertical inicio linea  observaciones                       
			->drawLine(810, $y-55, 810, $y-500) // vertical ultima linea observaciones CIERRE
                        
			->drawLine(16, $y-500, 810, $y-500) // horizontal linea footer CIERRE 
                        
                        
                        ->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.jpg'), 10, $y-50, 97, $y-8)
                        
                        //->drawImage(Zend_Pdf_Image::imageWithPath('./themes/v1/images/logo.png'), 17, $y - 50, 50, $y - 17)
			->setFont($font, 14)
			->drawText($titulo, 100, $y-25, $encoding)
			->setFont($font, 12)
			->drawText("Nro. Envío: " . $envio['env_nro'], 100, $y-45, $encoding)
			->drawText($efector, 350, $y-45, $encoding)
                        
			->drawText('Fecha muestra', 30, $y-70, $encoding)
			->drawText('Códigos', 140, $y-70, $encoding)
                        ->drawText('Nro Tubuladura', 230, $y-70, $encoding)
			->drawText('Tiempo de extracción', 355, $y-70, $encoding)
			->drawText('Estado', 490, $y-70, $encoding)
			->drawText('Observaciones', 605, $y-70, $encoding)
		;
		
        // Completado del comprobante
        // Cambiar de página cuando excede ----
       
        foreach ($donaciones as $i => $v) {
			$persona = $modeloPer->getByID($v['per_id']);
			if ($v['don_tiempo_extraccion'] > 10) {
				$tiempoExt = 'Mayor a 10';
			}
			else {
				$tiempoExt = 'Menor o igual a 10';
			}
		$obsTrunc = sprintf("%10.31s",$v['observaciones']);
		$estadoTrunc = sprintf("%10.18s", $estados_donaciones[$v['don_estado']]);
     
			if ($h < ($y+40)){		
				$page
					->setFont($font, 12)
					->drawText($v['fh_muestra'], 30, $y-90-$h, $encoding)
					->drawText($v['don_nro']/*.' - '.$v['cod_muni']*/, 135, $y-90-$h, $encoding)
                                        ->drawText($modelo_det_don->getByDonID($v['don_id'])['nroTubuladura'], 230, $y-90-$h, $encoding)
					->drawText($tiempoExt.' min.', 355, $y - 90 - $h, $encoding) 
					->drawText($estadoTrunc, 490, $y-90-$h, $encoding) 
					->drawText($obsTrunc, 605, $y-90-$h, $encoding) //hacer break
				//	->drawText($v['don_usu_extraccion'], 700, $y-90-$h, $encoding)
				;
				$h = $h + 20;
			} else {
				$page2 = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
				$page2
					->setFont($font, 12)
					->drawText($v['fh_muestra'], 30, $y-90-$h, $encoding)
					->drawText($v['don_nro']/*.' - '.$v['cod_muni']*/, 135, $y-90-$h, $encoding)
                                        ->drawText($modelo_det_don->getByDonID($v['don_id'])['nroTubuladura'], 230, $y-90-$h, $encoding)
					->drawText($tiempoExt.' min.', 355, $y - 90 - $h, $encoding) 
					->drawText($estadoTrunc, 490, $y-90-$h, $encoding) 
					->drawText($obsTrunc, 605, $y-90-$h, $encoding) //hacer break
                                        //
				//	->drawText($v['don_usu_extraccion'], 700, $y-90-$h, $encoding)                                        
//					->setFont($font, 12)
//					->drawText($v['fh_muestra'], 30, $y-90-$h, $encoding)
//					->drawText($v['don_nro'].' - '.$v['cod_muni'], 230, $y-90-$h, $encoding)
//					->drawText($tiempoExt.' minutos', 320, $y-90-$h, $encoding) 
//					->drawText($estadoTrunc, 490, $y-90-$h, $encoding) 
//					->drawText($obsTrunc, 605, $y-90-$h, $encoding) //hacer break
//				//	->drawText($v['don_usu_extraccion'], 700, $y-90-$h, $encoding)
				;
				$h2 = $h2 + 20;
			}

        }
		
		$pdf->pages[] = $page;
		//Si se generó una página extra, se agrega al PDF
		if ($h2 > 0) {
			$page2
			->drawLine(20, $y-27, 257, $y-27)
			->drawLine(16, $y-55, 810, $y-55)
			->drawLine(16, $y-75, 810, $y-75)
			->drawLine(16, $y-55, 16, $y-500)
			->drawLine(133, $y-55, 133, $y-500)
			->drawLine(315, $y-55, 315, $y-500)
			->drawLine(485, $y-55, 485, $y-500)
			->drawLine(600, $y-55, 600, $y-500)
			->drawLine(810, $y-55, 810, $y-500)
			->drawLine(16, $y-500, 810, $y-500)
			->setFont($font, 14)
			->drawText($titulo, 20, $y-25, $encoding)
			->setFont($font, 12)
			->drawText($envio['env_nro'], 20, $y-45, $encoding)
			->drawText($efector, 180, $y-45, $encoding)
			->drawText('Fecha muestra', 30, $y-70, $encoding)
			->drawText('Códigos', 140, $y-70, $encoding)
			->drawText('Tiempo de extracción', 320, $y-70, $encoding)
			->drawText('Estado', 490, $y-70, $encoding)
			->drawText('Observaciones', 605, $y-70, $encoding)
		;
			
			$pdf->pages[] = $page2;
		}
 
		header('Content-Type: application/pdf');
		echo $pdf->render();
    }

    
	public function mostrarEditarAction(){
		//parametro envio
		$envio= new envio($this->helperSistema->parametros['envio']);
		$this->helperSistema->template->tipo_accion="Editar:";
		$this->helperSistema->template->envio=$envio;
		$this->helperSistema->template->modo="E";
		$efector= new efectores();
		$this->helperSistema->template->efector=$efector->devefector();
		$this->helperSistema->template->mostrar("envios/amenvios");
	}

	public function buscarAction(){
		$helperDT = new dataTablesHelper($this->helperSistema->parametros);

		$cr = new Criterio("1",Criterio::IGUAL,"1");
		if (!isset($this->helperSistema->parametros["mini"])) {
			if ($helperDT->criterioBusqueda!="") {
				$helper = new helpers();
				if ($helperDT->criterioBusqueda->nro != ""){
					$cr->agregarAND(new Criterio("envios.nro",Criterio::IGUAL,"'".$helperDT->criterioBusqueda->nro."'"));
				}
				if ($helperDT->criterioBusqueda->fdesde != ""){
					$cr->agregarAND(new Criterio("envios.fecha",Criterio::MAYOR_IGUAL,"'".$helper->formatearFecha(2,$helperDT->criterioBusqueda->fdesde)."'"));
				}
				if ($helperDT->criterioBusqueda->fhasta != ""){
					$cr->agregarAND(new Criterio("envios.fecha",Criterio::MENOR_IGUAL,"'".$helper->formatearFecha(2,$helperDT->criterioBusqueda->fhasta)."'"));
				}
			}
		} else {

			if ($helperDT->textoFiltro!='') {
				$cr->agregarCriterio("1",Criterio::DISTINTO,"1");
			}
		}
		$crCuenta = clone $cr;
		$cr->agregarOrden($helperDT->nroColOrden,$helperDT->dirOrden);
		$cr->agregarLimite($helperDT->desde,$helperDT->cantidad);
		$oenvios = new envios();
		if (!isset($this->helperSistema->parametros["mini"])) {
			echo $helperDT->crearSalida($oenvios->contar(),$oenvios->contar($crCuenta),$oenvios->devenviosDT($cr));
		} else {
			echo $helperDT->crearSalida($oenvios->contar(),$oenvios->contar($crCuenta),$oenvios->devenviosMiniDT($cr));
		}
	}

	/**
	* Imprimir Envio
	*
	*/
	public function imprimirAction(){
		//pdf
		$env_id = (int)$this->getRequest()->getParam('env_id');

		$modeloEnv = new Efector_models_Envios();
		$envio = $modeloEnv->find($env_id)->current();

		$modeloDon = new Banco_models_Donaciones();
		$donaciones = $modeloDon->getDespacho($env_id);

		$this->view->envio = $envio;
		$this->view->donaciones = $donaciones;
	}

}
