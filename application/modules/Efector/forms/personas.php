<?php

class Efector_forms_personas extends Zend_Form {

    public function init() {

        $this->addElement($this->createElement('text', 'nombres')
                        ->setLabel('Nombres')
                        ->setRequired(true)
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'required' => 'required',
                            'class' => 'input-large',
                            'maxlength' => 50
        )));

        $this->addElement($this->createElement('text', 'apellido')
                        ->setLabel('Apellido')
                        ->setRequired(true)
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'required' => 'required',
                            'class' => 'input-large',
                            'maxlength' => 40
        )));

        $this->addElement($this->createElement('select', 'sexo')
                        ->setLabel('Sexo/Genero')
                        ->addMultiOptions(array(
                            'M' => 'Masculino',
                            'F' => 'Femenino'
        )));

        $this->addElement($this->createElement('text', 'fnac')
                        ->setRequired(true)
                        ->setLabel('Fecha Nacimiento')
                        ->setAttribs(array(
                            'maxlength' => 10,
                            'required' => 'required',
                            'class' => 'input-small',
                            'maxlength' => 10
        )));

        $this->addElement($this->createElement('select', 'estado_civil')
                        ->setLabel('Estado Civil')
                        ->addMultiOptions(array(
                            'disabled' => 'Selecione un estado civil',
                            '1' => 'Soltero/a',
                            '2' => 'Casado/a',
                            '3' => 'Separado/a',
                            '4' => 'Divorciado/a',
                            '5' => 'Viudo/a',
                            '6' => 'Unido de hecho',
                        ))
                        ->setAttribs(array(
                            'required' => 'required',
        )));

        $this->addElement($this->createElement('select', 'paisNacimiento')
                        ->setLabel('Pais Nacimiento')
                        ->setAttribs(array(
                            'maxlength' => 90,
        )));

        $this->addElement($this->createElement('text', 'paisResidencia')
                        ->setLabel('Pais Residencia')
        //s                ->setValue('ARGENTINA')
                        ->setAttribs(array(
                            'class' => 'input-large',
                            'disabled' => 'disabled',
                            'maxlength' => 90
        )));

        $this->addElement($this->createElement('select', 'provincia')
                        ->setLabel('Provincia')
                        ->setAttribs(array(
                            'maxlength' => 90
        )));

        $this->addElement($this->createElement('text', 'localidad')
                        ->setLabel('Localidad')
                        ->setAttribs(array(
                            'maxlength' => 90
        )));

        $this->addElement($this->createElement('text', 'barrio')
                        ->setLabel('Barrio')
                        ->setAttribs(array(
                            'class' => 'input-large',
                            'maxlength' => 90
        )));

        $this->addElement($this->createElement('text', 'calle')
                        ->setLabel('Calle')
                        ->addValidator('notEmpty', false)
                        ->setAttribs(array(
                            'class' => 'input-large',
                            'maxlength' => 90
        )));

        $this->addElement($this->createElement('text', 'numero')
                        ->setLabel('Numero')
                        ->addValidator('notEmpty', false)
                        ->setAttribs(array(
                            'class' => 'input-small',
                            'maxlength' => 90
        )));

        $this->addElement($this->createElement('text', 'cpostal')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 90,
                            'placeholder' => 'Codigo Postal'
        )));

        $this->addElement($this->createElement('text', 'telefono')
                        ->setLabel('Telefono')
                        ->setAttribs(array(
                            'class' => 'input-medium',
                            'maxlength' => 15,
        )));

        $this->addElement($this->createElement('text', 'celular')
                        ->setLabel('Celular')
                        ->setAttribs(array(
                            'class' => 'input-medium',
                            'maxlength' => 15,
        )));


        $this->addElement($this->createElement('text', 'email')
                        ->setLabel('Email')
                        ->addValidator('emailAddress')
                        ->setAttribs(array(
                            'class' => 'input-large',
                            'title' => 'Ingrese el correo electronico del donante. Este deberia ser el que más revisa.'
        )));

        $this->addElement($this->createElement('text', 'observacion')
                        ->setLabel('Observacion')
                        ->addValidator('observacion')
                        ->setAttribs(array(
                            'class' => 'input-large',
                            'maxlength' => 100
        )));
    }

}
