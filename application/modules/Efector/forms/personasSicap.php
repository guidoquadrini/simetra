<?php

class Efector_forms_personasSicap extends Zend_Form {
//TODO: addFilter('Alnum') para agregar un nombre legible.
    public function init() {
        $this->addElement(
                $this->createElement('select', 'CodTipDocumento', array('label'=>'Guido'))
                        ->setLabel('Tipo de documento')
                        ->setAttribs(array(
                            'title' => 'Selecione el tipo de documento del donante'
                        ))
        );

        $this->addElement($this->createElement('select', 'CodPais')
                        ->setLabel('Pais')
                        ->setAttribs(array(
                            'title' => 'Selecione el pais donde recide el donante'
                        ))
        );

        $this->addElement($this->createElement('select', 'CodEstCiv')
                        ->setLabel('Estado Civil')
                        ->setAttribs(array(
                            'title' => 'Selecione el estado civil del donante'
                        ))
                        ->setRegisterInArrayValidator(false));

        $this->addElement($this->createElement('text', 'CodEstadoReg')
                        ->setValue('A'));

        $this->addElement($this->createElement('select', 'CodSexo')
                        ->setLabel('Sexo/Genero')
                        ->setAttribs(array(
                            'title' => 'Seleccione el sexo del donante.'
                        ))
                        ->addMultiOptions(array(
                            '' => 'Selecione un Sexo',
                            'M' => 'Masculino',
                            'F' => 'Femenino'
        )));

        $this->addElement($this->createElement('text', 'Nombre')
                        ->setLabel('Nombres')
                        ->setRequired(true)
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 25,
                            'autocomplete' => 'off',
                            'title' => 'Ingrese el nombre completo del donante.'
        )));

        $this->addElement($this->createElement('text', 'Apellido')
                        ->setLabel('Apellido')
                        ->setRequired(true)
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'required' => 'required',
                            'maxlength' => 25,
                            'autocomplete' => 'off',
                            'title' => 'Ingrese el apellido del donante.'
        )));

        $this->addElement($this->createElement('text', 'ClaveLocalidad')
                        ->setLabel('Localidad')
                        ->setRequired(true)
                        ->setAttribs(array(
                            'title' => 'Ingrese la localidad (ciudad) donde vive el donante.'
        )));

        $this->addElement($this->createElement('text', 'Calle')
                        ->setLabel('Calle')
                        ->setRequired(false)//true
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'id' => 'Calle',
                            'title' => 'Ingrese la calle donde vive el donante, en caso de no existir, selecione OTRA CALLE.'
        )));

        $this->addElement($this->createElement('text', 'Numero')
                        ->setLabel('Domicilio')
                        ->setRequired(false)//true
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 5,
                            'style' => 'width:44px',
                            'placeholder' => 'N°',
                            'id' => 'numCall',
                            'title' => 'Ingrese el numero de la purta del domicilio del donante.'
        )));

        $this->addElement($this->createElement('text', 'Piso')
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 5,
                            'style' => 'width:44px',
                            'placeholder' => 'Piso',
                            'title' => 'Si el donante vive en un edificio, ingrese el piso en el que vive.'
        )));

        $this->addElement($this->createElement('text', 'Depto')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 3,
                            'style' => 'width:40px',
                            'placeholder' => 'Depto',
                            'title' => 'Si el donante vive en un departamente, ingrese el numero o letra del departamento.'
        )));

        $this->addElement($this->createElement('checkbox', 'bis')
                        ->setLabel('bis')
                        ->setAttribs(array(
                            'title' => 'Si la calle donde vive el donante es BIS, marque esta opcion..'
        )));

        $this->addElement($this->createElement('text', 'CodPostal')
                        ->setLabel('Codigo Postal')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 4,
                            'style' => 'width:44px',
                            'title' => 'Ingrese el codigo postal de la localidad donde vive el donante.'
        )));

        $this->addElement($this->createElement('text', 'Telefono')
                        ->setLabel('Telefono')
                        ->setAttribs(array(
                            'maxlength' => 15,
                            'title' => 'Si el donante posee telefono o un telefono de contacto, ingreselo aqui.'
        )));

        $this->addElement($this->createElement('text', 'celular')
                        ->setLabel('Celular')
                        ->setAttribs(array(
                            'maxlength' => 15,
                            'title' => 'Si el donante posee u telefono movil o un telefono movil de contacto, ingreselo aqui.'
        )));

        $this->addElement($this->createElement('textarea', 'observacion')
                        ->setLabel('Mas datos')
                        ->setAttribs(array(
                            'maxlength' => 200,
                            'id' => 'obs',
                            'title' => 'Escriba en este campo cualquier informacion que crea de importancia para identificar el donante. Es importante tener en cuenta que la informacion para ubicar al donante ante una emergencia es Muy Importante.'
        )));

        $this->addElement($this->createElement('text', 'FechaNacimiento')
                        ->setRequired(true)
                        ->setLabel('Fecha Nacimiento')
                        ->setAttribs(array(
                            'maxlength' => 10,
                            'required' => 'required',
                            'class' => 'text-center',
                            'maxlength' => 10,
                            'title' => 'Ingrese la fecha de nacimiento del donante, puede realizarlo por teclado con el formato DD/MM/AAAA, o bien mediante la intefaz. Elijiendo primero el año, luego el mes y por ultimo el dia.'
        )));

        $this->addElement($this->createElement('select', 'paisNacimiento')
                        ->setLabel('Pais de Nacionalidad')
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'title' => 'Selecione el pais donde nacio.'
        )));

        $this->addElement($this->createElement('select', 'CodProvincia')
                        ->setLabel('Provincia')
                        ->setAttribs(array(
                            'title' => 'Selecione la provincia donde vive el donante.'
                        ))
                        ->setRegisterInArrayValidator(false));

        $this->addElement($this->createElement('select', 'provinciaNacimiento')
                        ->setLabel('Provincia de Nacimiento')
                        ->setAttribs(array(
                        ))
                        ->setRegisterInArrayValidator(false));

        $this->addElement($this->createElement('text', 'localidadNacimiento')
                        ->setLabel('Localidad de Nacimiento')
                        ->setRequired(false)
                        ->setAttribs(array(
        )));

        $this->addElement($this->createElement('text', 'email')
                        ->setLabel('Email')
                        ->addValidator('emailAddress')
                        ->addFilter('StringToLower')
                        ->setAttribs(array(
        )));
    }

}
