<?php

class Efector_forms_personasFusion extends Zend_Form {

    public function init() {

        $this->addElement(
                $this->createElement('select', 'CodTipoDocumento')
                        ->setLabel('Tipo de documento')
                        ->setRegisterInArrayValidator(false)
                        ->setAttribs(array(
                            'title' => 'Selecione el tipo de documento del donante'
        )));

        $this->addElement(
                $this->createElement('text', 'Documento')
                        ->setLabel('Nro. de documento')
                        ->setAttribs(array(
                            'title' => 'Ingrese el nro. de documento del donante',
                            'placeholder' => 'Ingresar el nro. de documento'
        )));

        $this->addElement(
                $this->createElement('text', 'ReDocumento')
                        ->setLabel('Confirme el nro. de documento')
                        ->setAttribs(array(
                            'title' => 'El Documento y su confirmacion (re ingreso de documento) deben coincidir y no pueden esta vacios.',
                            'placeholder' => 'Vuelva a ingresar el documento.'
        )));

        $this->addElement($this->createElement('text', 'Nombres')
                        ->setLabel('Nombres')
                        ->setRequired(true)
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'required' => 'required',
                            //'class' => 'input-large',
                            'maxlength' => 50,
                            'autocomplete' => 'off',
                            'placeholder' => 'Nombre del donante',
                            'title' => 'Ingrese el nombre completo del donante.'
        )));


        $this->addElement($this->createElement('text', 'Apellido')
                        ->setLabel('Apellido')
                        ->setRequired(true)
                        ->addValidator('notEmpty', false)
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'required' => 'required',
                            //'class' => 'input-large',
                            'maxlength' => 40,
                            'autocomplete' => 'off',
                            'placeholder' => 'Apellido del donante',
                            'title' => 'Ingrese el apellido del donante.'
        )));

        $this->addElement($this->createElement('select', 'CodSexo')
                        ->setLabel('Sexo/Genero')
                        ->setAttribs(array(
                            'title' => 'Seleccione el sexo del donante.'
                        ))
                        ->addMultiOptions(array(
                            'M' => 'Masculino',
                            'F' => 'Femenino'
        )));

        $this->addElement($this->createElement('text', 'FechaNacimiento')
                        ->setRequired(true)
                        ->setLabel('Fecha Nacimiento')
                        ->setAttribs(array(
                            'maxlength' => 10,
                            'required' => 'required',
                            'class' => 'text-center',
                            'maxlength' => 10,
                            'title' => 'Ingrese la fecha de nacimiento del donante, puede realizarlo por teclado con el formato DD/MM/AAAA, o bien mediante la intefaz. Elijiendo primero el año, luego el mes y por ultimo el dia.'
        )));

        $this->addElement($this->createElement('select', 'EstadoCivil')
                        ->setLabel('Estado Civil')
                        ->addMultiOptions(array(                            
                            '1' => 'Soltero/a',
                            '2' => 'Casado/a',
                            '3' => 'Separado/a',
                            '4' => 'Divorciado/a',
                            '5' => 'Viudo/a',
                            '6' => 'Unido de hecho',
                        ))
                        ->setAttribs(array(
                            'required' => 'required',
        )));

        $this->addElement($this->createElement('select', 'CodPaisNacimiento')
                        ->setLabel('Pais de Nacionalidad')
                        ->addFilter('StringToUpper')
                        ->setRegisterInArrayValidator(false)
                        ->setAttribs(array(
                            'title' => 'Selecione el pais donde nacio.'
        )));
        $this->addElement($this->createElement('text', 'CodPaisNacimiento_Otro')
                        ->setLabel('Pais de Nacionalidad')
                        ->setRequired(false)
                        ->setAttribs(array()));
        $this->addElement($this->createElement('select', 'CodProvinciaNacimiento')
                        ->setLabel('Provincia de Nacimiento')
                        ->setAttribs(array())
                        ->setRegisterInArrayValidator(false));

        $this->addElement($this->createElement('select', 'CodLocalidadNacimiento')
                        ->setLabel('Localidad de Nacimiento')
                ->setRegisterInArrayValidator(false)
                        ->setRequired(false)
                        ->setAttribs(array(
        )));
        $this->addElement($this->createElement('text', 'CodLocalidadNacimiento_Otra')
                        ->setLabel('Localidad de Nacimiento')
                        ->setRequired(false)
                        ->setAttribs(array(
        )));
        $this->addElement($this->createElement('select', 'CodPais')
                        ->setLabel('Pais Residencia')
                        ->setRegisterInArrayValidator(false)
                        ->setAttribs(array(
                            'title' => 'Selecione el pais donde recide el donante'
        )));
        $this->addElement($this->createElement('text', 'CodPais_Otro')
                        ->setLabel('Pais Residencia')
                        ->setAttribs(array(
                            'title' => 'Selecione el pais donde recide el donante'
        )));
        $this->addElement($this->createElement('select', 'CodProvincia')
                        ->setLabel('Provincia')
                        ->setRegisterInArrayValidator(false)
                        ->setAttribs(array(
                            'title' => 'Selecione la provincia donde vive el donante.'
        )));
        $this->addElement($this->createElement('select', 'CodLocalidad')
                        ->setLabel('Localidad')
                ->setRegisterInArrayValidator(false)
                        ->setAttribs(array(
                            'title' => 'Ingrese la localidad (ciudad) donde vive el donante.'
        )));
        $this->addElement($this->createElement('text', 'CodLocalidad_Otra')
                        ->setLabel('Localidad')
                        ->setAttribs(array(
                            'title' => 'Ingrese la localidad (ciudad) donde vive el donante.'
        )));
        $this->addElement($this->createElement('text', 'CodPostal')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 10,
                            'placeholder' => 'Codigo Postal',
                            'title' => 'Ingrese el codigo postal de la localidad donde vive el donante.'
        )));
        $this->addElement($this->createElement('hidden', 'Calle')
                        ->setLabel('Calle')
                        ->setRequired(false)                        
                        ->setAttribs(array(
                            'title' => 'Ingrese la calle donde vive el donante, en caso de no existir, selecione OTRA CALLE.'
        )));
        $this->addElement($this->createElement('text', 'Calle_Otra')
                        ->setLabel('Calle')                        
                        ->setAttribs(array(                            
                            'maxlength' => 90,
                            'title' => 'Escriba la calle donde se domicilia el donante.',
                            'placeholder' => 'Escriba la calle...'
        )));
        $this->addElement($this->createElement('text', 'Numero')
                        ->setLabel('Domicilio')
                        ->setRequired(false)//true                        
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 5,
                            'style' => 'width:44px',
                            'placeholder' => 'N°',
                            'id' => 'numCall',
                            'title' => 'Ingrese el numero de la purta del domicilio del donante. Si no tuviese numero de portal, complete con "0(cero)".'
        )));
        $this->addElement($this->createElement('text', 'Piso')
                        ->addFilter('StringToUpper')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 5,
                            'style' => 'width:44px',
                            'placeholder' => 'Piso',
                            'title' => 'Si el donante vive en un edificio, ingrese el piso en el que vive.'
        )));
        $this->addElement($this->createElement('text', 'Depto')
                        ->setAttribs(array(
                            'class' => 'input-small text-right',
                            'maxlength' => 3,
                            'style' => 'width:40px',
                            'placeholder' => 'Depto',
                            'title' => 'Si el donante vive en un departamente, ingrese el numero o letra del departamento.'
        )));
        $this->addElement($this->createElement('checkbox', 'Bis')
                        ->setLabel('bis')
                        ->setAttribs(array(
                            'title' => 'Si la calle donde vive el donante es BIS, marque esta opcion..'
        )));
        $this->addElement($this->createElement('text', 'Telefono')
                        ->setLabel('Telefono')
                        ->setAttribs(array(
                            'maxlength' => 15,
                            'placeholder' => 'Telefono de Contacto',
                            'title' => 'Si el donante posee telefono o un telefono de contacto, ingreselo aqui.'
        )));
        $this->addElement($this->createElement('text', 'Celular')
                        ->setLabel('Celular')
                        ->setAttribs(array(
                            'maxlength' => 15,
                            'placeholder' => 'Celular de Contacto',
                            'title' => 'Si el donante posee u telefono movil o un telefono movil de contacto, ingreselo aqui.'
        )));
        $this->addElement($this->createElement('text', 'Email')
                        ->setLabel('Email')                        
                        ->addFilter('StringToLower')
                        ->setAttribs(array(
                            'placeholder' => 'Email de Contacto',
                            'title' => 'Ingrese el correo electronico del donante. Este deberia ser el que más revisa.'
        )));
        $this->addElement($this->createElement('textarea', 'Observacion')
                        ->setLabel('Mas datos')
                        ->setAttribs(array(
                            'maxlength' => 200,
                            'title' => 'Escriba en este campo cualquier informacion que crea de importancia para identificar el donante. Es importante tener en cuenta que la informacion para ubicar al donante ante una emergencia es Muy Importante.'
        )));
        
        $this->addElement($this->createElement('hidden', 'per_id'));
    }

}
