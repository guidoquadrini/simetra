<?php

class Efector_models_Solicitudes extends Zend_Db_Table_Abstract {

    protected $_name = 'sol_solicitudes';
    protected $_primary = 'sol_id';
    public $estados = array(0, 1, 2, 3, 4, 5);

    /**
     * Obtener por Paciente/Estado/s
     *
     * @param int $dni
     * @param int $estados
     * @param string $order
     *
     * 0 - Cerrada sin problemas
     * 1 - Aceptada
     * 2 - Diferida + Motivo
     * 3 - Rechazada + Motivo + texto?
     * 4 - Pendiente
     * 5 - Pasa a Donacion
     *
     */
    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getById($sol_id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'sol_id',
                                    'sol_cod',
                                    'per_id',
                                    'efe_id',
                                    'afe_id'
                                ))
                                ->where('sol_id=?', $sol_id)
                                ->order('sol_id'));
    }

    public function getByDni($dni, $estados = null, $order = 'sol_fh_ini DESC') {
        return $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
                                    'DATE_FORMAT(sol_fh_fin,"%d/%m/%Y %H:%i") AS hora_finaliza'
                                ))
                                ->where('per_id=?', $dni)
                                ->where('sol_estado IN (?)', (is_null($estados)) ? $this->estados : $estados)
                                ->order($order));
    }

    //Busqueda por Id de Persona, donde sol_diferido <> NULL y la fecha del dia y la fecha de inicio <= sol_diferido
    public function getSolicitudesDiferidasByPerId($per_id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array("DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(sol_fh_ini)), '%d')+0 as tiempo", 'sol_diferido'))
                                ->where('per_id=?', $per_id)
                                ->where('sol_diferido IS NOT NULL')
                                ->where("DATEDIFF(NOW(),sol_fh_ini) <= (sol_diferido +0)")
                                ->where('sol_estado =?', 2));
    }

    //Busqueda por Id de Persona, donde sol_diferido <> NULL y la fecha del dia y la fecha de inicio <= sol_diferido
    //Idem anterior se sostiene por commpatibilidad. 
    public function getByDniSolDiferida($per_id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array("DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(sol_fh_ini)), '%d')+0 as tiempo", 'mot_id', 'sol_diferido'))
                                ->where('per_id=?', $per_id)
                                ->where('sol_diferido IS NOT NULL')
                                ->where("DATEDIFF(NOW(),sol_fh_ini) <= (sol_diferido +0)")
                                ->where('sol_estado =?', 2));
    }

    public function getBySol($sol_id)

    /*

      $sql = "SELECT per_id FROM solicitudes "
      . "INNER JOIN per_personas ON solicitudes.per_id = per_personas.per_nrodoc "
      . "WHERE solicitudes.sol_id = ".$sol_id . " LIMIT 0,1" ;

      $r = $this->bd->select($sql);

      if(count($r) < 1){
      return 0;
      }
      else {
      return $r[0]['per_id'];
      }
     */ {
        return $this->fetchAll($this->select(true)
                                ->columns('per_id')
                                ->where('sol_id=?', $sol_id)
                                ->order('sol_id')
        );
    }

    public function getByDniPersona($dni, $page, $perPage, $estado = array(0, 1, 2, 3, 4, 5)) {
        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)->reset('columns')
                                ->columns(array(
                                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                    'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                ))
                                ->where('per_id=?', $dni)
                                ->where('sol_estado IN (?)', $estado)
                                ->order('sol_fh_ini')
                                ->joinLeft('per_personas as p', 'per_id=p.per_id', array('per_apellido', 'per_nombres'))
                                ->limitPage($page, $perPage));
    }

    public function getByDniPersonaAndEfe($efe, $dni, $page, $perPage, $estado = array(0, 1, 2, 3, 4, 5)) {
        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)->reset('columns')
                                ->columns(array(
                                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                    'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                ))
                                ->where('sol_solicitudes.per_id = ?', $dni)
                                ->where('sol_estado IN (?)', $estado)
                                ->where('efe_id IN (?)', $efe)
                                ->order('sol_fh_ini')
                                ->joinLeft('per_personas as p', 'sol_solicitudes.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                                ->limitPage($page, $perPage));
    }

    //Busca solicitures por Persona_id y Estado donde 
    public function getSolicituresPorPerIdEstado($per_id, $estados) {
        return $this->fetchAll($this->select(true)
                                ->where('per_id=?', $per_id)
                                ->where('sol_estado IN (?)', $estados)
                                ->order('sol_fh_fin DESC'));
    }

    public function getUltimaDni($dni, $estados) {
        return $this->fetchAll($this->select(true)
                                ->where('per_id=?', $dni)
                                ->where('sol_estado IN (?)', $estados)
                                ->where(new Zend_Db_Expr('sol_fh_ini > DATE_SUB(NOW(),INTERVAL 3 MONTH)'))
                                ->order('sol_fh_fin DESC')
                                ->limit(1))->current();
    }

    /**
     * Obtener por Fecha
     *
     * @param mixed $fecha Y-mm-dd
     */
    public function getByFecha($fecha, $page, $perPage) {

        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)->reset('columns')
                                ->columns(array(
                                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                    'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                ))
                                ->where('sol_fh_ini >?', $fecha)
                                ->order('sol_fh_ini')
                                ->joinLeft('per_personas as p', 'sol_solicitudes.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                                ->limitPage($page, $perPage));
    }

    /* Se agrega id de efectores */

    public function getByFechaAndEfe($efe, $fecha, $page, $perPage) {

        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)->reset('columns')
                                ->columns(array(
                                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                    'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                    
                                ))
                                ->where('sol_solicitudes.sol_fh_ini > ?', $fecha)
                                ->where('sol_solicitudes.efe_id IN (?)', $efe)
                                ->order('sol_solicitudes.sol_fh_ini')
                                ->joinLeft('per_personas as p', 'sol_solicitudes.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                                ->join('don_donaciones as d', 'sol_solicitudes.sol_id=d.sol_id', array('don_id', 'don_nro'))
                                ->limitPage($page, $perPage));
    }

    /**
     * Obtener por Estado
     *
     * @param mixed $estado
     */
    public function getByEstado($estado, $page, $perPage, $fecha = null, $sol_cod = null) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
            'CONCAT(per_sexo,SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
        ));

        if ($fecha != null)
            $query = $query->where("sol_fh_ini LIKE '{$fecha}%'");

        if ($sol_cod != null)
            $query = $query->where("sol_cod=?", $sol_cod);

        $query = $query->where('sol_estado=?', $estado)
                ->order('sol_fh_ini')
                ->joinLeft('per_personas as p', 'sol_solicitudes.per_id=p.per_id', array('per_apellido', 'per_nombres'))
                ->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getByEstadoAndEfe($efe, $estado, $page, $perPage, $fecha = null, $sol_cod = null) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(sol_fh_ini,"%d/%m/%Y %H:%i") AS hora_llegada',
            'CONCAT(per_sexo,SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
        ));

        if ($fecha != null)
            $query = $query->where("sol_fh_ini LIKE '{$fecha}%'");

        if ($sol_cod != null)
            $query = $query->where("sol_cod=?", $sol_cod);

        $query = $query->where('sol_estado=?', $estado)
                ->where('efe_id IN (?)', $efe)
                ->order('sol_fh_ini')
                ->joinLeft('per_personas as p', 'sol_solicitudes.per_id=p.per_id', array('per_apellido', 'per_nombres', 'per_tipodoc'))
                ->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

}
