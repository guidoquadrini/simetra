<?php

class Efector_models_Paises extends Zend_Db_Table_Abstract {

    protected $_name = 'pais';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_mod_sims;        
        parent::__construct();
    }
    
    public function getNombre($idPais) {
        return $this->fetchRow($this->select(true)
                                ->reset('columns')
                                ->columns('pais_nombre')
                                ->where('id_pais = ?', $idPais));
    }

    public function getAll() {
        return $this->fetchAll()->toArray();
    }

}
