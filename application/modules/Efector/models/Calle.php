<?php

class Efector_models_Calle extends Zend_Db_Table_Abstract {

    protected $_name = 'calles';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_sicap;
        parent::__construct();
    }

    public function getCalles($claveLocalidad) {
        $db = $this->getAdapter();

        $query = $db->select()->from($this->_name, array('calle'), $this->_schema)
                ->where('ClaveLocalidad=?', $claveLocalidad)
                ->order('calle');

        return $db->fetchCol($query);
    }

}
