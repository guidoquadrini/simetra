<?php

class Efector_models_Diferimientos extends Zend_Db_Table_Abstract {

    protected $_name = 'sol_diferimientos';
    protected $_primary = 'id';
    
    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }
     
    public function getMotivosPorSolicitudes($arraySolicitudesID){
        return $this->getDefaultAdapter()->fetchAll(
                $this->select()
                ->where('sol_id in (?)', $arraySolicitudesID)
                );
    }
}
