<?php

class Efector_models_Localidades extends Zend_Db_Table_Abstract {

    protected $_name = '_localidad';
    
    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_mod_sims;
        $this->_name = $registry->config->prefijo_tabla . $this->_name;
        parent::__construct();
    }

    public function getNombre($codLocalidad) {
        return $this->fetchRow($this->select(true)
                                ->reset('columns')
                                ->columns('nomloc')
                                ->where('claveloc = ?', $codLocalidad));
    }

}
