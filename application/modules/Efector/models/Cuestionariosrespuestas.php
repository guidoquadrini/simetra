<?php

class Efector_models_Cuestionariosrespuestas extends Zend_Db_Table_Abstract {

    protected $_name = 'cuestionarios_respuestas';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }
    
    public function getActivas() {
        return $this->fetchAll($this->select(true)
                                // ->columns('')
                                ->where('activo = ?', 1)
        );
    }
    
    
    
    public function getCabeceraCuestionarioByDonacion($don_nro){

        return $this->fetchAll($this->select(true)
                                ->columns('id')
                                ->where('don_nro=?', $don_nro)
                                ->order('id')
                                ->limit(1))->current();
 
    }
}