<?php

class Efector_models_PersonasSicap extends Zend_Db_Table_Abstract {

    protected $_schema = 'dsalud_sicap';
    protected $_name = 'paciente';
    protected $_primary = 'NumeroPaciente';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        //$this->_schema = $registry->config->db_sicap;
        //TODO: Se utiliza una tabla copiada de SICAP para arrancar con los donantes hasta que se
        //implemente el web service de paciente.
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByDni($dni) {
        return $this->fetchAll($this->select(true)
                                ->columns('DATE_FORMAT(FechaNacimiento,"%d/%m/%Y") as fecha_nac')
                                ->where('NroDocumento = ?', $dni))->current();
    }

    public function getByIDs($ids) {
        return $this->fetchAll($this->select(true)
                                ->columns('DATE_FORMAT(FechaNacimiento,"%d/%m/%Y") as fecha_nac')
                                ->where('NumeroPaciente IN (?)', $ids));
    }

    public function getByTipoYNum($dni, $tipo) {
        return $this->fetchAll($this->select(true)
                                ->columns('DATE_FORMAT(FechaNacimiento,"%d/%m/%Y") as fecha_nac')
                                ->where('NroDocumento = ?', $dni)
                                ->where('CodTipDocumento = ?', $tipo))->current();
    }

    public function getByDniSimple($dni, $tipo) {
        return $this->fetchAll($this->select(true)->reset('columns')
                                ->columns(array(
                                    'Nombre', 'Apellido', 'CodEstCiv', 'CodPais', 'CodSexo', 'NroDocumento', 'FechaNacimiento',
                                    'ClaveLocalidad', 'Calle', 'Numero', 'Piso', 'Depto', 'bis', 'CodPostal', 'Telefono', 'ApellidoCasada', 'CodProvincia',
                                    'obra', 'paisNacimiento', 'DATE_FORMAT(FechaNacimiento,"%d/%m/%Y") as fecha_nac', 'observacion', 'celular', 'email', 'NumeroPaciente'
                                ))
                                ->where('CodTipDocumento = ?', $tipo)
                                ->where('NroDocumento = ?', $dni))->current();
    }

    /* Para multiples resultados */

    public function getByDniMultiple($dni, $tipo) {
        return $this->fetchAll($this->select(true)->reset('columns')
                                ->columns(array(
                                    'NumeroPaciente', 'Nombre', 'Apellido', 'CodEstCiv', 'CodPais', 'CodSexo', 'NroDocumento', 'FechaNacimiento',
                                    'ClaveLocalidad', 'Calle', 'Numero', 'Piso', 'Depto', 'bis', 'CodPostal', 'Telefono', 'ApellidoCasada', 'CodProvincia',
                                    'obra', 'paisNacimiento', 'DATE_FORMAT(FechaNacimiento,"%d/%m/%Y") as fecha_nac', 'observacion', 'celular', 'email', 'NumeroPaciente'
                                ))
                                ->where('CodTipDocumento = ?', $tipo)
                                ->where('NroDocumento = ?', $dni));
    }

}
