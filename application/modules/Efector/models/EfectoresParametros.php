<?php

class Efector_models_EfectoresParametros extends Zend_Db_Table_Abstract {

    protected $_name = 'efectores_parametros';
    protected $_primary = 'id_establecimiento';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getParametros($id_efector) {
        return $this->fetchRow($this->select(true)->where('id_establecimiento=?', $id_efector));
    }

}
