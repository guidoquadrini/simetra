<?php

/**
 * estados de envios
 * 1 pendiente
 * 2 recibido parcial
 * 3 recibido total
 */
class Efector_models_Envios extends Zend_Db_Table_Abstract {

    protected $_name = 'env_envios';
    protected $_primary = 'env_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByNro($nro, $estados = array(0, 1, 2, 3, 4, 5)) {
        return $this->fetchAll($this->select(true)
                                ->columns('DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fh_registro')
                                ->where('env_nro=?', $nro));
        //->where('sol_estado IN (?)',$estados)
        //->order('sol_fh_ini'));
    }

    /**
     * Obtener por Fecha
     *
     * @param mixed $fecha Y-mm-dd
     */
    public function getByFecha($efe_id, $fecha, $page = 1, $perPage = 10, $env_nro) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fh_registro',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as sol_cod',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
        ));

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($env_nro != null)
            $query = $query->where("env_nro =?", $env_nro);

        $query = $query->where('efe_id=?', $efe_id);
        $query = $query->order('env_nro');

        $query = $query->limitPage($page, $perPage);
        return $this->fetchAll($query);
    }

    public function getByFechaAndEfe($efe, $fecha, $page = 1, $perPage = 10, $env_nro) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                    'count(*) as cantidad_donaciones',
                    'DATE_FORMAT(env_envios.env_fh,"%H:%i") as hora_llegada',
                    'DATE_FORMAT(env_envios.env_fh,"%d/%m/%Y %H:%i") as fh_registro',
                    'DATE_FORMAT(env_envios.env_fh,"%d/%m/%Y %H:%i") as sol_cod',
                    'DATE_FORMAT(env_envios.env_fh,"%d/%m/%Y %H:%i") as tdo_id',
                    'env_envios.env_nro',
                    'env_envios.efe_nombre_origen as apellido',
                    'env_envios.efe_nombre_origen as nombres',
                ))
                ->joinLeft('don_donaciones', ' env_envios.env_id = don_donaciones.despacho_id')
                ->group('env_envios.env_nro')
        ;

        if ($fecha != null)
            $query = $query->where("env_envios.env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_envios.env_fh DESC');

        if ($env_nro != null)
            $query = $query->where("env_envios.env_nro =?", $env_nro);

        $query = $query->where('env_envios.efe_id IN (?)', $efe);
        $query = $query->order('env_envios.env_nro DESC'); // env_nro

        $query = $query->limitPage($page, $perPage);
        return $this->fetchAll($query);
    }

    public function getDonacionesEnvio($page = 1, $perPage = 10, $env_id) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            "DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(per_fnac)), '%Y')+0 as edad",
        ));


        if ($env_id != null)
            $query = $query->where("env_id = ?", $env_id);


        $query = $query->order('env_envios.efe_id')
                ->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id')
                ->joinLeft('per_personas', ' per_personas.per_id=don_donaciones.per_id');


        return $this->fetchAll($query);
    }

    public function getDonacionesSinSerologia($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
            'env_nro as per_id',
        ));

        $query = $query->where('pro_estado=?', 1);

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($don_id != null)
            $query = $query->where("don_nro =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("env_envios.efe_id = ?", $efe_id);

        if ($todos == false)
            $query = $query->where('don_id NOT IN (SELECT don_id FROM serologia)');

        $query = $query->order('env_envios.efe_id');
        $query = $query->order('don_donaciones.don_id');
        $query = $query->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getDonacionesSinSerologiaAndEfe($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {

        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
            'env_nro as per_id'
        ));

        $query = $query->where('pro_estado=?', 1);

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($don_id != null)
            $query = $query->where("don_nro =?", $don_id);

        if ($efe_id != null)
        //IN en vez de =
            $query = $query->where("env_envios.efe_id IN (?)", $efe_id);

        if ($todos == false)
            $query = $query->where('don_donaciones.don_id NOT IN (SELECT serologia.don_id FROM serologia)');

        $query = $query->order('env_envios.efe_id');
        $query = $query->order('don_donaciones.don_id');
        $query = $query->join('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id', 'don_id as donacion_id');
        #$query = $query->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id');
        $query = $query->joinLeft('serologia', ' don_donaciones.don_id = serologia.don_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    //para contar
    public function getDonacionesSinSerologiaAndEfeCount($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
            'env_nro as per_id',
        ));

        $query = $query->where('pro_estado=?', 1);

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($don_id != null)
            $query = $query->where("don_nro =?", $don_id);

        if ($efe_id != null)
        //IN en vez de =
            $query = $query->where("env_envios.efe_id IN (?)", $efe_id);

        if ($todos == false)
            $query = $query->where('don_id NOT IN (SELECT don_id FROM serologia)');

        $query = $query->order('env_envios.efe_id');
        $query = $query->order('don_donaciones.don_id');
        $query = $query->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id');

        return $this->fetchAll($query);
    }

    public function getDonacionesSinInmunologiaAndEfe($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
            'env_nro as per_id',
        ));

        $query = $query->where('pro_estado=?', 1);

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($don_id != null)
            $query = $query->where("don_nro =?", $don_id);

        if ($efe_id != null)
        //Cambiado a IN en vez de =
            $query = $query->where("env_envios.efe_id IN (?)", $efe_id);

        if ($todos == false)
            $query = $query->where('don_id NOT IN (SELECT don_id FROM inmunologia)');

        $query = $query->order('env_envios.efe_id');
        $query = $query->order('don_donaciones.don_id');
        $query = $query->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getDonacionesSinInmunologiaAndEfeCount($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
            'env_nro as per_id',
        ));

        $query = $query->where('pro_estado=?', 1);

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($don_id != null)
            $query = $query->where("don_nro =?", $don_id);

        if ($efe_id != null)
        //Cambiado a IN en vez de =
            $query = $query->where("env_envios.efe_id IN (?)", $efe_id);

        if ($todos == false)
            $query = $query->where('don_id NOT IN (SELECT don_id FROM inmunologia)');

        $query = $query->order('env_envios.efe_id');
        $query = $query->order('don_donaciones.don_id');
        $query = $query->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id');

        return $this->fetchAll($query);
    }

    public function getDonacionesSinInmunologia($fecha, $page = 1, $perPage = 10, $don_id = null, $efe_id = null, $todos = false) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as tdo_id',
            'efe_nombre_origen as apellido',
            'efe_nombre_origen as nombres',
            'env_nro as per_id',
        ));

        $query = $query->where('pro_estado=?', 1);

        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($don_id != null)
            $query = $query->where("don_nro =?", $don_id);

        if ($efe_id != null)
            $query = $query->where("env_envios.efe_id = ?", $efe_id);

        if ($todos == false)
            $query = $query->where('don_id NOT IN (SELECT don_id FROM inmunologia)');

        $query = $query->order('env_envios.efe_id');
        $query = $query->order('don_donaciones.don_id');
        $query = $query->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id');
        $query = $query->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    public function getByFechaWithDonaciones($fecha, $page = 1, $perPage = 10, $env_nro = null, $efe_id = null, $estado = 1) {

        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
        ));


        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($env_nro != null)
            $query = $query->where("env_nro = ?", $env_nro);

        if ($efe_id != null)
            $query = $query->where("env_envios.efe_id = ?", $efe_id);

        if ($estado == 1 || $estado == null)
            $query = $query->where("
                    EXISTS (SELECT * FROM don_donaciones AS d 
                             WHERE d.despacho_id =env_envios.env_id 
                               AND (d.pro_estado = 0 OR d.pro_estado = 2 OR d.pro_estado IS NULL))");

        if ($estado == 3)
            $query = $query->where("
                    NOT EXISTS (SELECT * FROM don_donaciones AS d 
                                 WHERE d.despacho_id =env_envios.env_id 
                                   AND (d.pro_estado = 0 OR d.pro_estado = 2 OR d.pro_estado IS NULL))");

        $query = $query->where("env_envios.env_estado =?", 1);

        $query = $query->order('env_envios.efe_id');

        $query = $query->limitPage($page, $perPage);
        return $this->fetchAll($query);
    }

    public function getByFechaWithDonacionesByEfe($fecha, $page = 1, $perPage = 10, $env_nro = null, $efes_ids = null, $estado = 1) {

        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
            'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
            'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
            'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
        ));


        if ($fecha != null)
            $query = $query->where("env_fh LIKE '{$fecha}%'");
        $query = $query->order('env_fh DESC');

        if ($env_nro != null)
            $query = $query->where("env_nro = ?", $env_nro);

        if ($efe_id != null)
            $query = $query->where("env_envios.efe_id IN (?)", $efes_ids);

        if ($estado == 1 || $estado == null)
            $query = $query->where("
                    EXISTS (SELECT * FROM don_donaciones AS d 
                             WHERE d.despacho_id =env_envios.env_id 
                               AND (d.pro_estado = 0 OR d.pro_estado = 2 OR d.pro_estado IS NULL))");

        if ($estado == 3)
            $query = $query->where("
                    NOT EXISTS (SELECT * FROM don_donaciones AS d 
                                 WHERE d.despacho_id =env_envios.env_id 
                                   AND (d.pro_estado = 0 OR d.pro_estado = 2 OR d.pro_estado IS NULL))");

        $query = $query->where("env_envios.env_estado =?", 1);

        $query = $query->order('env_envios.efe_id');

        $query = $query->limitPage($page, $perPage);
        return $this->fetchAll($query);
    }

    public function getByFechaWithDonAndProp($fecha, $page = 1, $perPage = 10) {
        return $this->fetchAll($this->select(true)
                                ->setIntegrityCheck(false)->reset('columns')
                                ->columns(array(
                                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                    'DATE_FORMAT(env_fh,"%H:%i") as hora_llegada',
                                    'DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fecha_envio',
                                    'TIMEDIFF(NOW(), env_fh) as tiempo_envio',
                                    "DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(per_fnac)), '%Y')+0 as edad",
                                ))
                                ->where("env_fh LIKE '{$fecha}%'")
                                ->order('env_envios.efe_id')
                                ->joinLeft('don_donaciones', ' env_envios.env_id=don_donaciones.despacho_id')
                                ->joinLeft('detalles_produccion', ' detalles_produccion.iddonacion=don_donaciones.don_id')
                                ->joinLeft('hem_hemocomponentes', ' hem_hemocomponentes.hem_id=detalles_produccion.idhemocomponente')
                                ->joinLeft('per_personas', ' per_personas.per_id=don_donaciones.per_id')
        );
    }

    public function getEnvio($id) {
        return $this->fetchAll($this->select(true)
                                //->setIntegrityCheck(false)->reset('columns')
                                ->columns('DATE_FORMAT(env_fh,"%d/%m/%Y %H:%i") as fh_registro')
                                ->where('env_id=?', $id)
                        //->joinLeft('efectores',' envios.idefector=efectores.id',array('apellido','nombres'))
                )->current();
    }

    function devenviosDT($cr) {
        $sql = "SELECT envios.id,envios.nro,envios.fecha, efectores.nombre as efector FROM envios "
                . "INNER JOIN efectores ON envios.idefector=efectores.id "
                . "WHERE " . $cr->devCriterio();

        $cadena = "";
        $r = $this->bd->select($sql);
        if (count($r) > 0) {
            $helper = new helpers();
            for ($i = 0; $i < count($r); $i++) {
                $cadena .= "[";
                $cadena .= "\"" . $r[$i]["id"] . "\",";
                $cadena .= "\"<img class='selectores' src='css/imagenes/details_open.png'>\",";
                $cadena .= "\"" . date("d/m/Y", strtotime($r[$i]["fecha"])) . "\",";
                $cadena .= "\"" . sprintf("%06d", $r[$i]["nro"]) . "\",";
                $cadena .= "\"" . $r[$i]["efector"] . "\"";
                $cadena .= "],";
            }
            $cadena = substr($cadena, 0, -1);
        }
        return $cadena;
    }

    function devRecepcionEnviosDT($cr) {
        $sql = "SELECT envios.id,envios.nro,envios.fecha,envios.estado, efectores.nombre as efector FROM envios "
                . "INNER JOIN efectores ON envios.idefector=efectores.id "
                . "WHERE " . $cr->devCriterio();

        $cadena = "";
        $r = $this->bd->select($sql);
        if (count($r) > 0) {
            $helper = new helpers();
            for ($i = 0; $i < count($r); $i++) {
                $cadena .= "[";
                $cadena .= "\"" . $r[$i]["id"] . "\",";
                $cadena .= "\"" . $r[$i]["efector"] . "\",";
                $cadena .= "\"" . sprintf("%06d", $r[$i]["nro"]) . "\",";
                $cadena .= "\"" . date("d/m/Y", strtotime($r[$i]["fecha"])) . "\",";
                $estado = "";
                switch ($r[$i]['estado']) {
                    case 1:
                        $estado = "Aguardando Recepci&oacute;n";
                        break;
                    case 2:
                        $estado = "Recepcionado Parcial";
                        break;
                    case 3:
                        $estado = "Recepcionado";
                        break;
                }
                $cadena .= "\"" . $estado . "\"";
                $cadena .= "],";
            }
            $cadena = substr($cadena, 0, -1);
        }
        return $cadena;
    }

    public function getCantidadDonacionesPorEnvio($numero_envio) {

        $resultado = $this->fetchAll($this->select(true)
                        ->setIntegrityCheck(false)->reset('columns')
                        ->columns(array(
                            new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                        ))
                        ->where('env_nro=?', $numero_envio)
                        ->joinLeft('don_donaciones', ' env_envios.env_id = don_donaciones.despacho_id')
        );
        return $resultado;


//            if ($numero_envio !== '') {
//                $numero_envio = (int) $numero_envio;
//                
//  		$registro = array();
//		$sql = "SELECT COUNT(b.don_id) AS cantidad_donaciones FROM env_envios a" 
//                        . " INNER JOIN don_donaciones b ON a.env_id = b.despacho_id "
//			. " WHERE a.env_nro =". $numero_envio;
//	    }
//		$r = $this->bd->select($sql);
//		if(count($r) > 0) {
//			for($i=0;$i<count($r);$i++) {
//				$registro[] = $r[$i]['cantidad_donaciones'];
//			}
//		}
//		return $registro;          

        /*
         * SELECT COUNT(b.don_id) AS cantidad_donaciones FROM env_envios a  
         * inner join don_donaciones b on a.env_id = b.despacho_id WHERE a.env_nro = 124920160323115732
         */
    }

}
