<?php

class Efector_models_Sicap extends Zend_Db_Table_Abstract {

    private $tbl_calle = 'calles';
    private $tbl_localidad = '_localidad';
    private $tbl_ecivil = 'ecivil';
    
    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_sicap;
        $this->schema_mod_sims = $registry->config->db_mod_sims;
        $this->tbl_localidad = $registry->config->prefijo_tabla . $this->tbl_localidad;
        parent::__construct();
    }

    public function getEstadosCivil() {
        return array(
            '1' => 'Soltero/a',
            '2' => 'Casado/a',
            '3' => 'Separado/a',
            '4' => 'Divorciado/a',
            '5' => 'Viudo/a',
            '6' => 'Unido de hecho',
        );
        return $this->getDefaultAdapter()->fetchPairs(
                        $this->getDefaultAdapter()->select()->from($this->tbl_ecivil, '*', $this->_schema)
                                ->order('CodEstCiv'));
    }

    public function getProvincias() {
        return array(
            '06' => 'BUENOS AIRES',
            '02' => 'CAPITAL FEDERAL',
            '10' => 'CATAMARCA',
            '22' => 'CHACO',
            '26' => 'CHUBUT',
            '14' => 'CORDOBA',
            '18' => 'CORRIENTES',
            '30' => 'ENTRE RIOS',
            '34' => 'FORMOSA',
            '38' => 'JUJUY',
            '42' => 'LA PAMPA',
            '46' => 'LA RIOJA',
            '50' => 'MENDOZA',
            '54' => 'MISIONES',
            '58' => 'NEUQUEN',
            '62' => 'RIO NEGRO',
            '66' => 'SALTA',
            '70' => 'SAN JUAN',
            '74' => 'SAN LUIS',
            '78' => 'SANTA CRUZ',
            '82' => 'SANTA FE',
            '86' => 'SANTIAGO DEL ESTERO',
            '94' => 'TIERRA DEL FUEGO',
            '90' => 'TUCUMAN'
        );
    }

    public function getLocalidades($codprov) {
        return $this->getDefaultAdapter()->fetchAll(
                        $this->getDefaultAdapter()->select()->from($this->tbl_localidad, array('claveloc', 'nomloc'), $this->schema_mod_sims)
                                ->where('codprov=?', $codprov)
                                ->where('nomloc!=?', 'LOCALIDAD NO ESPECIFICADA')
                                ->where('nomloc NOT LIKE ?', 'KILOMETRO%')
                                ->order('nomloc ASC'));
    }

    public function getLocalidadesPairs($codprov) {
        return $this->getDefaultAdapter()->fetchPairs(
                        $this->getDefaultAdapter()->select()->from($this->tbl_localidad, array('claveloc', 'nomloc'), $this->schema_mod_sims)
                                ->where('codprov=?', $codprov)
                                ->where('nomloc!=?', 'LOCALIDAD NO ESPECIFICADA')
                                ->where('nomloc NOT LIKE ?', 'KILOMETRO%')
                                ->order('nomloc ASC'));
    }

    public function getCalles($claveLocalidad) {
        return $this->getDefaultAdapter()->fetchCol(
                        $this->getDefaultAdapter()->select()->from($this->tbl_calle, array('calle'), $this->_schema)
                                ->where('ClaveLocalidad=?', $claveLocalidad)
                                ->order('calle'));
    }

}
