<?php

class Efector_models_Cuestionariosrespuestasdetalle extends Zend_Db_Table_Abstract {

    protected $_name = 'cuestionarios_respuestas_detalle';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }
    
    public function getActivas() {
        return $this->fetchAll($this->select(true)
                                // ->columns('')
                                ->where('activo = ?', 1)
        );
    }

    
     public function getRespuestasPreguntasByCategoria($cuestionario_id, $categoria_id) {
         
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                 new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'),
                                'cuestionarios_respuestas_detalle.pregunta_id', 
                                'cuestionarios_respuestas_detalle.respuesta_id',
                                'cuestionarios_respuestas_detalle.observacion',
                                'plantillas_cuestionario_detalle.id', 
                                'plantillas_cuestionario_detalle.orden',
                                'plantillas_cuestionario_detalle.pregunta', 
                ));      


        if ($cuestionario_id != null)
            $query = $query->where("cuestionarios_respuestas_detalle.cuestionario_respuesta_id = ?", $cuestionario_id);


        if ($categoria_id != null)
            $query = $query->where("plantillas_cuestionario_detalle.cuestionario_pregunta_categoria_id = ?", $categoria_id);
        
        $query = $query->joinInner('plantillas_cuestionario_detalle', ' cuestionarios_respuestas_detalle.pregunta_id = plantillas_cuestionario_detalle.id');
        
        $query ->order('plantillas_cuestionario_detalle.orden');
        
        return $this->fetchAll($query);
        
    }   
    
    
    
}
