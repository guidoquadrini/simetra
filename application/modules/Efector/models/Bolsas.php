<?php

class Efector_models_Bolsas extends Zend_Db_Table_Abstract {

    protected $_name = 'bolsas';
    protected $_primary = 'id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }
    
    public function getActivas() {
        return $this->fetchAll($this->select(true)
                                // ->columns('')
                                ->where('activo = ?', 1)
        );
    }

}
