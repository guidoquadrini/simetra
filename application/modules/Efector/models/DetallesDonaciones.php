<?php

class Efector_models_DetallesDonaciones extends Zend_Db_Table_Abstract {

    protected $_name = 'don_donaciones_ext';
    protected $_primary = 'ext_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getByDonID($don_id) {
        return $this->fetchRow($this->select(true)->where('don_id=?', $don_id));
    }

    public function getByDonIDs($don_id) {
        return $this->fetchAll($this->select(true)->where('don_id IN (?)', $don_id))->toArray();
    }

    public function getTipoBolsa($don_id) {
        return $this->fetchAll($this->select(true)
                                ->reset('columns')
                                ->columns('tipo_bolsa')
                                ->where('don_id=?', $don_id)
                )->toArray();
    }

    public function codTubuladuraFueUtilizado($nrotubuladura) {
        return $this->fetchAll($this->select(true)
                                ->where('nrotubuladura=?', $nrotubuladura)
                )->count();
    }

}
