<?php

class Efector_models_Personas extends Zend_Db_Table_Abstract {

    protected $_name = 'per_personas';
    protected $_primary = 'per_id';

    public function __construct() {
        $registry = Zend_Registry::getInstance();
        $this->_schema = $registry->config->db_banco;
        parent::__construct();
    }

    public function getNombreCompleto($dni) {
        $query = $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'CONCAT(per_nombres, " " ,per_apellido) AS nombreCompleto'
                                ))
                                ->where('per_nrodoc = ?', $dni))->current();
        return $query['nombreCompleto'];
    }

    public function getByDni($dni) {
        return $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'DATE_FORMAT(per_fnac,"%d/%m/%Y") as fecha_nac',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                ))
                                ->where('per_nrodoc = ?', $dni))->current();
    }

    public function getByID($id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'DATE_FORMAT(per_fnac,"%d/%m/%Y") as fecha_nac',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                ))
                                ->where('per_id = ?', $id)); //->current();
    }

    public function getBySicap_ID($id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'DATE_FORMAT(per_fnac,"%d/%m/%Y") as fecha_nac',
                                    'CONCAT(SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS alfa'
                                ))
                                ->where('sicap_id = ?', $id)); //->current();
    }
    
    public function getByTipoYNum($dni, $tipo) {
        return $this->fetchAll($this->select(true)
                                ->where('per_nrodoc = ?', $dni)
                                ->where('per_tipodoc = ?', $tipo))->current();
    }

    public function getByDni2($dni, $tipo) {
        return $this->fetchAll($this->select(true)
                                ->where('per_nrodoc = ?', $dni))->current();
    }
    
    public function getByDocuemento($documento_tipo, $documento_nro) {
        return $this->fetchAll($this->select(true)
                                ->where('per_nrodoc = ?', $documento_nro)
                                ->where('per_tipodoc = ?', $documento_tipo))->current();
    }

    

    public function getPaginados($page, $perPage) {
        $query = $this->select(true)
                ->setIntegrityCheck(false)->reset('columns')
                ->columns(array(
                    new Zend_Db_Expr('SQL_CALC_FOUND_ROWS *')
                ))
                ->where('sicap_id IS NOT NULL')
                ->limitPage($page, $perPage);

        return $this->fetchAll($query);
    }

    
    /**
     * 
     * @param type $id - int - Codigo de Indentificacion unico de la persona.
     * @return type - String - Sistema de codificación de las personas con VIH
     * 
     *  GNNAADDMMAAAA
     *  G: género Femenino o Masculino. (A partir de la 2013 este caracter quedo en desuso para el sistema de VIH en ARG)
     *  NN: dos primeras letras del primer nombre del donante
     *  AA: dos primeras letras del apellido del donante
     *  DD: día de nacimiento del donante (el que figura en el DNI)
     *  MM: mes de nacimiento del donante (el que figura en el DNI)
     *  AAAA: año de nacimiento del donante (el que figura en el DNI)
     *  EJ: María Paula Perez Díaz , 01/12/1970 es FMAPE01121970
     *
     */
    public function getCodigoAutogenerado($id) {
        return $this->fetchAll($this->select(true)
                                ->columns(array(
                                    'CONCAT(per_sexo,SUBSTR(per_nombres,1,2),SUBSTR(per_apellido,1,2),DATE_FORMAT(per_fnac,"%d%m%Y")) AS codigo_autogenerado'
                                ))
                                ->where('per_id = ?', $id))->current()->toArray()['codigo_autogenerado'];
    }

}
