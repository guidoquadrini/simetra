<?php

class Bootstrap extends Zend_Application_Module_Bootstrap {

    protected function _initDb() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/db.ini", APPLICATION_ENV);
        $params = array(
            'host' => $config->host,
            'username' => $config->username,
            'password' => $config->password,
            'dbname' => $config->dbname,
            'charset' => 'utf8',
            'profiler' => false
        );
        $db = Zend_Db::factory('Mysqli', $params);
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
        return $db;
    }

    protected function _initSettings() {
        $configuracion = new Zend_Config(require APPLICATION_PATH . '/configs/settings.php', true);
        Zend_Registry::set('config', $configuracion);
        $adapter = new Zend_Db_Table();
        $adapter = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT * FROM parametros_globales;";

        $parametros_globales = $adapter->query($sql)->fetchAll();
        $parametros = array();

        foreach($parametros_globales as $parametro){
            $configuracion->{$parametro['variable']} = (array)$parametro;
        };

        return $configuracion;
        //return new Zend_Config(require APPLICATION_PATH.'/configs/settings.php',true);
    }

    protected function _initFront() {
        $this->bootstrap('frontController');
        $front = $this->getResource('frontController');
        $front->setControllerDirectory(array(
                    'default' => APPLICATION_PATH . '/modules/default/controllers', // Basico + models
                    'banco' => APPLICATION_PATH . '/modules/Banco/controllers',
                    'efector' => APPLICATION_PATH . '/modules/Efector/controllers',
                    'gestion' => APPLICATION_PATH . '/modules/Gestion/controllers',
                    'paciente' => APPLICATION_PATH . '/modules/Paciente/controllers',
                    'acceso' => APPLICATION_PATH . '/modules/Acceso/controllers',
                    'stock' => APPLICATION_PATH . '/modules/Stock/controllers',
                    'soporte' => APPLICATION_PATH . '/modules/Soporte/controllers',
                    'rest' => APPLICATION_PATH . '/modules/Rest/controllers',
                    'promocion' => APPLICATION_PATH . '/modules/Promocion/controllers'
                ))
                ->registerPlugin(new Autorizacion())
                ->setBaseUrl($this->getResource('settings')->baseurl)
                ->throwExceptions(false);

        return $front;
    }

    protected function _initView() {
        //$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
        //$mobile = preg_match('#iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm#',$userAgent);
        //$mobile = true;
        $mobile = false;
        Zend_Layout::startMvc(array(
            'layoutPath' => APPLICATION_PATH . '/layouts/scripts',
            'layout' => ($mobile) ? 'mobile' : 'default'
        ));
        $view = Zend_Layout::getMvcInstance()->getView();
        $view->setEncoding('UTF-8');
        $view->doctype('HTML5');
        $view->headTitle('Sistema Informático de Medicina Transfusional <b style="color:bisque;">(SIMETRA)</b>');
        $view->setBasePath(APPLICATION_PATH . '/modules/default/views');

        $config = new Zend_Config(require APPLICATION_PATH . '/modules/navigation.php', true);
        $container = new Zend_Navigation($config);
        $view->navigation($container)->menu()->setUlClass('nav')
                ->setFormatOutput(false)
                ->addPageClassToLi(true);

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);
        return $view;
    }

    protected function _initCache() {
        $frontendOptions = array('lifetime' => null, 'automatic_serialization' => true);
        $backendOptions = array('cache_dir' => "../cache", 'cache_file_perm' => 0777);
        $vRet =  Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        return $vRet;
    }

    protected function _initAuth() {
        $vRet =  new Zend_Auth_Adapter_DbTable(
                $this->getResource('db'), $this->getResource('settings')->dbusers . '.usuarios', 'usu_usuario', 'usu_clave', 'MD5(?) AND usu_activo=1'
        );
        return $vRet;
    }

    protected function _initTranslate() {
        $translate = new Zend_Translate(array(
            'adapter' => 'array',
            'content' => APPLICATION_PATH . '/../resources/languages/es/Zend_Validate.php',
            'locale' => 'es'
        ));
        Zend_Form::setDefaultTranslator($translate);
        Zend_Registry::set('Zend_Translate', $translate);
    }

    protected function _initAutoloader() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace("Custom_");
    }

}

function dump_var($var, $flag = true) {
    Zend_Debug::dump($var);
    if ($flag)
        die();
}

